/**
  * SBT project description
  */

name             := "N2S3-daemon"

/*********************************************************************************************************************
  * Dependencies
  *******************************************************************************************************************/
libraryDependencies ++= {
  val akkaV = "2.7.0"//"2.3.7"
  Seq(
    "fr.univ-lille.cristal" %% "n2s3"     % "0.3-SNAPSHOT",
    "com.typesafe.akka" %% "akka-cluster"   % akkaV
  )
}
