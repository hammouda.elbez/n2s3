package fr.univ_lille.cristal.emeraude.n2s3.cluster

import akka.actor.{ActorRef, ActorSystem, Props}
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.{AbstractActorSystem, ActorDeploymentStrategy, PropsBuilder}


class N2S3ClusteredActorSystem(val system : ActorSystem) extends AbstractActorSystem[ActorRef] {
  val cluster = akka.cluster.Cluster(system)

  def actorOf(props: Props, deploymentStrategy: ActorDeploymentStrategy, name: String = ""): ActorRef = {
    val deploy = deploymentStrategy.getDeployForActor()
    this.basicCreateActor(cluster.system, props.withDeploy(deploy), name)
  }

  def actorOf(propsBuilder: PropsBuilder, selector: ActorDeploymentStrategy) = this.actorOf(propsBuilder.build(), selector)
  def actorOf(propsBuilder: PropsBuilder, selector: ActorDeploymentStrategy, name : String) = this.actorOf(propsBuilder.build(), selector, name)

  def dispatcher = system.dispatcher

  override def shutdown() = cluster.system.terminate()// mazdak 
}
