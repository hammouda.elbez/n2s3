// create eclipse definition project
addSbtPlugin("com.typesafe.sbteclipse" % "sbteclipse-plugin" % "5.2.4")
addSbtPlugin("net.virtual-void" % "sbt-dependency-graph" % "0.9.2")
addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.14.7")
addSbtPlugin("com.typesafe.sbt" % "sbt-multi-jvm" % "0.4.0")

//Signing for publishing in Sonatype
addSbtPlugin("com.jsuereth" % "sbt-pgp" % "1.1.1")


//mazdak
// for scalatest
addSbtPlugin("ch.epfl.scala" % "sbt-scalafix" % "0.9.18-1")// scalafix 0.10.4 https://github.com/scalacenter/sbt-scalafix/releases


