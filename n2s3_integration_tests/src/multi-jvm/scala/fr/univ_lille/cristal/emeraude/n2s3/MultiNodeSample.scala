package fr.univ_lille.cristal.emeraude.n2s3

import akka.remote.testkit.MultiNodeSpec
import akka.testkit.ImplicitSender
import fr.univ_lille.cristal.emeraude.n2s3.cluster.{ExplicitActorDeploymentStrategy, N2S3ClusteredActorSystem}
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.{N2S3, NeuronGroupRef}
import fr.univ_lille.cristal.emeraude.n2s3.models.neurons.LIF
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.ActorPerNeuronPolicy

class MultiNodeSampleSpecMultiJvmMaster extends MultiNodeSample
class MultiNodeSampleSpecMultiJvmSlave1 extends MultiNodeSample
class MultiNodeSampleSpecMultiJvmSlave2 extends MultiNodeSample

class MultiNodeSample extends MultiNodeSpec(MultiNodeSampleConfig)
  with STMultiNodeSpec with ImplicitSender {

  import MultiNodeSampleConfig._

  def initialParticipants = roles.size

  "An explicit cluster node selector" must {

    "put all actors in given node if global" in {
      runOn(master) {
        enterBarrier("deployed")
        val actorSystem = new N2S3ClusteredActorSystem(system)
        val n2s3 = new N2S3(actorSystem)
        n2s3.buildProperties.actorDeploymentPolicy = new ExplicitActorDeploymentStrategy(node(slave1).address)

        val neuronGroup = new NeuronGroupRef(n2s3)
        neuronGroup.setNeuronModel(LIF)
        neuronGroup.setNumberOfNeurons(2)

        neuronGroup.setActorPolicy(new ActorPerNeuronPolicy(n2s3))
        neuronGroup.ensureActorDeployed(n2s3)

        val firstAddress = neuronGroup.neurons.head.getNetworkAddress
        val secondAddress = neuronGroup.neurons(1).getNetworkAddress

        firstAddress.actor.path.address shouldBe secondAddress.actor.path.address
        firstAddress.actor.path.address shouldBe node(slave1).address
        firstAddress.actor.path.address shouldNot be(node(master).address)
      }

      runOn(slave1) {
        enterBarrier("deployed")
      }

      runOn(slave2) {
        enterBarrier("deployed")
      }

      enterBarrier("finished")
    }

    "put all actors in locally configured node" in {
      runOn(master) {
        enterBarrier("deployed")
        val actorSystem = new N2S3ClusteredActorSystem(system)
        val n2s3 = new N2S3(actorSystem)

        val neuronGroup = new NeuronGroupRef(n2s3)
        neuronGroup.setIdentifier("1")
        neuronGroup.setNeuronModel(LIF)
        neuronGroup.setNumberOfNeurons(10)
        neuronGroup.setActorPolicy(new ActorPerNeuronPolicy(new ExplicitActorDeploymentStrategy(node(slave1).address)))
        neuronGroup.ensureActorDeployed(n2s3)

        val neuronGroup2 = new NeuronGroupRef(n2s3)
        neuronGroup2.setIdentifier("2")
        neuronGroup2.setNeuronModel(LIF)
        neuronGroup2.setNumberOfNeurons(10)
        neuronGroup2.setActorPolicy(new ActorPerNeuronPolicy(new ExplicitActorDeploymentStrategy(node(slave2).address)))
        neuronGroup2.ensureActorDeployed(n2s3)


        neuronGroup.neurons.foreach( n => {
          n.getNetworkAddress.actor.path.address should be(node(slave1).address)
        })

        neuronGroup2.neurons.foreach( n =>
          n.getNetworkAddress.actor.path.address should be (node(slave2).address))
      }

      runOn(slave1) {
        enterBarrier("deployed")
      }

      runOn(slave2) {
        enterBarrier("deployed")
      }

      enterBarrier("finished")
    }
  }
}