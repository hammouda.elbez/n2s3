

name             := "N2S3_Integration_Tests"

/*********************************************************************************************************************
  * Dependencies
  *******************************************************************************************************************/
libraryDependencies ++= {
  val scalaXmlV = "1.0.6"//"1.0.2"
  val akkaV = "2.7.0"//"2.3.7"
  val scalatestV = "3.2.15"//"2.2.1"
  Seq(
    "com.typesafe.akka" %% "akka-cluster"   % akkaV,
    "com.typesafe.akka" %% "akka-multi-node-testkit" % akkaV,
    "org.scalatest"     %% "scalatest"       % scalatestV % "test"
  )
}


