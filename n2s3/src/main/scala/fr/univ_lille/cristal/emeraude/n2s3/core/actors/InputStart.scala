package fr.univ_lille.cristal.emeraude.n2s3.core.actors

import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message

/**
  * Created by pfalez on 25/09/17.
  */
object InputStart extends Message
