package fr.univ_lille.cristal.emeraude.n2s3.features.logging.graph

import akka.actor.{Actor, Props}
import akka.pattern.ask
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.{Config, Done}
import fr.univ_lille.cristal.emeraude.n2s3.core.event._
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.N2S3
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias._
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.LocalActorDeploymentStrategy

import scala.collection.mutable
import scala.concurrent.Await

/**
  * Created by falezp on 16/11/16.
  */
class TimingNeuronState(n2s3 : N2S3, labels : Seq[String], neurons : Seq[NetworkEntityPath], delays : Seq[Timestamp]) {

  object Close
  object Result

  class NeuronDependenceMonitor(n2s3 : N2S3, labels : Seq[String], neurons : Seq[NetworkEntityPath], delays : Seq[Timestamp]) extends Actor {

    val results = Map(delays.map(t => (t, Map(neurons.map(n => (n, Map(labels.map(l => (l, mutable.ArrayBuffer[Timestamp]())):_*))):_*))):_*)
    val neuronHistory = mutable.HashMap(neurons.map(n => (n, -1L)):_*)


    var currentInputLabel : Option[String] = None
    var currentInputStart : Option[Timestamp] = None

    var currentTimestamp : Timestamp = -1L

    ExternalSender.askTo(n2s3.inputLayerRef.get.getContainer, SubscribeSynchronized(LabelChangeEvent, ExternalSender.getReference(self), n2s3.buildProperties.getSynchronizerPolicy.getInputSynchronizer))

    n2s3.layers.flatMap(_.neuronPaths).foreach { path =>
      ExternalSender.askTo(path, SubscribeSynchronized(NeuronFireEvent, ExternalSender.getReference(self), n2s3.buildProperties.getSynchronizerPolicy.getInputSynchronizer))
    }

    override def receive = {
      case SynchronizedEvent(s, _, m, _) =>
        m match {
          case LabelChangeResponse(start, end, label) =>
            compute(start)

            currentInputLabel = Some(label)
            currentInputStart = Some(start)
            neuronHistory.foreach(e => neuronHistory.update(e._1, -1L))

          case NeuronFireResponse(timestamp, source) =>

            if(neuronHistory.contains(source)) {
              neuronHistory.update(source, timestamp)
            }

            compute(timestamp)

          case _ => throw new RuntimeException("Unrecognized message : "+m)
        }
        ExternalSender.sendTo(s, Done)
      case Close =>
        ExternalSender.askTo(n2s3.inputLayerRef.get.getContainer, Unsubscribe(LabelChangeEvent, ExternalSender.getReference(self)))

        n2s3.layers.flatMap(_.neuronPaths).foreach { path =>
          ExternalSender.askTo(path, Unsubscribe(NeuronFireEvent, ExternalSender.getReference(self)))
        }
        sender ! Done
        context.stop(self)

      case Result =>
        compute(Long.MaxValue)

        print()

        sender ! Done
      case m => throw new RuntimeException("Unrecognized message : "+m)
    }

    def compute(timestamp : Timestamp) = {
      if(timestamp > currentTimestamp && currentInputLabel.isDefined && currentInputStart.isDefined) {

        delays.filter(delay => delay <= timestamp-currentInputStart.get && delay > currentTimestamp-currentInputStart.get).foreach { delay =>

          neurons.foreach { neuron =>
            results(delay)(neuron)(currentInputLabel.get).+=(if(neuronHistory(neuron) < 0L) -1L else currentInputStart.get+delay-neuronHistory(neuron))
          }
        }

        currentTimestamp = timestamp
      }
    }

    def print() = {
      labels.foreach { label =>
        println("# "+label)
        neurons.foreach { neuron =>

          println("\t"+neuron.toString+" : "+(delays.map { delay =>
            val data = results(delay)(neuron)(label).filter(_ != -1L).map(_.toDouble)
            val size = data.size.toDouble
            val sum = data.sum
            val avg = sum/size
            val std = data.map(t => math.pow(t-avg, 2.0)).sum/size

            "["+delay+": count="+size.toInt+"/"+results(delay)(neuron)(label).size+", avg="+sum+", std="+std+"]"
          }).mkString(" "))
        }

      }
    }

  }

  val actor = n2s3.system.actorOf(Props(new NeuronDependenceMonitor(n2s3, labels, neurons, delays)), LocalActorDeploymentStrategy)

  def print() : Unit = {
    implicit val timeout = Config.longTimeout
    Await.result(actor ? Result, timeout.duration)
  }

  def destroy() : Unit = {
    implicit val timeout = Config.longTimeout
    Await.result(actor ? Close, timeout.duration)
  }

}
