/**************************************************************************************************
 * Contributors:
 * 		- created by bdanglot
 *    - (add your name here if you contribute to this code). 
 **************************************************************************************************/
package fr.univ_lille.cristal.emeraude.n2s3.features.logging.graph

import java.awt.Color
import java.util.concurrent.CopyOnWriteArrayList
import javax.swing.JFrame
/**************************************************************************************************
 * Class to display weights of Synapses. Cannot run during simulation.
 *************************************************************************************************/
class SynapsesConductancesGraph extends Graph {
  
 // this.chart.getStyleManager().setChartType(ChartType.Scatter);

  /**************************************************************************************************
   * List of weights of Synapses
   *************************************************************************************************/
  private val  weights : java.util.List[Float] = new CopyOnWriteArrayList[Float]()
  
  /**************************************************************************************************
   * List full of 0 in order to add the couple (w,0)
   *************************************************************************************************/
  private val listZero : java.util.List[Integer] = new CopyOnWriteArrayList[Integer]()
  
  
  /**************************************************************************************************
   * Add content
   * @param val the value of weights
   *************************************************************************************************/
  def addSerie(w : Float) : Unit =  {
    weights add w
    listZero add 0
  }
  
  /**************************************************************************************************
   * @Overriding launch to add Series and change transparency (and then call super.launch() from Trait Graph)
   *************************************************************************************************/
   override def launch() : JFrame = {
   /* chart.addSeries("Synapses", weights, listZero)
    chart.getSeriesMap().get("Synapses").setMarkerColor(new Color(0,0,255, 32))*/
    super.launch()
  }

  
}