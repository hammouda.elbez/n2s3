package fr.univ_lille.cristal.emeraude.n2s3.features.io.persistence

import fr.univ_lille.cristal.emeraude.n2s3.core
import fr.univ_lille.cristal.emeraude.n2s3.core.{ConnectionProperty, NetworkEntity, Property}
import fr.univ_lille.cristal.emeraude.n2s3.features.builder._
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.{Connection, ConnectionPolicy}
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.{InputPacket, N2S3InputPacket, StreamSupport}
import fr.univ_lille.cristal.emeraude.n2s3.support.Time

import scala.collection.mutable

/**
  * Created by falezp on 23/11/16.
  */
object NetworkLoad {
  protected def createClass(fullyQualifiedName : String)(implicit man: Manifest[Property[Any]]) = {
    val c = Class.forName(fullyQualifiedName)
    val constructor = c.getConstructors.minBy(_.getParameterTypes.length)
    constructor.newInstance(constructor.getParameterTypes.indices.map(i => c.getMethod("$lessinit$greater$default$"+(i+1)).invoke(null)):_*)
  }

  private def loadProperty(fullyQualifiedName : String)(implicit man: Manifest[Property[Any]]) =
    Class.forName(fullyQualifiedName).getField("MODULE$").get(man.runtimeClass)

  class SpecializedNeuronRef(group : NeuronGroupRef, index : Seq[Int], fullyQualifiedName : String, properties : Seq[(String, Any)]) extends NeuronRef(group, index) {
    override def newNeuron(): NetworkEntity = {
      val c = Class.forName(fullyQualifiedName)
      val instance = c.getConstructors.head.newInstance().asInstanceOf[core.Neuron]
      properties.foreach { case(key, value) =>
        instance.setProperty(loadProperty(key).asInstanceOf[Property[Any]], value)
      }
      instance
    }
  }

  class SpecializedConnection(list : Seq[(Int, Int, String, Seq[(String, Any)])]) extends ConnectionPolicy {
    override def generate(from: NeuronGroupRef, to: NeuronGroupRef): Traversable[Connection] = {
      list.map { case(n_from, n_to, className, properties) =>
        val instance = createClass(className).asInstanceOf[core.NeuronConnection]
        Connection(from.neuronPaths(n_from), to.neuronPaths(n_to), Some(instance), Time(0), properties.map { case(key, value) =>
          (loadProperty(key).asInstanceOf[ConnectionProperty[Any]], value)
        })
      }
    }

    override def connects(aNeuron: NeuronRef, anotherNeuron: NeuronRef): Boolean = true
  }

  def from(filename : String, n2s3 : N2S3, stream: StreamSupport[_ <: InputPacket, N2S3InputPacket]) : Unit = {
    val model = new NetworkPersistenceModel
    model.loadFrom(filename)


    val layerRefs = model.layers.map { layer =>
      val layerRef = if(layer.isInput) new InputNeuronGroupRef(n2s3) else new NeuronGroupRef(n2s3)
      if(layer.isInput) {
        layerRef.asInstanceOf[InputNeuronGroupRef].setInput(stream)
        n2s3.inputLayerRef = Some(layerRef.asInstanceOf[InputNeuronGroupRef])
      }

      layerRef.setIdentifier(layer.identifier)
      layerRef.setShape(layer.shape.dimensions:_*)
      layerRef.neuronRefs = Some(mutable.ArrayBuffer[NeuronRef]())
      n2s3.layers += layerRef
      layerRef
    }

    val neuronLayerPosition = mutable.HashMap[Int, Int]()
    val layerPositionCounter = mutable.ArrayBuffer[Int]()
    layerPositionCounter ++= model.layers.map(_ => 0)

    model.neurons.zipWithIndex.foreach { case(neuron, index) =>
      val layerRef = layerRefs(neuron.layer)
      layerRef.neuronRefs.get.asInstanceOf[mutable.ArrayBuffer[NeuronRef]] += new SpecializedNeuronRef(layerRef, Seq(layerRef.neuronRefs.get.size), model.className(neuron.className), neuron.properties.map(entry => (model.className(entry._1), entry._2)))

      neuronLayerPosition += index -> layerPositionCounter(neuron.layer)
      layerPositionCounter(neuron.layer) += 1
    }
/*
    val neuronLayerPosition = mutable.ArrayBuffer[Int]()
    neuronLayerPosition ++= model.layers.map(_ => 0)
    */


    val connectionGroup = mutable.HashMap[(Int, Int), mutable.ArrayBuffer[(Int, Int, String, Seq[(String, Any)])]]()

    model.connections.foreach { connection =>
      val n_from = neuronLayerPosition(connection.from)
      val n_to = neuronLayerPosition(connection.to)

      connectionGroup.getOrElseUpdate(model.neurons(connection.from).layer -> model.neurons(connection.to).layer,
        mutable.ArrayBuffer[(Int, Int, String, Seq[(String, Any)])]()) += ((n_from, n_to, model.className(connection.className),
        connection.properties.map(entry => (model.className(entry._1), entry._2))))
    }

    connectionGroup.foreach { case ((from_layer, to_layer), list) =>
      layerRefs(from_layer).connectTo(layerRefs(to_layer), new SpecializedConnection(
        list.map { entry =>
          (entry._1, entry._2, entry._3, entry._4)
        }
      ))
    }

    n2s3.create()




  }
}
