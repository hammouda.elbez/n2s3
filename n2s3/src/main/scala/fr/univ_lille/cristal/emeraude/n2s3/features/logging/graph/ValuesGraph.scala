/**************************************************************************************************
 * Contributors:
 * 		- created by bdanglot
 *    - (add your name here if you contribute to this code). 
 **************************************************************************************************/
package fr.univ_lille.cristal.emeraude.n2s3.features.logging.graph

import java.util.concurrent.{ConcurrentHashMap, CopyOnWriteArrayList}

class ValuesGraph extends Graph {

  //Setting the graph as Line
  //this.chart.getStyleManager().setChartType(ChartType.Line);

  /*************************************************************************************************
   * Map which associate name of entity and a list of timestamp
   ************************************************************************************************/
  private val time: java.util.Map[String, java.util.List[Integer]] = new ConcurrentHashMap[String, java.util.List[Integer]]()

  /*************************************************************************************************
   * Map which associate name of entity and a list of values
   ************************************************************************************************/
  private val values: java.util.Map[String, java.util.List[java.lang.Float]] = new ConcurrentHashMap[String, java.util.List[java.lang.Float]]()

  /*************************************************************************************************
   * Method to refresh the framework while simuling
   *
   * @param name
   *            of the entity to refresh
   * @param time
   *            the timestamp of the event
   * @param value
   *            the new value of this entity
   ************************************************************************************************/
  def refreshSerie(name: String, t: Int, value: Float): Unit = {

    javax.swing.SwingUtilities.invokeLater(new Runnable() {

      override def run() {

        addSerie(name, t, value);
      /*  if (!chart.getSeriesMap().containsKey(name))
          chart.addSeries(name, time.get(name), values.get(name));*/

        while (t - time.get(name).get(0) > 200) {
          time.get(name).remove(0);
          values.get(name).remove(0);
        }

      //  chart.getSeriesMap().get(name).setMarker(SeriesMarker.NONE);

      //  pan.updateSeries(name, time.get(name), values.get(name));
      }
    })
  }

  /*************************************************************************************************
   * add a new content to the framework
   *
   * @param name
   *            of the entity to refresh
   * @param time
   *            the timestamp of the event
   * @param value
   *            the new value of this entity
   ************************************************************************************************/
  def addSerie(name: String, time: Int, value: Float): Unit = {
    if (values.containsKey(name)) {
      this.values.get(name).add(value);
      this.time.get(name).add(time);
    } else {
      // Create the new list
      val listPot: java.util.List[java.lang.Float] = new CopyOnWriteArrayList[java.lang.Float]();
      listPot.add(value);
      this.values.put(name, listPot);
      // Create the new list
      val listTime: java.util.List[Integer] = new CopyOnWriteArrayList[Integer]();
      listTime.add(time);
      this.time.put(name, listTime);
    }
  }

  /*************************************************************************************************
   * Method to launch in offLine (no simulation) It will add contents to the
   * framework (Beware to add some contents before using it) And then call the
   * super Launch()
   ************************************************************************************************/
  def launchOffline(): Unit = {
   /* val it = time.keySet().iterator()
    while (it.hasNext()) {
      val k = it.next()
      val series = chart.addSeries(k, this.time.get(k), this.values.get(k));
      series.setMarker(SeriesMarker.NONE);
    }*/
    super.launch();
  }

}