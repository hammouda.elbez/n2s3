package fr.univ_lille.cristal.emeraude.n2s3.features.io.persistence

import java.io.{DataInputStream, FileInputStream}

import fr.univ_lille.cristal.emeraude.n2s3.core.actors.Config
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.SynapticWeightFloat
import fr.univ_lille.cristal.emeraude.n2s3.core.{ConnectionPropertyValues, ExternalSender, GetAllConnectionProperty, SetAllConnectionProperty}
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.NeuronGroupRef
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.Shape

import scala.concurrent.Await

/**
  * Created by pfalez on 02/05/17.
  */


class WeightInfo(val inShape : Shape, val outShape : Shape, val weights : Seq[Seq[(Int, Float)]]) {

  def get(in : Seq[Int], out : Seq[Int]) : Seq[Float] = {
    val inIndex = inShape.toIndex(in:_*)
    weights(outShape.toIndex(out:_*)).filter(_._1 == inIndex).map(_._2)
  }

  def getInputs(out : Seq[Int]) : Seq[(Seq[Int], Float)] = {
    weights(outShape.toIndex(out:_*)).map{ case(index, value) =>
      inShape.fromIndex(index) -> value
    }
  }

  def getInputsFromIndex(out : Int) : Seq[(Int, Float)] = {
    weights(out)
  }

  def assignTo(input : NeuronGroupRef, output : NeuronGroupRef) : Unit = {
    implicit val timeout = Config.longTimeout

    val inputMap = input.neuronPaths.zipWithIndex.toMap

    assert(input.shape == inShape && output.shape == outShape, input.shape+" <> "+inShape+" | "+output.shape+" <> "+outShape)
    output.neuronPaths.zipWithIndex.map { case (outputPath, outIndex) =>
      (outputPath, outIndex, ExternalSender.askFuture(outputPath, GetAllConnectionProperty(SynapticWeightFloat)))
    }.map { case(outputPath, outIndex, future) =>
      val setting = Await.result(future, timeout.duration).asInstanceOf[ConnectionPropertyValues[Float]].values.groupBy(_._2).flatMap { case(inputPath, list) =>
        val inIndex = inputMap(inputPath)
        val listWeight = weights(outIndex).filter(_._1 == inIndex)
        assert(listWeight.size == list.size, listWeight.size+" != "+list.size+" between in=["+inShape.fromIndex(inIndex).mkString(", ")+"] and out=["+outShape.fromIndex(outIndex).mkString(", ")+"]")
        list.zip(listWeight).map { case ((connectionID, _, _), (_, weight)) =>
          connectionID -> weight
        }
      }.toSeq
      ExternalSender.askFuture(outputPath, SetAllConnectionProperty(SynapticWeightFloat, setting))
    }.foreach(f => Await.result(f, timeout.duration))
  }

  def subshape(inOffset : Shape, inSize : Shape, outOffset : Shape, outSize : Shape): WeightInfo = {
    assert(inOffset.dimensionNumber == inShape.dimensionNumber && inSize.dimensionNumber == inShape.dimensionNumber)
    assert(outOffset.dimensionNumber == outShape.dimensionNumber && outSize.dimensionNumber == outShape.dimensionNumber)
    val inList = for(inIndexTmp <- inSize.allIndex) yield {
      val inIndex = inIndexTmp.zip(inOffset.dimensions).map{case (i1, i2) => i1+i2}
      inShape.toIndex(inIndex:_*)
    }
    val newWeights = for(outIndexTmp <- outSize.allIndex) yield {
      val outIndex = outIndexTmp.zip(outOffset.dimensions).map{case (i1, i2) => i1+i2}
      val out = outShape.toIndex(outIndex:_*)
      weights(out).filter{case (index, _) => inList.contains(index)}.map{case(index, w) =>
        val inIndex = inSize.toIndex(inShape.fromIndex(index).zip(inOffset.dimensions).map{case (i1, i2) => i1-i2}:_*)
        inIndex -> w
      }
    }

    new WeightInfo(inSize, outSize, newWeights)
  }

  def reshape(inNewShape : Shape, outNewShape : Shape) : WeightInfo = {
    assert(inNewShape.getNumberOfPoints == inShape.getNumberOfPoints && outNewShape.getNumberOfPoints == outShape.getNumberOfPoints)
    new WeightInfo(inNewShape, outNewShape, weights)
  }
}


object WeightLoad {

  def loadShape(in : DataInputStream) : Shape = {
    val number = in.readInt()
    Shape((0 until number).map(_ => in.readInt()):_*)
  }

  def fromFile(filename : String) : WeightInfo = {
    val inputFile = new FileInputStream(filename)
    val reader = new DataInputStream(inputFile)

    val inShape = loadShape(reader)
    val outShape = loadShape(reader)

    val weughts = (0 until outShape.getNumberOfPoints).map { _ =>
      val number = reader.readInt()

      (0 until number).map { _ =>
        val weight = reader.readFloat()
        val in = reader.readInt()
        in -> weight
      }
    }

    new WeightInfo(inShape, outShape, weughts)
  }

}
