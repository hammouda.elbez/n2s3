package fr.univ_lille.cristal.emeraude.n2s3.features.io.persistence

import fr.univ_lille.cristal.emeraude.n2s3.core.{ConnectionClassName, _}
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.WrapMessage
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.{InputNeuronGroupRef, N2S3}

import scala.collection.mutable

/**
  * Created by falezp on 23/11/16.
  */
object NetworkSave {
  def to(filename : String, n2s3 : N2S3) : Unit = {

    val model = new NetworkPersistenceModel

    val neuronAssoc = mutable.HashMap[NetworkEntityPath, Int]()

    n2s3.layers.foreach { layer =>

      val layer_id = model.addLayer(layer.identifier, layer.shape, layer.isInstanceOf[InputNeuronGroupRef])



      layer.neuronPaths.map { neuron =>
        val neuron_id = model.addNeuron(
          layer_id,
          neuron.local.map(_.toString).mkString("_"),
          ExternalSender.askTo(neuron, GetAllProperties).asInstanceOf[PropertiesList].content.toSeq,
          ExternalSender.askTo(neuron, GetClassName).asInstanceOf[WrapMessage].content.asInstanceOf[String]
        )
        neuronAssoc += neuron -> neuron_id
      }
    }

    n2s3.layers.foreach { layer =>

      layer.neuronPaths.foreach { neuron =>

        val inputs = mutable.HashMap[ConnectionPath, mutable.ArrayBuffer[(ConnectionProperty[Any], Any)]]()
        val inputsPath = mutable.HashMap[ConnectionPath, NetworkEntityPath]()

        ExternalSender.askTo(neuron, GetAllConnectionProperties).asInstanceOf[ConnectionPropertiesList].content.foreach { case(k, v) =>
          v.values.foreach { case (id, path, value) =>
            val connectionPath = ConnectionPath(neuron, id)
            inputs.getOrElseUpdate(connectionPath, mutable.ArrayBuffer[(ConnectionProperty[Any], Any)]()) += k -> value
            inputsPath.getOrElseUpdate(connectionPath, path)
          }
        }

        inputs.foreach { case (k, v) =>
          model.addConnection(
            neuronAssoc(inputsPath(k)),
            neuronAssoc(neuron),
            v,
            ExternalConnectionSender.askTo(k, GetConnectionProperty(ConnectionClassName)).asInstanceOf[PropertyValue[String]].value
          )
        }
      }
    }

    model.saveTo(filename)

  }
}
