package fr.univ_lille.cristal.emeraude.n2s3.models.synapses

import fr.univ_lille.cristal.emeraude.n2s3.core.Neuron.Inhibition
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.{BackwardSpike, ElectricSpike}
import fr.univ_lille.cristal.emeraude.n2s3.core.{NeuronConnection, SynapseBuilder}
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.Time
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message

/**
  * Created by pfalez on 24/05/17.
  */

/***********************************************************************************************************
  *  														InhibitorySynapse
  ***********************************************************************************************************/
object InhibitorySynapse extends SynapseBuilder{
  override def createSynapse = new InhibitorySynapse
}

class InhibitorySynapse(ratio : Float = 0f, delay : Time = Time(0)) extends NeuronConnection {
  def processConnectionMessage(timestamp : Timestamp, message : Message) : Unit = message match {
    case ElectricSpike(_) =>
      getEnds.sendToOutput(timestamp+delay.timestamp, Inhibition(ratio))
    case BackwardSpike => // nothing to do
  }
}

class InhibitorySynapse2(ratio1 : Float, ratio2 : Float, ratio2Delay : Time) extends NeuronConnection {
  def processConnectionMessage(timestamp : Timestamp, message : Message) : Unit = message match {
    case ElectricSpike(_) =>
      getEnds.sendToOutput(timestamp, Inhibition(ratio1))
      getEnds.sendToOutput(timestamp+ratio2Delay.timestamp, Inhibition(ratio2))
    case BackwardSpike => // nothing to do
  }
}