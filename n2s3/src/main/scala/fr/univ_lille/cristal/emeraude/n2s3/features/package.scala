package fr.univ_lille.cristal.emeraude.n2s3

/**
 * This package provides additionnal features like monitoring or time, they can be used directly by
 * the end user mode
 * @author wgouzer & qbailleul
 */
package object features
