package fr.univ_lille.cristal.emeraude.n2s3.features.logging

import java.io.FileOutputStream

import akka.actor._
import fr.univ_lille.cristal.emeraude.n2s3.core.Synchronizer.WaitEndOfActivity
import fr.univ_lille.cristal.emeraude.n2s3.core.event.{EventTriggered, SynapseWeightResponse}
import fr.univ_lille.cristal.emeraude.n2s3.core.{ExternalSender, NetworkEntityPath}
import fr.univ_lille.cristal.emeraude.n2s3.features.logging.graph.{SynapsesConductancesGraph, SynapsesWeightsBarGraph, ValuesGraph}
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias._
import fr.univ_lille.cristal.emeraude.n2s3.support.Time
import fr.univ_lille.cristal.emeraude.n2s3.support.UnitTime._

import scala.collection.mutable
import scala.collection.mutable.ListBuffer


case class Run(beg: Time, end: Time)

/**
  * Will monitor weights of synapses in the file output/log/synapse.log
  */
class SynapsesLogText extends Actor {
  val fos: FileOutputStream = new FileOutputStream("output/log/synapse.log")
  def receive = {
    case SynapseWeightResponse(timestamp, source, value) =>
      fos.write((source.outputNeuron + ":" + source.connectionID + "\t" + timestamp + "\t" + value + "\n").map(_.toByte).toArray)
  }
}

/**
  * Will monitor weights of synapses during the simulation in a bar graph
  */
class SynapsesLogBarGraph extends Actor {
  val graph = new SynapsesWeightsBarGraph
  def receive = {
    case SynapseWeightResponse(timestamp, source, value) =>
      graph.refreshSerie(source.outputNeuron + ":" + source.connectionID, value.asInstanceOf[Float])
      if (!(graph isRunning()))
        graph.launch()
  }
}

/**
  * It will record the weights of subscribed synapses, and will display a plot
  * when it receive Run(beg, end)
  *
  * @param step : the difference in time between two points
  * @param unit : the unit of Time used (Second, MilliSecond, MicroSecond)
  */
class SynapsesGraph(step: Int = 50, unit : TimeUnitType = MilliSecond) extends Actor {
  val event = mutable.HashMap[Timestamp, ListBuffer[(String, Float)]]()
  var lastT : Timestamp = 0
  def receive = {
    case SynapseWeightResponse(t, source, value) =>
      if (t - lastT > step) {
        event.get(t) match {
          case Some(e) =>
            e += ((source.toString, value.asInstanceOf[Float]))
          case _ =>
            event += t -> new ListBuffer[(String, Float)]()
            event(t) += ((source.toString, value.asInstanceOf[Float]))
        }
        lastT = t
      }
    case Run(beg: Time, end: Time) =>
      val graph = new ValuesGraph
      event.keys.toList.sortBy(x => x).foreach { k =>
        if (k >= beg.timestamp && k <= end.timestamp)
          for (i <- 0 until event.get(k).size) {
            graph.addSerie(event(k)(i)._1, convert(unit, Time(k)), event(k)(i)._2)
          }
      }
      graph launchOffline()
  }
}

/**
  * Will monitor weights of synapses during the simulation
  *
  * @param step : the difference in time between two points
  * @param unit : the unit of Time used (Second, MilliSecond, MicroSecond)
  */
class SynapsesLogGraph(step: Int = 50, unit : TimeUnitType = MilliSecond) extends Actor {
  val graph = new ValuesGraph
  var lastT : Timestamp = 0
  def receive = {
    case SynapseWeightResponse(t, source, value) =>
      if (t - lastT > step) {
        graph.refreshSerie(source.toString, convert(unit,new Time(t)), value.asInstanceOf[Float])
        if (!graph.isRunning())
          graph.launch()
        lastT = t
      }
  }
}

/**
  * Will monitor 1 weight for each of synapses
  */
class SynapsesWeightRepartition(sync: NetworkEntityPath) extends Actor {
  val values = new scala.collection.mutable.HashMap[String, Float]()
  ExternalSender.askTo(sync, WaitEndOfActivity)
  def receive = {
    case SynapseWeightResponse(timestamp, source, value) =>
      values += source.toString -> value.asInstanceOf[Float]
    case EventTriggered =>
      val graph = new SynapsesConductancesGraph
      val fos: FileOutputStream = new FileOutputStream("output/log/synapseRepart.log")
      values.keys.foreach { k =>
        fos.write((k + "\t" + values.get(k).get + "\n").map(_.toByte).toArray)
        graph.addSerie(values.get(k).get)
      }
      graph.launch()
      fos.close()
  }
}