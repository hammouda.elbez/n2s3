package fr.univ_lille.cristal.emeraude.n2s3.features.io.input

import fr.univ_lille.cristal.emeraude.n2s3.core.actors.InputLayer
import fr.univ_lille.cristal.emeraude.n2s3.core.event.{Event, EventResponse}
import fr.univ_lille.cristal.emeraude.n2s3.support.io.InputStreamCombinators._
import fr.univ_lille.cristal.emeraude.n2s3.support.io._

/**
  * Created by guille on 6/3/16.
  */

abstract class InputGenerator[T <: InputPacket] extends InputStream[T]{
    def shape : Shape

  var stageStrategy: InputStageStrategy = new EndStageOnItemProcessed()
  def getStageManagementStrategy(): InputStageStrategy = this.stageStrategy

  def setContainer(container : InputLayer) : Unit = {
    stageStrategy.inputLayer = Some(container)
  }
}

abstract class StreamConverter[I <: InputPacket, O <: InputPacket] {

  protected var shape = Shape()
  var container : Option[InputLayer] = None

  def setShape(shape : Shape) : Unit = {
    this.shape = shape
  }

  def resetConverter() : Unit

  final def reset() : Unit = {
    resetConverter()
  }

  def dataConverter(in : I) : O
  def shapeConverter(in : Shape) : Shape = in

  def initialize() : Unit = {}

  def setContainer(container: InputLayer) : Unit = {
    this.container = Some(container)
    initialize()
  }

  def addEvent(event : Event[_ <: EventResponse]): Unit = {
    if(container.isDefined)
      container.get.addEvent(event)
    else
      throw new RuntimeException("No container")
  }

  def triggerEvent[Response <: EventResponse](event : Event[Response]) : Unit = {
    if(container.isDefined)
      container.get.triggerEvent(event)
    else
      throw new RuntimeException("No container")
  }

  def triggerEventWith[Response <: EventResponse](event : Event[Response], response : Response) : Unit = {
    if(container.isDefined)
      container.get.triggerEventWith(event, response)
    else
      println("warning : No container")
  }

}

object N2S3InputStreamCombinators{
  implicit def streamCombinator[T <: InputPacket](stream: InputGenerator[T]): N2S3InputStreamCombinator[T, T] = {
    new N2S3InputStreamCombinator[T, T](stream)
  }
}

class N2S3InputStreamCombinator[T <: InputPacket, T2 <: InputPacket](originalStream: InputGenerator[T], combinedStream: InputStream[T2]) extends InputGenerator[T2] {

  def this(stream: InputGenerator[T]) {
    this(stream, stream.asInstanceOf[InputStream[T2]])
  }

  override def setContainer(container : InputLayer) : Unit = {
    super.setContainer(container)
    originalStream.setContainer(container)

    combinedStream match {
      case value: InputGenerator[InputPacket] => value.setContainer(container)
      case _ =>
    }
  }

  def take(i: Int) = {
    val newCombinedStream = combinedStream.take(i)
    new N2S3InputStreamCombinator(originalStream, newCombinedStream)
  }

  def repeat(numberOfTimes: Int) = {
    val newCombinedStream = combinedStream.repeat(numberOfTimes)
    new N2S3InputStreamCombinator(originalStream, newCombinedStream)
  }

  def shuffle() = {
    val newCombinedStream = combinedStream.shuffle()
    new N2S3InputStreamCombinator(originalStream, newCombinedStream)
  }

  override def shape : Shape = originalStream.shape
  override def getStageManagementStrategy() = originalStream.getStageManagementStrategy()

  override def next(): T2 = combinedStream.next()
  override def atEnd(): Boolean = combinedStream.atEnd()
  override def reset(): Unit = combinedStream.reset()

  override def toString = combinedStream.toString
}
