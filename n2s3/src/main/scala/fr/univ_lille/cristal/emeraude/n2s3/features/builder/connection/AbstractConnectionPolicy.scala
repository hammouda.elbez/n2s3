package fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection

import fr.univ_lille.cristal.emeraude.n2s3.core.Neuron._
import fr.univ_lille.cristal.emeraude.n2s3.core.{Neuron, _}
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.{NeuronGroupRef, NeuronRef}
import fr.univ_lille.cristal.emeraude.n2s3.support.Time

/**
  * Created by falezp on 19/05/16.
  */

abstract class ConnectionTypeBuilder{
  def createConnection(neuronConnectionConstructor: () => NeuronConnection): ConnectionPolicy
}

case class Connection(from : NetworkEntityPath, to : NetworkEntityPath, connectionType : Option[NeuronConnection] = None, preDelay : Time = Time(0), connectionProperties: Seq[(ConnectionProperty[Any], Any)] = Seq())

class ConnectionSet(val list : Traversable[ConnectionPath]) {

  def disconnect() : Unit = {
    var count = 0
    var lastCount = 0
    val size = list.size

    list.map{ path =>
      ExternalConnectionSender.askTo(path, RemoveConnection)

      if(count - lastCount > 10000) {
        println("Destroy connection "+count+"/"+size)
        lastCount = count
      }

      count += 1
    }
  }

  /**
    * Remove connexion and send signals
    */
  def disconnectByPath(cnx:ConnectionPath) : Unit = {
  // ExternalConnectionSender.askTo(cnx, RemoveConnection)
   ExternalSender.askTo(cnx.outputNeuron, Neuron.RemoveNeuronInputConnection(cnx.connectionID))
   ExternalSender.askTo(cnx.outputNeuron, RemoveNeuronOutputConnection(cnx.outputNeuron,cnx.connectionID))
  }


  def size = list.size
}

trait ConnectionPolicy {

  var defaultConnectionConstructor : Option[() => NeuronConnection] = None

  protected final def construct(connections : Traversable[Connection]) : ConnectionSet = {
    var count = 0
    var lastCount = 0
    val size = connections.size
    new ConnectionSet(
      connections.toSeq.groupBy(_.from).flatMap { case(from, list) =>
        if(count - lastCount > 10000) {
          println("Create connection "+count+"/"+size)
          lastCount = count
        }

        count += list.size

        ExternalSender.askTo(from, NeuronConnectionRequests(list.map{ entry =>
          CreateNeuronConnectionWith(entry.to, entry.connectionType.getOrElse(defaultConnectionConstructor.get()), entry.connectionProperties, entry.preDelay.timestamp)
        })).asInstanceOf[NewConnections].paths
      }
    )
  }

  def setDefaultConnectionConstructor(constructor : () => NeuronConnection) : this.type = {
    this.defaultConnectionConstructor = Some(constructor)
    this
  }

  final def create(from : NeuronGroupRef, to : NeuronGroupRef) : ConnectionSet = {
    construct(generate(from, to))
  }

  def generate(from : NeuronGroupRef, to : NeuronGroupRef) : Traversable[Connection]

 /********************************************************************************************************************
  * Testing
  ******************************************************************************************************************/

 def connects(aNeuron: NeuronRef, anotherNeuron: NeuronRef): Boolean
}

object NoConnection extends ConnectionPolicy {
  override def generate(from: NeuronGroupRef, to: NeuronGroupRef): Traversable[Connection] = None

 /********************************************************************************************************************
  * Testing
  ******************************************************************************************************************/

  override def connects(aNeuron: NeuronRef, anotherNeuron: NeuronRef): Boolean = false
}