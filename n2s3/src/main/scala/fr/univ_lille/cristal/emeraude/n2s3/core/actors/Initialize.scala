package fr.univ_lille.cristal.emeraude.n2s3.core.actors

import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message

/***********************************************************************************************
 *  Message send for ask an actor to initiate itself
 **********************************************************************************************/
object Initialize extends Message
