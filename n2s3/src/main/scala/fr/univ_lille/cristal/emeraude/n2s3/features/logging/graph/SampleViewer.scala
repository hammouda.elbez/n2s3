package fr.univ_lille.cristal.emeraude.n2s3.features.logging.graph

import java.awt.{Color, Dimension, FlowLayout, Graphics}

import javax.swing.{JFrame, JPanel, WindowConstants}
import akka.actor.{ActorRef, Props}
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.Done
import fr.univ_lille.cristal.emeraude.n2s3.core.event._
import fr.univ_lille.cristal.emeraude.n2s3.core.{ExternalSender, NetworkActor}
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.{N2S3, NeuronGroupObserverRef}
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.{InputSample, InputSample2D, InputSample3D, OnOffCellEvent}
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.{LocalActorDeploymentStrategy, Message}

import scala.collection.mutable

/**
  * Created by pfalez on 10/04/17.
  */

class SampleViewerRef(n2s3 : N2S3, event : Event[_], eventResponse : EventResponse => InputSample[_], pixelSize : Int = 1, displayLast : Boolean = true, waitMasks : Boolean = false) extends NeuronGroupObserverRef {
  var actor: Option[ActorRef] = None
  def getActors : Seq[ActorRef] = this.actor.toSeq


  override def deploy(n2s3: N2S3): Unit = {
    this.actor = Some(n2s3.system.actorOf(Props(new SampleViewer(n2s3, event, eventResponse, pixelSize, displayLast, waitMasks)), LocalActorDeploymentStrategy))
  }
}

case class Mask(pos : Seq[Int], color : Color, width : Int, height : Int)
case class DisplayMask(masks : Seq[Mask]) extends Message

class SampleViewer(n2s3: N2S3, event : Event[_], eventResponse : EventResponse => InputSample[_], pixelSize : Int, displayLast : Boolean = true, waitMasks : Boolean) extends NetworkActor {

  class SamplePanel(width : Int, height : Int) extends JPanel {

    var data : Seq[Seq[Float]] = Seq()
    var nextData : Seq[Seq[Float]] = Seq()

    var mask : Seq[Mask] = Seq()
    var nextMask : mutable.ArrayBuffer[Mask] = mutable.ArrayBuffer()

    setPreferredSize(new Dimension(width*pixelSize, height*pixelSize))
    setMinimumSize(new Dimension(width*pixelSize, height*pixelSize))

    override def paintComponent(g: Graphics): Unit = {
      data.zipWithIndex.foreach{ case(l, x) =>
        l.zipWithIndex.foreach { case(v, y) =>
          g.setColor(new Color(v, v, v))
          g.fillRect(x*pixelSize, y*pixelSize, pixelSize, pixelSize)
        }
      }

      mask.foreach{ case Mask(pos, color, width, height) =>
        g.setColor(color)
        g.fillRect(pos(0)*pixelSize, pos(1)*pixelSize, width*pixelSize, height*pixelSize)
      }
    }

    def update(data : Seq[Seq[Float]]): Unit = {
      this.nextData = data
    }

    def addMask(mask: Mask): Unit = {
      this.nextMask += mask
    }

    def commit() : Unit = {
      data = nextData
      mask = nextMask
      nextData = Seq()
      nextMask = mutable.ArrayBuffer()
      repaint()
      validate()
    }


  }

  var panels : Seq[SamplePanel] = Seq()

  var initialized = false

  val frame = new JFrame()


  frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE)
  frame.validate()
  frame.setVisible(true)

  override def initialize(): Unit = {
    askTo(n2s3.inputLayerRef.get.getContainer, Subscribe(OnOffCellEvent, ExternalSender.getReference(self)))
  }

  override def destroy(): Unit = {

  }

  private def  transformValue(v : Any) : Float = v match {
    case i : Int => i.toFloat
    case f : Float => f
  }

  override def process(message: Message, sender : ActorRef): Unit = message match {
    case m : EventResponse =>
      eventResponse(m) match {
        case sample : InputSample2D[_] =>

          if(!initialized) {
            val panel = new SamplePanel(sample.getShape.dimensions(0), sample.getShape.dimensions(1))
            frame.add(panel)
            panels = Seq(panel)
          }

          if(displayLast)
            panels.head.commit()

          panels.head.update(sample.getData2D.map(l => l.map(v => transformValue(v))))

          if(!waitMasks && !displayLast) {
            panels.head.commit()
          }

          if(!initialized) {
            frame.pack()
            initialized = true
          }

        case sample : InputSample3D[_] =>
          if(!initialized) {
            frame.setLayout(new FlowLayout())
            panels = (0 until sample.getShape.dimensions(2)).map { _ =>
              val panel = new SamplePanel(sample.getShape.dimensions(0), sample.getShape.dimensions(1))
              frame.add(panel)
              panel
            }
          }

          if(displayLast)
            panels.foreach(_.commit())

          (0 until sample.getShape.dimensions(2)).foreach { z =>
            panels(z).update(
              for(x <- 0 until sample.getShape.dimensions(0)) yield {
                for(y <- 0 until sample.getShape.dimensions(1)) yield {
                  transformValue(sample.getDataOnChannel(x, y, z))
                }
              }
            )

            if(!waitMasks && !displayLast) {
              panels(z).commit()
            }

            if(!initialized) {
              frame.pack()
              initialized = true
            }
          }
      }
    case DisplayMask(seq) =>
      seq.foreach{ m =>
          if(m.pos.size == 3)
            panels(m.pos(2)).addMask(m)
        else
            panels.foreach(_.addMask(m))

      }

      panels.foreach(_.commit())
      sender ! Done
  }
}
