/************************************************************************************************************
 * Contributors: 
 * 		- created by falezp on 23/05/16
 ***********************************************************************************************************/
package fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.types

import fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.{Connection, ConnectionPolicy}
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.{NeuronGroupRef, NeuronRef}

class OneToOneConnection extends ConnectionPolicy {
	override def generate(from: NeuronGroupRef, to: NeuronGroupRef): Traversable[Connection] = {
		assert(from.neurons.size == to.neurons.size)
		from.neuronPaths.zip(to.neuronPaths).map{ case (in, out) => Connection(in, out)}
	}

  /** ******************************************************************************************************************
    * Testing
    * *****************************************************************************************************************/
  override def connects(aNeuron: NeuronRef, anotherNeuron: NeuronRef): Boolean = {
		aNeuron.getIndex == anotherNeuron.getIndex
	}
}