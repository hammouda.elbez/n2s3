/************************************************************************************************************
 * Contributors: 
 * 		-  Pierre
 ***********************************************************************************************************/

package fr.univ_lille.cristal.emeraude.n2s3.core

import fr.univ_lille.cristal.emeraude.n2s3.core.Neuron.{ConnectionID, InputConnection}
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.WrapMessage
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message

import scala.collection.mutable.Map
import scala.collection.{AbstractMap, immutable}


abstract class PropertyMessage extends Message

/***********************************************************************************************************
 * Property are visible attribute of a model.
 * Properties can be get and set
 * 
 * There are two type of property :
 *  Basic which are unique per actor
 *  Connection which can be several in an actor
 **********************************************************************************************************/

/**********************************************************************************************************
 *  							                         MANAGE PROPERTIES
 *********************************************************************************************************/
object GetAllProperties extends PropertyMessage
case class PropertiesList(content : immutable.Map[Property[Any], Any]) extends PropertyMessage

object  GetAllConnectionProperties extends PropertyMessage
case class ConnectionPropertiesList(content : immutable.Map[ConnectionProperty[Any], ConnectionPropertyValues[Any]]) extends PropertyMessage

object GetClassName extends PropertyMessage
/**********************************************************************************************************
 *  							                         NEURONS PROPERTIES
 *********************************************************************************************************/
case class SetProperty[A](property : Property[A], value : A) extends PropertyMessage
case class GetProperty[A](property : Property[A]) extends PropertyMessage
case class PropertyValue[A](value : A) extends PropertyMessage




/**********************************************************************************************************
 *  							                     CONNECTION PROPERTIES
 *********************************************************************************************************/
case class SetConnectionProperty[A](property : ConnectionProperty[A], value : A) extends PropertyMessage
case class GetConnectionProperty[A](property : ConnectionProperty[A]) extends PropertyMessage

case class GetAllConnectionProperty[A](property : ConnectionProperty[A]) extends PropertyMessage
case class ConnectionPropertyValues[A](values : Seq[(ConnectionID, NetworkEntityPath, A)]) extends PropertyMessage
case class SetAllConnectionProperty[A](property : ConnectionProperty[A], values : Seq[(ConnectionID, A)]) extends PropertyMessage

/*
case class SetAllConnectionProperty[A](property : ConnectionProperty[A], values : Seq[(NetworkEntityPath, A)]) extends PropertyMessage
case class GetAllConnectionProperty[A](property : ConnectionProperty[A]) extends PropertyMessage

*/
class PropertyHandler[A](val getter : () => A, val setter : A => Unit) extends Serializable {

  def setValue(value: Any): Unit ={
    setter(value.asInstanceOf[A])
  }

  def set(message : PropertyMessage) : Unit = {
    message match {
      case SetProperty(_, value) => this.setValue(value)
      case other => throw new RuntimeException("Incorrect PropertyMessage")
    }
  }
  
  def get(message : PropertyMessage) : A = {
    getter()
  }
}

class ConnectionPropertyHandler[A](val getter : NeuronConnection => Option[A], val setter : (NeuronConnection, A) => Unit) extends Serializable {

  def set(connections : AbstractMap[ConnectionID, InputConnection], message : PropertyMessage, connectionID : ConnectionID) : Unit = {
    message match {
      case SetConnectionProperty(_, value) =>
        setter(connections(connectionID).connection, value.asInstanceOf[A])
      case other => throw new RuntimeException("Incorrect PropertyMessage")
    }
  }

  def setAll(connections : AbstractMap[ConnectionID, InputConnection], message : PropertyMessage) : Unit = {
    message match {
      case SetAllConnectionProperty(_, values) =>
        values.foreach{ case(id, value) =>
          setter(connections(id).connection, value.asInstanceOf[A])
        }
      case other => throw new RuntimeException("Incorrect PropertyMessage")
    }
  }

  def get(connections : AbstractMap[ConnectionID, InputConnection], message : PropertyMessage, connectionID : ConnectionID) : A = {
    message match {
      case GetConnectionProperty(_) => getter(connections(connectionID).connection).get
      case other => throw new RuntimeException("Incorrect PropertyMessage")
    }
  }

  def getAll(connections : AbstractMap[ConnectionID, InputConnection], message : PropertyMessage) : Seq[(ConnectionID, NetworkEntityPath, A)] = {
    message match {
      case GetAllConnectionProperty(_) =>
        connections.toSeq.map{ case(connectionId, connectionObj) => (connectionId, connectionObj.path, getter(connectionObj.connection))}.filter(_._3.isDefined)
          .map{ case(id, path, value) => (id, path, value.get) }

      case other => throw new RuntimeException("Incorrect PropertyMessage")
    }
  }
}

abstract class Property[A] extends Serializable {
  def create(getter : () => A, setter : A => Unit) : PropertyHandler[A] = {
    new PropertyHandler[A](getter, setter)
  }
}

abstract class ConnectionProperty[A] extends Serializable {
  def create(getter : NeuronConnection => Option[A], setter : (NeuronConnection, A) => Unit) : ConnectionPropertyHandler[A] = {
    new ConnectionPropertyHandler[A](getter, setter)
  }
}

/*******************************************************************************************************
 * PropertyHolder contains properties
 ******************************************************************************************************/
trait PropertyHolder {
  val properties = Map[Property[_], PropertyHandler[_]]()
  //val connectionProperties = Map[ConnectionProperty[_], ConnectionPropertyHandler[_]]()
  //val connectionProperties = mutable.ArrayBuffer[ConnectionProperty[_]]()

  var connections : AbstractMap[ConnectionID, InputConnection] = _

  def initializePropertyHolder(connections : AbstractMap[ConnectionID, InputConnection]) : Unit = {
    this.connections = connections
  }
  
  def addProperty[A](property : Property[A], getter : () => A, setter : A => Unit) : Unit = {
    properties += (property -> property.create(getter, setter))
  }

  def addConnectionProperty[A](property : ConnectionProperty[A]) : Unit = {
    //connectionProperties += property
  }

  def unableToFindProperty[A](property : Property[A]) : Unit = {

 //     println("Warning : Unable to find property \""+property+"\"")
    throw new RuntimeException("Unable to find property \""+property+"\"")
  }

  def unableToFindConnection[A](property : ConnectionProperty[A], connectionID: ConnectionID) : Unit = {
    //     println("Warning : Unable to find property \""+property+"\"")
    throw new RuntimeException("Unable to find connection "+connectionID)
  }

  def unableToFindConnectionProperty[A](property : ConnectionProperty[A], connectionID: ConnectionID, connectionObject : NeuronConnection) : Unit = {
    //     println("Warning : Unable to find property \""+property+"\"")
    throw new RuntimeException("Unable to find connection property \""+property+"\" on connection "+connectionID+" (connection class="+connectionObject.getClass.getCanonicalName+")")
  }

  def setProperty(property: Property[_], value: Any): Unit = {
    properties.get(property) match {
      case Some(handler) => handler.setValue(value)
      case None => unableToFindProperty(property)
    }
  }

  def getConnectionProperty[T](property: ConnectionProperty[T], connectionID : ConnectionID) : T = {
    if(!connections.isDefinedAt(connectionID))
      unableToFindConnection(property, connectionID)
    val ret = connections(connectionID).connection.getProperty[T](property)

    if(ret.isEmpty)
      unableToFindConnectionProperty(property, connectionID, connections(connectionID).connection)

    ret.get
  }

  def setConnectionProperty[T](property: ConnectionProperty[T], value: T, connectionID: ConnectionID) : Unit = {
    if(!connections.isDefinedAt(connectionID))
      unableToFindConnection(property, connectionID)
    connections(connectionID).connection.setProperty[T](property, value)
  }

  def getAllConnectionProperty[T](property: ConnectionProperty[T]) : Seq[(ConnectionID, NetworkEntityPath, T)] = {
    connections.flatMap{ case(connectionId, connectionObj) =>
      val ret = connectionObj.connection.getProperty[T](property)

      if(ret.isDefined)
        Some((connectionId, connectionObj.path, ret.get))
      else
        None
    }.toSeq
  }

  def setAllConnectionProperty[T](property: ConnectionProperty[T], values : Seq[(ConnectionID, T)]) : Unit = {
    values.foreach{ case(connectionId, value) =>
      setConnectionProperty[T](property, value, connectionId)
    }
  }

  def processConnectionPropertyMessage(message : PropertyMessage, sender : NetworkEntityReference, connectionID: ConnectionID) : Unit = message match {
    case SetConnectionProperty(property, value) =>
      setConnectionProperty(property, value, connectionID)
    case GetConnectionProperty(property) =>
      sender.send(PropertyValue(getConnectionProperty(property, connectionID)))
  }

  def processPropertyMessage(message : PropertyMessage, sender : NetworkEntityReference) : Unit = message match {
    case SetProperty(property, _) =>
      properties.get(property) match {
        case Some(handler) => handler.set(message)
        case None => unableToFindProperty(property)
      }
    case GetProperty(property) =>
      properties.get(property) match {
        case Some(handler) => sender.send(PropertyValue(handler.get(message)))
        case None => unableToFindProperty(property)
      }
    case GetAllConnectionProperty(property) =>
      sender.send(ConnectionPropertyValues(getAllConnectionProperty(property)))
    case GetAllProperties =>
      sender.send(PropertiesList(properties.map(entry => (entry._1.asInstanceOf[Property[Any]], entry._2 match {
        case value : PropertyHandler[_] => value.getter()
      })).toMap))
    case GetAllConnectionProperties =>
      throw new UnsupportedOperationException
      /*
      sender.send(ConnectionPropertiesList(connectionProperties
        .map(property => property.asInstanceOf[ConnectionProperty[Any]] ->
          ConnectionPropertyValues[Any](getAllConnectionProperty(property))).toMap
      ))*/
    case SetAllConnectionProperty(property, values) =>
      setAllConnectionProperty(property, values)
    case GetClassName =>
      sender.send(WrapMessage(this.getClass.getCanonicalName))
    case other => throw new RuntimeException("Unknown message \""+message+"\"")
  }
  
}