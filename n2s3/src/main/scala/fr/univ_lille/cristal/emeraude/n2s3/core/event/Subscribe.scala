package fr.univ_lille.cristal.emeraude.n2s3.core.event

import fr.univ_lille.cristal.emeraude.n2s3.core.NetworkEntityReference

/** Subscribe an actor to an event */
case class Subscribe(event : Event[_ <: EventResponse], ref : NetworkEntityReference) extends EventHolderMessage