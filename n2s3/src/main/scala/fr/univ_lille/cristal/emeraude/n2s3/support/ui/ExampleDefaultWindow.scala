/**************************************************************************************************
 * Contributors:
 *		- damien.marchal@univ-lille1.fr
 **************************************************************************************************/
package fr.univ_lille.cristal.emeraude.n2s3.support.ui

import java.awt._

import javax.swing.event.{HyperlinkEvent, HyperlinkListener}
import javax.swing.{JEditorPane, JFrame, JPanel, WindowConstants}

class ExampleDefaultWindow(
    title : String = "N2S3::Example", 
    width : Int = 1000, height : Int = 550
    )
{
  	var frame : JFrame = null
  	var textPane : JEditorPane = new JEditorPane()
  	var viewPane : JPanel = new JPanel() ; 
  	
	  frame = new JFrame
    frame.setTitle(title);
		frame.setVisible(true)
		frame.setSize(new Dimension(width, height))
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE)

		frame.setLayout(new GridBagLayout());		
		var cst = new GridBagConstraints ;
		cst.fill = GridBagConstraints.HORIZONTAL;
    cst.weighty = 1;
    cst.gridx = 0;
    cst.gridy = 0;
    cst.gridwidth = 800 

    cst.anchor = GridBagConstraints.FIRST_LINE_START 
    
    frame.add(textPane, cst);
    textPane.setContentType("text/html");
    textPane.setEditable(false);
    
    textPane.addHyperlinkListener(new HyperlinkListener() {
        override def hyperlinkUpdate(e : HyperlinkEvent) {
          if(e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
            if(Desktop.isDesktopSupported()) {
              Desktop.getDesktop().browse(e.getURL().toURI());
            }
          }
        }
    })
    
    textPane.setText("loremipsum loremipsum loremipsum <br> loremipsum loremipsum <br>"); 
    
    viewPane.setVisible(true)
    viewPane.setLayout(new FlowLayout())
    cst.weighty = 1.0
    cst.gridx = 0
    cst.gridy = 1
    cst.ipadx = 800
    cst.ipady = 200
    
    cst.anchor = GridBagConstraints.CENTER 
    frame.add(viewPane, cst)
    
    def setText(text : String){
      textPane.setText(text); 
    }
    
    def getPane() : JPanel = {
      viewPane
    }
}

