package fr.univ_lille.cristal.emeraude.n2s3.features.learning

import akka.actor.Actor
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.event.{NeuronFireEvent, NeuronFireResponse, Subscribe}
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message

import scala.collection.mutable
/**
  * Created by falezp on 30/05/16.
  */
object GetAndReset extends Message

class SpikeCounter(neurons : Seq[NetworkEntityPath]) extends Actor {

  neurons.foreach { path =>
    ExternalSender.askTo(path, Subscribe(NeuronFireEvent, ExternalSender.getReference(self)))
  }

  var count = mutable.Map(neurons.map(e => (e, 0)):_*)

  def receive = {
    case GetAndReset =>
      sender ! count.toMap
      count = mutable.Map(neurons.map(e => (e, 0)):_*)
    case NeuronFireResponse(_, source) =>
      count(source) += 1
  }




}
