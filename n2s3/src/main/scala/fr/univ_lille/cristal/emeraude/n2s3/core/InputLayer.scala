package fr.univ_lille.cristal.emeraude.n2s3.core.actors

import fr.univ_lille.cristal.emeraude.n2s3.core.Neuron.NeuronMessage
import fr.univ_lille.cristal.emeraude.n2s3.core.Synchronizer.{AskInput, CleanQueue, ProcessUntil, ResponseInput}
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.event.{EventHolder, EventHolderMessage, LabelChangeEvent, LabelChangeResponse}
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input._
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message

/**
  * Specialization of the NetworkContainer for the input layer
  */
class InputLayer(stream: StreamSupport[_ <: InputPacket, N2S3InputPacket]) extends NetworkContainer with EventHolder {

  var lastDisplay : Long = 0L
  val displayInterval : Long = 10000000000L

  stream.setContainer(this)

 /********************************************************************************************************************
  * Declaration of events
  ******************************************************************************************************************/
  addEvent(LabelChangeEvent)

  override def receiveMessage(message: Message, sender: NetworkEntityReference): Unit = message match {
    case AskInput =>

      if(System.nanoTime() >= lastDisplay+displayInterval) {
        println(stream)
        lastDisplay = System.nanoTime()
      }

      if (!stream.atEnd()) {
        val input = stream.next()
        val dataList = input.getAllData
        val metaDataList = input.getMetaData

        if (dataList.isEmpty && metaDataList.isEmpty) {
          println("Input returned no more data or metadata")
          sender.send(CleanQueue)
          sender.send(ResponseInput)
        }
        else {

          dataList.foreach { case(index, l) =>
            val target = childEntityAt(stream.shape.toIndex(index:_*))

            l.foreach{
              case spike : N2S3InputSpike =>
                target.sendMessageFrom(NeuronMessage(spike.getStartTimestamp, spike.getMessage, null, null, None), TrashNetworkEntityReference)
              case m => println("Warning : unhandled input stimuli "+m.toString)
            }
          }

          metaDataList.foreach{
            case label : N2S3InputLabel =>
              this.triggerEventWith(LabelChangeEvent, LabelChangeResponse(label.getStartTimestamp, label.getStartTimestamp+label.getDuration, label.getLabel))
            case m => println("Warning : unhandled meta data "+m.toString)
          }
          //this.stream.getStageManagementStrategy().itemProcessed()
          sender.send(ResponseInput)
          sender.send(ProcessUntil(input.getStartTimestamp+input.getDuration))
        }
      }
      else{
        sender.send(NoMoreInput)
        sender.send(ProcessUntil(-1L))
      }
    case AskRemainInput =>
      sender.send(WrapMessage(!stream.atEnd()))
    case m: EventHolderMessage =>
      processEventHolderMessage(m)
    case _ => super.receiveMessage(message, sender)
  }

}
