package fr.univ_lille.cristal.emeraude.n2s3.core.actors

import squants.electro.ElectricCharge
import squants.electro.ElectricChargeConversions.ElectricalChargeConversions
/**
  * Created by guille on 10/14/16.
  */
case class ElectricSpike(charge : ElectricCharge = 1 millicoulombs) extends Spike
