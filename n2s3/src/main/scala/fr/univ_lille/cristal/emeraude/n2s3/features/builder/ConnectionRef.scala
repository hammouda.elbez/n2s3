package fr.univ_lille.cristal.emeraude.n2s3.features.builder

import fr.univ_lille.cristal.emeraude.n2s3.core.ConnectionPath
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.{ConnectionPolicy, ConnectionSet}

class ConnectionRef(from: NeuronGroupRef, to: NeuronGroupRef, connectionType: ConnectionPolicy) {

  var deployed = false
  def isDeployed: Boolean = this.deployed
  var connections : ConnectionSet = _

  def ensureDeployed(n2s3: N2S3): Unit = {
    if (!this.isDeployed){
      deployed = true
      this.deploy(n2s3)
    }
  }

  def deploy(n2s3: N2S3): Unit = {
    println("Create connections from "+from.getIdentifier+" to "+to.getIdentifier)
    connections = connectionType.create(from, to)
  }

  def disconnect() : Unit = {
    assert(connections != null, "connection already disconnected")
    connections.disconnect()
    connections = null
  }

  def disconnectByPath(cnx:ConnectionPath) : Unit = {
    connections.disconnectByPath(cnx)
    connections = new ConnectionSet(connections.list.filter(c => c != cnx))
  }

  def connects(aNeuron: NeuronRef, anotherNeuron: NeuronRef): Boolean = {
    (aNeuron.group == from) & (anotherNeuron.group == to) & connectionType.connects(aNeuron, anotherNeuron)
  }

  def getFrom: NeuronGroupRef = { from }

  def getTo: NeuronGroupRef = { to }

}