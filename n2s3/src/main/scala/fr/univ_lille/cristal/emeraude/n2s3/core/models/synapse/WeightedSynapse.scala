package fr.univ_lille.cristal.emeraude.n2s3.core.models.synapse

import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.event.{SynapseWeightChange, SynapseWeightResponse}
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.{RemoteWeightChange, SynapticDelay, SynapticWeightFloat}
import fr.univ_lille.cristal.emeraude.n2s3.support.Time

/**
  * Created by falezp on 08/06/16.
  */

/**
  * Synapse which had a weight parameter
  */
abstract class WeightedSynapse[WeightType](initialWeight : WeightType, initialDelay : Time = Time(0)) extends NeuronConnection {

  private var weight : WeightType = initialWeight
  private var delay : Time = initialDelay
  private var remoteWeightChange = false


  override def initialize() : Unit = {
    super.initialize()
    addProperty(SynapticDelay)
    addProperty(RemoteWeightChange)

    addEvent(SynapseWeightChange)
  }

  override def getProperty[T](property : ConnectionProperty[T]) : Option[T] = property match {
    case SynapticDelay => Some(getDelay.asInstanceOf[T])
    case RemoteWeightChange => Some(remoteWeightChange.asInstanceOf[T])
    case _ => super.getProperty[T](property)
  }

  override def setProperty[T](property : ConnectionProperty[T], value : T) : Unit = property match {
    case RemoteWeightChange => remoteWeightChange = value.asInstanceOf[Boolean]
    case _ => super.setProperty[T](property, value)
  }

  def getWeight : WeightType = weight
  def setWeight(weight : WeightType, force : Boolean = false) : Unit = {
    if(force || (!hasFixedParameter && !remoteWeightChange)) this.weight = weight
    if(remoteWeightChange)
      triggerEvent(SynapseWeightChange, SynapseWeightResponse[WeightType](getEnds.getCurrentTimestamp, ConnectionPath(getEnds.neuron.getNetworkAddress, getEnds.connectionId), weight))
  }

  def getDelay : Time = delay
  def setDelay(delay : Time) : Unit = {
    if(delay.timestamp < 0) {
      throw new RuntimeException("Negative delays are forbidden")
    }

    this.delay = delay
  }

  def getRemoteWeightChange : Boolean = this.remoteWeightChange
  def setRemoteWeightChange(state : Boolean) : Unit = this.remoteWeightChange = state
}

abstract class FloatSynapse(initialWeight : Float = 1f, initialDelay : Time = Time(0)) extends WeightedSynapse[Float](initialWeight, initialDelay) {

  override def initialize() : Unit = {
    super.initialize()
    addProperty(SynapticWeightFloat)
  }

  override def getProperty[T](property : ConnectionProperty[T]) : Option[T] = property match {
    case SynapticWeightFloat => Some(getWeight.asInstanceOf[T])
    case _ => super.getProperty[T](property)
  }

  override def setProperty[T](property : ConnectionProperty[T], value : T) : Unit = property match {
    case SynapticWeightFloat => setWeight(value.asInstanceOf[Float], force = true)
    case _ => super.setProperty[T](property, value)
  }
}
abstract class BinarySynapse(initialWeight : Boolean = true, initialDelay : Time = Time(0)) extends WeightedSynapse[Boolean](initialWeight, initialDelay)