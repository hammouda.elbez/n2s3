package fr.univ_lille.cristal.emeraude.n2s3.core.event

import fr.univ_lille.cristal.emeraude.n2s3.core.{NetworkEntityPath, NetworkEntityReference}

/** Subscribe an actor to an event and send the triggered event though a synchronizer */
case class SubscribeSynchronized(event : TimedEvent[_ <: TimedEventResponse], ref : NetworkEntityReference, synchronizer: NetworkEntityPath) extends EventHolderMessage
