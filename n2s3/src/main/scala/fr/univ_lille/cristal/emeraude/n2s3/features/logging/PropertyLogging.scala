package fr.univ_lille.cristal.emeraude.n2s3.features.logging

import akka.actor.{ActorRef, Props}
import akka.pattern.ask
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.{Config, Done}
import fr.univ_lille.cristal.emeraude.n2s3.core.event._
import fr.univ_lille.cristal.emeraude.n2s3.core.{ExternalSender, NetworkActor, NetworkEntityPath}
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.{N2S3, NeuronGroupObserverRef}
import fr.univ_lille.cristal.emeraude.n2s3.features.logging.PropertyLogging.{GetData, ResponseData}
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.{LocalActorDeploymentStrategy, Message}

import scala.collection.mutable
import scala.concurrent.Await
/**
  * Created by pfalez on 29/05/17.
  */
object PropertyLogging {
  object GetData extends Message
  case class ResponseData[T](data : Map[NetworkEntityPath, Seq[(Timestamp, T)]]) extends Message
}

class PropertyLoggingRef[T](n2s3 : N2S3, event : TimedEvent[_ <: TimedEventResponse], entities : Seq[NetworkEntityPath], extractInfoFunction : Message => (NetworkEntityPath, T)) extends NeuronGroupObserverRef {
  var actor: Option[ActorRef] = None
  def getActors : Seq[ActorRef] = this.actor.toSeq


  override def deploy(n2s3: N2S3): Unit = {
    this.actor = Some(n2s3.system.actorOf(Props(new PropertyLogging[T](n2s3, event, entities, extractInfoFunction)), LocalActorDeploymentStrategy))
  }

  def get : Map[NetworkEntityPath, Seq[(Timestamp, T)]] = {
    Await.result(ask(actor.get, GetData)(Config.longTimeout), Config.longTimeout.duration).asInstanceOf[ResponseData[T]].data
  }
}


class PropertyLogging[T](n2s3 : N2S3, event : TimedEvent[_ <: TimedEventResponse], entities : Seq[NetworkEntityPath], extractInfoFunction : Message => (NetworkEntityPath, T)) extends NetworkActor {

  val logs = entities.map(_ -> mutable.ArrayBuffer[(Timestamp, T)]()).toMap
  override def initialize(): Unit = {
    entities.map(p => askFuture(p, SubscribeSynchronized(event, ExternalSender.getReference(self), n2s3.buildProperties.getSynchronizerPolicy.getNeuronSynchronizer(p.local))))
      .foreach(f => Await.result(f, Config.longTimeout.duration))
  }

  override def destroy(): Unit = {
    entities.map(p => askFuture(p, Unsubscribe(event, ExternalSender.getReference(self))))
      .foreach(f => Await.result(f, Config.longTimeout.duration))
  }

  override def process(message: Message, sender: ActorRef): Unit = message match {
    case SynchronizedEvent(s, _, m, t) =>
      val (sender, value) = extractInfoFunction(m)
      logs(sender) += t -> value
      ExternalSender.sendTo(s, Done)
    case GetData =>
      sender ! ResponseData(logs)
  }
}
