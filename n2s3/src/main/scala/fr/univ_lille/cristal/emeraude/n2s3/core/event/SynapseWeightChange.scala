package fr.univ_lille.cristal.emeraude.n2s3.core.event

import fr.univ_lille.cristal.emeraude.n2s3.core.ConnectionPath
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp

/**
  * Created by falezp on 21/03/17.
  */

object SynapseWeightChange extends TimedEvent[SynapseWeightResponse[_]]
case class SynapseWeightResponse[T](timestamp : Timestamp, source : ConnectionPath, value : T) extends TimedEventResponse
