package fr.univ_lille.cristal.emeraude.n2s3.core.models.properties

import fr.univ_lille.cristal.emeraude.n2s3.core.Property

/**
  * Created by falezp on 25/10/16.
  */

object MembraneThresholdTypeEnum extends Enumeration {
  val Static, Dynamic = Value
}

/**
  * Property used to control the type of the threshold (dynamic or static)
  */
object MembraneThresholdType extends Property[MembraneThresholdTypeEnum.Value]