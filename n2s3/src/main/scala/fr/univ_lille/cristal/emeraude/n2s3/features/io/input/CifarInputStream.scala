package fr.univ_lille.cristal.emeraude.n2s3.features.io.input

import java.io.{DataInputStream, FileInputStream, IOException}

/**
  * Created by pfalez on 07/05/18.
  */

object CifarInput extends InputFormat[InputSample2D[Float]](Shape(32, 32, 3)) {

  val CIFAR_IMAGE_PER_FILE = 10000
  val CIFAR_WIDTH = 32
  val CIFAR_HEIGHT = 32
  val CIFAR_DEPTH = 3

  def DataFrom(filename: String) = new CifarInputStream(filename)
}

object CifarInputStream {

  def apply(filename: String) = new CifarInputStream(filename)

}

class CifarInputStream (val filename: String) extends InputGenerator[InputSample3D[Float]] {

  import CifarInput._

  //stream of file
  var fileInputStream = new DataInputStream(new FileInputStream(filename))

  private var numberOfReadImages: Int = 0

  def next(): InputSample3D[Float] = {

    if (this.numberOfReadImages >= CIFAR_IMAGE_PER_FILE) {
      throw new IOException("Attempt to read after the end of the file")
    }

    val label = fileInputStream.readUnsignedByte().toString

    val pixels = for(z <- 0 until CIFAR_DEPTH) yield {
      for(y <- 0 until CIFAR_HEIGHT) yield {
        for(x <- 0 until CIFAR_WIDTH) yield {
          fileInputStream.readUnsignedByte().toFloat/255f
        }
      }
    }

    val image = for(x <- 0 until CIFAR_WIDTH) yield {
        for (y <- 0 until CIFAR_HEIGHT) yield {
          for (z <- 0 until CIFAR_DEPTH) yield {
            pixels(z)(y)(x)
          }
        }
      }

    this.numberOfReadImages += 1

    new InputSample3D[Float](image).setMetaData(InputLabel(label))
  }

  def atEnd(): Boolean = this.numberOfReadImages >= CIFAR_IMAGE_PER_FILE

  def reset() = {
    fileInputStream = new DataInputStream(new FileInputStream(filename))
    numberOfReadImages = 0
  }

  def shape: Shape = Shape(CIFAR_WIDTH, CIFAR_HEIGHT, CIFAR_DEPTH)

  override def toString = "[Mnist] "+filename+" : "+ numberOfReadImages + " / " + CIFAR_IMAGE_PER_FILE
}
