/************************************************************************************************************
 * Contributors: 
 * 		- created by falezp 
 ***********************************************************************************************************/
package fr.univ_lille.cristal.emeraude.n2s3.core

import fr.univ_lille.cristal.emeraude.n2s3.core.actors.NetworkEntityActor.AddChildEntity
import fr.univ_lille.cristal.emeraude.n2s3.core.exceptions.DuplicatedChildException
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.{Message, UnhandledMessageException}

import scala.collection.mutable

/**
  * This class is used to contain several NetworkEntity children
  * Each of them is associated to an getIdentifier, used to route messages
  */
class NetworkContainer extends NetworkEntity {

  /**
    * Map of all children entities associated to their identifiers
    */
  private var childEntities = mutable.HashMap[Any, NetworkEntity]()

  /**
    * Return the number of direct children entities
    */
  def numberOfChildEntity = childEntities.size

  def isEmpty = this.childEntities.isEmpty

  /**
    * Return the entity child associated to the getIdentifier
    *
    * @param identifier is the getIdentifier of the child
    * @throws NoSuchElementException if getIdentifier not associated to a child
    */
  def childEntityAt(identifier : Any) = {
    childEntities.get(identifier) match {
      case Some(entity) => entity
      case None => throw new NoSuchElementException(identifier.toString)
    }
  }

  /**
    * Retrieve the entity designated by the parameter path
    *
    * @param target is the local path, which is constituted of a sequence of getIdentifier
    * @return the NetworkEntity symbolized by the parameter destination
    */
  override def resolvePath(target : Traversable[Any])  : NetworkEntity = {
    if(target.isEmpty){
      this
    } else {
      this.childEntityAt(target.head).resolvePath(target.tail)
    }
  }

  /**
    * The only message handled by this class is AddChildEntity, which add a child entity to the current container
    * All other message throws an exception
    *
    * @param message is the content to be processed
    * @param sender is the reference of the sender
    * @throws RuntimeException when a child with the same getIdentifier already exists in this container
    * @throws UnhandledMessageException when the message is unrecognized
    */
  def receiveMessage(message : Message, sender : NetworkEntityReference) : Unit = message match {
    case AddChildEntity(identifier, entity) => addChild(identifier, entity)
    case _ => throw new UnhandledMessageException(this.getClass, message)
  }

  def addChild(identifier: Any, entity: NetworkEntity): Unit = {
    if (childEntities.isDefinedAt(identifier)) {
      throw new DuplicatedChildException("Child \"" + identifier + "\" already exists")
    }
    else {
      childEntities += identifier -> entity
      entity.setParent(this, container)
      entity.setLocalPath(localPath ++ Seq(identifier))
    }
  }
}