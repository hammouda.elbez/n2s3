/************************************************************************************************************
 * Contributors: 
 * 		- created by Pierre Falez
 ***********************************************************************************************************/
package fr.univ_lille.cristal.emeraude.n2s3.features.io.report

import java.io.{File, PrintWriter}
import java.sql.Timestamp

class BenchmarkListView(results : scala.collection.Map[Timestamp, BenchmarkResult]) extends Report {

  def save(filename : String) : Unit = {
    val writer = new PrintWriter(new File(filename))

    results.foreach { case(timestamp, result) =>
      writer.print(timestamp+" "+result.evaluationByMaxSpiking.score+" "+result.evaluationByOneSpiking.score+" "+result.quietInputCount)
      //result.theta.foreach { case(_, value) => writer.print(" "+value) }
      writer.println()
    }

    writer.close()
  }
}
