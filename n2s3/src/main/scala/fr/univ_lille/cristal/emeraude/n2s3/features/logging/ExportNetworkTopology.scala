package fr.univ_lille.cristal.emeraude.n2s3.features.logging

import java.io.{File, PrintWriter}

import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.SynapticWeightFloat
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.N2S3

/**
  * Created by falezp on 26/10/16.
  */
object ExportNetworkTopology {

  def convert_name(string : String) : String = {
    string.replace('/', '_').replace(':', '_')
    //"a"
  }

  def save(filename : String, n2s3 : N2S3) : Unit = {

    val out = new PrintWriter(new File(filename))

    out.println("digraph G {")
    n2s3.layers.zipWithIndex.foreach { case(layer, layer_index) =>


      out.println("\tsubgraph "+layer.identifier+"{")
      layer.neuronPaths.zipWithIndex.foreach { case(neuron, neuron_index) =>
        out.println("\t\t"+convert_name(neuron.toString)+"[pos=\""+neuron_index+","+layer_index+"!\"];")
      }
      out.println("\t}")

      layer.neuronPaths.foreach { neuron =>

        val synapses = ExternalSender.askTo(neuron, GetAllConnectionProperty(SynapticWeightFloat)) match {
          case PropertyValue(values: Seq[(NetworkEntityPath, Float)] @unchecked) => values
        }

        synapses.foreach { case (input, w) =>
          out.println("\t"+convert_name(input.toString)+" -> "+convert_name(neuron.toString)+" [label=\""+w+"\"];")
        }

      }


    }
    out.println("}")
    out.close()
  }

}
