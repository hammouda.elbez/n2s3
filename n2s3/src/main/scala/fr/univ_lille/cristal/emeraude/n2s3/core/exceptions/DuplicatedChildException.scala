package fr.univ_lille.cristal.emeraude.n2s3.core.exceptions

/**
  * Created by guille on 8/5/16.
  */
class DuplicatedChildException(message: String) extends Exception(message)
