/************************************************************************************************************
 * Contributors:
 * 		- created by Pierre 08/06/16.
 ***********************************************************************************************************/
package fr.univ_lille.cristal.emeraude.n2s3.models.synapses

import fr.univ_lille.cristal.emeraude.n2s3.core.actors.{BackwardSpike, ElectricSpike}
import fr.univ_lille.cristal.emeraude.n2s3.core.models.synapse.FloatSynapse
import fr.univ_lille.cristal.emeraude.n2s3.core.{NeuronConnection, SynapseBuilder}
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.Time
import fr.univ_lille.cristal.emeraude.n2s3.support.UnitCast._
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message

import scala.math.{exp, max, min}
import scala.util.Random

/********************************************************************************************************
  * Formulae to increase and decrease the weight of the synapses.
  * The formulas are taken from: http://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=6033439
  *******************************************************************************************************/
object STDPormula {
  def increaseWeight(g: Float, alf_p: Float, g_min: Float, g_max: Float, beta: Float) : Float = {
    val dg = (alf_p * exp(-beta * ((g - g_min) / (g_max - g_min)))).toFloat
    min(g_max, g + max(dg, Float.MinPositiveValue))
  }

  def decreaseWeight(g: Float, alf_m: Float, g_min: Float, g_max: Float, beta: Float) : Float = {
    val dg = (alf_m * exp(-beta * ((g_max - g) / (g_max - g_min)))).toFloat
    max(g_min, g - max(dg, Float.MinPositiveValue))
  }

  // positive/negative version
  def increaseWeightWithNeg(g: Float, alf_p: Float, g_min: Float, g_max: Float, beta: Float) : Float = {
    val g_norm = (g+1f)/2f
    val dg = (alf_p * exp(-beta * ((g_norm - g_min) / (g_max - g_min)))).toFloat
    min(g_max, g_norm + max(dg, Float.MinPositiveValue))*2f-1f
  }

  def decreaseWeightWithNeg(g: Float, alf_m: Float, g_min: Float, g_max: Float, beta: Float) : Float = {
    val g_norm = (g+1f)/2f
    val dg = (alf_m * exp(-beta * ((g_max - g_norm) / (g_max - g_min)))).toFloat
    max(g_min, g_norm - max(dg, Float.MinPositiveValue))*2f-1f
  }
}

object QBGParameters {
  var g_max = 1.0f
  var g_min = 0.0001f
  var alf_p = 0.01f
  var alf_m = 0.005f
  var beta_p = 1.5f
  var beta_m = 2.5f

  // Weight initialize
  var init_mean = 0.5f
  var init_std = 0.25f

  var stdpWindow = 50 MilliSecond
}


/***********************************************************************************************************
 * 																				 SimplifiedSTDP
 ***********************************************************************************************************/
object SimplifiedSTDP extends SynapseBuilder {
  def apply() = new SimplifiedSTDPBuilder()
  def apply(stdpWindow: Time) = new SimplifiedSTDPBuilder(stdpWindow)
  def createSynapse: NeuronConnection = this().createSynapse
}

class SimplifiedSTDPBuilder(stdpWindow : Time = QBGParameters.stdpWindow) extends SynapseBuilder {
  override def createSynapse: NeuronConnection = new SimplifiedSTDP(stdpWindow)
}

class SimplifiedSTDP(stdpWindow : Time = QBGParameters.stdpWindow, initialWeight : Float = math.max(0f, math.min(1f, QBGParameters.init_mean + Random.nextGaussian().toFloat * QBGParameters.init_std))) extends FloatSynapse(initialWeight) {
  var lastPreSpikeTimestamp : Timestamp = Int.MinValue
  var lastPostSpikeTimestamp : Timestamp = Int.MinValue

  def processConnectionMessage(timestamp : Timestamp, message : Message) : Unit = message match {
    case ElectricSpike(charge) =>
      lastPreSpikeTimestamp = timestamp
      getEnds.sendToOutput(timestamp, ElectricSpike(charge*getWeight))
    case BackwardSpike =>
      setWeight(
        if(lastPreSpikeTimestamp < timestamp - stdpWindow.timestamp) {
          STDPormula.decreaseWeight(getWeight, QBGParameters.alf_m, QBGParameters.g_min, QBGParameters.g_max, QBGParameters.beta_m)
        }
        else {
          STDPormula.increaseWeight(getWeight, QBGParameters.alf_p, QBGParameters.g_min, QBGParameters.g_max, QBGParameters.beta_p)
        }
      )
      lastPostSpikeTimestamp = timestamp
  }
}

/***********************************************************************************************************
 *  														SimplifiedSTDPWithNegative
 ***********************************************************************************************************/
object SimplifiedSTDPWithNegative extends SynapseBuilder {
  def apply() = new SimplifiedSTDPWithNegativeBuilder()
  def apply(stdpWindow: Time) = new SimplifiedSTDPWithNegativeBuilder(stdpWindow)
  def createSynapse: NeuronConnection = this().createSynapse
}

class SimplifiedSTDPWithNegativeBuilder(stdpWindow : Time = QBGParameters.stdpWindow) extends SynapseBuilder {
  override def createSynapse: NeuronConnection = new SimplifiedSTDPWithNegative(stdpWindow)
}

class SimplifiedSTDPWithNegative(stdpWindow : Time = QBGParameters.stdpWindow, weight : Float = math.max(0f, math.min(1f, QBGParameters.init_mean+Random.nextGaussian().toFloat*QBGParameters.init_std))) extends FloatSynapse(weight) {
  var lastPreSpikeTimestamp : Timestamp = Long.MinValue

  def processConnectionMessage(timestamp : Timestamp, message : Message) : Unit = message match {
    case ElectricSpike(charge) =>
      lastPreSpikeTimestamp = timestamp
      getEnds.sendToOutput(timestamp, ElectricSpike(charge*getWeight))
    case BackwardSpike =>
      setWeight(
        if(lastPreSpikeTimestamp < timestamp - stdpWindow.timestamp) {
          STDPormula.decreaseWeightWithNeg(getWeight, QBGParameters.alf_m, QBGParameters.g_min, QBGParameters.g_max, QBGParameters.beta_m)
        }
        else {
          STDPormula.increaseWeightWithNeg(getWeight, QBGParameters.alf_p, QBGParameters.g_min, QBGParameters.g_max, QBGParameters.beta_p)
        }
      )
  }
}