package fr.univ_lille.cristal.emeraude.n2s3.features.logging.graph

import java.io.{File, PrintWriter}

import fr.univ_lille.cristal.emeraude.n2s3.core.NetworkEntityPath
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.Time

/**
  * Created by falezp on 16/11/16.
  */

object NeuronDependenceTikz {

  val color = Seq("red", "blue", "orange", "green", "Purple", "Yellow", "RawSienna", "Aquamarine", "Periwinkle", "Peach", "Goldenrod", "Rhodamine")

  case class Data(from : (NetworkEntityPath, Timestamp), to : (NetworkEntityPath, Timestamp), weight : Float)

  def formatName(path : NetworkEntityPath) : String = {
    "[:*/]".r.replaceAllIn(path.toString, "")
  }

  def export(filename : String, title : Option[String], neurons : Seq[Seq[NetworkEntityPath]], data : Seq[Data], maxDelay : Time, additionalFires : Seq[(NetworkEntityPath, Timestamp)]) = {
    assert(neurons.size <= color.size)


    val min_x = math.min(data.map(d => d.from._2).min, additionalFires.map(_._2).min)
    val max_x = math.max(data.map(d => d.to._2).max, additionalFires.map(_._2).max)

    val writer = new PrintWriter(new File(filename))

    writer.println("\\begin{tikzpicture}[")
    writer.println("\tinput_spike/.style={fill=red, line width=1pt, text width=0mm,inner sep=0pt, minimum height=8mm},")
    writer.println("\toutput_spike/.style={fill=black, line width=1pt, text width=0mm,inner sep=0pt, minimum height=4mm}")
    writer.println("]")

    writer.println("\\begin{axis}[")
    if(title.isDefined)
      writer.println("\ttitle={"+title.get+"},")
    writer.println("\twidth=0.95\\textwidth,")
    writer.println("\theight=0.95\\textheight,")
    writer.println("\tymajorgrids=true,")
    writer.println("\tpoint meta=explicit,")
    writer.println("\txlabel=Time (ms),")
    writer.println("\txmin = 0,")
    writer.println("\txmax = "+((max_x-min_x).toDouble/1e3)+",")
    writer.println("\tymin="+formatName(neurons.head.head)+",")
    writer.println("\tymax="+formatName(neurons.last.last)+",")
    writer.println("\tenlarge x limits=0.01,")
    writer.println("\tenlarge y limits=0.03,")
    writer.println("\tytick={"+neurons.flatten.map(n => formatName(n)).mkString(",")+"},")
    writer.println("\tsymbolic y coords={minTick,"+neurons.flatten.map(n => formatName(n)).mkString(",")+",maxTick}")
    writer.println("]")

    neurons.zipWithIndex.zip(color).foreach { case((list, index), c) =>
      writer.println("\t\\fill [fill="+c+",opacity=0.2] " +
        "($(axis cs:0,"+formatName(list.head)+")!0.5!(axis cs:0,"+(if(index == 0) "minTick" else formatName(neurons(index-1).last))+")$)" +
        "rectangle " +
        "($(axis cs:"+((max_x-min_x).toDouble/1e3)+","+formatName(list.last)+")!0.5!(axis cs:"+((max_x-min_x).toDouble/1e3)+","+(if(index == neurons.size-1) "maxTick" else formatName(neurons(index+1).head))+")$);")
    }

    data.zipWithIndex.foreach{ case(d, index) =>

      writer.println("\t\\node[input_spike] (f"+index+") at (axis cs:"+((d.from._2-min_x).toDouble/1e3)+","+formatName(d.from._1)+"){};")
      writer.println("\t\\node[output_spike] (t"+index+") at (axis cs:"+((d.to._2-min_x).toDouble/1e3)+","+formatName(d.to._1)+"){};")
      writer.println("\t\\draw[->, dashed, "+(if(d.weight < 0) "red" else "black")+"](f"+index+") -- node [midway, above] {"+d.weight+"} (t"+index+");")
      writer.println("\t\\draw[ultra thick] (axis cs:"+math.max((d.from._2-min_x-maxDelay.timestamp).toDouble/1e3, 0.0)+","+formatName(d.from._1)+") -- (f"+index+");")
    }


    additionalFires.zipWithIndex.foreach { case((path, timestamp), i) =>
      writer.println("\t\\node[output_spike] (an"+i+") at (axis cs:"+((timestamp-min_x).toDouble/1e3)+","+formatName(path)+"){};")
      writer.println("\t\\draw[ultra thick] (axis cs:"+math.max((timestamp-min_x-maxDelay.timestamp).toDouble/1e3, 0.0)+","+formatName(path)+") -- (an"+i+");")
    }



    writer.println("\\end{axis}")
    writer.println("\\end{tikzpicture}")

    writer.close()
  }
}
