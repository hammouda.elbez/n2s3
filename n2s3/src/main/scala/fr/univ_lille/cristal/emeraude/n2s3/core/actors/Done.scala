package fr.univ_lille.cristal.emeraude.n2s3.core.actors

/*************************************************************************************************
 * Act like an asynchronous acknowledgment.
 * This message is send when a general action is terminated
 ************************************************************************************************/
case object Done extends SyncMessage
