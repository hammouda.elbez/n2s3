package fr.univ_lille.cristal.emeraude.n2s3.core.event

/***************************************************************************************************
 * Event triggered when update arise in synapse
 **************************************************************************************************/
abstract class SynapseUpdateEvent(val connectionId : Int) extends Event[EventTriggered.type]