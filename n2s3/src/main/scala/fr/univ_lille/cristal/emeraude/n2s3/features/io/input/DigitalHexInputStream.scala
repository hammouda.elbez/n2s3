package fr.univ_lille.cristal.emeraude.n2s3.features.io.input

/**
  * Created by falezp on 19/04/16.
  */

object InputDigitalHex extends InputFormat[InputSample2D[Float]](Shape(3, 5)) {

  // Stream data
  def Data() = new DigitalHexInputStream
}



class DigitalHexInputStream extends InputGenerator[InputSample2D[Float]] {


  val characters = List(
    List(
      List(1, 1, 1),
      List(1, 0, 1),
      List(1, 0, 1),
      List(1, 0, 1),
      List(1, 1, 1)
    ),
    List(
      List(0, 0, 1),
      List(0, 0, 1),
      List(0, 0, 1),
      List(0, 0, 1),
      List(0, 0, 1)
    ),
    List(
      List(1, 1, 1),
      List(0, 0, 1),
      List(1, 1, 1),
      List(1, 0, 0),
      List(1, 1, 1)
    ),
    List(
      List(1, 1, 1),
      List(0, 0, 1),
      List(1, 1, 1),
      List(0, 0, 1),
      List(1, 1, 1)
    ),
    List(
      List(1, 0, 1),
      List(1, 0, 1),
      List(1, 1, 1),
      List(0, 0, 1),
      List(0, 0, 1)
    ),
    List(
      List(1, 1, 1),
      List(1, 0, 0),
      List(1, 1, 1),
      List(0, 0, 1),
      List(1, 1, 1)
    ),
    List(
      List(1, 1, 1),
      List(1, 0, 0),
      List(1, 1, 1),
      List(1, 0, 1),
      List(1, 1, 1)
    ),
    List(
      List(1, 1, 1),
      List(0, 0, 1),
      List(0, 0, 1),
      List(0, 0, 1),
      List(0, 0, 1)
    ),
    List(
      List(1, 1, 1),
      List(1, 0, 1),
      List(1, 1, 1),
      List(1, 0, 1),
      List(1, 1, 1)
    ),
    List(
      List(1, 1, 1),
      List(1, 0, 1),
      List(1, 1, 1),
      List(0, 0, 1),
      List(1, 1, 1)
    ),
    List(
      List(1, 1, 1),
      List(1, 0, 1),
      List(1, 1, 1),
      List(1, 0, 1),
      List(1, 0, 1)
    ),
    List(
      List(1, 0, 0),
      List(1, 0, 0),
      List(1, 1, 1),
      List(1, 0, 1),
      List(1, 1, 1)
    ),
    List(
      List(1, 1, 1),
      List(1, 0, 0),
      List(1, 0, 0),
      List(1, 0, 0),
      List(1, 1, 1)
    ),
    List(
      List(0, 0, 1),
      List(0, 0, 1),
      List(1, 1, 1),
      List(1, 0, 1),
      List(1, 1, 1)
    ),
    List(
      List(1, 1, 1),
      List(1, 0, 0),
      List(1, 1, 0),
      List(1, 0, 0),
      List(1, 1, 1)
    ),
    List(
      List(1, 1, 1),
      List(1, 0, 0),
      List(1, 1, 0),
      List(1, 0, 0),
      List(1, 0, 0)
    )
  )

  val label = List("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F")

  var cursor = 0

  def atEnd() = cursor >= characters.size

  def next() : InputSample2D[Float] = {
    val next = new InputSample2D[Float](
      for(i <- 0 until 3) yield {
        for (j <- 0 until 5) yield characters(cursor)(j)(i).toFloat
      }
    ).setMetaData(InputLabel(label(cursor)))
    cursor += 1
    next
  }

  def reset() = {
    cursor = 0
  }

  def shape = InputDigitalHex.shape

  override def toString = "[DigitHax] "+cursor+" / "+characters.size
}
