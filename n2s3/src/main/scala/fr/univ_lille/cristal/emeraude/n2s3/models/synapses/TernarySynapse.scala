package fr.univ_lille.cristal.emeraude.n2s3.models.synapses

import fr.univ_lille.cristal.emeraude.n2s3.core.actors.{BackwardSpike, ElectricSpike}
import fr.univ_lille.cristal.emeraude.n2s3.core.models.synapse.FloatSynapse
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias._
import fr.univ_lille.cristal.emeraude.n2s3.support.UnitCast._
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message

import scala.util.Random

/**
  * Created by falezp on 30/06/16.
  */

class TernarySynapse extends FloatSynapse(0f) {

  private var v_exc = 0f
  private var v_inh = 0f

  private var exc_state = Random.nextFloat() < 0.4
  private var inh_state = Random.nextFloat() < 0.15 && !exc_state
  computeWeight()

  private var lastPreSpikeTimestamp : Timestamp = 0
  private var lastPostSpikeTimestamp : Timestamp = 0

  private val d_v_exc = 0.08f
  private val window_exc = 16.8 MilliSecond
  private val leak_exc = 1 Second
  private val d_v_inh = 0.85f*d_v_exc
  private val window_inh = 33 MilliSecond
  private val leak_inh =  1 Second

  private val v_th1 = 0.2f
  private val v_th2 = 0.6f

  private val forget_delay = 100 Second



  def processConnectionMessage(timestamp : Timestamp, message : Message) : Unit = message match {

    case ElectricSpike(charge) =>

      v_inh *= math.exp(-(timestamp - lastPreSpikeTimestamp).toDouble / leak_inh.timestamp.toDouble).toFloat

      /*if(/*lastPostSpikeTimestamp > lastPreSpikeTimestamp && */)*/
      v_inh += d_v_inh * math.exp(-(timestamp - lastPostSpikeTimestamp).toDouble / window_inh.timestamp.toDouble).toFloat*
        (if(timestamp-lastPostSpikeTimestamp <= window_inh.timestamp) 1 else -1)



      if (v_inh > v_th1 && v_inh <= v_th2) {
        inh_state = false
      }
      else if (v_inh > v_th2) {
        exc_state = false
        inh_state = true
      }

      computeWeight()
      lastPreSpikeTimestamp = timestamp

      getEnds.sendToOutput(timestamp, ElectricSpike(charge*getWeight))
    case BackwardSpike =>

      v_exc *= math.exp(-(timestamp - lastPostSpikeTimestamp).toDouble / leak_exc.timestamp.toDouble).toFloat
      /*if(/*lastPreSpikeTimestamp > lastPostSpikeTimestamp &&*/)*/
      v_exc += d_v_exc * math.exp(-(timestamp - lastPreSpikeTimestamp).toDouble / window_exc.timestamp.toDouble).toFloat*
        (if(timestamp-lastPreSpikeTimestamp <= window_exc.timestamp) 1 else -1)


      if(timestamp-lastPreSpikeTimestamp > forget_delay.timestamp) {
        exc_state = false
        inh_state = false
      }

      if(v_exc > v_th1 && v_exc <= v_th2) {
        inh_state = false
      }
      else if(v_exc > v_th2) {
        exc_state = true
        inh_state = false
      }
      computeWeight()

      lastPostSpikeTimestamp = timestamp
  }

  private def computeWeight() : Unit = {
    if(exc_state)
      setWeight(1f)
    else if(inh_state)
      setWeight(-1f)
    else
      setWeight(0f)
  }
}