package fr.univ_lille.cristal.emeraude.n2s3.core.event

/**
  * Default response when a event don't need specific response
  */
object EventTriggered extends EventResponse
