package fr.univ_lille.cristal.emeraude.n2s3.features.learning

import akka.actor.{ActorRef, Props}
import fr.univ_lille.cristal.emeraude.n2s3.core.Neuron.NeuronMessage
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.{Done, ResetState}
import fr.univ_lille.cristal.emeraude.n2s3.core.event._
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.SynapticWeightFloat
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.{N2S3, NeuronGroupObserverRef}
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.{LocalActorDeploymentStrategy, Message}

/**
  * Created by pfalez on 10/05/17.
  */

class ResetNeuronBetweenInputRef(n2s3 : N2S3, neurons : Seq[NetworkEntityPath], normalizeWeight : Boolean = false, normalizeTarget : Float = 0f) extends NeuronGroupObserverRef {
  var actors : Option[ActorRef] = None
  def getActors : Seq[ActorRef] = actors.toSeq


  override def deploy(n2s3: N2S3): Unit = {
    this.actors = Some(n2s3.system.actorOf(Props(new ResetNeuronBetweenInput(n2s3, neurons, normalizeWeight, normalizeTarget)), LocalActorDeploymentStrategy))
  }
}
class ResetNeuronBetweenInput(n2s3 : N2S3, neurons : Seq[NetworkEntityPath], normalizeWeight : Boolean, normalizeTarget : Float) extends NetworkActor {

  override def initialize(): Unit = {
    askTo(n2s3.inputLayerRef.get.getContainer, SubscribeSynchronized(LabelChangeEvent, ExternalSender.getReference(self), n2s3.buildProperties.getSynchronizerPolicy.getInputSynchronizer))
  }

  override def destroy(): Unit = {
    askTo(n2s3.inputLayerRef.get.getContainer, Unsubscribe(LabelChangeEvent, ExternalSender.getReference(self)))
  }

  override def process(message: Message, sender : ActorRef): Unit = message match {
    case SynchronizedEvent(s, _, m, _) =>
      m match {
        case LabelChangeResponse(start, end, label) =>

          neurons.foreach{ path =>
            ExternalSender.askTo(path, NeuronMessage(start, ResetState, null, null, None))

          }

          if(normalizeWeight) {
            val weights = neurons.map { path =>
              val inputs = ExternalSender.askTo(path, GetAllConnectionProperty(SynapticWeightFloat))
                .asInstanceOf[ConnectionPropertyValues[Float]].values.filter(_._3 > 0f)

              val maxInput = inputs.map(_._3).max
              val normalizedInputs = inputs.map{case(id, _, value) => id -> value/maxInput}

              path -> normalizedInputs
            }.toMap

            val weightsSum = weights.map{ case(neuron, list) =>
              neuron -> list.map(_._2).sum
            }

            val minWeightsSum = weightsSum.map(_._2).min

            weights.foreach { case(neuron, list) =>
              val factor = minWeightsSum/weightsSum(neuron)

              val newWeights = list.map{ case(id, value) =>
                id -> value*factor
              }
              //println(factor+" -> "+newWeights.map(_._2).sum+" ("+newWeights.map(_._2).min+" - "+newWeights.map(_._2).max+")")
              ExternalSender.askTo(neuron, SetAllConnectionProperty(SynapticWeightFloat, newWeights))
            }



          }
      }
      ExternalSender.sendTo(s, Done)
  }

}