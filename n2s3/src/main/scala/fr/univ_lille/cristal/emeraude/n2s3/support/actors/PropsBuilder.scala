/**************************************************************************************************
 * Contributors:
 * 		- created by guillermo.polito@univ-lille.1
 *    - (add your name here if you contribute to this code).
 **************************************************************************************************/
package fr.univ_lille.cristal.emeraude.n2s3.support.actors

import akka.actor.Props

abstract class PropsBuilder {
  def build() : Props
}
