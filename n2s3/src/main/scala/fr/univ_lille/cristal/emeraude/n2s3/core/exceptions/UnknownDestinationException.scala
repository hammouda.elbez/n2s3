package fr.univ_lille.cristal.emeraude.n2s3.core.exceptions

import fr.univ_lille.cristal.emeraude.n2s3.core.NetworkEntityPath
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message

/**
  * Created by falezp on 02/06/16.
  */

/**
  *  Exception throw when a message can't be deliver to a a NetworkEntityPath
  *
  * @param message is the content
  * @param path is the target
  */
class UnknownDestinationException(val message: Message, val path: NetworkEntityPath) extends RuntimeException("Unknown path \""+
  path.local.foldLeft(if(path.actor != null) path.actor.path.name+":" else "")((acc, curr) => acc+"/"+curr.toString)+"\" with message "+message) {
}