package fr.univ_lille.cristal.emeraude.n2s3.features.logging.graph

import java.awt._
import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO
import javax.swing.{JFrame, JPanel, SwingUtilities}

import akka.actor.{ActorRef, Props}
import akka.util.Timeout
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.Config
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.SynapticWeightFloat
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.{N2S3, NeuronGroupObserverRef}
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.{LocalActorDeploymentStrategy, Message}

import scala.collection.mutable

/**
  * Created by falezp on 07/03/17.
  */

object ReconstructReceptiveField {

  def of(path : NetworkEntityPath, coordMapper : Map[NetworkEntityPath, (Float, Float)], g : Graphics, radius : Float, oversampling : Float): Rectangle = {
    val neuronValue = mutable.HashMap[NetworkEntityPath, Float]()
    val neuronQueue = mutable.Queue[(NetworkEntityPath, Float, Boolean)]()

    neuronQueue += ((path, 1f, true))

    while(neuronQueue.nonEmpty) {
      val (currNeuron, currValue, start) = neuronQueue.dequeue()

      neuronValue.update(currNeuron, neuronValue.getOrElse(currNeuron, 0f) + currValue)
      ExternalSender.askTo(currNeuron, GetAllConnectionProperty(SynapticWeightFloat))
          .asInstanceOf[ConnectionPropertyValues[Float]].values.foreach { case (connectionId, inputNeuron, weight) =>
        if (math.abs(weight * currValue) > 0.01 && !neuronValue.contains(inputNeuron))
          if(start || weight > 0)
            neuronQueue.enqueue((inputNeuron, weight * currValue / 2f, false/*weight > 0 && start*/))
      }
    }

    val minX = math.floor(coordMapper.map(_._2._1-radius).min*oversampling).toInt
    val minY = math.floor(coordMapper.map(_._2._2-radius).min*oversampling).toInt
    val maxX = math.ceil(coordMapper.map(_._2._1+radius).max*oversampling).toInt
    val maxY = math.ceil(coordMapper.map(_._2._2+radius).max*oversampling).toInt

    g.setColor(Color.WHITE)
    g.fillRect(0, 0, maxX-minX, maxY-minY)

    val minV = neuronValue.values.min
    val maxV = neuronValue.values.max

    neuronValue.foreach{ case(p, value) =>
      if(coordMapper.isDefinedAt(p)) {
        val (x, y) = coordMapper(p)
        g.setColor(new Color(if(value < 0f) 1f else 0f, if(value >= 0f) 1f else 0f, 0f, if(value < 0f) math.min(1f, -value) else if(value > 0f) math.min(1f, value) else 0f))
        g.fillOval(-minX+math.round((x-radius)*oversampling).toInt, -minY+math.round((y-radius)*oversampling).toInt, math.round(2f*radius*oversampling).toInt, math.round(2f*radius*oversampling).toInt)
      }
    }

    new Rectangle(minX, minY, maxX-minX, maxY-minY)

  }

  def saveTo(path : NetworkEntityPath, coordMapper : Map[NetworkEntityPath, (Float, Float)], filename : String): Unit = {
    val radius = 0.5f
    val oversampling = 100f
    val minX = math.floor(coordMapper.map(_._2._1-radius).min*oversampling).toInt
    val minY = math.floor(coordMapper.map(_._2._2-radius).min*oversampling).toInt
    val maxX = math.ceil(coordMapper.map(_._2._1+radius).max*oversampling).toInt
    val maxY = math.ceil(coordMapper.map(_._2._2+radius).max*oversampling).toInt
    val img = new BufferedImage(maxX-minX, maxY-minY, BufferedImage.TYPE_INT_RGB)
    of(path, coordMapper, img.createGraphics(), radius, oversampling)

    ImageIO.write(img, "png", new File(filename))
  }
}

class ReconstructReceptiveFieldGraphRef(views : Seq[NetworkEntityPath], coordMapper : Map[NetworkEntityPath, (Float, Float)], cellRadius : Float, oversampling : Float) extends NeuronGroupObserverRef {
  var actor: Option[ActorRef] = None
  def getActors : Seq[ActorRef] = this.actor.toSeq


  override def deploy(n2s3: N2S3): Unit = {
    this.actor = Some(n2s3.system.actorOf(Props(new ReconstructReceptiveFieldGraph(views, coordMapper, cellRadius, oversampling)), LocalActorDeploymentStrategy))
  }
}

class ReconstructReceptiveFieldGraph(targets : Seq[NetworkEntityPath], coordMapper : Map[NetworkEntityPath, (Float, Float)], cellRadius : Float, oversampling : Float, refreshRate : Int = 1000/24) extends AutoRefreshNetworkActor(refreshRate, targets.size) {

  implicit val timeout : Timeout = Config.defaultTimeout

  val views = mutable.Map[NetworkEntityPath, ReconstructReceptiveFieldPanel]()
  var frame : Container = null

  override def update(n : Int): Unit = {
    views.toSeq(n) match { case (path, view) =>
      view.repaint()
    }
  }

  override def initialize(): Unit = {
    SwingUtilities.invokeAndWait(new Runnable() {
      def run() {

        val minX = math.floor(coordMapper.map(_._2._1-cellRadius).min*oversampling).toInt
        val minY = math.floor(coordMapper.map(_._2._2-cellRadius).min*oversampling).toInt
        val maxX = math.ceil(coordMapper.map(_._2._1+cellRadius).max*oversampling).toInt
        val maxY = math.ceil(coordMapper.map(_._2._2+cellRadius).max*oversampling).toInt

        if(frame == null){
          frame = new JFrame()
          frame.setLayout(new FlowLayout)
        }

        targets.foreach { path =>
          val panel = new ReconstructReceptiveFieldPanel(path, coordMapper, cellRadius, oversampling)
          panel.setPreferredSize(new Dimension(maxX-minX, maxY-minY))
          views += path -> panel
          frame.add(panel)
        }

        frame.revalidate()
        frame.setVisible(true)
        frame.setSize(new Dimension(800, 1000))
      }
    }
    )

  }

  override def destroy(): Unit = {
    views.values.foreach(v => v.destroy())
  }

  override def process(message: Message, sender : ActorRef): Unit = {

  }
}

class ReconstructReceptiveFieldPanel(path : NetworkEntityPath, coordMapper : Map[NetworkEntityPath, (Float, Float)], cellRadius : Float, oversampling : Float) extends JPanel {

  override def paintComponent(g : Graphics): Unit = {
    val rect = ReconstructReceptiveField.of(path, coordMapper, g, cellRadius, oversampling)
  }

  def destroy() : Unit = {

  }

}