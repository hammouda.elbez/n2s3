package fr.univ_lille.cristal.emeraude.n2s3.core.models.properties

import fr.univ_lille.cristal.emeraude.n2s3.core.Property

/**
  * Created by pfalez on 13/01/18.
  */

object InhibitionRatio extends Property[Float]