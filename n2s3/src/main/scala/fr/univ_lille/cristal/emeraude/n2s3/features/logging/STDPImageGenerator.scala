package fr.univ_lille.cristal.emeraude.n2s3.features.logging

import java.awt.image.BufferedImage
import java.io.File
import java.nio.file.{Files, Paths}
import javax.imageio.ImageIO

/**
  * Created by falezp on 09/06/16.
  */
object STDPImageGenerator {


  def save(prefix : String, values : Traversable[Boolean], width : Int, pixelSize : Int = 1) : Unit = {

    val imageWidth = math.min(width, values.size)
    val imageHeight = math.ceil(values.size.toFloat/width.toFloat).toInt

    val image = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_INT_RGB)

    values.toSeq.zipWithIndex.foreach { case (v, neuronIndex) =>
      val x = (neuronIndex % width) * pixelSize
      val y = (neuronIndex / width) * pixelSize

      image.setRGB(x, y, if(v) 0xFFFFFF else 0x0)
    }


    var id = 0
    while(Files.exists(Paths.get(prefix+"_"+id)))
      id += 1

    ImageIO.write(image, "png", new File(prefix+"_"+id))

  }
}
