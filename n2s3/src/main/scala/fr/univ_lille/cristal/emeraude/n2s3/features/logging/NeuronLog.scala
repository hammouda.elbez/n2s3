package fr.univ_lille.cristal.emeraude.n2s3.features.logging

import java.io.{File, PrintWriter}

import akka.actor.{Actor, ActorRef, Props}
import akka.util.Timeout
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.{Done, Initialize}
import fr.univ_lille.cristal.emeraude.n2s3.core.event._
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.{N2S3, NeuronGroupObserverRef, NeuronGroupRef}
import fr.univ_lille.cristal.emeraude.n2s3.features.logging.graph.{InputOutputGraph, NeuronsGraph, ValuesGraph}
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.Time
import fr.univ_lille.cristal.emeraude.n2s3.support.UnitTime.{TimeUnitType, _}
import fr.univ_lille.cristal.emeraude.n2s3.support.actors._

import scala.concurrent.duration._



object NeuronsFireLogText extends ActorCompanion {
  override def newPropsBuilder(): PropsBuilder = new SinglePropsBuilder[NeuronsFireLogText]

  def LogNeuronsInFile(filename : String, views : NetworkEntityPath*) = new NeuronsFireLogTextRef(filename, views)
  def LogNeuronGroupsInFile(filename : String, views : NeuronGroupRef*) = new NeuronsFireLogTextRef(filename, views.flatMap(_.neuronPaths))
}



class NeuronsFireLogTextRef(filename : String, views : Seq[NetworkEntityPath]) extends NeuronGroupObserverRef {


  var actor: Option[ActorRef] = None
  def getActors : Seq[ActorRef] = this.actor.toSeq


  override def deploy(n2s3: N2S3): Unit = {
    this.actor = Some(n2s3.system.actorOf(Props(new NeuronsFireLogText(filename)), LocalActorDeploymentStrategy))

    views.foreach{ path =>
      ExternalSender.askTo(path, Subscribe(NeuronFireEvent, ExternalSender.getReference(this.actor.get)))
    }
  }
}

/**
 * Will monitor neurons which are firing in the file output/log/neuronfire.log
 */
class NeuronsFireLogText(filename : String) extends NetworkActor {
  val fos = new PrintWriter(new File(filename))

  override def initialize(): Unit = {}

  override def destroy(): Unit = {
    fos.close()
  }

  override def process(message: Message, sender : ActorRef): Unit = message match {
    case NeuronFireResponse(timestamp, path) =>
      fos.println(path + "\t" + timestamp )
    case InputRestartResponse(source) =>
      fos.println(source.path.name + " restart")
    case m => println("[NeuronsFireLogText] Unknown message "+m)
  }
}

/**
 * Will monitor potentials of neurons in the file output/log/neuron.log
 */

class NeuronInputOutputGraph(layer : Seq[(String, Int, Seq[NetworkEntityPath], Int)]) extends Actor {

  object ProcessDisplay
  
  implicit val timeout = Timeout(1000 seconds)
  
  override def postRestart(t : Throwable) = {System.exit(0)}
  
  var graph : InputOutputGraph =  new InputOutputGraph(layer)
  var hasRequestDisplay = false

  layer.foreach{ case (_, _, list, _) => list.foreach{ path => ExternalSender.sendTo(path,  Subscribe(NeuronFireEvent, ExternalSender.getReference(self))) }}

  def receive = {
    case NeuronFireResponse(timestamp, path) =>
      graph.event(timestamp, path)
      if(!hasRequestDisplay) {
        self ! ProcessDisplay
        hasRequestDisplay = true
      }

    case ProcessDisplay =>
      hasRequestDisplay = false

    case Initialize =>
      sender ! Done
  }
}

/**
 * Will monitor neurons which are firing during the simulation
 *
 * @param step : the difference in time between two points
 * @param unit : the unit of Time used (Second, MilliSecond, MicroSecond)
 */
class NeuronsFireLogGraph(step: Int = 50, unit: TimeUnitType = MilliSecond) extends Actor {
  var lastT = 0L
  val l = new scala.collection.mutable.ListBuffer[NetworkEntityPath]()
  val graph = new NeuronsGraph
  def receive = {
    case NeuronFireResponse(timestamp, path) =>
      if (!l.contains(path))
        l += path
      if (timestamp - lastT > step) {
        graph.refreshSerie(convert(unit, Time(timestamp)), l.indexOf(path))
        if (!(graph isRunning()))
          graph.launch()
        lastT = timestamp
      }
  }
}

object NeuronsPotentialLogText extends ActorCompanion {
  override def newPropsBuilder(): PropsBuilder = new SinglePropsBuilder[NeuronsPotentialLogText]

  def LogNeuronsInFile(filename : String, views : NetworkEntityPath*) = new NeuronsPotentialLogTextRef(filename, views)
  def LogNeuronGroupsInFile(filename : String, views : NeuronGroupRef*) = new NeuronsPotentialLogTextRef(filename, views.flatMap(_.neuronPaths))
}



class NeuronsPotentialLogTextRef(filename : String, views : Seq[NetworkEntityPath]) extends NeuronGroupObserverRef {


  var actor: Option[ActorRef] = None
  def getActors : Seq[ActorRef] = this.actor.toSeq


  override def deploy(n2s3: N2S3): Unit = {
    this.actor = Some(n2s3.system.actorOf(Props(new NeuronsPotentialLogText(filename)), LocalActorDeploymentStrategy))

    views.foreach{ path =>
      ExternalSender.askTo(path, Subscribe(NeuronPotentialUpdateEvent, ExternalSender.getReference(this.actor.get)))
    }
  }
}

class NeuronsPotentialLogText(filename : String, startTimestamp : Timestamp = 0) extends NetworkActor {
  val fos = new PrintWriter(new File(filename))

  override def initialize(): Unit = {}

  override def destroy(): Unit = {
    fos.close()
  }


  override def process(message: Message, sender : ActorRef): Unit = message match {
    case NeuronPotentialResponse(timestamp, source, value) =>
      fos.println(source+"\t"+(timestamp-startTimestamp)+"\t"+value)
      println(source+"\t"+(timestamp-startTimestamp)+"\t"+value)
      fos.flush()
    case m => println("[NeuronsPotentialLogText] Unknown message "+m)
  }
}

/**
 * Will monitor potentials of neurons during the simulation
 *
 * @param step the difference in time between two points
 * @param unit the unit of Time used (Second, MilliSecond, MicroSecond)
 */
class NeuronsPotentialsLogGraph(step: Int = 50, unit: TimeUnitType = MilliSecond) extends Actor {
  val graph = new ValuesGraph
  var lastT = 0L
  def receive = {
    case NeuronPotentialResponse(timestamp, source, value) => {
      if (timestamp - lastT > step) {
        graph.refreshSerie(source.toString, convert(unit, Time(timestamp)), value)
        if (!(graph isRunning()))
          graph.launch()
        lastT = timestamp
      }
    }
  }
}
