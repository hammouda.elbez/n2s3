package fr.univ_lille.cristal.emeraude.n2s3.features.io.input

import java.io.{EOFException, File, IOException}

import fr.univ_lille.cristal.emeraude.n2s3.core.actors.ElectricSpike
import fr.univ_lille.cristal.emeraude.n2s3.support.FormatTimestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import net.sf.jaer.aemonitor.AEPacketRaw
import net.sf.jaer.eventio.AEFileInputStream

/**
  * Created by falezp on 09/03/16.
  */

object InputAER extends InputFormat[InputTemporalPacket](Shape(Int.MaxValue)) {

  // Stream transformers
  def Retina(width : Int, height : Int, separateSign : Boolean) = new AERRetina(width, height, separateSign)
  def Cochlea(length : Int) = new AERCochlea(length)

  // Stream data
  def DataFromFile(filename: String, chunkSize : Int = 64) = new AERInputStream(filename, chunkSize)
}


abstract class AERFormat extends StreamConverter[InputTemporalPacket, InputTemporalPacket] {

  def transformAddress(address : Int) : Seq[Int]

  def dataConverter(in: InputTemporalPacket): InputTemporalPacket = {
    in.mapUnitDataWithShape(shape, (index, data) =>
      transformAddress(index.head) -> N2S3InputSpike(ElectricSpike(), data.getStartTimestamp)
    )
  }

  def resetConverter() : Unit = {

  }
}

class AERRetina(width : Int, height : Int, separateSign : Boolean) extends AERFormat {

  def transformAddress(address : Int) : Seq[Int] = {
    val x = (width-1)-((address >> 1) & (width-1))
    val y = (height-1)-((address >> 8) & (height-1))

    val sign = address & 0x1
    if(separateSign){
      Seq(x, y, sign)
    } else {
      Seq(x, y)
    }
  }

  override def shapeConverter(shape : Shape) : Shape = {
    if (separateSign) {
      Shape(width, height, 2)
    } else {
      Shape(width, height)
    }
  }
}

class AERCochlea(length : Int) extends AERFormat {

  def transformAddress(address : Int) : Seq[Int] = {
    Seq(address)
  }

  override def shapeConverter(shape : Shape) : Shape = {
    assert(shape.dimensionNumber == 1)
    Shape(length)
  }
}

case class AERInput(t: Timestamp) extends InputInstantaneousData(t)

/*object FreeWay {
  def input(filename: String, enableSpikeSign: Boolean = false) = {
    new AERInputStream("data/aerdata/freeway.mat.dat")
      .groupBy(x => x.timestamp)
      .map(events => events.map{ event => Input(ShapelessSpike, event.timestamp, AERRetinaInputTransformation.transformAddress(event.destinationIndex, enableSpikeSign)) })
      .repeat(8)
  }
}*/

/**
 * Reads AER files using jAER (jar file in lib directory).
 * All the methods return an IndexedSeq[Event]. The timestamps are
 * normalized to start at 0.
 *
 * @author boulet
 * @param filename: String, the name of the file to read from
 */
//TODO expliquer la relation entre AERInputStream AERInput

class AERInputStream(filename: String, chunkSize : Int = 64) extends InputGenerator[InputTemporalPacket] {

  private val aeis = new AEFileInputStream(new File(filename))
  val startTime : Timestamp = aeis.getFirstTimestamp.toLong
  var peekEvent: Option[Seq[(Int, AERInput)]] = None

  this.stageStrategy = new EndStageOnStreamEnd

  var currentTimestamp = 0L
  val lastTimestamp : Timestamp = aeis.getLastTimestamp.toLong-startTime

  aeis.mark()

  /**
   * reset event reader
   */
  override def reset() : Unit = {
    this.stageStrategy.streamFinished()
    this.peekEvent = None
    aeis.rewind() }

  /**
   * @param p: AEPacketRaw, the packet of raw events read from the file
   * @return an IndexedSeq of Events
   */
  private def eventsFromAEPacket(p: AEPacketRaw) : Seq[(Int, AERInput)] = {
    for {
      i <- 0 until p.getNumEvents
      e = (p.getAddresses()(i), AERInput(p.getTimestamps()(i) - startTime)) // because getEvent always points to the same memory area
    } yield e
  }


  /**
   * @param n, the number of events to read
   * @return sequence of events
   */
  private def readEventsByNumber(n: Int) = {
    eventsFromAEPacket(aeis.readPacketByNumber(n))
  }

  /**
   * @param dt, the time interval in µs to read events from the last read event
   * @return sequence of events
   */
  def readEventsByTime(dt: Int) = {
    eventsFromAEPacket(aeis.readPacketByTime(dt))
  }

  /**
   * @return Sequence of all the events in the file.
   */
  def readAllEvents() = eventsFromAEPacket(aeis.readPacketByNumber(aeis.size().toInt))

  override def shape: Shape = Shape(Int.MaxValue)
  override def next() = {
    val list = this.innerNext(chunkSize)

    if(list.nonEmpty)
      currentTimestamp = list.last._2.getStartTimestamp
    else
      throw new IOException("End of AER file reached")

    new InputTemporalPacket(shape, list.groupBy(_._1).map(e=>Seq(e._1)->e._2.map(_._2)))
  }

  def innerNext(n: Int): Seq[(Int, AERInput)] = {
    if (aeis.position() >= aeis.size() && this.peekEvent.isEmpty){
      throw new EOFException()
    }

    var list = Seq[(Int, AERInput)]()

    while(list.size < n && !atEnd()) {
      list = list++this.basicNext(n)
    }


    if(list.size > n) {
      val split = list.splitAt(n)
      this.peekEvent = Some(split._2)
      list = split._1

    }
    list
  }

  private def basicNext(n: Int) : Seq[(Int, AERInput)] = {
    var list : Seq[(Int, AERInput)] = if (this.peekEvent.nonEmpty){
      val first = this.peekEvent.get
      this.peekEvent = None
      first
    } else {
      Seq()
    }

    try {
        list = list++eventsFromAEPacket(aeis.readPacketByNumber(math.min(n, (size - aeis.position()).toInt)))
    } catch {
      case e : EOFException =>
        if(list.isEmpty)
          throw new RuntimeException("empty next()")
    }

    list
  }

  private def peek() = {
    if (this.peekEvent.isEmpty && aeis.position() < size){
      try{
        val read = eventsFromAEPacket(aeis.readPacketByNumber(1))
        if (read.nonEmpty){
          this.peekEvent = Some(read)
        }
      } catch {
        case e: EOFException =>
      }
    }
    this.peekEvent
  }

  /**
    * return if we reach the end of the file
    */
  override def atEnd(): Boolean = aeis.position() >= size && this.peekEvent.isEmpty

  def size : Int = aeis.size().toInt-1

  override def toString: String = "[AER] "+filename+" : "+FormatTimestamp.format(currentTimestamp)+" / "+FormatTimestamp.format(lastTimestamp) + " (" + aeis.position() + "/" + aeis.size() +" events)"
}