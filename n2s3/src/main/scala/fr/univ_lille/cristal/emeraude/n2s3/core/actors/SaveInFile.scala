package fr.univ_lille.cristal.emeraude.n2s3.core.actors

import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message

/**
  * Created by guille on 10/14/16.
  */
case class SaveInFile(path : String) extends Message
