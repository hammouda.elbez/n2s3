package fr.univ_lille.cristal.emeraude.n2s3.core.models.properties

import fr.univ_lille.cristal.emeraude.n2s3.core.ConnectionProperty
import fr.univ_lille.cristal.emeraude.n2s3.support.Time

/**
  * Created by pfalez on 24/05/17.
  */
object SynapseWeightAndDelay extends ConnectionProperty[(Float, Time)]
