/************************************************************************************************************
 * Contributors: 
 * 		- created by Pierre Falez on 10/05/16.
 ***********************************************************************************************************/
package fr.univ_lille.cristal.emeraude.n2s3.core

import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message

object GetGlobalTimestamp extends Message

class GlobalTimestamp {
	var timestamp : Timestamp = 0
}

trait TimestampGenerator {
	var currentMaxTimestamp : Timestamp = 0

			def getGlobalTimestamp : GlobalTimestamp

			def toGlobalTimestamp(rawTimestamp : Timestamp) : Timestamp = {
				currentMaxTimestamp = math.max(rawTimestamp, currentMaxTimestamp)
				getGlobalTimestamp.timestamp+rawTimestamp
		}

def endOfStage() : Unit = {
		getGlobalTimestamp.timestamp += currentMaxTimestamp
				currentMaxTimestamp = 0
}
}