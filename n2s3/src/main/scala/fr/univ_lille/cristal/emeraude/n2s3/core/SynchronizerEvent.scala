package fr.univ_lille.cristal.emeraude.n2s3.core

import akka.actor.Props
import fr.univ_lille.cristal.emeraude.n2s3.core.Synchronizer.SynchronizedMessage
import fr.univ_lille.cristal.emeraude.n2s3.core.SynchronizerEvent.{AskEvents, ResponseEvents}
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.NetworkEntityActor
import fr.univ_lille.cristal.emeraude.n2s3.core.event.EventResponse
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.{ActorCompanion, Message, PropsBuilder}

import scala.collection.mutable

/**
  * Created by pfalez on 28/03/17.
  */
object SynchronizerEvent {
  case class AskEvents(until : Timestamp) extends Message
  case class ResponseEvents(events : Seq[EventResponse]) extends Message
}

class SynchronizerEvent extends NetworkEntity{

  val queue : mutable.HashMap[NetworkEntityReference, mutable.ArrayBuffer[SynchronizedMessage]] = mutable.HashMap()
  var currentTimestamp : Timestamp = 0L

  def enqueue(message: SynchronizedMessage) : Unit = {
    if(message.timestamp < currentTimestamp){
      throw new RuntimeException("[SynchronizerEvent] causality error : messageTimestamp="+message.timestamp+" < globalTime="+currentTimestamp+"\n")
    }
    queue.getOrElseUpdate(message.postSync, mutable.ArrayBuffer[SynchronizedMessage]()) += message

    val n = queue.foldLeft(0){case(acc, curr) => acc+curr._2.size}
    if(n%1000==0)
    println(queue.size+" "+n)
  }

  def dequeue(sender: NetworkEntityReference, timestamp: Timestamp) : Unit = {
    val (valid, invalid) = queue.getOrElseUpdate(sender, mutable.ArrayBuffer[SynchronizedMessage]()).partition(_.timestamp <= timestamp)

    queue(sender).clear()
    queue(sender) ++= invalid

    sender.send(ResponseEvents(valid.map(_.message.asInstanceOf[EventResponse])))
  }

  /**
    * Process a message not handled by ReferenceableNetworkEntity
    *
    */
  object RefreshMessage extends Message
  var counter = 0
  var refreshCounter = 0
  override def receiveMessage(message: Message, sender: NetworkEntityReference): Unit = message match {
    //case PullUntil(timestamp) =>..

    case message : SynchronizedMessage =>
      if(counter==0)
        getReferenceOf(getNetworkAddress).send(RefreshMessage)
      counter += 1
      if(counter%1000==0)
        println(counter)
      //enqueue(message)
    case AskEvents(timestamp) =>
      dequeue(sender, timestamp)
    case RefreshMessage =>
      refreshCounter += 1

      if(refreshCounter%1000==0)
      println("refresh "+refreshCounter)

      getReferenceOf(getNetworkAddress).send(RefreshMessage)
    case m => throw new RuntimeException("[SynchronizerEvent] Unknown message : \""+m+"\"")
  }
}

/**
  * SynchronizerActor companion object
  */
object SynchronizerEventActor extends ActorCompanion {
  override def newPropsBuilder() = new PropsBuilder{
    def build() : Props = Props(new SynchronizerEventActor)
  }
}

/**
  * mixin of Synchronizer with NetworkEntityActor
  */
class SynchronizerEventActor extends NetworkEntityActor(new SynchronizerEvent)