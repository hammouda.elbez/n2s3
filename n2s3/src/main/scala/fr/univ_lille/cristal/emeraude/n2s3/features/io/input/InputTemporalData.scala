package fr.univ_lille.cristal.emeraude.n2s3.features.io.input

import fr.univ_lille.cristal.emeraude.n2s3.support.FormatTimestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message

/**
  * Created by falezp on 06/02/17.
  */
abstract class InputTemporalData {
  def getStartTimestamp : Timestamp
  def getEndTimestamp : Timestamp
  final def getDuration : Timestamp = getEndTimestamp-getStartTimestamp

  def modifyTimestamp(delta : Timestamp) : Unit
  def setTimestamp(t : Timestamp) : Unit

  override def toString: String = "InputTemporalData("+FormatTimestamp.format(getStartTimestamp)+" -> "+FormatTimestamp.format(getEndTimestamp)+" = "+FormatTimestamp.format(getDuration)+")"
}

abstract class InputInstantaneousData(t : Timestamp) extends InputTemporalData {

  protected var timestamp : Timestamp = t

  def getStartTimestamp : Timestamp = timestamp
  def getEndTimestamp : Timestamp = timestamp

  def modifyTimestamp(delta : Timestamp) : Unit = {
    timestamp += delta
  }

  def setTimestamp(t : Timestamp) : Unit = {
    timestamp = t
  }

  override def toString: String = "InputInstantaneousData("+FormatTimestamp.format(timestamp)+")"
}

abstract class InputDurationData(start : Timestamp, end : Timestamp) extends InputTemporalData {

  protected var startTimestamp : Timestamp = start
  protected var endTimestamp : Timestamp = end

  assert(start <= end)

  def getStartTimestamp : Timestamp = startTimestamp
  def getEndTimestamp : Timestamp = endTimestamp

  def modifyTimestamp(delta : Timestamp) : Unit = {
    startTimestamp += delta
    endTimestamp += delta
  }

  def setTimestamp(t : Timestamp) : Unit = {
    modifyTimestamp(t-startTimestamp)
  }
}

trait InputTemporalMetaData extends InputTemporalData

/**
  * index = empty list -> extra channel
  * @param s
  * @param d
  * @param start
  * @param duration
  */

abstract class AbstractInputTemporalPacket[T <: InputTemporalData](s : Shape, d : Map[Seq[Int], Seq[T]], start : Timestamp = -1L, duration : Timestamp = -1L)
  extends InputSeqDataPacket {

  override type InputUnitDataType = T

  var data : Map[Seq[Int], InputDataType] = d

  def this(data : Seq[T], start : Timestamp, duration : Timestamp) = {
    this(Shape(1), Map((Seq(0), data)),  start, duration)
  }

  def this(data : Seq[T]) = {
    this(Shape(1), Map((Seq(0), data)))
  }

  def withData(newData : Map[Seq[Int], InputDataType]) : this.type = {
    data = newData
    this
  }

  def checkShape() : Unit = {
    data.foreach{ case(index, _) =>
      if(!shape.belongTo(index:_*))
        throw new IndexOutOfBoundsException
    }
  }

  var (minDataTimestamp, maxDataTimestamp) = data.foldLeft((Long.MaxValue, 0L)) { case((currMin, currMax), (index, list)) =>
    (
      math.min(currMin, if(list.isEmpty) Long.MaxValue else list.map(_.getStartTimestamp).min),
      math.max(currMax, if(list.isEmpty) 0L else list.map(_.getEndTimestamp).max)
    )
  }

  var startTimestamp : Timestamp = if(start < 0L) minDataTimestamp else start
  var endTimestamp : Timestamp = if(duration < 0L) maxDataTimestamp else startTimestamp+duration

  def getAllData : Map[Seq[Int], InputDataType] = data

  def getDataOnChannel(index : Int*) : InputDataType = {
    data.getOrElse(index, Seq[T]())
  }


  def getStartTimestamp : Timestamp = startTimestamp
  def getEndTimestamp : Timestamp = endTimestamp
  def getDuration : Timestamp = endTimestamp-startTimestamp

  def modifyMetaDataTimestamp(delta : Timestamp) : Unit

  def modifyTimestamp(delta : Timestamp) : Unit = {
    startTimestamp += delta
    endTimestamp += delta
    minDataTimestamp += delta
    maxDataTimestamp += delta
    data.foreach(_._2.foreach(_.modifyTimestamp(delta)))
    modifyMetaDataTimestamp(delta)
  }

  def setTimestamp(t : Timestamp) : Unit = {
    val delta = t-getStartTimestamp
    modifyTimestamp(delta)
    modifyMetaDataTimestamp(delta)
  }

  def mapData(f : (Seq[Int], InputDataType) => InputDataType) : this.type = {
    withData(data.map(entry=>entry._1->f(entry._1, entry._2)))
  }

  def mapUnitData(f : (Seq[Int], InputUnitDataType) => InputUnitDataType) : this.type = {
    withData(data.map(entry=>entry._1->entry._2.map(e => f(entry._1, e))))
  }

  def mapDataWithShape(nShape : Shape, f : (Seq[Int], InputDataType) => (Seq[Int], InputDataType)) : this.type = {
    withShape(nShape).withData(data.toSeq.map{case(i, d) => f(i, d)}.groupBy(_._1).map(e=>e._1->e._2.map(_._2).asInstanceOf[InputDataType])).checkShape()
    this
  }

  def mapUnitDataWithShape(nShape : Shape, f : (Seq[Int], InputUnitDataType) => (Seq[Int], InputUnitDataType)) : this.type = {
    withShape(nShape).withData(data.toSeq.flatMap{case(i, d) => d.map(e => f(i, e))}.groupBy(_._1).map(e=>e._1->e._2.map(_._2))).checkShape()
    this
  }

  override def flatMapData(f: (Seq[Int], InputDataType) => Seq[InputDataType]): this.type = {
    withData(data.map(entry=>entry._1->f(entry._1, entry._2).flatten))
  }

  override def flatMapDataWithShape(nShape: Shape, f: (Seq[Int], InputDataType) => Seq[(Seq[Int], InputDataType)]): this.type = {
    withShape(nShape).withData(data.toSeq.flatMap{case(i, v) => f(i, v)}.groupBy(_._1).map(e=>e._1->e._2.map(_._2).asInstanceOf[InputDataType])).checkShape()
    this
  }

  override def setAllData(data: Seq[(Seq[Int], InputDataType)]): this.type = {
    withData(data.toMap)
  }

  override def setAllDataWithShape(ns: Shape, data: Seq[(Seq[Int], InputDataType)]): this.type = {
    withShape(ns).withData(data.toMap)
  }


  def addData(newData : Map[Seq[Int], InputDataType]) : this.type = {
    data = data++newData
    checkShape()
    this
  }

  override def toString: String = "InputTemporalPacket["+FormatTimestamp.format(getStartTimestamp)+" -> "+FormatTimestamp.format(getEndTimestamp)+"]"+
    " meta-data("+getMetaData.mkString(", ")+")"+
    " data("+getAllData.map(e => "["+e._1.mkString(";")+"]:{"+e._2.mkString(", ")+"}").mkString(", ")+")"
}

class InputTemporalPacket(s : Shape, d : Map[Seq[Int], Seq[InputTemporalData]], start : Timestamp = -1L, duration : Timestamp = -1L)
  extends AbstractInputTemporalPacket[InputTemporalData](s, d, start ,duration) with InputMetaData[InputTemporalMetaData] {

  if(!d.exists(_._2.nonEmpty)) {
    println("Warning : Empty packet")

  }

  override type InputMetaDataType = InputTemporalMetaData

  override var metaDataContainer = new InputMetaDataContainer[InputMetaDataType]()

  override def modifyMetaDataTimestamp(delta: Timestamp): Unit = mapMetaData{ m =>
    m.modifyTimestamp(delta)
    m
  }
}

//
//  N2S3 Input
//

trait N2S3InputData extends InputTemporalData
trait N2S3InputMetaData extends InputTemporalMetaData

object N2S3InputSpike {
  def apply(message: Message, t: Timestamp) = new N2S3InputSpike(message, t)
}

class N2S3InputSpike(message: Message, t: Timestamp) extends InputInstantaneousData(t) with N2S3InputData {
  def getMessage : Message = message
}

object N2S3InputLabel {
  def apply(label: String, start : Timestamp, end : Timestamp) = new N2S3InputLabel(label, start, end)
}

class N2S3InputLabel(label: String, start : Timestamp, end : Timestamp) extends InputDurationData(start, end) with N2S3InputMetaData with InputTemporalMetaData {
  def getLabel : String = label

  override def toString: String = label+" ["+FormatTimestamp.format(getStartTimestamp)+" -> "+FormatTimestamp.format(getEndTimestamp)+" = "+FormatTimestamp.format(getEndTimestamp-getStartTimestamp)+"]"
}

object N2S3InputPacket {
  val defaultMetaDataConverter: InputTemporalMetaData => Option[N2S3InputMetaData] = {
    case d: N2S3InputMetaData => Some(d)
    case o =>
      println("Warning : unknown how to convert metadata \"" + o.toString + "\" to N2S3")
      None
  }

  val defaultDataConvert: (Seq[Int], InputTemporalData) => Option[N2S3InputData] = { case (i, d) => d match {
      case d: N2S3InputData => Some(d)
      case o =>
        println("Warning : unknown how to convert data \"" + o.toString + "\" to N2S3")
        None
    }
  }
}

class N2S3InputPacket(s : Shape, d : Map[Seq[Int], Seq[N2S3InputData]], start : Timestamp, duration : Timestamp)
  extends AbstractInputTemporalPacket(s, d, start, duration) with InputMetaData[N2S3InputMetaData] {

  override type InputUnitDataType = N2S3InputData
  override type InputMetaDataType = N2S3InputMetaData

  override var metaDataContainer = new InputMetaDataContainer[N2S3InputMetaData]()

  def this(s : Shape, d : Map[Seq[Int], Seq[N2S3InputData]]) = {
    this(s, d, -1L, -1L)
  }

  def this(packet : InputTemporalPacket, convertData : (Seq[Int], InputTemporalData) => Option[N2S3InputData]) = {
    this(packet.getShape, packet.getAllData.map{case(i, d) => i -> d.flatMap(e => convertData(i, e))}, packet.getStartTimestamp, packet.getDuration)
    this.setMetaData(packet.getMetaData.flatMap(m =>  N2S3InputPacket.defaultMetaDataConverter(m)):_*)
  }

  def this(packet : InputTemporalPacket, convertData : (Seq[Int], InputTemporalData) => Option[N2S3InputData], convertMetaData : InputTemporalMetaData => Option[N2S3InputMetaData]) = {
    this(packet.getShape, packet.getAllData.map{case(i, d) => i -> d.flatMap(e => convertData(i, e))}, packet.getStartTimestamp, packet.getDuration)
    this.setMetaData(packet.getMetaData.flatMap(m => convertMetaData(m)):_*)
  }

  def this(packet : InputTemporalPacket, s : Shape, convertData : (Seq[Int], InputTemporalData) => Option[(Seq[Int], N2S3InputData)] ) = {
    this(s, packet.getAllData.flatMap{case(i, d) => d.flatMap(e=>convertData(i, e))}.groupBy(_._1).map(e => e._1 -> e._2.values.toSeq), packet.getStartTimestamp, packet.getDuration)
    this.setMetaData(packet.getMetaData.flatMap(m => N2S3InputPacket.defaultMetaDataConverter(m)):_*)
  }

  def this(packet : InputTemporalPacket, s : Shape, convertData : (Seq[Int], InputTemporalData) => Option[(Seq[Int], N2S3InputData)], convertMetaData : InputTemporalMetaData => Option[N2S3InputMetaData]) = {
    this(s, packet.getAllData.flatMap{case(i, d) => d.flatMap(e=>convertData(i, e))}.groupBy(_._1).map(e => e._1 -> e._2.values.toSeq), packet.getStartTimestamp, packet.getDuration)
    this.setMetaData(packet.getMetaData.flatMap(m => convertMetaData(m)):_*)
  }

  override def modifyMetaDataTimestamp(delta: Timestamp): Unit = mapMetaData{ m =>
    m.modifyTimestamp(delta)
    m
  }
}

class N2S3InputConverter[T <: InputTemporalPacket] extends StreamConverter[T, N2S3InputPacket] {

  override def dataConverter(in: T): N2S3InputPacket = {
    new N2S3InputPacket(
      in,
      N2S3InputPacket.defaultDataConvert,
      N2S3InputPacket.defaultMetaDataConverter
    )
  }

  def resetConverter() : Unit = {}
}

object N2S3Entry extends N2S3InputConverter[InputTemporalPacket]