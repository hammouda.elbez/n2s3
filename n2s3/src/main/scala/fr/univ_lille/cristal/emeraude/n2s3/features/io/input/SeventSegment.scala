package fr.univ_lille.cristal.emeraude.n2s3.features.io.input

/**
  * Created by falezp on 31/08/16.
  */
object InputSevenSegment extends InputFormat[InputFloatSample1D](Shape(7)) {

  def Data : SevenSegmentInputStream = new SevenSegmentInputStream
}

class SevenSegmentInputStream extends InputGenerator[InputFloatSample1D] {

  //     0
  //     -
  //  1 | | 2
  //     - 3
  //  4 | | 5
  //     -
  //     6

  val characters = List(
    List(1, 1, 1, 0, 1, 1, 1), // 0
    List(0, 0, 1, 0, 0, 1, 0), // 1
    List(1, 0, 1, 1, 1, 0, 1), // 2
    List(1, 0, 1, 1, 0, 1, 1), // 3
    List(0, 1, 1, 1, 0, 1, 0), // 4
    List(1, 1, 0, 1, 0, 1, 1), // 5
    List(1, 1, 0, 1, 1, 1, 1), // 6
    List(1, 0, 1, 0, 0, 1, 0), // 7
    List(1, 1, 1, 1, 1, 1, 1), // 8
    List(1, 1, 1, 1, 0, 1, 1)  // 9
  )

  val labels = List("0", "1", "2", "3", "4", "5", "6", "7", "8", "9")

  var cursor = 0

  def atEnd() = cursor >= characters.size
  def next() = {

    val next = InputFloatSample1D(
      (for(i <- 0 until 7) yield characters(cursor)(i).toFloat).toArray
    ).setMetaData(InputLabel(labels(cursor)))
    cursor += 1
    next
  }

  def reset() = {
    cursor = 0
  }

  def shape = Shape(7)

  override def toString = "SevenSegment"
}
