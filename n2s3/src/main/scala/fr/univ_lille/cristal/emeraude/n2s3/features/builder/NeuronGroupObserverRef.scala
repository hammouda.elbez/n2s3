package fr.univ_lille.cristal.emeraude.n2s3.features.builder

import akka.actor.ActorRef
import akka.actor.SupervisorStrategy.Stop
import akka.pattern.ask
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.{Config, Initialize}

import scala.concurrent.Await
/**
  * A reference to a neuron group observer
  */
abstract class NeuronGroupObserverRef {


  var deployed: Boolean = false
  var destroyed : Boolean = false
  def isDeployed: Boolean = this.deployed
  def isDestroyed: Boolean = this.destroyed

  def ensureActorDeployed(n2s3: N2S3): Unit = {
    implicit val timeout = Config.longTimeout
    if (!this.isDeployed){
      this.deploy(n2s3)
      this.deployed = true
      println("initialize "+this.getClass.getCanonicalName)
      getActors.foreach { actor =>
        Await.result(ask(actor, Initialize), timeout.duration)
      }
      println("Done")
    }
  }

  protected def deploy(n2S3: N2S3)

  def destroy() : Unit = {
    implicit val timeout = Config.longTimeout
    if(this.deployed) {
      println("destroy "+this.getClass.getCanonicalName)
      getActors.foreach { actor =>
        Await.result(ask(actor, Stop), timeout.duration)
      }
      this.deployed = false
      this.destroyed = true
    }
  }

  def getActors : Seq[ActorRef]
}
