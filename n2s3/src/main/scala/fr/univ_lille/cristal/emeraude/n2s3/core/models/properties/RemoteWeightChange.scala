package fr.univ_lille.cristal.emeraude.n2s3.core.models.properties

import fr.univ_lille.cristal.emeraude.n2s3.core.ConnectionProperty

/**
  * Created by falezp on 22/03/17.
  */
object RemoteWeightChange extends ConnectionProperty[Boolean]