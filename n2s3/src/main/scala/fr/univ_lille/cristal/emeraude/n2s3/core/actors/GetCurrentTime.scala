package fr.univ_lille.cristal.emeraude.n2s3.core.actors

import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message

/***********************************************************************************************
 *                                   TIME MANAGEMENT MESSAGES
 **********************************************************************************************/
case class GetCurrentTime() extends Message
