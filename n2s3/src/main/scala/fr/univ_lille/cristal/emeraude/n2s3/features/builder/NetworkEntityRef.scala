package fr.univ_lille.cristal.emeraude.n2s3.features.builder
import fr.univ_lille.cristal.emeraude.n2s3.core.{ExternalSender, NetworkEntityPath}
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message

/**
  * Reference to an network entity.
  * It contains a [[NetworkEntityPath]] to the referenced entity and provides facility methods to communicate with it.
  */
trait NetworkEntityRef {

  var actorPath: Option[NetworkEntityPath] = None
  def setActor(path: NetworkEntityPath) = this.actorPath = Some(path)
  def getNetworkAddress = this.actorPath.get

  /**
    * Returns a boolean that indicates wether the actor of this ref is already deployed
    */
  def isDeployed = this.actorPath.nonEmpty

  /**
    * Sends a message to the entity without waiting for an answer
    */
  def send(message: Message): Unit =  {
    ExternalSender.sendTo(this.actorPath.get, message )
  }

  /**
    * Sends a message to the entity and waits for it to answer
    */
  def ask(message: Message): Unit =  {
    ExternalSender.askTo(this.actorPath.get, message )
  }
}
