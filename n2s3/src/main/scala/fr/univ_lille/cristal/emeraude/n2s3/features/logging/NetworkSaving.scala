package fr.univ_lille.cristal.emeraude.n2s3.features.logging

import java.io.{BufferedWriter, File, FileWriter}

import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.{MembranePotentialThreshold, SynapticWeightFloat}
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.N2S3
import squants.electro.ElectricPotential

/**
  * Class responsible on saving simulation network data
  * */

object NetworkSaving {

  def save(n2s3 : N2S3, croppedCnx : List[ConnectionPath],networkIndex : List[(Integer,Integer,Integer)],filename : String){
    val file = new File(filename)
    val bw = new BufferedWriter(new FileWriter(file,true))
    var x = 0

    /**
    *  Save Network synapses order to be able to load later in the correct order
    **/

    bw.write(networkIndex.length+"\n")
    networkIndex.foreach(index =>{
      bw.write(x+" "+index._1+"\n")
      x+=1
    })

    /**
      *  Save number of synaptic connexions
      **/

    var Nnbr = 0

    n2s3.layers.head.connections.foreach(c =>{
      Nnbr += c.connections.size
    })

    bw.write(Nnbr+"\n")

    /**
      *  Save Synapses information
      **/

    n2s3.layers.foreach(nGroup => {
      nGroup.neurons.foreach(NeurRef => {
        var values : ConnectionPropertyValues[Float] = ExternalSender.askTo(NeurRef.getNetworkAddress, GetAllConnectionProperty(SynapticWeightFloat)) match {
          case x:ConnectionPropertyValues[Float] => x
        }
        values.values.foreach(F => {
          bw.write(F._1+" "+NeurRef.getNetworkAddress.actor.path.name+" "+F._3+"\n")
        })
      })
    })

    /**
      *  Save number of neurons
      **/

    Nnbr = 0

    n2s3.layers.foreach(l =>{
    if(!l.identifier.equals("Input")){
         Nnbr+= l.neurons.length
    }
    })

    bw.write(Nnbr+"\n")

    /**
      *  Save Neurones threshold
      **/

    n2s3.layers.foreach(nGroup =>{
      if(!nGroup.identifier.contains("Input")){
      nGroup.neurons.foreach(n => {
        bw.write(n.getNetworkAddress.actor.path.name+" "+ExternalSender.askTo(n.getNetworkAddress, GetProperty(MembranePotentialThreshold)).asInstanceOf[PropertyValue[ElectricPotential]].value.value+"\n")

      })}
    })

    /**
      *  Save Network Cropped connexions (if exists)
      **/

    bw.write(croppedCnx.length+"\n")
    croppedCnx.foreach(cnx => {
      bw.write(cnx.outputNeuron.actor.path.name+" "+cnx.connectionID.toString+"\n")
    })

    bw.close()
}

}
