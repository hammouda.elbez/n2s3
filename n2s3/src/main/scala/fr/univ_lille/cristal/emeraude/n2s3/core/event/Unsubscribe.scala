package fr.univ_lille.cristal.emeraude.n2s3.core.event

import fr.univ_lille.cristal.emeraude.n2s3.core.NetworkEntityReference

/** Unsubscribe an actor to an event */
case class Unsubscribe(event : Event[_ <: EventResponse], ref : NetworkEntityReference) extends EventHolderMessage