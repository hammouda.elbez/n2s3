/************************************************************************************************************
 * Contributors:
 * 		- created by falezp on 09/03/16
 ***********************************************************************************************************/
package fr.univ_lille.cristal.emeraude.n2s3.core.actors

import akka.util.Timeout

import scala.concurrent.duration._

/**
  * This object contains default parameters for a N2S3 simulation.
  * This object is immutable.
  */
object Config {

  /**
    * Default timeout to use in wait patterns
    */
  val defaultTimeout = Timeout(60 seconds)

  /**
    * Default timeout to use in wait patterns when a long process is in progress
    */
  val longTimeout = Timeout(21474835 seconds)
}
