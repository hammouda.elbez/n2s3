package fr.univ_lille.cristal.emeraude.n2s3.models.neurons

import fr.univ_lille.cristal.emeraude.n2s3.core
import fr.univ_lille.cristal.emeraude.n2s3.core.Neuron.{ConnectionID, Inhibition, NeuronEnds, NeuronMessage}
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.{BackwardSpike, ElectricSpike, ForecastFire}
import fr.univ_lille.cristal.emeraude.n2s3.core.event.{NeuronFireEvent, NeuronFireResponse, NeuronPotentialUpdateEvent}
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties._
import fr.univ_lille.cristal.emeraude.n2s3.core.models.synapse.FloatSynapse
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias._
import fr.univ_lille.cristal.emeraude.n2s3.support.Time
import fr.univ_lille.cristal.emeraude.n2s3.support.UnitCast._
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message
import squants.electro.CapacitanceConversions.CapacitanceConversions
import squants.electro.ElectricPotentialConversions.ElectricPotentialConversions
import squants.electro.{Capacitance, ElectricCharge, ElectricPotential}

import scala.collection.mutable

/**
  * Created by falezp on 13/07/16.
  * Implementation of the Spike Response Model neuron
  *
  * More informations can be found at the following address:
  * 	- http://icwww.epfl.ch/~gerstner/SPNM/node27.html
  */



object SRM extends NeuronModel {
  override def createNeuron(): SRM = new SRM()
}

class SRM extends core.Neuron {
  /********************************************************************************************************************
    * Model Parameters
    ******************************************************************************************************************/

  private var t_refract = 1 MilliSecond

  private var T = 1 millivolts // Neuron threshold
  private val K1 = 2.0
  private val K2 = 4.0
  private val alpha = 0.25

  private val K = 2.0 // 1/max(exp(-x/tau_m)-exp(-x/tau_s))

  private var tau_m = 10 MilliSecond // Membrane time constant
  private var tau_s = 2.5 MilliSecond // Synaptic time constant

  private var u_rest = 0 millivolts
  var membraneCapacitance : Capacitance = 1 farads
  /*
    var theta = 0f
    var thetaLeak = 1e7 MilliSecond
    var theta_step = 0.05f
*/

  /********************************************************************************************************************
    * Remembered properties
    ******************************************************************************************************************/
  private var tLastTheta : Timestamp = 0
  private var tLastSpike : Timestamp = -7*tau_m.timestamp-1
  private var tLastFire : Timestamp = -7*tau_m.timestamp-1

  private var fixedParameter = false
  private var membraneThresholdType = MembraneThresholdTypeEnum.Dynamic

  private val lastExcSpikes = mutable.HashMap[Int, (Timestamp, ElectricCharge)]()
  private val lastInhSpikes = mutable.HashMap[Int, Timestamp]()

  var fire_id = 0

  /********************************************************************************************************************
    * Properties and Events
    ******************************************************************************************************************/
  addProperty[ElectricPotential](MembranePotentialThreshold, () => T, T = _)
  addProperty[ElectricPotential](MembraneRestingPotential, () => u_rest, u_rest = _)
  addProperty[Time](MembraneTimeConstant, () => tau_m, tau_m = _)
  addProperty[Time](SynapseTimeConstant, () => tau_s, tau_s = _)
  addProperty[Time](MembraneRefractoryDuration, () => t_refract, t_refract = _)
  addProperty[Boolean](FixedParameter, () => fixedParameter, b => {
    fixedParameter = b
    inputConnections.foreach { case(id, obj) =>
      obj.connection match {
        case s : FloatSynapse => s.setFixedParameter(b)
      }
    }
  })
  addProperty[MembraneThresholdTypeEnum.Value](MembraneThresholdType, () => membraneThresholdType, membraneThresholdType = _)

/*
  addConnectionProperty[Float](SynapticWeightFloat, {
    case s : FloatSynapse => Some(s.getWeight)
    case _ => None
  },
    (connection,  value) => connection match {
      case s : FloatSynapse => s.setWeight(value)
      case _ =>
    })
*/

  addEvent(NeuronPotentialUpdateEvent)

  /********************************************************************************************************************
    * Neuron Behavior
    ******************************************************************************************************************/


  private def theta(delta_t : Timestamp) : Double = {
    if(delta_t >= 0)
      1.0
    else
      0.0
  }

  private def epsilon(delta_t : Timestamp) : Double = {
    if(delta_t > 7*tau_m.timestamp)
      0.0
    else
      K*(math.exp(-delta_t.toDouble/tau_m.timestamp.toDouble)-math.exp(-delta_t.toDouble/tau_s.timestamp.toDouble))*theta(delta_t)
  }

  private def eta(delta_t : Timestamp) : ElectricPotential  = {
    if(delta_t > 7*tau_m.timestamp)
      0 millivolts
    else
      T*(K1*math.exp(-delta_t.toDouble/tau_m.timestamp.toDouble)-K2*(math.exp(-delta_t.toDouble/tau_m.timestamp.toDouble)-math.exp(-delta_t.toDouble/tau_s.timestamp.toDouble)))*theta(delta_t)
  }

  private def mu(delta_t : Timestamp) : ElectricPotential = {
    if(delta_t > 7*tau_m.timestamp)
      0 millivolts
    else
      -T*alpha*epsilon(delta_t)
  }


  def computeMembranePotential(timestamp : Timestamp) : ElectricPotential = {
    u_rest+eta(timestamp-tLastFire)+lastExcSpikes.foldLeft(0 millivolts) { case (acc, (_, (t, charge))) =>
      acc+charge/membraneCapacitance*epsilon(timestamp-t)
    }/*+lastInhSpikes.foldLeft(0.0) { case (acc, (_, t)) =>
      acc+mu(timestamp-t)
    }*/
  }


  def processSomaMessage(timestamp : Timestamp, message : Message, fromConnection : Option[(ConnectionID, NeuronConnection)], ends : NeuronEnds) : Unit = message match {
    case ElectricSpike(spikeCharge) =>

      if(timestamp-tLastFire < t_refract.timestamp)
        return

      if(fromConnection.isDefined) {
        if(lastExcSpikes.contains(fromConnection.get._1))
          lastExcSpikes(fromConnection.get._1) = timestamp -> spikeCharge
        else
          lastExcSpikes += fromConnection.get._1 -> (timestamp,  spikeCharge)
      }

      var t = timestamp
      var end = false
      while(!end && t <= timestamp+tau_m.timestamp) {
        val v = computeMembranePotential(t)
        //println(getNetworkAddress.toString+" : t="+t+" (d="+(t-timestamp)+") => "+v)
        if(v >= T) {
          end = true
          if(t == timestamp) {
            fire_id += 1
            triggerEventWith(NeuronFireEvent, NeuronFireResponse(timestamp, getNetworkAddress))
            ends.sendToAllInput(timestamp, BackwardSpike)
            ends.sendToAllOutput(timestamp, ElectricSpike())
            tLastFire = timestamp
            lastExcSpikes.clear()
            lastInhSpikes.clear()
          }
          else
            this.thisToSyncRef.send(NeuronMessage(t, ForecastFire(fire_id), thisToSyncRef, syncToThisRef, None))
        }
        t += tau_m.timestamp/10
      }



      tLastSpike = timestamp
    case ForecastFire(id) =>
      if(id == fire_id) {
        triggerEventWith(NeuronFireEvent, NeuronFireResponse(timestamp, getNetworkAddress))
        ends.sendToAllOutput(timestamp, ElectricSpike())
        lastExcSpikes.clear()
        lastInhSpikes.clear()
        fire_id += 1
      }
    case Inhibition(_) =>
      tLastSpike = timestamp

      lastExcSpikes.clear()

      if(fromConnection.isDefined)
        lastInhSpikes += fromConnection.get._1 -> timestamp
  }
}
