package fr.univ_lille.cristal.emeraude.n2s3.models.synapses

import fr.univ_lille.cristal.emeraude.n2s3.core.{NeuronConnection, SynapseBuilder}
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.{BackwardSpike, ElectricSpike}
import fr.univ_lille.cristal.emeraude.n2s3.core.models.synapse.FloatSynapse
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias._
import fr.univ_lille.cristal.emeraude.n2s3.support.Time
import fr.univ_lille.cristal.emeraude.n2s3.support.UnitCast._
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message
/**
  * Created by falezp on 18/11/16.
  *
  * Implementation of STDP observed in the squid, described in the article:
  * Synaptic Modifications in Cultured Hippocampal Neurons: Dependence on Spike Timing, Synaptic Strength, and Postsynaptic Cell Type
  * Guo-qiang Bi and Mu-ming Poo, 1998
  */

object StandardSTDP{
  def apply(w : Float, d : Time = Time(0)) = new StandardSTDPBuilder(w, d)
}

class StandardSTDPBuilder(w : Float, d : Time = Time(0)) extends SynapseBuilder {
  override def createSynapse: NeuronConnection = new StandardSTDP(w, d)
}

class StandardSTDP(w : Float, d : Time = Time(0)) extends FloatSynapse(w, d) {
  var a_p = 0.1f
  var a_m = 0.1f

  val tau_p = 20 MilliSecond
  var tau_m = 100 MilliSecond

  var t_last_pre : Timestamp = -1
  var t_last_post : Timestamp = -1

  def processConnectionMessage(timestamp : Timestamp, message : Message) : Unit = message match {
    case ElectricSpike(charge) =>
      if (t_last_post >= 0 && timestamp+getDelay.timestamp - t_last_post <= tau_m.timestamp && t_last_post > t_last_pre) {
        val delta_w  = (delta_t : Timestamp, tau_t : Timestamp) => delta_t.toDouble/tau_t.toDouble*0.5
        setWeight(getWeight-a_m *getWeight*delta_w(timestamp+getDelay.timestamp - t_last_post, tau_m.timestamp).toFloat)
      }
      t_last_pre = timestamp+getDelay.timestamp
      getEnds.sendToOutput(timestamp+getDelay.timestamp, ElectricSpike(charge*getWeight))
    case BackwardSpike =>
      if (t_last_pre >= 0 && timestamp - t_last_pre <= tau_p.timestamp && t_last_pre > t_last_post) {
        val delta_w = (delta_t: Timestamp, tau_t: Timestamp) => {
          if (delta_t <= tau_t / 2)
            1.0 - delta_t.toDouble / (tau_t.toDouble / 2.0)
          else
            (delta_t.toDouble - tau_t.toDouble / 2.0) / (tau_t.toDouble / 2.0)
        }
        setWeight(getWeight + a_p * (1f - getWeight) * delta_w(timestamp - t_last_pre, tau_p.timestamp).toFloat)
      }
      t_last_post = timestamp
  }
}
