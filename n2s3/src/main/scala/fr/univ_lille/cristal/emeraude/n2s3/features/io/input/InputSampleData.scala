package fr.univ_lille.cristal.emeraude.n2s3.features.io.input

/**
  * Created by falezp on 06/02/17.
  */

//
//  MetaData
//
trait InputSampleMetaData

case class InputLabel(label : String) extends InputSampleMetaData

//
//  Data
//


abstract class InputSample[T] extends InputPacket with InputMetaData[InputSampleMetaData] {

  override type InputDataType = T
  override type InputMetaDataType = InputSampleMetaData

  override var metaDataContainer = new InputMetaDataContainer[InputMetaDataType]()

  protected def checkMap(dimension : Int, ns : Shape, nd : Seq[(Seq[Int], T)]) : Seq[(Seq[Int], T)] = {
    if(dimension == 0 && ns.dimensionNumber == 1 && ns.dimensions.head == 1 || ns.dimensionNumber == dimension) {

      val expectedIndex = ns.allIndex.sortBy(e => ns.toIndex(e:_*))

      val d1 = nd.groupBy(_._1).map { case(i, l) =>
        if(l.size != 1)
          throw new UnsupportedOperationException("Multiple value for index ["+i.mkString(";")+"]")
        (i, l.head._2)
      }.toSeq.sortBy(e => ns.toIndex(e._1:_*))

      if(d1.size != expectedIndex.size)
        throw new UnsupportedOperationException("Missing index, get: "+d1.size+", expected: "+expectedIndex.size)

      d1.zip(expectedIndex).map{ case((i, d), e) =>
        val ii = ns.toIndex(i:_*)
        val ie = ns.toIndex(e:_*)
        if(ii != ie)
          throw new UnsupportedOperationException("Missing index ["+e.mkString(";")+"]")
        (i, d)
      }
    }
    else {
      throw new UnsupportedOperationException("Incompatible shape : "+dimension+" - "+ns)
    }
  }
}

class InputValue[T](v : T) extends InputSample[T] {

  withShape(Shape(1))

  private val index = Seq()
  private var value : T = v

  def getDataOnChannel(index : Int*) : T = {
    if(index.nonEmpty)
      throw new NoSuchElementException("["+index.mkString(";")+"]")
    value
  }

  def withValue(v : T) : this.type = {
    value = v
    this
  }

  def getAllData : Map[Seq[Int], T] = Map((index, value))

  override def mapData(f : (Seq[Int], InputDataType) => InputDataType) : this.type = {
    val r = checkMap(0, shape, Seq(index -> f(index, value)))
    withValue(r.head._2)
  }

  override def mapDataWithShape(s : Shape, f: (Seq[Int], T) => (Seq[Int], T)) : this.type = {
    val r = checkMap(0, s, Seq(f(index, value)))
    withValue(r.head._2)
  }

  override def flatMapData(f: (Seq[Int], T) => Seq[T]) : this.type = {
    val r = checkMap(0, shape, f(index, value).map(e => index -> e))
    withValue(r.head._2)
  }

  override def flatMapDataWithShape(s : Shape, f: (Seq[Int], T) => Seq[(Seq[Int], T)]) : this.type = {
    val r = checkMap(0, s, f(index, value))
    withValue(r.head._2)
  }

  override def setAllData(data: Seq[(Seq[Int], T)]): this.type = {
    val r = checkMap(0, shape, data)
    withValue(r.head._2)
  }

  override def setAllDataWithShape(s: Shape, data: Seq[(Seq[Int], T)]): this.type = {
    val r = checkMap(0, s, data)
    withValue(r.head._2)
  }
}

class InputSample1D[T](d : Seq[T]) extends InputSample[T] {

  assert(d.nonEmpty)
  withShape(Shape(d.size))

  private var data : Seq[T] = d

  def withData(nd : Seq[T]) : this.type = {
    data = nd
    this
  }

  def getDataOnChannel(index : Int*) : T = {
    if(index.size != 1)
      throw new NoSuchElementException

    data(index.head)
  }

  def getAllData : Map[Seq[Int], T] = {
    Map(
      (for {
        x <- data.indices
      } yield (Seq(x), data(x))):_*
    )
  }

  override def mapData(f: (Seq[Int], T) => T): this.type = {
    val r = checkMap(1, shape, data.zipWithIndex.map{case(v, i) => Seq(i) -> f(Seq(i), v)})
    withData(r.map(_._2))
  }

  override def mapDataWithShape(ns: Shape, f: (Seq[Int], T) => (Seq[Int], T)): this.type = {
    val r = checkMap(1, ns, data.zipWithIndex.map{case(v, i) => f(Seq(i), v)})
    withShape(ns).withData(r.map(_._2))
  }

  override def flatMapData(f: (Seq[Int], T) => Seq[T]): InputSample1D.this.type = {
    val r = checkMap(1, shape, data.zipWithIndex.flatMap{case(v, i) => f(Seq(i), v).map(v1 => Seq(i) -> v1)})
    withData(r.map(_._2))
  }

  override def flatMapDataWithShape(ns : Shape, f: (Seq[Int], T) => Seq[(Seq[Int], T)]): InputSample1D.this.type = {
    val r = checkMap(1, ns, data.zipWithIndex.flatMap{case(v, i) => f(Seq(i), v)})
    withShape(ns).withData(r.map(_._2))
  }

  override def setAllData(data: Seq[(Seq[Int], T)]): this.type = {
    val r = checkMap(1, shape, data)
    withData(r.map(_._2))
  }

  override def setAllDataWithShape(ns: Shape, data: Seq[(Seq[Int], T)]): this.type = {
    val r = checkMap(1, ns, data)
    withShape(ns).withData(r.map(_._2))
  }

  override def toString: String = {
    "=== InputFloatSample1D "+shape+"\n"+
      (for(x <- 0 until shape.dimensions.head) yield data(x)).mkString(" ")+"\n"+
      "========================="
  }
}

case class InputFloatSample1D(d : Seq[Float]) extends InputSample1D[Float](d)

class InputSample2D[T](d : Seq[Seq[T]]) extends InputSample[T] {

  assert(d.nonEmpty && d.tail.forall(_.length == d.head.length))

  withShape(Shape(d.length, d.head.length))

  private var data : Seq[Seq[T]] = d

  def getData2D : Seq[Seq[T]] = data

  def withData(nd : Seq[Seq[T]]) : this.type = {
    data = nd
    this
  }

  def getDataOnChannel(index : Int*) : T = {
    if(index.size != 2)
      throw new NoSuchElementException

    data(index(0))(index(1))
  }

  def getAllData : Map[Seq[Int], T] = {
    Map(
      (for {
        x <- 0 until shape.dimensions(0)
        y <- 0 until shape.dimensions(1)
      } yield (Seq(x, y), data(x)(y))):_*
    )
  }

  override def mapData(f: (Seq[Int], T) => T): this.type = {
    val r = checkMap(2, shape, for{
      x <- 0 until shape.dimensions(0)
      y <- 0 until shape.dimensions(1)
    } yield Seq(x, y) -> f(Seq(x, y), data(x)(y))).toMap
    withData(
      for(x <- 0 until shape.dimensions(0)) yield {
        for(y <- 0 until shape.dimensions(1)) yield {
          r(Seq(x, y))
        }
      }
    )
  }

  override def mapDataWithShape(ns: Shape, f: (Seq[Int], T) => (Seq[Int], T)): this.type = {
    val r = checkMap(2, ns, for{
      x <- 0 until shape.dimensions(0)
      y <- 0 until shape.dimensions(1)
    } yield f(Seq(x, y), data(x)(y))).toMap
    withShape(ns).withData(
      for(x <- 0 until ns.dimensions(0)) yield {
        for(y <- 0 until ns.dimensions(1)) yield {
          r(Seq(x, y))
        }
      }
    )
  }

  override def flatMapData(f: (Seq[Int], T) => Seq[T]): InputSample2D.this.type = {
    val r = checkMap(2, shape, (for{
      x <- 0 until shape.dimensions(0)
      y <- 0 until shape.dimensions(1)
    } yield f(Seq(x, y), data(x)(y)).map(v => Seq(x, y) -> v)).flatten).toMap
    withData(
      for(x <- 0 until shape.dimensions(0)) yield {
        for(y <- 0 until shape.dimensions(1)) yield {
          r(Seq(x, y))
        }
      }
    )
  }

  override def flatMapDataWithShape(ns: Shape, f: (Seq[Int], T) => Seq[(Seq[Int], T)]): InputSample2D.this.type = {
    val r = checkMap(2, ns, (for{
      x <- 0 until shape.dimensions(0)
      y <- 0 until shape.dimensions(1)
    } yield f(Seq(x, y), data(x)(y))).flatten).toMap
    withShape(ns).withData(
      for(x <- 0 until ns.dimensions(0)) yield {
        for(y <- 0 until ns.dimensions(1)) yield {
          r(Seq(x, y))
        }
      }
    )
  }

  override def setAllData(data: Seq[(Seq[Int], T)]): this.type = {
    val r = checkMap(2, shape, data).toMap
    withData(
      for(x <- 0 until shape.dimensions(0)) yield {
        for(y <- 0 until shape.dimensions(1)) yield {
          r(Seq(x, y))
        }
      }
    )
  }

  override def setAllDataWithShape(ns: Shape, data: Seq[(Seq[Int], T)]): this.type = {
    val r = checkMap(2, ns, data).toMap
    withShape(ns).withData(
      for(x <- 0 until ns.dimensions(0)) yield {
        for(y <- 0 until ns.dimensions(1)) yield {
          r(Seq(x, y))
        }
      }
    )
  }

  override def toString: String = {
    "=== InputFloatSample2D "+shape+"\n"+
      (for(y <- 0 until shape.dimensions(1)) yield {
        (for(x <- 0 until shape.dimensions(0)) yield data(x)(y)).mkString(" ")
      }).mkString("\n")+"\n"+
      "========================="
  }

}

class InputSample3D[T](d : Seq[Seq[Seq[T]]]) extends InputSample[T] {

  withShape(Shape(d.length, d.head.length, d.head.head.length))

  private var data : Seq[Seq[Seq[T]]] = d

  def getData3D : Seq[Seq[Seq[T]]] = data

  def withData(nd : Seq[Seq[Seq[T]]]) : this.type = {
    data = nd
    this
  }

  def getDataOnChannel(index : Int*) : T = {
    if(index.size != 3)
      throw new NoSuchElementException

    data(index(0))(index(1))(index(2))
  }

  def getAllData : Map[Seq[Int], T] = {
    Map(
      (for {
        x <- 0 until shape.dimensions(0)
        y <- 0 until shape.dimensions(1)
        z <- 0 until shape.dimensions(2)
      } yield (Seq(x, y, z), data(x)(y)(z))):_*
    )
  }

  override def mapData(f: (Seq[Int], T) => T): this.type = {
    val r = checkMap(3, shape, for{
      x <- 0 until shape.dimensions(0)
      y <- 0 until shape.dimensions(1)
      z <- 0 until shape.dimensions(2)
    } yield Seq(x, y, z) -> f(Seq(x, y, z), data(x)(y)(z))).toMap
    withData(
      for(x <- 0 until shape.dimensions(0)) yield {
        for(y <- 0 until shape.dimensions(1)) yield {
          for(z <- 0 until shape.dimensions(2)) yield {
            r(Seq(x, y, z))
          }
        }
      }
    )
  }

  override def mapDataWithShape(ns: Shape, f: (Seq[Int], T) => (Seq[Int], T)): this.type = {
    val r = checkMap(3, ns, for{
      x <- 0 until shape.dimensions(0)
      y <- 0 until shape.dimensions(1)
      z <- 0 until shape.dimensions(2)
    } yield f(Seq(x, y, z), data(x)(y)(z))).toMap
    withShape(ns).withData(
      for(x <- 0 until ns.dimensions(0)) yield {
        for(y <- 0 until ns.dimensions(1)) yield {
          for(z <- 0 until ns.dimensions(2)) yield {
            r(Seq(x, y, z))
          }
        }
      }
    )
  }

  override def flatMapData(f: (Seq[Int], T) => Seq[T]): this.type = {
    val r = checkMap(3, shape, (for{
      x <- 0 until shape.dimensions(0)
      y <- 0 until shape.dimensions(1)
      z <- 0 until shape.dimensions(2)
    } yield f(Seq(x, y, z), data(x)(y)(z)).map(v => Seq(x, y, z) -> v)).flatten).toMap
    withData(
      for(x <- 0 until shape.dimensions(0)) yield {
        for(y <- 0 until shape.dimensions(1)) yield {
          for(z <- 0 until shape.dimensions(2)) yield {
            r(Seq(x, y, z))
          }
        }
      }
    )
  }

  override def flatMapDataWithShape(ns: Shape, f: (Seq[Int], T) => Seq[(Seq[Int], T)]): this.type = {
    val r = checkMap(3, ns, (for{
      x <- 0 until shape.dimensions(0)
      y <- 0 until shape.dimensions(1)
      z <- 0 until shape.dimensions(2)
    } yield f(Seq(x, y, z), data(x)(y)(z))).flatten).toMap
    withShape(ns).withData(
      for(x <- 0 until ns.dimensions(0)) yield {
        for(y <- 0 until ns.dimensions(1)) yield {
          for(z <- 0 until shape.dimensions(2)) yield {
            r(Seq(x, y, z))
          }
        }
      }
    )
  }

  override def setAllData(data: Seq[(Seq[Int], T)]): this.type = {
    val r = checkMap(3, shape, data).toMap
    withData(
      for(x <- 0 until shape.dimensions(0)) yield {
        for(y <- 0 until shape.dimensions(1)) yield {
          for(z <- 0 until shape.dimensions(2)) yield {
            r(Seq(x, y, z))
          }
        }
      }
    )
  }

  override def setAllDataWithShape(ns: Shape, data: Seq[(Seq[Int], T)]): this.type = {
    val r = checkMap(3, ns, data).toMap
    withShape(ns).withData(
      for(x <- 0 until ns.dimensions(0)) yield {
        for(y <- 0 until ns.dimensions(1)) yield {
          for(z <- 0 until shape.dimensions(2)) yield {
            r(Seq(x, y, z))
          }
        }
      }
    )
  }

  override def toString: String = {
    "=== InputFloatSample3D "+shape+"\n"+
      (for(z <- 0 until shape.dimensions(2)) yield {
        (for(y <- 0 until shape.dimensions(1)) yield {
          (for (x <- 0 until shape.dimensions(0)) yield data(x)(y)(z)).mkString(" ")
        }).mkString("\n")
      }).mkString("--------------------\n")+"\n"+
      "========================="
  }

}