package fr.univ_lille.cristal.emeraude.n2s3.features.logging.graph

import akka.actor.{Actor, Props}
import akka.pattern.ask
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.{Config, Done}
import fr.univ_lille.cristal.emeraude.n2s3.core.event._
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.SynapseWeightAndDelay
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.N2S3
import fr.univ_lille.cristal.emeraude.n2s3.features.logging.graph.NeuronDependenceTikz.Data
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias._
import fr.univ_lille.cristal.emeraude.n2s3.support.Time
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.LocalActorDeploymentStrategy

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import scala.concurrent.Await

/**
  * Created by falezp on 16/11/16.
  */
class NeuronDependenceMonitor(n2s3 : N2S3, maxDelay : Time, processCondition : (NetworkEntityPath, Timestamp) => Boolean, outputFilename : String) {

  object Close
  object Print
  object Result

  case class ResultEntry(label : String, winner : NetworkEntityPath, neuronInvolved : Int) {

  }

  class Result(data : Seq[ResultEntry]) {
    val involvedNeuron = data.groupBy(_.label).map{case (label, list) => list.foldLeft(0){ case(acc, el) => acc+el.neuronInvolved}/list.size}
  }

  class NeuronDependenceMonitor(n2s3 : N2S3, maxDelay : Time, processCondition : (NetworkEntityPath, Timestamp) => Boolean, outputFilename : String) extends Actor {

    val inputs = mutable.ArrayBuffer[(String, Timestamp)]()
    val fires = mutable.HashMap[NetworkEntityPath, ArrayBuffer[Timestamp]]()
    val data = mutable.ArrayBuffer[Data]()
    val results = mutable.ArrayBuffer[ResultEntry]()

    ExternalSender.askTo(n2s3.inputLayerRef.get.getContainer, SubscribeSynchronized(LabelChangeEvent, ExternalSender.getReference(self), n2s3.buildProperties.getSynchronizerPolicy.getInputSynchronizer))

    n2s3.layers.flatMap(_.neuronPaths).foreach { path =>
      ExternalSender.askTo(path, SubscribeSynchronized(NeuronFireEvent, ExternalSender.getReference(self), n2s3.buildProperties.getSynchronizerPolicy.getInputSynchronizer))
    }

    override def receive = {
      case SynchronizedEvent(s, _, m, _) =>
        m match {
          case LabelChangeResponse(start, end, label) =>
            inputs += ((label, start))
          case NeuronFireResponse(timestamp, source) =>

            val inputs = ExternalSender.askTo(source, GetAllConnectionProperty(SynapseWeightAndDelay)) match {
              case PropertyValue(values: Seq[(NetworkEntityPath, (Float, Time))]@unchecked) => values
              case _ => Seq()
            }

            inputs.foreach { case(input_neuron, (weight, delay)) =>

              fires.get(input_neuron) match {
                case Some(listSpike) =>
                  listSpike.filter(t => timestamp >= t && timestamp-(t+delay.timestamp) < maxDelay.timestamp).foreach { t =>
                    data += Data((input_neuron, t), (source, t+delay.timestamp), weight)
                  }
                case None =>
              }
            }

            fires.getOrElseUpdate(source, ArrayBuffer()) += timestamp

          case _ => throw new RuntimeException("Unrecognized message : "+m)
        }
        ExternalSender.sendTo(s, Done)
      case Close =>
        ExternalSender.askTo(n2s3.inputLayerRef.get.getContainer, Unsubscribe(LabelChangeEvent, ExternalSender.getReference(self)))

        n2s3.layers.flatMap(_.neuronPaths).foreach { path =>
          ExternalSender.askTo(path, Unsubscribe(NeuronFireEvent, ExternalSender.getReference(self)))
        }
        sender ! Done
        context.stop(self)

      case Print =>
        var i = 0
        fires.foreach { case (neuron, list) =>
          list.foreach { timestamp =>
            if (processCondition(neuron, timestamp)) {

              val currentData = mutable.ArrayBuffer[Data]()
              val processQueue = mutable.Queue[(NetworkEntityPath, Timestamp)]()
              processQueue.enqueue((neuron, timestamp))

              while (processQueue.nonEmpty) {
                val (n, t) = processQueue.dequeue()
                data.filter(d => d.to._2 <= t && d.to._2 >= t-maxDelay.timestamp && n == d.to._1).foreach {d =>
                  processQueue.enqueue((d.from._1, d.from._2))
                  currentData += d
                }
              }


              NeuronDependenceTikz.export(
                outputFilename + "_" + i,
                None,
                n2s3.layers.map(_.neuronPaths.filter(n1 => currentData.exists(d1 => d1.from._1 == n1 || d1.to._1 == n1))).filter(_.nonEmpty),
                currentData,
                maxDelay,
                Seq((neuron, timestamp))
              )

              i += 1
            }
          }
        }
        sender ! Done

      case Result =>
        sender ! new Result(results)
      case m => throw new RuntimeException("Unrecognized message : "+m)
    }
  }

  val actor = n2s3.system.actorOf(Props(new NeuronDependenceMonitor(n2s3, maxDelay, processCondition, outputFilename)), LocalActorDeploymentStrategy)

  def print() : Unit = {
    implicit val timeout = Config.longTimeout
    Await.result(actor ? Print, timeout.duration)
  }

  def getResult : Result = {
    implicit val timeout = Config.longTimeout
    Await.result(actor ? Result, timeout.duration).asInstanceOf[Result]
  }

  def destroy() : Unit = {
    implicit val timeout = Config.longTimeout
    Await.result(actor ? Close, timeout.duration)
  }

}
