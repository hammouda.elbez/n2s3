package fr.univ_lille.cristal.emeraude.n2s3.core.event

import fr.univ_lille.cristal.emeraude.n2s3.core.NetworkEntityPath
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp

/**
  * Created by pfalez on 04/04/17.
  */
case class NeuronPotentialResponse(timestamp : Timestamp, source : NetworkEntityPath, value : Float) extends TimedEventResponse
object NeuronPotentialUpdateEvent extends TimedEvent[NeuronPotentialResponse]