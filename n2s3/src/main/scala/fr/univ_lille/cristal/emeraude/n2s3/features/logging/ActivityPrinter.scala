package fr.univ_lille.cristal.emeraude.n2s3.features.logging

import java.io.{DataOutputStream, FileOutputStream}

import akka.actor.{ActorRef, Props}
import fr.univ_lille.cristal.emeraude.n2s3.core.{ExternalSender, NetworkActor, NetworkEntityPath}
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.{Config, Done}
import fr.univ_lille.cristal.emeraude.n2s3.core.event._
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.{N2S3, NeuronGroupObserverRef, NeuronGroupRef}
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.Shape
import fr.univ_lille.cristal.emeraude.n2s3.features.io.persistence.ActivityPersistenceModel
import fr.univ_lille.cristal.emeraude.n2s3.features.io.persistence.ActivityPersistenceModel.{Label, LabelObj, Spike}
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.{LocalActorDeploymentStrategy, Message}
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.N2S3InputStreamCombinators._

import scala.collection.mutable
import scala.concurrent.Await
import akka.pattern.ask
import fr.univ_lille.cristal.emeraude.n2s3.features.logging.ActivityPrinter.{AnswerMessage, GetMessage, PrintMessage}
/**
  * Created by pfalez on 25/09/17.
  */

object ActivityPrinter {
  object PrintMessage extends Message
  object GetMessage extends Message
  case class AnswerMessage(list : Map[NetworkEntityPath, Seq[Timestamp]]) extends Message
}

class ActivityPrinterRef(n2s3 : N2S3, group : NeuronGroupRef) extends NeuronGroupObserverRef {



  var actor: Option[ActorRef] = None
  def getActors : Seq[ActorRef] = this.actor.toSeq


  override def deploy(n2s3: N2S3): Unit = {
    this.actor = Some(n2s3.system.actorOf(Props(new ActivityPrinterActor(n2s3, group)), LocalActorDeploymentStrategy))
  }

  def print(): Unit = {
    implicit val timeout = Config.longTimeout
    Await.result(ask(actor.get, PrintMessage), timeout.duration)
  }

  def get():  Map[NetworkEntityPath, Seq[Timestamp]] = {
    implicit val timeout = Config.longTimeout
    Await.result(ask(actor.get, GetMessage), timeout.duration).asInstanceOf[AnswerMessage].list
  }
}

class ActivityPrinterActor(n2s3 : N2S3, group : NeuronGroupRef) extends NetworkActor {
  val neuronActivity = mutable.HashMap[(Int, String), Map[NetworkEntityPath, mutable.ArrayBuffer[Timestamp]]]()
  var currentEntry : Map[NetworkEntityPath, mutable.ArrayBuffer[Timestamp]] = null


  var startTimestamp : Option[Timestamp] = None

  var currentTimestamp : Timestamp = -1L
  var eventCount : Long = 0


  val buffer : mutable.ArrayBuffer[ActivityPersistenceModel.EventObj] = mutable.ArrayBuffer()
  var startPosition : Long  = 0


  override def initialize(): Unit = {
    askTo(n2s3.inputLayerRef.get.getContainer, SubscribeSynchronized(LabelChangeEvent, ExternalSender.getReference(self), n2s3.buildProperties.getSynchronizerPolicy.getInputSynchronizer))

    group.neuronPaths.map(p => askFuture(p, SubscribeSynchronized(NeuronFireEvent, ExternalSender.getReference(self), n2s3.buildProperties.getSynchronizerPolicy.getGlobalSynchronizer)))
      .foreach(f => Await.result(f, Config.longTimeout.duration))

    group.neuronPaths
  }

  override def destroy(): Unit = {
    askTo(n2s3.inputLayerRef.get.getContainer, Unsubscribe(LabelChangeEvent, ExternalSender.getReference(self)))

    group.neuronPaths.map(p => askFuture(p, Unsubscribe(NeuronFireEvent, ExternalSender.getReference(self)))).foreach(f => Await.result(f, Config.longTimeout.duration))
  }

  override def process(message: Message, sender: ActorRef): Unit = message match {
    case SynchronizedEvent(s, _, m, _) =>
      m match {
        case LabelChangeResponse(start, end, label) =>
          currentEntry = group.neuronPaths.map(_ -> mutable.ArrayBuffer[Timestamp]()).toMap
          neuronActivity += (neuronActivity.size, label) -> currentEntry


        case NeuronFireResponse(t, source) =>
          currentEntry(source) += t
      }
      ExternalSender.sendTo(s, Done)
    case PrintMessage =>
      neuronActivity.toSeq.sortBy(_._1._1).foreach{ case((i, label), list) =>
          println("#"+i+" : "+label)
          list.foreach{ case(p, ts) =>
            println("\t"+p+" : ["+ts.sorted.mkString(", ")+"]")
          }

      }
      sender ! Done
    case GetMessage =>
      sender ! AnswerMessage(neuronActivity.head._2)

  }
}