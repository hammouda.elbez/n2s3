package fr.univ_lille.cristal.emeraude.n2s3.features.builder

/**
  * Created by guille on 3/15/17.
  */
abstract class N2S3Simulation() extends App {
  var n2s3 = new N2S3()

  def prepareTesting()
  def test(): Unit = {
    println("Start Testing...")
    this.prepareTesting()
    n2s3.runAndWait()
  }

  def prepareTraining()
  def train(): Unit = {
    println("Start Training...")
    this.prepareTraining()
    n2s3.runAndWait()
  }

  def setUp(): Unit = {}
  def tearDown(): Unit = {}
  def simulationPhases(): Unit = {}

  this.setUp()
  this.simulationPhases()
  this.tearDown()
  System.exit(0)
}
