package fr.univ_lille.cristal.emeraude.n2s3.core.models.properties

import fr.univ_lille.cristal.emeraude.n2s3.core.ConnectionProperty

/**
  * Created by falezp on 25/10/16.
  */

/**
  * Property used to represent weight of a synapse
  * @tparam T is the synapse weight value type
  */
class SynapticWeight[T] extends ConnectionProperty[Float]

object SynapticWeightFloat extends SynapticWeight[Float]