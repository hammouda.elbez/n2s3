package fr.univ_lille.cristal.emeraude.n2s3.features.logging.graph

import java.awt.image.BufferedImage
import java.awt.{Color, Dimension, FlowLayout, Graphics}
import java.io.File
import javax.imageio.ImageIO
import javax.swing.{JFrame, JLabel, JPanel, WindowConstants}

import akka.actor.{ActorRef, Props}
import akka.util.Timeout
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.Done
import fr.univ_lille.cristal.emeraude.n2s3.core.event._
import fr.univ_lille.cristal.emeraude.n2s3.core.{ExternalSender, NetworkActor, NetworkEntityPath}
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.{N2S3, NeuronGroupObserverRef, NeuronGroupRef}
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.{LocalActorDeploymentStrategy, Message}

import scala.collection.mutable
import scala.concurrent.duration._
/**
  * Created by falezp on 27/02/17.
  */



class ActivityGraphRef(tuples: Seq[(NeuronGroupRef, Int, Int)]) extends NeuronGroupObserverRef {

  var actor: Option[ActorRef] = None
  def getActors : Seq[ActorRef] = this.actor.toSeq


  override def deploy(n2s3: N2S3): Unit = {
    this.actor = Some(n2s3.system.actorOf(Props(new ActivityGraphActor(n2s3, tuples)), LocalActorDeploymentStrategy))
  }
}


class ActivityGraphActor(n2s3 : N2S3, tuples: Seq[(NeuronGroupRef, Int, Int)]) extends NetworkActor {

  implicit val timeout = Timeout(1000 seconds)



  var graph : ActivityGraph =  new ActivityGraph(tuples.map(each => (each._1.getIdentifier, each._2, each._1.neuronPaths, each._3)))
  var hasRequestDisplay = false

  override def initialize(): Unit = {
    tuples.foreach{ case (group, _, _) =>
      group.neuronPaths.foreach{ path =>
        ExternalSender.sendTo(path,  Subscribe(NeuronFireEvent, ExternalSender.getReference(self)))
      }
    }
    ExternalSender.askTo(n2s3.inputLayerRef.get.getContainer, SubscribeSynchronized(LabelChangeEvent,
      ExternalSender.getReference(self), n2s3.buildProperties.getSynchronizerPolicy.getInputSynchronizer))
  }


  override def destroy(): Unit = {
    tuples.foreach{ case (group, _, _) =>
      group.neuronPaths.foreach{ path =>
        ExternalSender.sendTo(path,  Unsubscribe(NeuronFireEvent, ExternalSender.getReference(self)))
      }
    }
    ExternalSender.askTo(n2s3.inputLayerRef.get.getContainer,
      Unsubscribe(LabelChangeEvent, ExternalSender.getReference(self)))
    graph.destroy()
  }

  override def process(message: Message, sender : ActorRef): Unit = message match {
    case NeuronFireResponse(timestamp, path) =>
      graph.spikeEvent(timestamp, path)
    case SynchronizedEvent(s, _, LabelChangeResponse(start, end, _), _) =>
      graph.labelChangeEvent(start, end)
      ExternalSender.sendTo(s, Done)
    case m => println("[ActivityGraphActor] Unknown message " + m)
  }

}
class ActivityGraph(rawLayers : Seq[(String, Int, Seq[NetworkEntityPath], Int)]) {

  var currentInterval : Option[(Timestamp, Timestamp)] = None
  val futureBuffer = mutable.ArrayBuffer[(Timestamp, NetworkEntityPath)]()

  val frame = new JFrame("Network Activity")

  val pan = new JPanel

  pan setLayout new FlowLayout

  frame add pan

  val layers = Map(rawLayers.map{ case (name, width, actors, caseSize) =>
    val panel = new ActivityPanel(width, actors, caseSize)
    pan add new JLabel(name)
    pan add panel
    name -> panel
  }:_*)


  val neuronMapping = Map(rawLayers.flatMap{ case (name, width, actors, caseSize) =>
    actors.map(key => (key, layers(name)))
  }:_*)

  frame setLocation (100, 100)

  frame setVisible true

  frame pack

  frame setDefaultCloseOperation WindowConstants.EXIT_ON_CLOSE

  def labelChangeEvent(start : Timestamp, end : Timestamp) : Unit = {
    if(currentInterval.isDefined) {
      layers.foreach(_._2.resetSpike())
    }

    currentInterval = Some(start -> end)

    val (curr, other) = futureBuffer.partition(e => e._1 >= currentInterval.get._1 && e._1 <= currentInterval.get._2)
    curr.foreach(c => spikeEvent(c._1, c._2))

    futureBuffer.clear()
    futureBuffer ++= other.filter(e => e._1> currentInterval.get._2)
  }

  def spikeEvent(timestamp : Timestamp, sender : NetworkEntityPath) : Unit = {
    if (currentInterval.isEmpty || (timestamp >= currentInterval.get._1 && timestamp <= currentInterval.get._2)) {
      neuronMapping.get(sender) match {
        case Some(panel) =>
          panel.addSpike(timestamp, sender)
        case None => throw new RuntimeException("Unable to find entry")
      }
    }
    else if(timestamp > currentInterval.get._2) {
      //println("[ActivityGraph] warning : spike ("+timestamp+", "+sender+") out of interval ["+currentInterval.get._1+";"+currentInterval.get._2+"]")
      futureBuffer += timestamp -> sender
    } else {
      println("[ActivityGraph] warning : spike ("+timestamp+", "+sender+") out of interval ["+currentInterval.get._1+";"+currentInterval.get._2+"]")
    }
  }

  def destroy() : Unit = {
    layers.foreach(_._2.destroy())
  }

}

class ActivityPanel(val layerWidth: Int, val sources : Seq[NetworkEntityPath], val neuronSize: Int = 25) extends JPanel {
  private val mapper = Map(sources.zipWithIndex:_*)

  private val pixels = Array.ofDim[Int](mapper.size)

  resetSpike(true)

  setPreferredSize(new Dimension(neuronSize * layerWidth + 1, neuronSize * math.ceil(mapper.size.toFloat/layerWidth.toFloat).toInt + 1))
  override def paintComponent(g: Graphics): Unit = {
    g.setColor(Color.black)
    g.fillRect(0, 0, layerWidth * neuronSize, math.ceil(mapper.size.toFloat/layerWidth.toFloat).toInt * neuronSize)

    val max_v = pixels.max

    pixels.zipWithIndex.foreach{ case (value, index) =>
      val c = if(value <= 0) {
        0
      }
      else {
        val v = value/math.max(1.0f, max_v)
        val rc = (math.max(0f, -2f*v*v+4f*v-1f)*255f).toInt
        val bc = (math.max(0f, -2f*v*v+1)*255f).toInt
        val gc = (math.max(0f, 4f*v*(-v+1f))*255f).toInt
        (rc << 16) + (gc << 8) + bc
      }
      g.setColor(new Color(c))
      g.fillRect((index%layerWidth) * neuronSize, (index/layerWidth) * neuronSize, neuronSize, neuronSize)
    }
  }

  def addSpike(timestamp : Timestamp, sender : NetworkEntityPath) : Unit = {
    val index = mapper(sender)
    pixels(index) += 1
  }

  def print() : Unit = {

    //return

    val image = new BufferedImage(layerWidth * neuronSize, (sources.size / layerWidth) * neuronSize, BufferedImage.TYPE_INT_RGB)
    pixels.indices.foreach { i =>

      val value = pixels(i)
      val max_v = pixels.max.toDouble

      val c = if(value <= 0) {
        (1f*255f).toInt << 16 | (1f*255f).toInt << 8 | (1f*255f).toInt << 0
      }
      else {
        val v = value.toDouble/math.max(1.0f, max_v)
        val rc = (math.max(0f, -2f*v*v+4f*v-1f)*255f).toInt
        val bc = (math.max(0f, -2f*v*v+1)*255f).toInt
        val gc = (math.max(0f, 4f*v*(-v+1f))*255f).toInt
        (rc*255f).toInt << 16 | (bc*255f).toInt << 8 | (gc*255f).toInt << 0
      }

      for {
        x <- 0 until neuronSize
        y <- 0 until neuronSize
      } {
        image.setRGB((i % layerWidth) * neuronSize+x, (i / layerWidth) * neuronSize+y, c)
      }
    }

    val fileNumber = new File("tmp/").listFiles().length

    ImageIO.write(image, "png", new File("tmp/"+sources.head.actor.path.name+"_"+fileNumber+".png"))
  }

  def resetSpike(initialize : Boolean = false) : Unit = {
    if(!initialize)
      print()

    repaint()
    for (i <- pixels.indices)
      pixels(i) = 0
  }

  def destroy() : Unit = {
    repaint()
    print()
  }
}
