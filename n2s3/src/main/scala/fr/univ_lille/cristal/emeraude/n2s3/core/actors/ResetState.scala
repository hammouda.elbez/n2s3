package fr.univ_lille.cristal.emeraude.n2s3.core.actors

/**
  * Created by pfalez on 10/05/17.
  */
case object ResetState extends SyncMessage
