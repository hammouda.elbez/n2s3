package fr.univ_lille.cristal.emeraude.n2s3.features.logging

import java.io.{File, PrintWriter}

import fr.univ_lille.cristal.emeraude.n2s3.core.NetworkEntityPath
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.N2S3
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp

/**
  * Created by falezp on 12/12/16.
  */
class ExportNetworkTopologyChange(n2S3: N2S3, filename : String) {

  val writer = new PrintWriter(new File(filename))

  def formatName(path : NetworkEntityPath) : String = {
    "[:*/]".r.replaceAllIn(path.toString, "")
  }

  def next(fires : Map[NetworkEntityPath, Seq[Timestamp]]) : Unit = {
    /*
    writer.println("\\begin{tikzpicture}[")
    writer.println("\tneuron/.style={circle,draw=black}")
    writer.println("]")

    writer.println("\\begin{axis}[")
    writer.println("\tpoint meta=explicit,")
    writer.println("\txlabel=Time (ms),")
    writer.println("\txmin = 0,")
    writer.println("\txmax = "+((max_x-min_x).toDouble/1e3)+",")
    writer.println("\tymin="+formatName(neurons.head.head)+",")
    writer.println("\tymax="+formatName(neurons.last.last)+",")
    writer.println("\tenlarge x limits=0.01,")
    writer.println("\tenlarge y limits=0.03,")
    writer.println("\tytick={"+neurons.flatten.map(n => formatName(n)).mkString(",")+"},")
    writer.println("\tsymbolic y coords={minTick,"+neurons.flatten.map(n => formatName(n)).mkString(",")+",maxTick}")
    writer.println("]")

    n2S3.layers.zipWithIndex.foreach{ case(layer, layer_index) =>
      layer.neuronPaths.zipWithIndex.foreach{ case(neuron, neuron_index) =>
        writer.println("node[neuron] ("+formatName(neuron)+") at ("+layer_index+","+neuron_index+") {N"+neuron_index+"};")
      }
    }

    n2S3.layers.foreach{ layer =>
      layer.neuronPaths.foreach{ neuron =>
        writer.println("node[neuron] ("+formatName(neuron)+") at ("+layer_index+","+neuron_index+") {N"+neuron_index+"};")
      }

    }

    writer.println("\\end{tikzpicture}")


    writer.flush()*/
  }

}
