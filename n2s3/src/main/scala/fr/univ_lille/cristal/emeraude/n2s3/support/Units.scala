package fr.univ_lille.cristal.emeraude.n2s3.support

import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias._

object UnitTime extends Enumeration {
  type TimeUnitType = Value
  val Second, MilliSecond, MicroSecond = Value

  def convert(unit : TimeUnitType, t : Time) : Int = {
    unit match  {
      case Second =>  (t.timestamp / 1e6).toInt
      case MilliSecond => (t.timestamp / 1e3).toInt
      case MicroSecond =>  t.timestamp.toInt
    }
  }
}

/**********************************************************************************************************
  * Time Unit
  * basic time unit is microsecond
  **********************************************************************************************************/
case class Time(timestamp : Timestamp) {
  def *(n : Timestamp) : Time = Time(n*timestamp)
  def *(v : Double) : Time = Time((v*timestamp.toDouble).toLong)
  def +(time : Time) : Time = Time(timestamp+time.timestamp)
  def /(n : Float) : Time = Time((timestamp.toDouble/n).toLong)

  def asSecond = timestamp.toFloat/1e6f
  def asMilliSecond = timestamp.toFloat/1e3f
  def asMicroSecond = timestamp.toFloat
}

class TimeUnit(val value : Double) {
  def Second = Time((value*1e6).toLong)
  def MilliSecond = Time((value*1e3).toLong)
  def MicroSecond = Time(value.toLong)
}

object UnitCast {
  implicit def timeCast(value : Double) : TimeUnit = new TimeUnit(value.toFloat)
  implicit def timeCast(value : Float) : TimeUnit = new TimeUnit(value)
  implicit def timeCast(value : Int) : TimeUnit = new TimeUnit(value)
}

object FormatTimestamp {
  def format(timestamp: Timestamp) : String = {
    var str = if(timestamp%1000 != 0 || timestamp == 0) (timestamp%1000)+"us" else ""

    val ms = timestamp/1000
    str = if(ms!= 0.0) (ms%1000)+"ms"+(if(str != "") " "+str else "") else str

    val s = ms/1000
    str = if(s != 0.0) s+"s"+(if(str != "") " "+str else "")  else str

    str
  }
}