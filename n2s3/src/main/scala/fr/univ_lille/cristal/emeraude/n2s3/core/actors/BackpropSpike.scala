package fr.univ_lille.cristal.emeraude.n2s3.core.actors

import squants.electro.ElectricCharge

/**
  * Created by pfalez on 12/12/17.
  */
case class BackpropSpike(charge : ElectricCharge) extends Spike