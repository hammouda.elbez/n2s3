package fr.univ_lille.cristal.emeraude.n2s3.features.io.input

/**
  * Created by pfalez on 24/05/17.
  */

object ReshapeSample {
  def apply[T, ST <: InputSample[T]](newShape : Shape, fillWith : T) = new ReshapeSample[T, ST](newShape, fillWith)
}

class ReshapeSample[T, ST <: InputSample[T]](val newShape : Shape, val fillWith : T) extends StreamConverter[ST, ST] {

    override def shapeConverter(in : Shape) : Shape = newShape

    def dataConverter(in: ST): ST = {
      in.setAllDataWithShape(newShape, for(index <- newShape.allIndex) yield {
        index -> (if(in.getShape.belongTo(index:_*))  in.getDataOnChannel(index:_*) else fillWith)
      })
    }

  override def resetConverter(): Unit = {}
}