/**************************************************************************************************
 * Contributors:
 * 		- created by bdanglot
 *    - (add your name here if you contribute to this code). 
 **************************************************************************************************/
package fr.univ_lille.cristal.emeraude.n2s3.features.logging.graph

import java.awt._
import java.awt.event.{ActionEvent, ActionListener}
import javax.swing.{JFrame, JPanel, SwingUtilities, Timer}

import akka.actor.SupervisorStrategy.Stop
import akka.actor._
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.{Config, Done, Initialize}
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.SynapticWeightFloat
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.{N2S3, NeuronGroupRef}
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.{LocalActorDeploymentStrategy, PropsBuilder}

import scala.collection.mutable

class SynapsesWeightGraphBuilder() {
  var n2s3: N2S3 = _
  var ignoreLeft = 0
  var visualizationWidth: Int = 0
  var layerToObserve: NeuronGroupRef = _
	var viewPanel : JPanel = _

	var caseWidth = 8
	var caseHeight = 8
	var refreshRate = 1000/24

	def setViewPanel(viewPanel : JPanel) = {
    this.viewPanel = viewPanel
    this
  }
	
  def setN2S3(n2s3: N2S3) ={
    this.n2s3 = n2s3
    this
  }

  def setLayerToObserve(neuronGroup: NeuronGroupRef) = {
    this.layerToObserve = neuronGroup
    this
  }

  def setVisualizationWidth(width: Int) = {
    this.visualizationWidth = width
    this
  }

  def setIgnoreLeft(i: Int) = {
    this.ignoreLeft = i
    this
  }

	def setCaseDimension(width : Int, height : Int) = {
		this.caseWidth = width
		this.caseHeight = height
		this
	}

	def setRefreshRate(refreshRate : Int) : this.type = {
		this.refreshRate = refreshRate
		this
	}

  def build() = {
    this.n2s3.system.actorOf(new PropsBuilder() {
      override def build(): Props = {
        Props(new SynapsesWeightGraphActor(
          for (neuron <- layerToObserve.neuronPaths) yield {(neuron, visualizationWidth)},
          ignoreLeft, caseWidth, caseHeight, refreshRate, viewPanel))
      }
    }, LocalActorDeploymentStrategy)
  }
}

class SynapsesWeightGraphActor(outputs : Seq[(NetworkEntityPath, Int)], 
                               numberToIgnoreLeft: Int = 0, 
                               caseWidth : Int = 8, caseHeight : Int = 8, 
                               refreshRate : Int = 1000/24, viewPanel : JPanel = null) extends Actor {

	implicit val timeout = Config.defaultTimeout

	val views = mutable.Map[NetworkEntityPath, SynapsesWeight2DPixel]()
	var frame : Container = viewPanel
		
	SwingUtilities.invokeAndWait(new Runnable() {
		def run() {
		  if(frame == null){
		    frame = new JFrame()  
		    frame.setLayout(new FlowLayout)
		  }
		  
		  outputs.foreach { case (path, width) =>
				val aSynapseWeight2DPixel = new SynapsesWeight2DPixel(width, caseWidth, caseHeight)
				views. += (path -> aSynapseWeight2DPixel)
				frame.add(aSynapseWeight2DPixel)
			}

			frame.setVisible(true)
			frame.setSize(new Dimension(800, 1000))
		}
	}
	)

	val delay = refreshRate/outputs.size
	var currentN = 0
	val taskPerformer = new ActionListener() {
		override def actionPerformed(actionEvent: ActionEvent): Unit = {
			update(currentN)
			currentN = (currentN+1)%outputs.size
		}
	}
	val timer = new Timer(delay, taskPerformer)
	timer.start()

	def update(n : Int): Unit = {
		views.toSeq(n) match { case (path, view) =>
			ExternalSender.askTo(path, GetAllConnectionProperty(SynapticWeightFloat)) match {
				case ConnectionPropertyValues(values) =>
					view.updateValue(values.sortBy(_._1).map(_._3.asInstanceOf[Float]).takeRight(values.size - numberToIgnoreLeft))
					SwingUtilities.invokeLater(new Runnable() {
						def run() {
							view.repaint()
							frame.revalidate()
						}
					})

				case other => throw new Exception("Unknown " + other)
			}
		}
	}

	def receive = {
		case Initialize =>
			sender ! Done
		case Stop =>
      views.values.foreach(v => v.destroy())
			timer.stop()
			sender ! Done
			context.stop(self)
		case m => println("[SynapsesWeightGraphActor] Unknown message "+m)
	}
}

class SynapsesWeight2DPixel(layerWidth : Int = 0, caseWidth : Int = 1, caseHeight : Int = 1) extends JPanel {
	var hasValidValues = false 
	var isInited = false
	var colorMap = Array.ofDim[Int](1, 1)

	def updateValue(values : Seq[Float]) = {
		hasValidValues = true
		val width = math.min(layerWidth, values.size)
		val height = Math.ceil(values.size.asInstanceOf[Float] / layerWidth).asInstanceOf[Int]
		colorMap = Array.ofDim[Int](width, height)

		val min_v = values.min
		val max_v = values.max

		values.zipWithIndex.foreach { case (value, index) =>
			val color = if(value < 0) {
				val v = ((value/math.min(-1.0f, min_v))*255f).toInt
				(v << 16) + (v << 8) + v
			}
			else {
				val v = value/math.max(1.0f, max_v)
				val rc = (math.max(0f, -2f*v*v+4f*v-1f)*255f).toInt
				val bc = (math.max(0f, -2f*v*v+1)*255f).toInt
				val gc = (math.max(0f, 4f*v*(-v+1f))*255f).toInt
				(rc << 16) + (gc << 8) + bc
			}
			colorMap(index%layerWidth)(index/layerWidth) = color
		}

		if(!isInited){
			val d = new Dimension(width * caseWidth, height * caseHeight)
			setPreferredSize(d)
			setSize(d)
			isInited=true
		}
	}

	override def paintComponent(g : Graphics) : Unit = {
	  if(!hasValidValues){
		    return 
		}

	  for((line, x) <- colorMap.zipWithIndex) {
			for((v, y) <- line.zipWithIndex) {
				g.setColor(new Color(v))
				g.fillRect(x * caseWidth, y * caseHeight, caseWidth, caseHeight)
			}
		}
  }

  def destroy() : Unit = {

  }
}
