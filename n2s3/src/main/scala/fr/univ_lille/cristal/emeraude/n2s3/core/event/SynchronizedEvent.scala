package fr.univ_lille.cristal.emeraude.n2s3.core.event

import fr.univ_lille.cristal.emeraude.n2s3.core.Synchronizer.SynchronizedMessage
import fr.univ_lille.cristal.emeraude.n2s3.core.{NetworkEntityPath, NetworkEntityReference}
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message

/**
  * Created by guille on 10/20/16.
  */
case class SynchronizedEvent(synchronizer : NetworkEntityPath, override val postSync : NetworkEntityReference, override val message : Message, override val timestamp : Timestamp) extends SynchronizedMessage