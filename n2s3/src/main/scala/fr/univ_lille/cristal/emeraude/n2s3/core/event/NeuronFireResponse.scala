package fr.univ_lille.cristal.emeraude.n2s3.core.event

import fr.univ_lille.cristal.emeraude.n2s3.core.NetworkEntityPath
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp

/********************************************************************************************************
 *  Event triggered when a neuron fire
 *******************************************************************************************************/

case class NeuronFireResponse(timestamp : Timestamp, source : NetworkEntityPath) extends TimedEventResponse