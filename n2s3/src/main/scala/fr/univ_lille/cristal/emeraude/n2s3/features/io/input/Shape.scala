package fr.univ_lille.cristal.emeraude.n2s3.features.io.input

import scala.annotation.tailrec

/**
  * Created by falezp on 06/07/16.
  */

case class Shape(dimensions : Int*) {

  def dimensionNumber = dimensions.size

  def isEmpty = dimensions.isEmpty
  def nonEmpty = dimensions.nonEmpty

  def tail : Shape = Shape(dimensions.tail:_*)
  def head : Int = dimensions.head

  def product : Int = dimensions.product

  def indices = dimensions.indices
  def drop(n : Int) = Shape(dimensions.drop(n):_*)

  def apply(n : Int) = dimensions(n)

  def belongTo(index : Int*) : Boolean = {
    dimensions.size == index.size && dimensions.zip(index).forall{case(d, i) => i >= 0 && i < d}
  }

  /*
   * Operator
   */
  def *(n : Int) = Shape(dimensions.map(_*n):_*)
  def /(n : Int) = Shape(dimensions.map(_/n):_*)

  def getNumberOfPoints : Int = dimensions.product

  def toIndex(index : Int*) : Int = {
    if(index.size != dimensionNumber || !dimensions.zip(index).forall{case (d, i) => i >= 0 && i < d})
      throw new RuntimeException("Invalid index : ["+index.mkString(";")+"] for shape "+toString)
    @tailrec
    def inner(remainDimension : Seq[Int], remainIndex : Seq[Int], acc : Int) : Int = {
      if(remainDimension.nonEmpty && remainIndex.nonEmpty)
        inner(remainDimension.tail, remainIndex.tail, acc+remainIndex.head*remainDimension.tail.product)
      else
        acc
    }
    inner(dimensions, index, 0)
  }

  def fromIndex(index : Long) : Seq[Int] = {

    @tailrec
    def inner(remainDimension : Seq[Int], remainIndex : Long, acc : Seq[Int]) : Seq[Int] = {
      if(remainDimension.nonEmpty) {
        val product = remainDimension.tail.product
        val currentIndex = remainIndex / product
        inner(remainDimension.tail, remainIndex-currentIndex*product, acc++Some(currentIndex.toInt))
      }
      else
        acc
    }

    inner(dimensions, index, Seq())
  }

  def allIndex : Seq[Seq[Int]] = {
    def inner(remainDimension : Seq[Int], computedIndex : Seq[Int]) : Seq[Seq[Int]] = {
      if(remainDimension.isEmpty)
        Seq(computedIndex)
      else
        (0 until remainDimension.head).flatMap(i => inner(remainDimension.tail, computedIndex++Some(i)))
    }
    inner(dimensions, Seq())
  }

  def transpose : Shape = Shape(dimensions.reverse:_*)

  override def toString = "("+dimensions.mkString(", ")+")"
}
