package fr.univ_lille.cristal.emeraude.n2s3.core.event

import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message

/*********************************************************************************************************
 * List of event which can be trigger by neuron
 * Event can be added by an entity which inherited from Observable trait.
 * Each event carry a message which it send when the event is triggered by the observable entity
 ********************************************************************************************************/

abstract class EventResponse extends Message
