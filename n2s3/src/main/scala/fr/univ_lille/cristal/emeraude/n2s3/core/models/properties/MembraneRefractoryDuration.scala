package fr.univ_lille.cristal.emeraude.n2s3.core.models.properties

import fr.univ_lille.cristal.emeraude.n2s3.core.Property
import fr.univ_lille.cristal.emeraude.n2s3.support.Time

/**
  * Created by falezp on 25/10/16.
  */

/**
  * Property used to represent membrane refractory duration
  */
object MembraneRefractoryDuration extends Property[Time]
