package fr.univ_lille.cristal.emeraude.n2s3.features.io.input

import fr.univ_lille.cristal.emeraude.n2s3.core.actors.ElectricSpike
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.PoissonDistribution
import squants.time.Frequency

import scala.collection.mutable

/**
  * Created by falezp on 05/07/16.
  */

// TODO adapt to new system...
/*
  InputNoiseGenerator
 */

object InputNoiseGenerator {
  def apply(frequency : Frequency) = new InputNoiseGenerator(frequency)
}

class InputNoiseGenerator[T <: InputTemporalPacket](frequency : Frequency) extends StreamConverter[T, T] {

  val lastTimestamps = mutable.HashMap[Seq[Int], Timestamp]()


  override def dataConverter(in: T): T = {

    val max_timestamp = in.getAllData.flatMap(_._2.map(_.getStartTimestamp)).max

    def recurseShape(remainDim : Seq[Int], index : Seq[Int]): Seq[(Seq[Int], Seq[InputTemporalData])] = {
      if(remainDim.isEmpty) {
        val times = new PoissonDistribution(lastTimestamps(index), max_timestamp, 0f, frequency.toHertz).applyTo(1.0f)
        Seq(index -> times.map(t => N2S3InputSpike(ElectricSpike(), t)))
      }
      else
        (0 until remainDim.head).flatMap { i =>
          recurseShape(remainDim.tail, index++Some(i))
        }
    }

    in.addData(Map(recurseShape(shape.dimensions, Nil):_*))
  }

  def resetConverter() : Unit = {
    lastTimestamps.keys.foreach{ k =>
      lastTimestamps.update(k, 0L)
    }
  }
}

/*
  StreamOversampling
 */

// TODO abstract constructor for packet and type

object StreamSubSampling {
  def average[T](implicit x: scala.math.Fractional[T]) : Seq[T] => T = seq => x.div(seq.sum, x.fromInt(seq.size))
  def maximum[T](implicit x: scala.math.Fractional[T]) : Seq[T] => T = seq => seq.max

  def apply[T, ST <: InputSample[T]](reduce : Seq[T] => T, factors : Int*) = new StreamSubSampling[T, ST](reduce, factors:_*)
}

class StreamSubSampling[T, ST <: InputSample[T]](reduce : Seq[T] => T, factors : Int*) extends StreamConverter[ST, ST] {

  assert(factors.forall(_ > 0))


  override def dataConverter(in: ST): ST = {

    def recurse(remainFactor : Seq[Int], remainSize : Seq[Int], newIndex : Seq[Int], indexList : Seq[Seq[Int]]): Seq[(Seq[Int], T)] = {
      if(remainFactor.isEmpty) {
        Seq(newIndex -> reduce(indexList.map(index => in.getDataOnChannel(index:_*))))
      }
      else {
        (0 until remainSize.head/remainFactor.head).flatMap{ i =>
          recurse(remainFactor.tail, remainSize.tail, newIndex++Some(i),
            if(indexList.isEmpty) {
              (0 until remainFactor.head).map(j => Seq(i * remainFactor.head + j))
            }
            else {
              indexList.flatMap { prefix =>
                (0 until remainFactor.head).map(j => prefix ++ Some(i * remainFactor.head + j))
              }
            })
        }
      }
    }

    in.setAllDataWithShape(shape, recurse(factors, in.getShape.dimensions, Seq(), Seq()))
  }

  override def shapeConverter(shape : Shape): Shape = {
    assert(shape.dimensionNumber == factors.size)
    Shape(shape.dimensions.zip(factors).map{case(a, b) => a/b}:_*)
  }

  override def resetConverter(): Unit = {}
}


object StreamOversampling {
  def apply[T <: InputPacket](factors : Int*) = new StreamOversampling[T](factors:_*)
}

class StreamOversampling[T <: InputPacket](factors : Int*) extends StreamConverter[T, T] {

  assert(factors.forall(_ > 0))


  override def dataConverter(in: T): T = {

    def recurse(remainFactor : Seq[Int], remainIndex : Seq[Int], newIndex : Seq[Seq[Int]], data : in.InputDataType) : Seq[(Seq[Int], in.InputDataType)] = {
      if(remainFactor.isEmpty) {
        newIndex.map(i => i -> data)
      }
      else {
        val ni = (0 until remainFactor.head).map(i => remainIndex.head * remainFactor.head + i)
        recurse(remainFactor.tail, remainIndex.tail,
          if(newIndex.nonEmpty)
            newIndex.flatMap(h => ni.map(i => h++Some(i)))
          else
            ni.map(i => Seq(i))
          ,
          data)
      }
    }

    in.flatMapDataWithShape(shape, { case(index, data) =>
      recurse(factors, index, Seq(), data)
    })
    in
  }

  override def shapeConverter(shape : Shape): Shape = {
    assert(shape.dimensionNumber == factors.size)
    Shape(shape.dimensions.zip(factors).map{case(a, b) => a*b}:_*)
  }

  override def resetConverter(): Unit = {}
}

object StreamReshapeSample {
  def apply[T, ST <: InputSample[T]](offset : Seq[Int], size : Seq[Int]) = new StreamReshapeSample[T, ST](offset, size)
}

class StreamReshapeSample[T, ST <: InputSample[T]](offset : Seq[Int], size : Seq[Int]) extends StreamConverter[ST, ST] {

  override def dataConverter(in: ST): ST = {

    def generate(in : ST, offset : Seq[Int], size : Seq[Int], newIndex : Seq[(Seq[Int], Seq[Int])]): Seq[(Seq[Int], T)] = {
      if(offset.isEmpty && size.isEmpty) {
        newIndex.map{ case(inIndex, outIndex) => outIndex -> in.getDataOnChannel(inIndex:_*)}
      }
      else {
        val nIndex = if(newIndex.isEmpty) {
          (0 until size.head).map(i => Seq(i+offset.head) -> Seq(i))
        }
        else {
          newIndex.flatMap{ case(inIndex, outIndex) =>
            (0 until size.head).map(i => (inIndex++Some(i+offset.head)) -> (outIndex++Some(i)))
          }
        }
        generate(in, offset.tail, size.tail, nIndex)
      }
    }
    in.setAllDataWithShape(shape, generate(in, offset, size, Seq()))
  }

  override def shapeConverter(shape : Shape): Shape = {
    Shape(size:_*)
  }

  override def resetConverter(): Unit = {}
}