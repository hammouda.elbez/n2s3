package fr.univ_lille.cristal.emeraude.n2s3.features.io.persistence

import java.io.{DataInputStream, DataOutputStream}

import fr.univ_lille.cristal.emeraude.n2s3.core.actors.ElectricSpike
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.{N2S3InputData, N2S3InputLabel, N2S3InputMetaData, N2S3InputSpike}
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp

/**
  * Created by pfalez on 26/04/17.
  */
object ActivityPersistenceModel {

  object EventFactory {

    val classes : Seq[Event] = Seq(Spike, Label)

    // Computed
    val (metaDataClasses, dataClasses) = classes.partition(_.isMetaData)
    val metaDataClassesAssoc : Map[Byte, MetaDataEvent] = metaDataClasses.map(c => c.getID -> c.asInstanceOf[MetaDataEvent]).toMap
    val dataClassesAssoc : Map[Byte, DataEvent] = dataClasses.map(c => c.getID -> c.asInstanceOf[DataEvent]).toMap

    def createMetaData(in : DataInputStream, timestamp : Timestamp) : N2S3InputMetaData = {
      val id = in.readByte()
      metaDataClassesAssoc(id).readFrom(in, timestamp)
    }

    def createData(in : DataInputStream, timestamp : Timestamp) : (Long, N2S3InputData) = {
      val id = in.readByte()
      dataClassesAssoc(id).readFrom(in, timestamp)
    }

  }

  abstract class Event(id : Byte) {
    def getID : Byte = id
    def isMetaData : Boolean
  }

  abstract class EventObj(id : Byte) extends Event(id) {
    def write(out : DataOutputStream)
    def writeTo(out : DataOutputStream) : Unit = {
      out.writeByte(id)
      write(out)
    }
  }

  abstract class MetaDataEvent(id : Byte) extends Event(id) {
    override def isMetaData : Boolean = true
    def readFrom(in : DataInputStream, timestamp : Timestamp) : N2S3InputMetaData
  }

  abstract class DataEvent(id : Byte) extends Event(id) {
    override def isMetaData : Boolean = false
    def readFrom(in : DataInputStream, timestamp : Timestamp) : (Long, N2S3InputData)
  }

  abstract class MetaDataEventObj(id : Byte) extends EventObj(id) {
    override def isMetaData : Boolean = true
  }

  abstract class DataEventObj(id : Byte) extends EventObj(id) {
    override def isMetaData : Boolean = false
  }

  case class SpikeObj(index : Long) extends DataEventObj(0x01) {
    override def write(out: DataOutputStream): Unit = {
      out.writeLong(index)
    }
  }
  object Spike extends DataEvent(0x01) {

    def create(index : Long) : SpikeObj = {
      SpikeObj(index)
    }

    override def readFrom(in : DataInputStream, timestamp : Timestamp) : (Long, N2S3InputData) = {
      val index = in.readLong()
      index -> N2S3InputSpike(ElectricSpike(), timestamp)
    }
  }

  case class LabelObj(end : Timestamp, name : String) extends MetaDataEventObj(0x11) {
    override def write(out: DataOutputStream): Unit = {
      out.writeLong(end)
      out.writeUTF(name)
    }
  }

  object Label extends MetaDataEvent(0x11) {

    def create(end : Timestamp, name : String) : LabelObj = {
      LabelObj(end, name)
    }

    override def readFrom(in : DataInputStream, timestamp : Timestamp) : N2S3InputMetaData = {
      val end =  in.readLong()
      val label = in.readUTF()
      N2S3InputLabel(label, timestamp, end)
    }
  }

}
