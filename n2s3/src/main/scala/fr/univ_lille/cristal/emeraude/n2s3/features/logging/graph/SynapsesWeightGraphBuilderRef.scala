package fr.univ_lille.cristal.emeraude.n2s3.features.logging.graph

import javax.swing.JPanel

import akka.actor.ActorRef
import fr.univ_lille.cristal.emeraude.n2s3.features.builder._

/**
  * Created by guille on 6/2/16.
  */
class SynapsesWeightGraphBuilderRef(fromLayer: NeuronGroupRef, toLayer: NeuronGroupRef, viewPanel: JPanel = null) extends NeuronGroupObserverRef {

// assert(fromLayer.shape.dimensionNumber == 1 || fromLayer.shape.dimensionNumber == 2)

  var caseWidth = 5
  var caseHeight = 5
  var refreshRate = 1000/24
  var width : Option[Int] = None

  var actor: Option[ActorRef] = None
  def getActors : Seq[ActorRef] = this.actor.toSeq


  def setCaseDimension(width : Int, height : Int) : this.type = {
    this.caseWidth = width
    this.caseHeight = height
    this
  }

  def setRefreshRate(refreshRate : Int) : this.type = {
    this.refreshRate = refreshRate
    this
  }

  def setWidth(width : Int) : this.type = {
    this.width = Some(width)
    this
  }

  override def deploy(n2S3: N2S3): Unit = {
    this.actor = Some(new SynapsesWeightGraphBuilder()
      .setN2S3(n2S3)
      .setLayerToObserve(toLayer.getActor)
      .setVisualizationWidth(width.getOrElse(fromLayer.shape.head))
      .setCaseDimension(this.caseWidth, this.caseHeight)
      .setRefreshRate(refreshRate)
      .setViewPanel(this.viewPanel)
      .build())
  }
}
