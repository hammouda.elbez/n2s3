package fr.univ_lille.cristal.emeraude.n2s3.features.learning

import fr.univ_lille.cristal.emeraude.n2s3.core.Synchronizer.{Next, WaitEndOfActivity}
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.{AskRemainInput, SetInput, WrapMessage}
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.{SynapseWeightAndDelay, SynapticWeightFloat}
import fr.univ_lille.cristal.emeraude.n2s3.core.{NetworkEntityPath, _}
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.N2S3
import fr.univ_lille.cristal.emeraude.n2s3.features.logging.{LabelMonitor, SpikeMonitor}
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias._
import fr.univ_lille.cristal.emeraude.n2s3.support.Time
import fr.univ_lille.cristal.emeraude.n2s3.support.UnitCast._

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import scala.concurrent.duration._
import scala.language.postfixOps
/**
  * Created by falezp on 19/10/16.
  */

/*
 *  From "SpikeProp: backpropagation for networks of spiking neurons."
 *  http://homepages.cwi.nl/~sbohte/publication/backprop.pdf
 */
object SpikeProp {

  def execute(n2s3 : N2S3, outputs : Map[String, NetworkEntityPath], learningRate : Double, spikeResponse : Long => Double, spikeResponseDerivative : Long => Double) : Unit = {

    def timestampOfNeuron(spikes : Seq[(NetworkEntityPath, Timestamp)], neuron : NetworkEntityPath) : Long = {
      /* println(spikes)
      println(neuron)
      println(spikes.filter(_._1 == neuron))*/
      spikes.filter(_._1 == neuron) match {
        case s if s.isEmpty => spikes.maxBy(_._2)._2+(20 MilliSecond).timestamp//tmp Long.MaxValue
        case other => other.minBy(_._2)._2
      }
    }

    ExternalSender.sendTo(n2s3.buildProperties.getSynchronizerPolicy.getInputSynchronizer, SetInput(n2s3.inputLayerRef.get.actorPath.get))

    val spikeMonitor = new SpikeMonitor(n2s3, n2s3.layers.flatMap(_.neuronPaths))
    val labelMonitor = new LabelMonitor(n2s3, n2s3.inputLayerRef.get.getContainer)

    val weightEvolution = mutable.HashMap[(NetworkEntityPath, NetworkEntityPath, Time), (Float, Float)]()
    def checkWeightEvolution(from : NetworkEntityPath, to : NetworkEntityPath, delay : Time, weight : Float) : Unit = {
      if(!weightEvolution.isDefinedAt((from, to, delay)))
        weightEvolution += (from, to, delay) -> (weight, weight)
      else
        weightEvolution((from, to, delay)) = weightEvolution((from, to, delay))._1 -> weight

    }

    def checkWeightConstrainte(oldWeight : Float, newWeight : Float) : Float = {
      /*if(math.signum(oldWeight) != math.signum(newWeight))
        oldWeight
      else if(newWeight < 0)
        math.max(-1f, math.min(-0f, newWeight))
      else
        math.max(0f, math.min(1f, newWeight))*/

      newWeight
    }

    while(ExternalSender.askTo(n2s3.inputLayerRef.get.getContainer, AskRemainInput).asInstanceOf[WrapMessage].content.asInstanceOf[Boolean]) {
      ExternalSender.askTo(n2s3.buildProperties.getSynchronizerPolicy.getInputSynchronizer, Next)
      ExternalSender.askTo(n2s3.buildProperties.getSynchronizerPolicy.getInputSynchronizer, WaitEndOfActivity, 100 seconds)

      val spikes = spikeMonitor.getSpikes
      //println(spikes.mkString(", "))

      val labels = labelMonitor.getLabels
      if(labels.size != 1)
        throw new RuntimeException("Only one label is allowed for each input step")
      val label = labels.head._1

      //println("label = "+label)

      val ref_timestamp = spikes.minBy(_._2)._2

      val delta_w = mutable.HashMap[NetworkEntityPath, Double]()

      val neuron_queue = mutable.Queue[NetworkEntityPath]()

      val input_connection_list = mutable.HashMap[NetworkEntityPath, ArrayBuffer[(NetworkEntityPath, Float, Timestamp)]]()

      def check_neuron(path : NetworkEntityPath, from : NetworkEntityPath, w : Float, d : Timestamp) : Unit = {
        if(!delta_w.isDefinedAt(path) && !neuron_queue.contains(path)) {
          neuron_queue.enqueue(path)
        }


        if(!input_connection_list.isDefinedAt(path)) {
          input_connection_list += path -> ArrayBuffer[(NetworkEntityPath, Float, Timestamp)]()
        }
        //println("1 = "+path+":"+input_connection_list(path))
        input_connection_list(path) += ((from, w , d))
        //println("2 = "+path+":"+input_connection_list(path))

      }

      val neuron_winner = outputs.map{ case(_, neuron_path) => (neuron_path, timestampOfNeuron(spikes, neuron_path))}.minBy(_._2)._1

        // Output neurons
      val err = outputs.map{ case(neuron_label, neuron_path) =>

        val output_timestamp = timestampOfNeuron(spikes, neuron_path)

        val current_synapses = ExternalSender.askTo(neuron_path, GetAllConnectionProperty(SynapseWeightAndDelay))
          .asInstanceOf[ConnectionPropertyValues[(Float, Time)]].values

        val den = math.max(0.1, current_synapses.foldLeft(0.0){ case(acc, (connectionId, inputNeuron, (w, d))) =>
          check_neuron(inputNeuron, neuron_path, w, d.timestamp)
          checkWeightEvolution(inputNeuron, neuron_path, d, w)
          acc+w*spikeResponseDerivative(output_timestamp-timestampOfNeuron(spikes, inputNeuron)-d.timestamp)
        })

        val expected_timestamp = if(label == neuron_label) ref_timestamp+(10 MilliSecond).timestamp else ref_timestamp+(16 MilliSecond).timestamp // TMP
        delta_w += neuron_path -> ((expected_timestamp-output_timestamp).toDouble/1e3)/den

        ExternalSender.askTo(neuron_path, SetAllConnectionProperty(SynapticWeightFloat, current_synapses.map { case(connectionId, inputNeuron, (w, d)) =>
          /*println("["+input_neuron+"->"+neuron_path+" ("+d+")] "+w+" -> "+(w-learningRate*spikeResponse(output_timestamp-timestampOfNeuron(spikes, input_neuron)-d.timestamp)*delta_w(neuron_path)).toFloat+
            " ("+(-(learningRate*spikeResponse(output_timestamp-timestampOfNeuron(spikes, input_neuron)-d.timestamp)*delta_w(neuron_path)).toFloat)+")")
          */(connectionId, checkWeightConstrainte(w, (w-learningRate*spikeResponse(output_timestamp-timestampOfNeuron(spikes, inputNeuron)-d.timestamp)*delta_w(neuron_path)).toFloat))
        }))

        //println(neuron_path+" : "+(expected_timestamp-output_timestamp)+" "+den)

        math.abs((expected_timestamp-output_timestamp).toDouble)/1e3
      }

      println("err = "+err.sum/err.size.toDouble+" ["+(if(outputs.isDefinedAt(label) && outputs(label) == neuron_winner) "GOOD" else "ERROR")+"] : "+
        outputs.map(e => e._1+"="+((timestampOfNeuron(spikes, e._2)-ref_timestamp)/1e3).toString+"ms"))
      // Hidden neurons
      while(neuron_queue.nonEmpty) {
        val current_neuron = neuron_queue.dequeue()

        if(!input_connection_list(current_neuron).forall(n => delta_w.isDefinedAt(n._1))) {
          neuron_queue.enqueue(current_neuron)
        }
        else {
          val output_timestamp = timestampOfNeuron(spikes, current_neuron)

          //println(current_neuron+" -> "+input_connection_list(current_neuron).mkString(", "))
          //println(current_neuron+" "+input_connection_list(current_neuron).groupBy(_._1).map(_._2.size).mkString(", "))

          val next_d = input_connection_list(current_neuron).groupBy(_._1).foldLeft(0.0){ case(acc, (next_neuron, l_w)) =>
            acc+delta_w(next_neuron)*l_w.foldLeft(0.0)((acc, e) => acc+e._2*spikeResponseDerivative(timestampOfNeuron(spikes, next_neuron)-output_timestamp-e._3))
          }

          val input_connection = ExternalSender.askTo(current_neuron, GetAllConnectionProperty(SynapseWeightAndDelay))
            .asInstanceOf[ConnectionPropertyValues[(Float, Time)]].values

          val prev_d = input_connection.foldLeft(0.0){ case(acc, (connectionId, inputNeuron, (w, d))) =>
            check_neuron(inputNeuron, current_neuron, w, d.timestamp)
            checkWeightEvolution(inputNeuron, current_neuron, d, w)
            acc+w*spikeResponseDerivative(output_timestamp-timestampOfNeuron(spikes, inputNeuron)-d.timestamp)
          }

          if(input_connection.nonEmpty) {
            delta_w += current_neuron -> next_d / prev_d

            ExternalSender.askTo(current_neuron, SetAllConnectionProperty(SynapticWeightFloat, input_connection.map { case (connectionId, inputNeuron, (w, d)) =>
              //println("["+input_neuron+"->"+current_neuron+"] "+w+" -> "+(w-learningRate * spikeResponse(output_timestamp - timestampOfNeuron(spikes, input_neuron)) * delta_w(current_neuron)).toFloat+" ("+(-(learningRate * spikeResponse(output_timestamp - timestampOfNeuron(spikes, input_neuron)) * delta_w(current_neuron)).toFloat)+")")
              (connectionId, checkWeightConstrainte(w, (w - learningRate * spikeResponse(output_timestamp - timestampOfNeuron(spikes, inputNeuron)-d.timestamp) * delta_w(current_neuron)).toFloat))
            }))
          }

        }
      }
/*
      println(delta_w)
      System.exit(0)
*/
      labelMonitor.clearLabels()
      spikeMonitor.clearSpikes()
    }

    weightEvolution.toSeq.sortWith{case((k1, _),(k2, _)) =>
      k1._1.toString < k2._1.toString ||
      (k1._1.toString == k2._1.toString && k1._2.toString < k2._2.toString) ||
       (k1._1.toString == k2._1.toString && k1._2.toString == k2._2.toString && k1._3.timestamp < k2._3.timestamp)
    }
      .foreach { case((i, o, d), (b, a)) =>
      println("# ["+i+" -> "+o+" : "+d+"] "+b+" -> "+a+" ("+(a-b)+")")
    }

  }

}
