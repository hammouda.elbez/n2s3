package fr.univ_lille.cristal.emeraude.n2s3.models.neurons

import fr.univ_lille.cristal.emeraude.n2s3.core
import fr.univ_lille.cristal.emeraude.n2s3.core.Neuron.{ConnectionID, Inhibition, NeuronEnds, NeuronMessage}
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.{BackwardSpike, ElectricSpike, ResetState}
import fr.univ_lille.cristal.emeraude.n2s3.core.event.{NeuronFireEvent, NeuronFireResponse, NeuronPotentialResponse, NeuronPotentialUpdateEvent}
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties._
import fr.univ_lille.cristal.emeraude.n2s3.core.models.synapse.FloatSynapse
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias._
import fr.univ_lille.cristal.emeraude.n2s3.support.Time
import fr.univ_lille.cristal.emeraude.n2s3.support.UnitCast._
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message
import squants.electro.CapacitanceConversions.CapacitanceConversions
import squants.electro.{ElectricCharge, ElectricPotential}
import squants.electro.ElectricPotentialConversions.ElectricPotentialConversions

import scala.collection.mutable
import scala.math._

/**
  * Created by falezp on 13/07/16.
  * Implementation of the Leaky-Integrate-And-Fire neuron model
  *
  * More informations can be found at the following address:
  * 	- http://www.cns.nyu.edu/~eorhan/notes/lif-neuron.pdf
  */



object BioInsp extends NeuronModel {
  override def createNeuron(): BioInsp = new BioInsp()
}

class BioInsp extends core.Neuron {

  case class FutureFire(id : Long) extends Message
  /********************************************************************************************************************
    * Model Parameters
    ******************************************************************************************************************/

  private var membraneThresholdType = MembraneThresholdTypeEnum.Dynamic
  private var restingPotential = 0 millivolts
  private var membranePotentialThreshold = 1 millivolts
  private var membraneCapacitance = 1 farads
  private var tRefract = 1 MilliSecond
  private var tInhibit = 10 MilliSecond
  private var tLeak = 100 MilliSecond


  private var theta = 0 millivolts
  private var thetaLeak = 1e7 MilliSecond
  private var theta_step = 0.05  millivolts

  private val spikeHistory = mutable.HashMap[ConnectionID, (Timestamp, ElectricCharge)]()

  private var tau_spike : Time = 1 MilliSecond
  private var delta_v : Float = 1f

  private var fire_id : Long = 0


  /********************************************************************************************************************
    * Remembered properties
    ******************************************************************************************************************/
  private var tLastTheta : Timestamp = Int.MinValue
  private var tLastInhib : Timestamp = Int.MinValue
  private var tLastSpike : Timestamp = Int.MinValue
  private var tLastFire : Timestamp = Int.MinValue
  /********************************************************************************************************************
    * Properties and Events
    ******************************************************************************************************************/
  addProperty[ElectricPotential](MembranePotentialThreshold, () => membranePotentialThreshold, membranePotentialThreshold = _)
  addProperty[ElectricPotential](MembraneRestingPotential, () => restingPotential, restingPotential = _)
  addProperty[Time](MembraneLeakTime, () => tLeak, tLeak = _)
  addProperty[Time](MembraneRefractoryDuration, () => tRefract, tRefract = _)
  addProperty[Time](MembraneInhibitionDuration, () => tInhibit, tInhibit = _)
  addProperty[MembraneThresholdTypeEnum.Value](MembraneThresholdType, () => membraneThresholdType, membraneThresholdType = _)
  addProperty[ElectricPotential](AdaptiveThreshold, () => theta, theta = _)
  addProperty[ElectricPotential](AdaptiveThresholdAdd, () => theta_step, theta_step = _)
  addProperty[Time](AdaptiveThresholdLeak, () => thetaLeak, thetaLeak = _)


  addEvent(NeuronPotentialUpdateEvent)

  /********************************************************************************************************************
    * Neuron Behavior
    ******************************************************************************************************************/

  def spike_shape(timestamp: Timestamp) : Float = {
    val x = timestamp.toFloat/tau_spike.timestamp.toFloat
    x*math.exp(1.0-x).toFloat
  }

  def spike_max(timestamp: Timestamp) : Float = {
    val x = timestamp.toFloat/tau_spike.timestamp.toFloat
    x*math.exp(1.0-x/2.0).toFloat
  }

  def compute_v(timestamp: Timestamp) : ElectricPotential = {
    spikeHistory.foldLeft(restingPotential){ case(acc, (_, (t, c))) =>
      acc+c*spike_shape(timestamp-t)/membraneCapacitance*delta_v
    }
  }

  def compute_v_max(timestamp: Timestamp) : ElectricPotential = {
    spikeHistory.foldLeft(restingPotential){ case(acc, (_, (t, c))) =>
      acc+c*spike_max(timestamp-t)/membraneCapacitance*delta_v
    }
  }

  def processSomaMessage(timestamp : Timestamp, message : Message, fromConnection : Option[(ConnectionID, NeuronConnection)], ends : NeuronEnds) : Unit = message match {
    case ElectricSpike(spikeCharge) =>

      //println(membranePotentialThreshold+" + "+theta)

      if(timestamp-tLastFire < tRefract.timestamp || timestamp-tLastInhib < tInhibit.timestamp)
        return

      if(membraneThresholdType == MembraneThresholdTypeEnum.Dynamic && !hasFixedParameter) {
        this.theta = this.theta * exp(-(timestamp - this.tLastSpike).toDouble / thetaLeak.timestamp.toDouble).toFloat
        this.tLastTheta = timestamp
      }

      spikeHistory.update(fromConnection.get._1, (timestamp, spikeCharge))





      //println(compute_v(min_search)+" -> "+compute_v(max_search)+" : "+(spikeCharge/membraneCapacitance).toMillivolts)

      triggerEventWith(NeuronPotentialUpdateEvent, NeuronPotentialResponse(timestamp, getNetworkAddress, compute_v(timestamp).toMillivolts.toFloat))

      // t > t0 ?
      val v_max = compute_v(timestamp+tau_spike.timestamp)
      if(v_max >= membranePotentialThreshold+theta) {

        // Dichotomy
        var min_search = timestamp
        var max_search = timestamp+2*tau_spike.timestamp
        val step = 5


        for(_ <- 0 until step) {
          val middle = min_search+(max_search-min_search)/2
          if(compute_v(middle) > membranePotentialThreshold+theta) {
            min_search = middle
          }
          else {
            max_search = middle
          }
        }

        fire_id += 1
        val fire_timestamp = min_search+(max_search-min_search)/2

        sendToMyself(fire_timestamp, FutureFire(fire_id))

      }

      /*
      if(compute_v_max(max_search) >= membranePotentialThreshold+theta) { // Check if it's a good assertion
        for(_ <- 0 until step) {
          val middle = min_search+(max_search-min_search)/2
          if(compute_v(middle) > membranePotentialThreshold+theta) {
            min_search = middle
          }
          else {
            max_search = middle
          }
        }

        fire_id += 1
        val fire_timestamp = min_search+(max_search-min_search)/2

        sendToMyself(fire_timestamp, FutureFire(fire_id))
      }*/


    case FutureFire(id) =>
      if(id == fire_id) {


        if(membraneThresholdType == MembraneThresholdTypeEnum.Dynamic && !hasFixedParameter) {
          theta += theta_step
        }

        spikeHistory.clear()

        triggerEventWith(NeuronFireEvent, NeuronFireResponse(timestamp, getNetworkAddress))
        triggerEventWith(NeuronPotentialUpdateEvent, NeuronPotentialResponse(timestamp, getNetworkAddress, compute_v(timestamp).toMillivolts.toFloat))
        ends.sendToAllInput(timestamp, BackwardSpike)
        ends.sendToAllOutput(timestamp, ElectricSpike())
        tLastFire = timestamp


      }
    case Inhibition(_) =>
      tLastInhib = timestamp
      spikeHistory.clear()

      triggerEventWith(NeuronPotentialUpdateEvent, NeuronPotentialResponse(timestamp, getNetworkAddress, compute_v(timestamp).toMillivolts.toFloat))
  }
}
