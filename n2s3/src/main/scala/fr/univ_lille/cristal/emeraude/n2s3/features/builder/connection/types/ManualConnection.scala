package fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.types

import fr.univ_lille.cristal.emeraude.n2s3.core.NeuronConnection
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.{Connection, ConnectionPolicy}
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.{NeuronGroupRef, NeuronRef}
import fr.univ_lille.cristal.emeraude.n2s3.support.Time

/**
  * Created by falezp on 14/10/16.
  */

case class ManualConnectionObject(from_index : Seq[Int], to_index: Seq[Int], connection : Option[NeuronConnection], preDelay : Time = Time(0))
class ManualConnection(connections : Traversable[ManualConnectionObject]) extends ConnectionPolicy {

  override def generate(from: NeuronGroupRef, to: NeuronGroupRef): Traversable[Connection] = {
    connections.toStream.zipWithIndex.map{ case (ManualConnectionObject(in, out, connection, preDelay), i) =>

      val input = try {
        from.getNeuronPathAt(in: _*)
      }
      catch {
        case e : Exception =>
          throw new RuntimeException("In input neuron group \""+from.getIdentifier+"\" : "+e.getMessage)
      }

      val output = try {
        to.getNeuronPathAt(out: _*)
      }
      catch {
        case e : RuntimeException =>
          throw new RuntimeException("In output neuron group \""+to.getIdentifier+"\" : "+e.getMessage)
      }

      Connection(input, output, connection, preDelay)
    }
  }

  /** ******************************************************************************************************************
    * Testing
    * *****************************************************************************************************************/
  override def connects(aNeuron: NeuronRef, anotherNeuron: NeuronRef): Boolean = true
}