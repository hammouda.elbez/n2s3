/************************************************************************************************************
 * Contributors: 
 * 		- created by Pierre Falez
 ***********************************************************************************************************/
package fr.univ_lille.cristal.emeraude.n2s3.features.io.report

class SetStats(list : Seq[Float]) {
  val avg = list.sum/list.size.toFloat
  val variance = (1f/list.size.toFloat)*list.map(value => math.pow(value-avg, 2)).sum
  val std = math.sqrt(variance).toFloat
  val min = list.min
  val max = list.max
}
class BenchmarkSetResult(val list : Seq[BenchmarkResult]) {
  val inputNumber = avg(list.map(_.inputNumber.toFloat))
  val maxSpikingScore = new SetStats(list.map(_.evaluationByMaxSpiking.score.toFloat))
  val oneSpikingScore = new SetStats(list.map(_.evaluationByOneSpiking.score.toFloat))
  val quietCount = new SetStats(list.map(_.quietInputCount.toFloat))

  def avg(list : Seq[Float]) = list.sum/list.size.toFloat
}
