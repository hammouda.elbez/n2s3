package fr.univ_lille.cristal.emeraude.n2s3.core

import akka.actor.ActorRef
import akka.util.Timeout
import fr.univ_lille.cristal.emeraude.n2s3.core.Neuron.ConnectionID
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.Config
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message

import scala.concurrent.Future

/**
  * Created by falezp on 20/03/17.
  */

/********************************************************************************************************
  * Represent an absolute path of a connection
  *******************************************************************************************************/
case class ConnectionPath(outputNeuron : NetworkEntityPath, connectionID: ConnectionID) {
  override def toString : String = outputNeuron+":"+connectionID
}

/**
  * Connection references
  */
abstract class ConnectionReference {
  def send(message: Message) : Unit
  def ask(message : Message) : Message
}

class LocalConnectionReference(entityRef : LocalNetworkEntityReference, connectionID: ConnectionID) extends ConnectionReference {

  def send(message: Message) : Unit = {
    entityRef.send(RoutedConnectionMessage(connectionID, message))
  }

  def ask(message : Message) : Message = {
    entityRef.ask(RoutedConnectionMessage(connectionID, message))
  }


}

class RemoteConnectionReference(path : ConnectionPath) extends ConnectionReference {

  def send(message: Message) : Unit = {
    ExternalConnectionSender.sendTo(path, message)
  }

  def ask(message : Message) : Message = {
    ExternalConnectionSender.askTo(path, message).asInstanceOf[Message]
  }

}

case class RoutedConnectionMessage(connectionID: ConnectionID, message: Message) extends Message
/**
  * Helper for interact with connections. Extends ExternalSender
  */
object ExternalConnectionSender {

  implicit val timeout: Timeout = Config.longTimeout

  def sendTo(path : ConnectionPath, message: Message) : Unit = {
    ExternalSender.sendTo(path.outputNeuron, RoutedConnectionMessage(path.connectionID, message))
  }

  def askTo(path : ConnectionPath, message: Message, timeout: Timeout = timeout) : Any = {
    ExternalSender.askTo(path.outputNeuron, RoutedConnectionMessage(path.connectionID, message), timeout)
  }

  def askFuture(path : ConnectionPath, message: Message) : Future[Any] = {
    ExternalSender.askFuture(path.outputNeuron, RoutedConnectionMessage(path.connectionID, message))
  }

  def getReference(actor: ActorRef) : NetworkEntityReference = new ExternalActorReference(actor)

}

object ConnectionPathHelper {

  def getConnectionsBetween(input : NetworkEntityPath, output : NetworkEntityPath) : Seq[ConnectionPath] = {
    ExternalSender.askTo(output, GetAllConnectionProperty(ConnectionClassName))
      .asInstanceOf[ConnectionPropertyValues[String]].values.filter(_._2 == input).map(e => ConnectionPath(output, e._1))
  }

  def getConnectionsBetweenAsync(input : NetworkEntityPath, output : NetworkEntityPath) : Future[Seq[ConnectionPath]] = {
    import scala.concurrent.ExecutionContext.Implicits.global
    ExternalSender.askFuture(output, GetAllConnectionProperty(ConnectionClassName))
      .asInstanceOf[Future[ConnectionPropertyValues[String]]].map(f => f.values.filter(_._2 == input).map(e => ConnectionPath(output, e._1)))
  }
}