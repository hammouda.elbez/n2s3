package fr.univ_lille.cristal.emeraude.n2s3.core.actors

import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.{InputPacket, N2S3InputPacket, StreamSupport}
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.{ActorCompanion, PropsBuilder, SinglePropsBuilder}

/**
  * Created by guille on 10/14/16.
  */
class InputLayerActor(stream: StreamSupport[_ <: InputPacket, N2S3InputPacket]) extends NetworkEntityActor(new InputLayer(stream))

/** ******************************************************************************************************************
  * Actor companion
  * *****************************************************************************************************************/

object InputLayerActor extends ActorCompanion {
  override def newPropsBuilder(): PropsBuilder = new SinglePropsBuilder[InputLayerActor]

  case class Stimulus(neuronIndex: Int, timestamp: Timestamp)

}