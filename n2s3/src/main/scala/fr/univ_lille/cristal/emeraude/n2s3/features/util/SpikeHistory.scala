/************************************************************************************************************
 * Contributors: 
 * 		- created by Pierre Falez
 ***********************************************************************************************************/
package fr.univ_lille.cristal.emeraude.n2s3.features.util

import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp

class SpikeHistory {
  class SpikeHistoryUnit(val timestamp : Timestamp) {
    var number = 1
    var next : SpikeHistoryUnit = null
  }

  var oldest : SpikeHistoryUnit = _
  var newest : SpikeHistoryUnit = _
  var size = 0

  def add(timestamp : Timestamp) : Unit = {
    newest match {
      case null =>
        newest = new SpikeHistoryUnit(timestamp)
        oldest = newest
      case unit : SpikeHistoryUnit if unit.timestamp == timestamp =>
        unit.number += 1
      case unit : SpikeHistoryUnit =>

        newest.next = new SpikeHistoryUnit(timestamp)
        newest = newest.next
    }

    size += 1
  }

  def popUntil(timestamp : Timestamp) : Unit = {
    while (oldest != null && oldest.timestamp < timestamp) {
      size -= oldest.number
      oldest = oldest.next
    }

    if(oldest == null)
      newest = null

  }

  def foreach(f : (Timestamp, Int) => Unit) : Unit = {
    var current = oldest
    while(current != null) {
      f(current.timestamp, current.number)
      current = current.next
    }
  }

  def foldLeft[B](accInit : B)(f : (B, Timestamp, Int) => B) : B = {
    var current = oldest
    var acc = accInit
    while(current != null) {
      acc = f(acc, current.timestamp, current.number)
      current = current.next
    }
    acc
  }

  def isEmpty = newest == null
  def nonEmpty = newest != null

}