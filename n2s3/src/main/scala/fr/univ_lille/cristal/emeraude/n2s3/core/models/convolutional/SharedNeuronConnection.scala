package fr.univ_lille.cristal.emeraude.n2s3.core.models.convolutional

import fr.univ_lille.cristal.emeraude.n2s3.core.NeuronConnection

/**
  * Specialization of a connection with a shared object between several instances
  *
  * @param obj is the object to be shared with others connections
  * @tparam A is the type of the shared object
  */
abstract class SharedNeuronConnection[A](val obj : A) extends NeuronConnection
