package fr.univ_lille.cristal.emeraude.n2s3.features.builder

import akka.actor.ActorRef
import akka.pattern.gracefulStop
import fr.univ_lille.cristal.emeraude.n2s3.core.Neuron.SetSynchronizer
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.Config
import fr.univ_lille.cristal.emeraude.n2s3.support.actors._

import scala.concurrent.Await

/**
  * Created by falezp on 19/05/16.
  */
class BuildProperties(val system : AbstractActorSystem[ActorRef]) {

  private var synchronizerPolicy : SynchronizerPolicy = _
  setSynchronizerPolicy(new GlobalSynchronizerPolicy())
  var actorDeploymentPolicy: ActorDeploymentStrategy = LocalActorDeploymentStrategy

  private var networkEntitiesPolicy : NetworkEntityDeploymentPolicy = _
  def getNetworkEntitiesPolicy : NetworkEntityDeploymentPolicy = networkEntitiesPolicy

  setNetworkEntitiesPolicy(new ActorPerNeuronPolicy(this.actorDeploymentPolicy))


  def setNetworkEntitiesPolicy(policy : NetworkEntityDeploymentPolicy) : Unit = {
    networkEntitiesPolicy = policy
  }

  def getSynchronizerPolicy : SynchronizerPolicy = synchronizerPolicy

  def setSynchronizerPolicy(policy : SynchronizerPolicy) : Unit = {
    if(this.synchronizerPolicy != null)
      this.synchronizerPolicy.destroy(system)

    this.synchronizerPolicy = policy
    this.synchronizerPolicy.initialize(system)
  }
}

/*
 *  Synchronizer policy
 */
trait SynchronizerPolicy {

  protected var system: AbstractActorSystem[ActorRef] = _

  def initialize(system: AbstractActorSystem[ActorRef]) : Unit = {
    this.system = system
  }

  def createNeuronSynchronizer(path : Traversable[Any]) : Option[NetworkEntity]
  def getNeuronSynchronizer(path : Traversable[Any]) : NetworkEntityPath

  def getInputSynchronizer : NetworkEntityPath
  def getGlobalSynchronizer : NetworkEntityPath
  def getEventSynchronizer : NetworkEntityPath

  def ensureActorDeployed(nessy: N2S3)

  def destroy(system: AbstractActorSystem[ActorRef]) : Unit = {
    //Does nothing
  }
}

class GlobalSynchronizerPolicy extends SynchronizerPolicy {

  println("Usage of Global Synchronizer")

  implicit val timeout = Config.defaultTimeout

  var synchronizer : Option[NetworkEntityPath] = None
  var EventSynchronizer : Option[NetworkEntityPath] = None

  override def destroy(system: AbstractActorSystem[ActorRef]) : Unit = {
    Await.result(gracefulStop(synchronizer.get.actor, timeout.duration), timeout.duration)
  }

  def getInputSynchronizer : NetworkEntityPath = synchronizer.get
  def getGlobalSynchronizer : NetworkEntityPath = synchronizer.get
  def getEventSynchronizer : NetworkEntityPath = EventSynchronizer.get

  override def createNeuronSynchronizer(path: Traversable[Any]): Option[NetworkEntity] = {
    None
  }

  override def getNeuronSynchronizer(path: Traversable[Any]): NetworkEntityPath = {
    synchronizer.get
  }

  override def ensureActorDeployed(n2s3 : N2S3): Unit = {
    if(synchronizer.isEmpty){
      synchronizer = Some(NetworkEntityPath(system.actorOf(SynchronizerActor, LocalActorDeploymentStrategy, name = "synchronizer")))
      n2s3.layers.foreach { neuronGroup =>
        neuronGroup.neurons.foreach{ n =>
          n.ask(SetSynchronizer(synchronizer.get) )
        }
      }
    }

    if(EventSynchronizer.isEmpty) {
      EventSynchronizer = Some(NetworkEntityPath(system.actorOf(SynchronizerEventActor, LocalActorDeploymentStrategy, name = "synchronizer_event")))
    }
  }
}

class LocalSynchronizerPolicy extends SynchronizerPolicy {

  var synchronizers = scala.collection.mutable.HashMap[Traversable[Any], NetworkEntityPath]()
  var inputSynchronizer : NetworkEntityPath = _

  def getInputSynchronizer: NetworkEntityPath = inputSynchronizer
  def getGlobalSynchronizer : NetworkEntityPath  = inputSynchronizer
  def getEventSynchronizer : NetworkEntityPath  = inputSynchronizer

  override def createNeuronSynchronizer(path: Traversable[Any]): Option[NetworkEntity] = {
    synchronizers.get(path) match {
      case Some(_) => None
      case None => Some(new Synchronizer)
    }
  }

  override def getNeuronSynchronizer(path: Traversable[Any]): NetworkEntityPath = {
    synchronizers(path)
  }

  override def ensureActorDeployed(nessy: N2S3): Unit = {
    if (inputSynchronizer == null){
      inputSynchronizer = NetworkEntityPath(system.actorOf(SynchronizerActor, LocalActorDeploymentStrategy, name = "synchronizer"))
    }
  }
}