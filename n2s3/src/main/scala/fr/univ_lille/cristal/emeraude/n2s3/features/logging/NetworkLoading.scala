package fr.univ_lille.cristal.emeraude.n2s3.features.logging

import java.io._

import fr.univ_lille.cristal.emeraude.n2s3.core
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.{MembranePotentialThreshold, SynapticWeightFloat}
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.N2S3
import squants.electro.ElectricPotentialConversions.ElectricPotentialConversions

/**
  *  Load Network information
  **/

object NetworkLoading {

  def from(n2s3 : N2S3,filename : String){
    val file = new File(filename)
    val br = new BufferedReader(new FileReader(file))
    var indexes = List[(Integer,Integer)]()
    var loadedSyn = List[(Int,String,Float)]()
    var loadedNeurone = List[(String,Float)]()
    var targetNeurone : (String,Float) = ("",0.0F)
    var targetSyn : (Int,String,Float) = (-1,"",0.0F)
    var buffer = ""
    var toBeCropped : ConnectionPath = null
    var stop : Boolean = false

    /**
      *  Loading indexes
      **/
    println("loading indexes")
    for (i <- 0 until br.readLine().toInt){
      buffer = br.readLine()
      indexes = indexes.::(buffer.split(" ")(0).toInt,buffer.split(" ")(1).toInt)
    }

    /**
      *  Loading synapses
      **/
    println("loading synapses")
    var x = br.readLine().toInt

    //   loadedSyn = List[(Int,String,Float)]()

    for (i <- 0 until x){
      buffer = br.readLine()
      loadedSyn = loadedSyn.::(buffer.split(" ")(0).toInt,buffer.split(" ")(1),buffer.split(" ")(2).toFloat)
    }

/*
    loadedSyn = loadedSyn.sortBy(elt => (elt._1,elt._2))
    var Syns = List[(ConnectionPath,Int,Int)]()

    n2s3.layers.foreach(nGroup => {
      if(nGroup.identifier.equals("Input")){
        nGroup.neurons.head.group.connections.head.connections.list.foreach(cnxPath =>{
          Syns = Syns.::(cnxPath,cnxPath.connectionID,cnxPath.outputNeuron.toString.split("/")(1).split(":")(1).toInt)
        })}})

    Syns = Syns.sortBy(elt => (elt._2,elt._3))

    var SynFinal = List[(ConnectionPath,Float)]()
    Syns.foreach(elt => {
      if(x < loadedSyn.length) {
        if(elt._2 == loadedSyn(x)._1 && elt._3 == loadedSyn(x)._2){
          SynFinal = SynFinal.::(elt._1,loadedSyn(x)._3)
          x = x + 1
        }else{
          n2s3.layers.head.neurons.head.group.connections.head.disconnectByPath(elt._1)
        }
      }else{
        n2s3.layers.head.neurons.head.group.connections.head.disconnectByPath(elt._1)
      }
    })

    SynFinal.foreach(elt => {
      ExternalConnectionSender.askTo(elt._1,SetConnectionProperty(SynapticWeightFloat.asInstanceOf[ConnectionProperty[Any]] ,elt._2))
    })
*/
    println("modify synapses")
    var t = System.currentTimeMillis()
    var i = 0
    n2s3.layers.foreach(nGroup => {
      if(nGroup.identifier.equals("Input")){
        nGroup.neurons.head.group.connections.head.connections.list.foreach(cnxPath =>{
          try{
            targetSyn = loadedSyn.find(elt => elt._1 == cnxPath.connectionID && elt._2 == cnxPath.outputNeuron.actor.path.name).get
            ExternalConnectionSender.askTo(cnxPath,SetConnectionProperty(SynapticWeightFloat.asInstanceOf[ConnectionProperty[Any]] ,targetSyn._3))
          }catch{
            case x : NoSuchElementException => {
              nGroup.neurons.head.group.connections.head.disconnectByPath(cnxPath)}
          }

          i = i+1
          if((i%500) == 0){
            println(i)
          }
        })
      }
    })
    println("done in :",(System.currentTimeMillis()-t))

    /**
      *  Loading neurones threshold
      **/

    x = br.readLine().toInt

    for (i <- 0 until x){
      buffer = br.readLine()
      loadedNeurone = loadedNeurone.::(buffer.split(" ")(0),buffer.split(" ")(1).toFloat)
    }

    n2s3.layers.foreach(nGroup => {
      if(!nGroup.identifier.equals("Input")){

        nGroup.neurons.foreach(NeurRef => {
          targetNeurone = loadedNeurone.find(item => item._1 == NeurRef.getNetworkAddress.actor.path.name).get
          ExternalSender.askTo(NeurRef.getNetworkAddress,core.SetProperty(MembranePotentialThreshold, targetNeurone._2 millivolts))
        })}

    })

    /**
      *  Applying prune if exists
      *
          bw.write(cnx.outputNeuron.actor.path.name+" "+cnx.connectionID.toString+"\n")
      **/
/*
    x = br.readLine().toInt
    nu = 0
    for ( i <- 0 to x) {
      try {
        toBeCropped = n2s3.inputLayerRef.get.connections.head.connections.list.find(cnx => {
          cnx.outputNeuron.actor.path.name == buffer.split(" ")(0) && cnx.connectionID.toString == buffer.split(" ")(1)}).get
        n2s3.inputLayerRef.get.connections.head.disconnectByPath(toBeCropped)
      } catch {
        case e : NoSuchElementException => nu+=1
      }
      if((((i * 100).toFloat / x) % 10) == 0){
        println(((i * 100).toFloat / x).toInt+"% ("+i+"/"+x+")")
      }
    }*/

    br.close()

  }

  def fromWithDefaultWeights(n2s3 : N2S3,filename : String,weights: String,loadRandomWeights:Boolean){
    val file = new File(filename)
    val weightsFile = new File(weights)
    var loadRandom = loadRandomWeights
    val br = new BufferedReader(new FileReader(file))
    val brWeights = new BufferedReader(new FileReader(weightsFile))

    var indexes = List[(Integer,Integer)]()

    var loadedSyn = List[(Int,String,Float)]()
    var loadedInitialSyn = List[(Int,String,Float)]()
    var loadedNeurone = List[(String,Float)]()
    var targetNeurone : (String,Float) = ("",0.0F)
    var targetSyn : (Int,String,Float) = (-1,"",0.0F)
    var buffer = ""
    var toBeCropped : ConnectionPath = null
    var nu = 0
    var stop : Boolean = false

    /**
      *  Loading indexes
      **/

    for (i <- 0 until br.readLine().toInt){
      buffer = br.readLine()
      indexes = indexes.::(buffer.split(" ")(0).toInt,buffer.split(" ")(1).toInt)
    }

    /*
    *  Going pass indexes to reach synapses in weights file
    *  */

    for (i <- 0 until brWeights.readLine().toInt){
      brWeights.readLine()}

    /**
      *  Loading synapses
      **/

    var x = br.readLine().toInt

    //   loadedSyn = List[(Int,String,Float)]()

    for (i <- 0 until x){
      buffer = br.readLine()
      loadedSyn = loadedSyn.::(buffer.split(" ")(0).toInt,buffer.split(" ")(1),buffer.split(" ")(2).toFloat)
    }

    // loading intial weights

    x = brWeights.readLine().toInt

    for (i <- 0 until x){
      buffer = brWeights.readLine()
      loadedInitialSyn = loadedInitialSyn.::(buffer.split(" ")(0).toInt,buffer.split(" ")(1),buffer.split(" ")(2).toFloat)
    }

    x = 0
    n2s3.layers.foreach(nGroup => {
      if(nGroup.identifier.equals("Input")){
        nGroup.neurons.head.group.connections.head.connections.list.foreach(cnxPath =>{
          var i = 0
          stop = false
          try{
            loadedSyn.find(elt => elt._1 == cnxPath.connectionID && elt._2 == cnxPath.outputNeuron.actor.path.name).get
            targetSyn = loadedInitialSyn.find(elt => elt._1 == cnxPath.connectionID && elt._2 == cnxPath.outputNeuron.actor.path.name).get
            if(loadRandom) {
              ExternalConnectionSender.askTo(cnxPath, SetConnectionProperty(SynapticWeightFloat.asInstanceOf[ConnectionProperty[Any]], targetSyn._3))
            } }catch{
            case x : NoSuchElementException => nGroup.neurons.head.group.connections.head.disconnectByPath(cnxPath)
          }
        })

      }
    })

    /**
      *  Loading neurones threshold
      **/

    x = br.readLine().toInt

    for (i <- 0 until x){
      buffer = br.readLine()
      loadedNeurone = loadedNeurone.::(buffer.split(" ")(0),buffer.split(" ")(1).toFloat)
    }

    n2s3.layers.foreach(nGroup => {
      if(!nGroup.identifier.equals("Input")){

        nGroup.neurons.foreach(NeurRef => {
          targetNeurone = loadedNeurone.find(item => item._1 == NeurRef.getNetworkAddress.actor.path.name).get
          ExternalSender.askTo(NeurRef.getNetworkAddress,core.SetProperty(MembranePotentialThreshold, targetNeurone._2 millivolts))
        })}

    })

    println("Done loading")
    br.close()

  }

}
