package fr.univ_lille.cristal.emeraude.n2s3.core.actors

import fr.univ_lille.cristal.emeraude.n2s3.core.NetworkEntityPath
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message

/** Set the current actor input generator to the synchronizer of inputs */
case class SetInput(path : NetworkEntityPath) extends Message
