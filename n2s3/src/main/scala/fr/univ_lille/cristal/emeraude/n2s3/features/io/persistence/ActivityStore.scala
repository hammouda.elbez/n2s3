package fr.univ_lille.cristal.emeraude.n2s3.features.io.persistence

import java.io.{DataOutputStream, FileOutputStream}

import akka.actor.{ActorRef, Props}
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.{Config, Done}
import fr.univ_lille.cristal.emeraude.n2s3.core.event._
import fr.univ_lille.cristal.emeraude.n2s3.core.{ExternalSender, NetworkActor, NetworkEntityPath}
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.{N2S3, NeuronGroupObserverRef, NeuronGroupRef}
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.Shape
import fr.univ_lille.cristal.emeraude.n2s3.features.io.persistence.ActivityPersistenceModel.{Label, Spike}
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.{LocalActorDeploymentStrategy, Message}

import scala.collection.mutable
import scala.concurrent.Await

/**
  * Created by pfalez on 26/04/17.
  */

class ActivityStoreRef(n2s3 : N2S3, group : NeuronGroupRef, filename : String) extends NeuronGroupObserverRef {

  var actor: Option[ActorRef] = None
  def getActors : Seq[ActorRef] = this.actor.toSeq


  override def deploy(n2s3: N2S3): Unit = {
    this.actor = Some(n2s3.system.actorOf(Props(new ActivityStoreActor(n2s3, group, filename)), LocalActorDeploymentStrategy))
  }
}

class ActivityStoreActor(n2s3 : N2S3, group : NeuronGroupRef, filename : String) extends NetworkActor {

  val neuronAddress : Map[NetworkEntityPath, Int] = group.neuronPaths.zipWithIndex.toMap

  val outputFile = new FileOutputStream(filename)
  val writer = new DataOutputStream(outputFile)
  var startTimestamp : Option[Timestamp] = None

  var currentTimestamp : Timestamp = -1L
  var eventCount : Long = 0

  val buffer : mutable.ArrayBuffer[ActivityPersistenceModel.EventObj] = mutable.ArrayBuffer()
  var startPosition : Long  = 0

  def writeHeader(count : Long, shape : Shape) : Unit = {
    writer.writeInt(shape.dimensionNumber)
    shape.dimensions.foreach(d => writer.writeInt(d))
    writer.writeLong(count)
  }


  override def initialize(): Unit = {
    askTo(n2s3.inputLayerRef.get.getContainer, SubscribeSynchronized(LabelChangeEvent, ExternalSender.getReference(self), n2s3.buildProperties.getSynchronizerPolicy.getInputSynchronizer))

    group.neuronPaths.map(p => askFuture(p, SubscribeSynchronized(NeuronFireEvent, ExternalSender.getReference(self), n2s3.buildProperties.getSynchronizerPolicy.getGlobalSynchronizer)))
      .foreach(f => Await.result(f, Config.longTimeout.duration))

    startPosition = outputFile.getChannel.position()
    writeHeader(0, group.shape)
  }

  override def destroy(): Unit = {
    checkBuffer(Long.MaxValue)

    outputFile.getChannel.position(startPosition)
    writeHeader(eventCount, group.shape)

    writer.close()

    askTo(n2s3.inputLayerRef.get.getContainer, Unsubscribe(LabelChangeEvent, ExternalSender.getReference(self)))

    group.neuronPaths.map(p => askFuture(p, Unsubscribe(NeuronFireEvent, ExternalSender.getReference(self)))).foreach(f => Await.result(f, Config.longTimeout.duration))
  }

  override def process(message: Message, sender: ActorRef): Unit = message match {
    case SynchronizedEvent(s, _, m, _) =>
      m match {
        case LabelChangeResponse(start, end, label) =>
          checkBuffer(start)
          buffer += Label.create(end-startTimestamp.get, label)

        case NeuronFireResponse(t, source) =>
          checkBuffer(t)
          buffer += Spike.create(neuronAddress(source))
      }
      ExternalSender.sendTo(s, Done)
  }

  def checkBuffer(timestamp : Timestamp): Unit = {
    if(startTimestamp.isEmpty)
      startTimestamp = Some(timestamp)

    if(timestamp != currentTimestamp) {

      if(buffer.nonEmpty) {

        eventCount += buffer.size
        writer.writeLong(currentTimestamp - startTimestamp.get)
        val (metaData, data) = buffer.partition(_.isMetaData)

        writer.writeInt(metaData.size)
        writer.writeInt(data.size)


        metaData.foreach(_.writeTo(writer))
        data.foreach(_.writeTo(writer))
        buffer.clear()
      }

      currentTimestamp = timestamp
    }
  }
}