/************************************************************************************************************
 * Contributors: 
 * 		- created by Pierre Falez
 ***********************************************************************************************************/
package fr.univ_lille.cristal.emeraude.n2s3.features.io.report

import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO

import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.Config
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.SynapticWeightFloat

object ImageSynapsesWeight {

  implicit val timeout = Config.defaultTimeout

  def save(neuron : Seq[(NetworkEntityPath, Int, Int)], pixelSize : Int, borderSize : Int, filename : String) : Unit = {
    val weights = neuron.map { case (path, _, _) =>
      ExternalSender.askTo(path, GetAllConnectionProperty(SynapticWeightFloat)) match {
        case PropertyValue(values: Seq[(NetworkEntityPath, Float)] @unchecked) => values.map(_._2)
      }
    }

    val minValue = weights.flatten.min
    val maxValue = weights.flatten.max

    val nWidth = math.ceil(math.sqrt(weights.size)).toInt
    val nHeight = math.ceil(weights.size.toFloat/nWidth).toInt

    val imageWidth = neuron.map(entry => entry._2).max
    val imageHeight = neuron.map(entry => entry._3).max

    val width = nWidth*(imageWidth*pixelSize+borderSize)-borderSize
    val height = nHeight*(imageHeight*pixelSize+borderSize)-borderSize

    val image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB)

    weights.zipWithIndex.foreach { case (list, neuronIndex) =>
      val imageX = (neuronIndex%nWidth)*(imageWidth*pixelSize+borderSize)
      val imageY = (neuronIndex/nWidth)*(imageHeight*pixelSize+borderSize)
      list.zipWithIndex.foreach { case (value, pixelIndex) =>
        for(x <- imageX+(pixelIndex%imageWidth)*pixelSize until imageX+(pixelIndex%imageWidth)*pixelSize+pixelSize;
            y <- imageY+(pixelIndex/imageWidth)*pixelSize until imageY+(pixelIndex/imageWidth)*pixelSize+pixelSize) {



          if(value >= 0) {
            val v = value/math.max(maxValue, 1f)

            val rc = math.max(0f, -2f*v*v+4f*v-1f)
            val bc = math.max(0f, -2f*v*v+1)
            val gc = math.max(0f, 4f*v*(-v+1f))


            image.setRGB(x, y, math.round(bc*255) + (math.round(gc*255) << 8) + (math.round(rc*255) << 16))
          }
          else {
            val v = (value/math.min(minValue, -1f) * 255).toInt
            image.setRGB(x, y, v << 16)
          }
        }
      }
    }


    val outputfile = new File(filename)
    ImageIO.write(image, "png", outputfile)

  }
}
