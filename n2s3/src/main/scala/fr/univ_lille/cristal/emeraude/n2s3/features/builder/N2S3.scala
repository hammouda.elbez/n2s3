package fr.univ_lille.cristal.emeraude.n2s3.features.builder

import java.io.File
import java.nio.file.{Files, Paths}

import akka.actor.{ActorRef, ActorSystem}
import akka.util.Timeout
import com.typesafe.config.ConfigFactory
import fr.univ_lille.cristal.emeraude.n2s3.core.Synchronizer.{Next, Start, WaitEndOfActivity, WaitUntil}
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.{Config, InputNeuron, SetInput}
import fr.univ_lille.cristal.emeraude.n2s3.core.exceptions.N2S3Exception
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input._
import fr.univ_lille.cristal.emeraude.n2s3.features.io.report.BenchmarkMonitorRef
import fr.univ_lille.cristal.emeraude.n2s3.features.logging.graph.SynapsesWeightGraphBuilderRef
import fr.univ_lille.cristal.emeraude.n2s3.support.Time
import fr.univ_lille.cristal.emeraude.n2s3.support.actors._

/**
  * Main Simulation class.
  * Represents a simulation. A simulation is built by adding neuron groups to it, making connections between them and then observing the neuron using specific observers.
  *
  * N2S3's API hides the usage of actors to the end-user. It provides specific reference objects. These reference objects provide a clean, typed and synchronous API to interact with the underlying system.
  *
  * The simulation objects are created in a lazy fashion. All actors and concrete connections are created when the simulation is asked to run.
  *
  * @param system The actor system used in the simulation. For test purposes mainly.
  */
class N2S3(val system: AbstractActorSystem[ActorRef]) {

  def this(actorSystemName: String = "N2S3ActorSystem", config: Option[com.typesafe.config.Config] = None) {
    this(new N2S3ActorSystem(ActorSystem(actorSystemName, if(config.isDefined) {
      println("Use provided Akka configuration")
      config.get
    }
    else if(Files.exists(Paths.get("akka.conf"))) {
      println("Use Akka configuration in file \"akka.conf\"")
      ConfigFactory.parseFile(new File("akka.conf"))
    }
    else {
      println("No Akka configuration found (no file \"akka.conf\"). Use default configuration")
      ConfigFactory.empty()
    })))
  }

  implicit val timeout = Config.longTimeout
  val buildProperties = new BuildProperties(system)
  var first = true
  var inputLayerRef: Option[InputNeuronGroupRef] = None
  var layers = new scala.collection.mutable.ArrayBuffer[NeuronGroupRef]()
  var layerObservers = new scala.collection.mutable.ArrayBuffer[NeuronGroupObserverRef]()
  val messageQueue = new MessageSynchronizer(Config.longTimeout.duration)(system.dispatcher)

 /********************************************************************************************************************
  * Network Configuration Methods
  ******************************************************************************************************************/

  def getClusterNodeSelectionPolicy: ActorDeploymentStrategy = buildProperties.actorDeploymentPolicy

  /**
    * Creates an input layer for the simulation.
    * The input layer feeds the simulation with inputs from the given stream.
 *
    * @param stream input stream to feed the simulation
    * @return An [[InputNeuronGroupRef]] that provides a clean, typed and synchronous API to interact with the underlying input layer
    */
  def createInput(stream: StreamSupport[_ <: InputPacket, N2S3InputPacket], inputNeuron : Option[() => Neuron] = None) = {
    val newInputLayerRef = new InputNeuronGroupRef(this, if(inputNeuron.isDefined) inputNeuron.get else () => new InputNeuron)
    newInputLayerRef.setInput(stream.asInstanceOf[StreamSupport[InputPacket, N2S3InputPacket]])
    newInputLayerRef.shape = stream.shape
    this.inputLayerRef = Some(newInputLayerRef)
    this.layers += newInputLayerRef
    newInputLayerRef
  }

  private def getInputSynchronizer = buildProperties.getSynchronizerPolicy.getInputSynchronizer

  /**
    * Creates a new Neuron group in the simulation.
    * The created neuron group will have by default the name "NeuronGroup[N]" where N is the number of neuron groups in the simulation.
 *
    * @return A [[NeuronGroupRef]] that provides a clean, typed and synchronous API to interact with the underlying neuron group
    */
  def createNeuronGroup(identifier: String = "NeuronGroup" + this.layers.size, numberOfNeurons: Int = 0) = {
    val layerRef = new NeuronGroupRef(this)
    layerRef.setIdentifier(identifier)
    layerRef.setNumberOfNeurons(numberOfNeurons)
    this.layers += layerRef
    layerRef
  }

  def getNeuronGroup(identifier: String) = {
    val candidates = layers.filter(_.identifier == identifier)
    assert(candidates.size == 1)
    candidates.head
  }

  /**
    * Adds a [[NeuronGroupObserverRef]] to the network.
    * This is an extension point of the simulation. End-users can implement their own network observers and add them to the simulator using this API.
    * For normal users, use the already implemented observers such as [[N2S3.createSynapseWeightGraphOn()]] and [[N2S3.createBenchmarkMonitor()]]
 *
    * @param networkObserver the [[NeuronGroupObserverRef]] to add to the network
    * @tparam T The specific type of the [[NeuronGroupObserverRef]] argument
    * @return The [[NeuronGroupObserverRef]] given as argument
    */
  def addNetworkObserver[T <: NeuronGroupObserverRef](networkObserver: T): T = {
    this.layerObservers += networkObserver
    networkObserver
  }

 /********************************************************************************************************************
  * Pre-built [[NeuronGroupObserverRef]] Creation Methods
  ******************************************************************************************************************/

  /**
    * @deprecated
    * Creates a Weight graph that observes all synaptic weights of the synapses that go from the @fromGroup [[NeuronGroupRef]] to the @toGroup [[NeuronGroupRef]] and shows them in a 'heat' graph.
    *
    * @param fromGroup the neuron group whose synapses are connected to the toGroup we want to observe
    * @param toGroup the neuron group whose synapses are connected to the fromGroup we want to observe
    * @return A [[SynapsesWeightGraphBuilderRef]] that provides a clean, typed and synchronous interface to interact with the underlying graph actor
    */
 def createSynapseWeightGraphOn(fromGroup: NeuronGroupRef, toGroup: NeuronGroupRef): SynapsesWeightGraphBuilderRef = {
    this.addNetworkObserver(new SynapsesWeightGraphBuilderRef(fromGroup, toGroup))
  }

  /**
    * Creates a Benchmark monitor that observes when the input label changes and compares it to the spikes of a given layer.
    * It then calculates the recognition ratio of the simulation run.
    * Use this after the network is trained.
 *
    * @param neuronGroup The neuron group to observe spiking
    * @return A [[BenchmarkMonitorRef]] that provides a clean, typed and synchronous interface to interact with the underlying graph actor
    */
  def createBenchmarkMonitor(neuronGroup: NeuronGroupRef, timeOffset : Time = Time(0)) = {
    this.addNetworkObserver(new BenchmarkMonitorRef(neuronGroup, timeOffset))
  }

 /********************************************************************************************************************
  * Running Methods
  ******************************************************************************************************************/

  def create() : Unit = {
   this.assertSimulatorIsReady()

   layers.foreach( layer => layer.ensureActorDeployed(this) )
   buildProperties.getSynchronizerPolicy.ensureActorDeployed(this)
   layers.foreach( layer => layer.ensureConnectionsDeployed(this) )
    layerObservers =  layerObservers.filter(!_.isDestroyed)
   layerObservers.foreach( observer => observer.ensureActorDeployed(this) )
    ExternalSender.sendTo(getInputSynchronizer, SetInput(this.inputLayerRef.get.actorPath.get))

 }
  /**
    * Starts the current simulation.
    * All the required elements for the simulation (e.g., actors, actor references) will be created and connected according to the given configuration.
    * Then, it activates the simulation so the input starts flowing into it.
    *
    * This method can be safely called several times. All elements created in previous runs will be duly reused. Newly configured elements will be created and connected.
    *
    * Important: this method does not block the current thread. The simulation will start in a separate thread and return immediately.
    *
    * @param timesToRun Times to repeat the current setup
    */
  def run(timesToRun: Int = 1, timeout: Timeout = Config.longTimeout): Unit = {
    this.create()

    for (i <- 1 to timesToRun){
      ExternalSender.sendTo(getInputSynchronizer, SetInput(this.inputLayerRef.get.actorPath.get))
      this.inputLayerRef.get.reset()
      this.waitScheduledMessages()
      ExternalSender.askTo(getInputSynchronizer, Start)
      this.waitEndOfActivity(timeout)
    }
  }

  def nextEntry(timeout: Timeout = Config.longTimeout): Unit = {
    this.create()

    this.waitScheduledMessages()

    ExternalSender.askTo(getInputSynchronizer, Next)
    this.waitEndOfActivity(timeout)

  }

  def remainInputs : Boolean = {
    true // TODO
  }


  /**
    * Waits until there is no more activity in the simulation. I.e., no more messages are sent between neurons.
 *
    * @param timeout a timeout value for waiting
    * @return [[Unit]]
    */
  def waitEndOfActivity(timeout: Timeout = Config.longTimeout): Unit = {
    ExternalSender.askTo(getInputSynchronizer, WaitEndOfActivity, timeout)
  }

  def waitUntil(duration : Time) : Unit = {
    // TMP
    this.create()

    ExternalSender.sendTo(getInputSynchronizer, SetInput(this.inputLayerRef.get.actorPath.get))
    this.inputLayerRef.get.reset()
    this.waitScheduledMessages()
    ExternalSender.askTo(getInputSynchronizer, Start)

    // END TMP

    ExternalSender.askTo(getInputSynchronizer, WaitUntil(duration), timeout)
    ExternalSender.askTo(getInputSynchronizer, WaitEndOfActivity, timeout)

    GlobalStream.setPrefix(duration.timestamp)
  }

  /**
    * Variant of [[N2S3.run()]] that waits until the network has no more activity. It is a mixture of [[N2S3.run()]] and [[N2S3.waitEndOfActivity()]]
 *
    * @param timeout a timeout value for waiting
    */
  def runAndWait(timesToRun: Int = 1, timeout: Timeout = Config.longTimeout): Unit = {
    this.run(timesToRun, timeout)
    this.waitEndOfActivity(timeout)
  }

  def destroy(): Unit = {
    layerObservers.foreach(observer => observer.destroy())
    system.shutdown()
  }

 /********************************************************************************************************************
  * Not for public simulation usage
  ******************************************************************************************************************/

  /**
    * Method used to ensure that the network has the minimal elements to run.
    * It validates that there is an input layer, and at least one output layer.
    */
  def assertSimulatorIsReady() = {
    if (this.inputLayerRef.isEmpty) throw new N2S3Exception("N2S3 Simulator has no input layer")
    if (this.layers.size < 2) throw new N2S3Exception("N2S3 Simulator has no output layer")
  }

  /**
    * Schedules a message to be sent to a specific actor. Scheduling is useful to guarantee that all messages arrived to their destination and were correctly processed.
 *
    * @see [[N2S3.waitScheduledMessages()]]
    * @param target the target of the message
    * @param message the message to schedule
    */
  def scheduleMessage(target: NetworkEntityPath, message: Message) = {
    this.messageQueue.scheduleMessage(target, message)
  }

  /**
    * Waits all scheduled messages to be processed.
    * This is to avoid that the network starts running while its configuration did not yet finish.
    */
  private def waitScheduledMessages() = {
    this.messageQueue.waitForAll()
  }

 /********************************************************************************************************************
  * Synchronization configuration
  ******************************************************************************************************************/
  def getNetworkEntitiesPolicy = buildProperties.getNetworkEntitiesPolicy

  def setSynchronizerPolicy(policy: SynchronizerPolicy): Unit = buildProperties.setSynchronizerPolicy(policy)
  def createSynchronizerActor(actorCompanion: ActorCompanion = SynchronizerActor) = system.actorOf(actorCompanion, LocalActorDeploymentStrategy)

}
