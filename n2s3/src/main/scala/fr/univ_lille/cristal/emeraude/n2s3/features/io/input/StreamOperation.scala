package fr.univ_lille.cristal.emeraude.n2s3.features.io.input

/**
  * Created by falezp on 15/02/17.
  */

object InputSeqPacket {
  def apply[T <: InputPacket](ppackets : Seq[T]) = new InputSeqPacket[T](ppackets)
}

class InputSeqPacket[T <: InputPacket](ppackets : Seq[T]) extends InputPacket {
  assert(ppackets.isEmpty || ppackets.tail.forall(_.getShape == ppackets.head.getShape))

  var packets : Seq[T] = ppackets

  override type InputDataType = Seq[T#InputDataType]
  override type InputMetaDataType = Seq[T#InputMetaDataType]

  private def applyOnAll[O](f : T => O) : Seq[O] = {
    packets.map(p => f(p))
  }

  override def getDataOnChannel(index: Int*): InputDataType = {
    applyOnAll(_.getDataOnChannel(index:_*))
  }

  override def getAllData: Map[Seq[Int], InputDataType] = {
    applyOnAll(_.getAllData).flatten.groupBy(_._1).map{case(k, v) => (k, v.map(_._2))}
  }

  override def getMetaData: Seq[InputMetaDataType] = {
    applyOnAll(_.getMetaData)
  }

  override def mapData(f: (Seq[Int], InputDataType) => InputDataType): this.type = {
    val newValues = getAllData.map{case(k, v) =>
      k -> f(k, getDataOnChannel(k:_*))
    }

    ppackets.zipWithIndex.foreach{ case(p, i) =>
      p.mapData{case (k, v) => newValues(k)(i).asInstanceOf[p.InputDataType]}
    }
    this
  }

  override def mapDataWithShape(shape: Shape, f: (Seq[Int], InputDataType) => (Seq[Int], InputDataType)): this.type =  {
    val newValues = getAllData.map{case(k, v) =>
      k -> f(k, getDataOnChannel(k:_*))
    }

    ppackets.zipWithIndex.foreach{ case(p, i) =>
      p.mapDataWithShape(shape, {case (k, v) => newValues(k).asInstanceOf[(Seq[Int], p.InputDataType)]})
    }
    this
  }

  override def flatMapData(f: (Seq[Int], InputDataType) => Seq[InputDataType]): this.type = {
    val newValues = getAllData.map{case(k, v) =>
      k -> f(k, getDataOnChannel(k:_*))
    }

    ppackets.zipWithIndex.foreach{ case(p, i) =>
      p.flatMapData{case (k, v) => newValues(k)(i).asInstanceOf[Seq[p.InputDataType]]}
    }
    this
  }

  override def flatMapDataWithShape(shape: Shape, f: (Seq[Int], InputDataType) => Seq[(Seq[Int], InputDataType)]): this.type = {
    val newValues = getAllData.map{case(k, v) =>
      k -> f(k, getDataOnChannel(k:_*))
    }

    ppackets.zipWithIndex.foreach{ case(p, i) =>
      p.flatMapDataWithShape(shape, {case (k, v) => newValues(k).asInstanceOf[Seq[(Seq[Int], p.InputDataType)]]})
    }
    this
  }

  override def mapMetaData(f: InputMetaDataType => InputMetaDataType): this.type = {
    ppackets.foreach(p => mapMetaData(f))
    this
  }

  override def setAllData(data: Seq[(Seq[Int], InputDataType)]): this.type = {
    throw new UnsupportedOperationException
  }

  override def setAllDataWithShape(ns: Shape, data: Seq[(Seq[Int], InputDataType)]): this.type = {
    throw new UnsupportedOperationException
  }
}