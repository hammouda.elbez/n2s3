package fr.univ_lille.cristal.emeraude.n2s3.features.logging.graph

import java.awt.{Color, Dimension, FlowLayout, Graphics}

import javax.swing.{JFrame, JPanel, WindowConstants}
import akka.actor.{ActorRef, Props}
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.SynapticWeightFloat
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.{N2S3, NeuronGroupObserverRef}
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.{LocalActorDeploymentStrategy, Message}

import scala.collection.mutable

/**
  * Created by pfalez on 09/05/17.
  */
class SynapticWeightSelectAndMergeGraphRef(list : Seq[Seq[Seq[Seq[ConnectionPath]]]], mergeFunction : Iterable[Float] => Float, colorFunction : Float => Color, normalize : Boolean = false, synapseSize : Int = 4, refreshRate : Int = 1000/24, name : String = "") extends NeuronGroupObserverRef {
  var actor: Option[ActorRef] = None
  def getActors : Seq[ActorRef] = this.actor.toSeq


  override def deploy(n2s3: N2S3): Unit = {
    this.actor = Some(n2s3.system.actorOf(Props(new SynapticWeightSelectAndMergeGraph(list, mergeFunction, colorFunction, normalize, synapseSize, refreshRate, name)), LocalActorDeploymentStrategy))
  }
}

object SynapticWeightSelectAndMergeGraph {

  def mergeOnOff(list : Iterable[Float]) : Float = {
    if(list.size != 2)
      0f
    else
      0.5f+list.head/2f-list.last/2f
  }

  def paint(list : Map[(Int, Int), Iterable[Float]], mergeFunction : Iterable[Float] => Float, g : Graphics, factor : Int = 1, color : Float => Color): Unit = {
    list.foreach{ case((x, y), l) =>
      g.setColor(color(mergeFunction(l)))
      g.fillRect(x * factor, y * factor, factor, factor)
    }
  }
}

class SynapticWeightSelectAndMergeGraph(list : Seq[Seq[Seq[Seq[ConnectionPath]]]], mergeFunction : Iterable[Float] => Float, colorFunction : Float => Color, normalize : Boolean, synapseSize : Int = 4, refreshRate : Int = 1000/24, name : String) extends AutoRefreshNetworkActor(refreshRate, list.size) {

  class WeightPanel(list : Seq[Seq[Seq[ConnectionPath]]]) extends JPanel {

    val synapseList = list.zipWithIndex.flatMap{case(l1, x) => l1.zipWithIndex.flatMap{case(l2, y) => l2.map{p => (x, y, p)}}}.groupBy(_._3.outputNeuron)
    val xMax = synapseList.flatMap(_._2.map(_._1)).max
    val yMax = synapseList.flatMap(_._2.map(_._2)).max

    setPreferredSize(new Dimension((xMax+1)*synapseSize, (yMax+1)*synapseSize))
    setMinimumSize(new Dimension((xMax+1)*synapseSize, (yMax+1)*synapseSize))

    var values = synapseList.flatMap(_._2.map(e => (e._1, e._2) -> mutable.ArrayBuffer[Float]())).toMap

    def update(): Unit = {
      values.foreach(_._2.clear())

      var maxValue = 0f

      synapseList.flatMap{ case(neuron, connectionList) =>
        if(connectionList.size == 1) {
          Some((connectionList.head._1, connectionList.head._2,
            ExternalConnectionSender.askTo(connectionList.head._3, GetConnectionProperty(SynapticWeightFloat))
              .asInstanceOf[PropertyValue[Float]].value))
        }
        else {
          val values = ExternalSender.askTo(neuron, GetAllConnectionProperty(SynapticWeightFloat))
            .asInstanceOf[ConnectionPropertyValues[Float]].values.groupBy(_._1)
          connectionList.map{case(x, y, p) => (x, y, values(p.connectionID).head._3)}
        }
      }.foreach{ case(x, y, v) =>
        maxValue = math.max(maxValue, math.abs(v))
        values((x, y)) += v
      }

      if(normalize) {
        values.foreach { case (k, v) =>
          val newV = v.map(_ / maxValue)
          v.clear()
          v ++= newV
        }
      }

    }

    override def paintComponent(g: Graphics): Unit = {
      SynapticWeightSelectAndMergeGraph.paint(values, mergeFunction, g, synapseSize, colorFunction)
    }

  }

  val frame = new JFrame()
  frame.setLayout(new FlowLayout())

  val panels : Seq[WeightPanel] = list.map{ l =>
    val panel = new WeightPanel(l)
    frame.add(panel)
    panel
  }

  frame.setTitle(name)

  frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE)
  frame.validate()
  //frame.setSize(600, 400)
  frame.pack()
  frame.setVisible(true)

  override def initialize(): Unit = {

  }

  override def destroy(): Unit = {

  }

  override def process(message: Message, sender : ActorRef): Unit = {

  }

  override def update(n: Int): Unit = {
    panels(n).update()
    panels(n).repaint()
    panels(n).revalidate()
  }
}