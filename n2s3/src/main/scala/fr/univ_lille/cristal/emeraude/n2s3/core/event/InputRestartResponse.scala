package fr.univ_lille.cristal.emeraude.n2s3.core.event

import akka.actor.ActorRef

/**
  * Created by guille on 10/14/16.
  */
case class InputRestartResponse(source : ActorRef) extends EventResponse
