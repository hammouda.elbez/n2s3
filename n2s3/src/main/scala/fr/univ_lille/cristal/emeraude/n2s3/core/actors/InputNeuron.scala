package fr.univ_lille.cristal.emeraude.n2s3.core.actors

import fr.univ_lille.cristal.emeraude.n2s3.core.Neuron.{ConnectionID, NeuronEnds}
import fr.univ_lille.cristal.emeraude.n2s3.core.event.{NeuronFireEvent, NeuronFireResponse}
import fr.univ_lille.cristal.emeraude.n2s3.core.{Neuron, NeuronConnection}
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias._
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message

/**
  * Created by guille on 10/14/16.
  */
class InputNeuron extends Neuron {

  def defaultConnection = throw new UnsupportedOperationException

  def processSomaMessage(timestamp: Timestamp, message: Message, fromConnection : Option[(ConnectionID, NeuronConnection)], ends : NeuronEnds): Unit = message match {
    case s : Spike =>
      triggerEventWith(NeuronFireEvent, NeuronFireResponse(timestamp, getNetworkAddress))
      ends.sendToAllOutput(timestamp, s)
    case InputStart =>
      // nothing to do
    case other =>
      println("[InputNeuron] Warning: Unknown message "+other)
  }

}
