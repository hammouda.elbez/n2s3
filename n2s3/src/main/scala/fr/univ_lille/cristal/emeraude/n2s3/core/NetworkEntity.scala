/************************************************************************************************************
 * Contributors: 
 * 		- created by Pierre Falez on 02/05/16
 ***********************************************************************************************************/

package fr.univ_lille.cristal.emeraude.n2s3.core

import akka.actor.ActorRef
import fr.univ_lille.cristal.emeraude.n2s3.core.NetworkEntity.AskReference
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.{Done, NetworkEntityActor, WrapMessage}
import fr.univ_lille.cristal.emeraude.n2s3.core.exceptions.UnknownPathException
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message

/**
  * NetworkEntity companion object
  */
object NetworkEntity {

  /**
    * Message used to get the reference of the parameter path according to the receiver.
    * Response of this message is a WrapMessage with the reference in it content
    *
    * @param path is the target of the asked reference
    */
  case class AskReference(path: NetworkEntityPath) extends Message

}

/**
  * A trait with common behaviour for entities in a N2S3 simulation.
  */
trait NetworkEntity {


  /**
    * The NetworkEntityActor in which the current entity is
    */
  var container : NetworkEntityActor = _

  private var parent: Option[NetworkEntity] = None
  def getContainerActorRef: ActorRef = {
    this.container match {
      case null => null
      case _ => this.container.self
    }
  }

  /**
    * Method used to send a message to this NetworkEntity with a sender
    */
  final def sendMessageFrom(message : Message, sender : NetworkEntityReference) : Unit = {
    processMessage(message, sender)

    if(message.expectResponse && !sender.hasSendMessage) {
      sender.send(Done)
    }
  }

  /**
    * The local path prefix of the current entity
    */
  protected var localPath : Traversable[Any] = Nil

  def getParent = this.parent.get
  def setParent(container: NetworkEntity, containerActor: NetworkEntityActor) = {
    this.parent = Some(container)
    this.container = containerActor
  }
  def setLocalPath(localPath: Traversable[Any]): Unit = this.localPath = localPath

  def getNetworkAddress = new NetworkEntityPath(this.getContainerActorRef, this.localPath)

  /**
    * Return the reference from a NetworkEntityPath. The sender is consider to be the current entity
    *
    * @param path is the target to reference
    * @return a reference to the target (e.g. local or remote)
    */
  def getReferenceOf(path : NetworkEntityPath) : NetworkEntityReference = {
    getReferenceOf(path, getNetworkAddress)
  }

  def isLocalReference(path : NetworkEntityPath) : Boolean = {
    this.getContainerActorRef == path.actor
  }

  /**
    * Return the reference from a NetworkEntityPath. The sender is will be the sender parameter
    *
    * @param path is the target to reference
    * @param sender is the sender of the reference
    * @return
    */
  def getReferenceOf(path : NetworkEntityPath, sender: NetworkEntityPath) : NetworkEntityReference = {
    if(isLocalReference(path, sender))
      new LocalNetworkEntityReference(container, container.resolvePath(path.local), container.resolvePath(sender.local))
    else
      new RemoteNetworkEntityReference(path,sender)
  }

  def isLocalReference(path : NetworkEntityPath, sender: NetworkEntityPath) : Boolean = {
    if(container == null)
      throw new RuntimeException("null container")
    if(path == null)
      throw new RuntimeException("null path")
    if(sender == null)
      throw new RuntimeException("null sender")

    container.self == path.actor && sender.actor == container.self
  }

  /**
    * If the current path is empty, return the current entity
    * Otherwise throw an exception
    *
    * @param destination is a local path
    * @return the NetworkEntity symbolized by the parameter destination
    * @throws UnknownPathException is the local path is not empty
    */
  def resolvePath(destination : Traversable[Any]) : NetworkEntity = {
    if(destination.isEmpty)
      this
    else
      throw new UnknownPathException(localPath++destination)
  }

  /**
    * Process message AskReference
    * All other messages are delegate to receiveMessage
    *
    * @param message is the content to be processed
    * @param sender is a reference of the sender, which can be used to send response
    */
  final def processMessage(message : Message, sender : NetworkEntityReference) : Unit = message match {
    case AskReference(path) =>
      val reference = getReferenceOf(path)
      val wrappedMessage = WrapMessage(reference)
      sender.send(wrappedMessage)
    case _ =>
       receiveMessage(message, sender)
  }

  /**
    * Process a message not handled by ReferenceableNetworkEntity
    *
    * @param message is the content to be processed
    * @param sender is a reference of the sender, which can be used to send response
    */
  def receiveMessage(message : Message, sender : NetworkEntityReference) : Unit

  def referenceToSynchronizer(synchronizer : NetworkEntityPath) : NetworkEntityReference = {
    getReferenceOf(synchronizer)
  }
}

