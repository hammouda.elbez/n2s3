package fr.univ_lille.cristal.emeraude.n2s3.core.event

import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp

/********************************************************************************************************
  *  Event triggered when an input has a new label
  *******************************************************************************************************/
case class LabelChangeResponse(timestamp: Timestamp, endTime : Timestamp, label : String) extends TimedEventResponse