package fr.univ_lille.cristal.emeraude.n2s3.features.learning

import akka.actor.{Actor, Props}
import akka.pattern.ask
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.{Config, Done}
import fr.univ_lille.cristal.emeraude.n2s3.core.event._
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.SynapseWeightAndDelay
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.N2S3
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.Time
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.LocalActorDeploymentStrategy

import scala.collection.mutable
import scala.concurrent.Await

/**
  * Created by falezp on 22/12/16.
  */
class SupervisedLinearClassifier(n2s3 : N2S3, targetNeuron : Map[String, NetworkEntityPath], learningRate : Double) {

  object Close

  implicit val timeout = Config.longTimeout

  class SupervisedLinearClassifierActor(n2s3: N2S3, targetNeuron: Map[String, NetworkEntityPath], learningRate: Double) extends Actor {

    var currentInputStart : Option[Timestamp] = None
    var currentInputLabel : Option[String] = None

    var currentTimestamp : Timestamp = 0
    val currentTimestampFire = mutable.ArrayBuffer[NetworkEntityPath]()
    val inputPattern = mutable.HashMap[NetworkEntityPath, Timestamp]()
    val neuronSubscribed = mutable.ArrayBuffer[NetworkEntityPath]()

    val connections = mutable.ArrayBuffer[(NetworkEntityPath, NetworkEntityPath, Double)]()
    val forwardConnections = mutable.HashMap[NetworkEntityPath, Seq[Int]]()
    val backwardConnections = mutable.HashMap[NetworkEntityPath, Seq[Int]]()
    load()

    override def receive = {
      case SynchronizedEvent(s, _, m, _) =>
        m match {
          case LabelChangeResponse(start, end, label) =>
            if(start > currentTimestamp) {

              currentTimestampFire.foreach { neuron =>
                if(!inputPattern.isDefinedAt(neuron))
                  inputPattern += neuron -> currentTimestamp
              }

              currentTimestampFire.clear()
              currentTimestamp = start
            }

            if(currentInputStart.isDefined && currentInputLabel.isDefined) {
              execute()
              save()
            }

            inputPattern.clear()
            currentInputStart = Some(start)
            currentInputLabel = Some(label)

          case NeuronFireResponse(timestamp, source) =>
            if(timestamp > currentTimestamp) {

              currentTimestampFire.foreach { neuron =>
                if(!inputPattern.isDefinedAt(neuron))
                  inputPattern += neuron -> currentTimestamp
              }

              currentTimestampFire.clear()
              currentTimestamp = timestamp
            }

            currentTimestampFire += source


          case _ => throw new RuntimeException("Unrecognized message : "+m)
        }
        ExternalSender.sendTo(s, Done)
      case Close =>
        currentTimestampFire.foreach { neuron =>
          if(!inputPattern.isDefinedAt(neuron))
            inputPattern += neuron -> currentTimestamp
        }
        if(currentInputStart.isDefined && currentInputLabel.isDefined) {
          execute()
          save()
        }

        ExternalSender.askTo(n2s3.inputLayerRef.get.getContainer, Unsubscribe(LabelChangeEvent, ExternalSender.getReference(self)))

        neuronSubscribed.foreach{ neuron =>
          ExternalSender.askTo(neuron, Unsubscribe(NeuronFireEvent, ExternalSender.getReference(self)))
        }

        sender ! Done
        context stop self
      case Done => sender ! Done
      case m => throw new RuntimeException("Unrecognized message : "+m)
    }


    def load() : Unit = {
      ExternalSender.askTo(n2s3.inputLayerRef.get.getContainer, SubscribeSynchronized(LabelChangeEvent, ExternalSender.getReference(self), n2s3.buildProperties.getSynchronizerPolicy.getInputSynchronizer))
      targetNeuron.foreach{ case(_, output_neuron) =>
        ExternalSender.askTo(output_neuron, GetAllConnectionProperty(SynapseWeightAndDelay))
          .asInstanceOf[ConnectionPropertyValues[(Float, Time)]].values
          .map { case (connectionId, inputNeuron, (weight, delay)) =>
          if(!neuronSubscribed.contains(inputNeuron)) {
            ExternalSender.askTo(inputNeuron, SubscribeSynchronized(NeuronFireEvent, ExternalSender.getReference(self), n2s3.buildProperties.getSynchronizerPolicy.getInputSynchronizer))
            neuronSubscribed += inputNeuron
          }
          connections += ((inputNeuron, output_neuron, weight))
        }
      }

      forwardConnections ++= connections.zipWithIndex.groupBy(_._1._1).map{ case (input_neuron, list) =>
        (input_neuron, list.map(_._2))
      }

      backwardConnections ++= connections.zipWithIndex.groupBy(_._1._2).map{ case (output_neuron, list) =>
        (output_neuron, list.map(_._2))
      }
    }

    def execute() : Unit = {
      val winner = targetNeuron(currentInputLabel.get)

      inputPattern.foreach{ case(neuron, timestamp) =>
        forwardConnections(neuron).foreach{ index =>
          val (input_neuron, output_neuron, weight) = connections(index)
          if(output_neuron == winner) {
            connections.update(index, (input_neuron, output_neuron, weight+learningRate*(1.0-weight)))
          }
          else {
            connections.update(index, (input_neuron, output_neuron, weight-learningRate*weight))
          }
        }
      }

      // TODO th = sum(infl)

    }

    def save() : Unit = {

      targetNeuron.foreach { case(label, neuron) =>

        val properties = ExternalSender.askTo(neuron, GetAllConnectionProperty(SynapseWeightAndDelay))
          .asInstanceOf[ConnectionPropertyValues[(Float, Time)]].values
          .map { case (connectionId, inputNeuron, (weight, delay)) =>
          val find_connection = connections.filter(c => c._1 == inputNeuron && c._2 == neuron)
          val connection = (inputNeuron, neuron, delay, if(find_connection.size == 1) find_connection.head._3 else weight.toDouble)

          (connectionId, (connection._4.toFloat, connection._3))
        }
        ExternalSender.askTo(neuron, SetAllConnectionProperty(SynapseWeightAndDelay, properties))
      }
    }

  }

  val actor = n2s3.system.actorOf(Props(new SupervisedLinearClassifierActor(n2s3, targetNeuron, learningRate)), LocalActorDeploymentStrategy)
  Await.result(actor ? Done, timeout.duration)

  def destroy() : Unit = {
    Await.result(actor ? Close, timeout.duration)
  }
}