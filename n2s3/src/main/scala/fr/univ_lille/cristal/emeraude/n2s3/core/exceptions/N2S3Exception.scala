package fr.univ_lille.cristal.emeraude.n2s3.core.exceptions

/**
  * Created by guille on 6/2/16.
  */
class N2S3Exception(message: String) extends Exception(message)