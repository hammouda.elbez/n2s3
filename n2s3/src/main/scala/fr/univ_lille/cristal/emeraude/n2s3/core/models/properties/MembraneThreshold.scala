package fr.univ_lille.cristal.emeraude.n2s3.core.models.properties

import fr.univ_lille.cristal.emeraude.n2s3.core.Property
import squants.electro.ElectricPotential

/**
  * Created by falezp on 25/10/16.
  */

/**
  * Property used to represent membrane threshold of a neuron
  * @tparam T is the threshold value type
  */
class MembraneThreshold[T] extends Property[T]

//
//  Type specializations
//
object MembraneThresholdFloat extends MembraneThreshold[Float]

object MembranePotentialThreshold extends MembraneThreshold[ElectricPotential]
