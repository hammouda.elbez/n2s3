package fr.univ_lille.cristal.emeraude.n2s3.features.io.input

import java.awt.image.BufferedImage
import java.io.{File, FileNotFoundException}
import javax.activation.MimetypesFileTypeMap
import javax.imageio.ImageIO

import scala.util.Random

/**
  * Created by falezp on 23/03/17.
  */

object ImageAdapter {

  def center(expected : Int, actual : Int) : Int = {
    (actual-expected)/2
  }

  def random(expected : Int, actual : Int) : Int = {
    Random.nextInt(actual-expected)
  }
}


case class ImageAdapterInput(labelFunction : String => String, expectedWidth : Int, expectedHeight : Int, downSizeXfactor : Int = 1, downSizeYfactor : Int = 1, offsetFunction : (Int, Int) => Int = ImageAdapter.center, shuffle : Boolean = false, trainRatio : Float = 0.8f) extends InputFormat[InputSample2D[Float]](Shape(expectedWidth, expectedHeight)) {
  def DataFrom(directory: String, recursive : Boolean = false) = new ImageAdapter(directory, recursive, labelFunction, expectedWidth, expectedHeight, downSizeXfactor, downSizeYfactor, offsetFunction, shuffle, trainRatio)
}

class ImageAdapter(val directory: String, recursive : Boolean, labelFunction : String => String, expectedWidth : Int, expectedHeight : Int, downSizeXfactor : Int = 1, downSizeYfactor : Int = 1, offsetFunction : (Int, Int) => Int, shuffle : Boolean, trainRatio : Float) extends InputGenerator[InputSample2D[Float]] {

  private val folder = new File(directory)

  if(!folder.exists())
    throw new FileNotFoundException(directory)


  def getImageRecursive(file : File) : Seq[File] = {
    if(file.isDirectory) {
      file.listFiles().flatMap(f => getImageRecursive(f))
    }
    else if(file.isFile && file.getAbsolutePath.contains(".")) {
      val ext = file.getAbsolutePath.split("\\.").last.toLowerCase

      if(ext == "jpg" || ext == "jpeg")
        Seq(file)
      else
        Seq()
    }
    else
      Seq()
  }

  private var imageList = if(recursive) {
    getImageRecursive(folder)
  }
  else {
    folder.listFiles().filter { image =>
      new MimetypesFileTypeMap().getContentType(image).split("/").head == "image"
    }.toSeq
  }

  if(shuffle) {
    imageList = Random.shuffle(imageList)
  }

  val(trainList, testList) = imageList.splitAt((imageList.size.toFloat*trainRatio).toInt)

  var imageCursor = 0

  var isTraining = true

  def setTrain(state : Boolean) : Unit = {
    reset()
    isTraining = state
  }

  private def toRGB(i : Int) : (Float, Float, Float) = (
    ((i >> 16) & 0xFF).toFloat/255f,
    ((i >> 8) & 0xFF).toFloat/255f,
    ((i >> 0) & 0xFF).toFloat/255f
  )

  private def toGrayScale(rbg : (Float, Float, Float)) : Float =  0.2126f*rbg._1+0.7152f*rbg._2+0.0722f*rbg._3

  def next(): InputSample2D[Float] = {

    var file : File = null
    var image : BufferedImage = null

    while(image == null) {

      try {
        file = if (isTraining) trainList(imageCursor) else testList(imageCursor)
        image = ImageIO.read(file)
      }
      catch {
        case e : Exception => image = null
      }
      imageCursor = if (isTraining) (imageCursor + 1) % trainList.length else (imageCursor + 1) % testList.length
    }

    val name = file.getName

    val xOffset = offsetFunction(expectedWidth, image.getWidth)
    val yOffset = offsetFunction(expectedHeight, image.getHeight)

    val pixels = for(x <- 0 until expectedWidth) yield {
      for(y <- 0 until expectedHeight) yield {
        val computedX = xOffset+x
        val computedY = yOffset+y

        if(computedX < 0 || computedX >= image.getWidth || computedY < 0 || computedY >= image.getHeight)
          0f
        else
          toGrayScale(toRGB(image.getRGB(computedX, computedY)))
      }
    }

    new InputSample2D[Float](pixels).setMetaData(InputLabel(labelFunction(name)))
  }

  def atEnd(): Boolean = imageCursor >= (if(isTraining) trainList.length else testList.length)

  def reset(): Unit = {
    imageCursor = 0
  }

  def shape: Shape = Shape(expectedWidth/downSizeXfactor, expectedHeight/downSizeYfactor)

  override def toString: String = "[ImagePart] "+directory+" : "+ imageCursor + " / " + imageList.length
}
