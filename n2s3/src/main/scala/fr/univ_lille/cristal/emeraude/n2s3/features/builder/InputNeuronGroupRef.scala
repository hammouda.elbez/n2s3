package fr.univ_lille.cristal.emeraude.n2s3.features.builder

import fr.univ_lille.cristal.emeraude.n2s3.core.Neuron
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.{InputLayer, InputNeuron, NetworkEntityActor}
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.{InputPacket, N2S3InputPacket, StreamSupport}
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.{ActorPerNeuronGroupPolicy, LocalActorDeploymentStrategy}


/**
  * An [[InputNeuronGroupRef]] is a [[NeuronGroupRef]] that includes input neurons and is bound to a stream of data.
  * This input neuron group contains as many neurons as the dimension of the input. I.e. if the input has a dimension of 748 bits, then the input neuron group will contain 748 neurons.
  * Each of the input neurons can be connected to neurons of another neuron group.
 *
  * @param simulation the current simulation object
  */
class InputNeuronGroupRef(val simulation: N2S3, neuronConstructor : () => Neuron = () => new InputNeuron) extends NeuronGroupRef(simulation) {

  var stream: Option[StreamSupport[InputPacket, N2S3InputPacket]] = None
  this.setIdentifier("Input")
  this.actorPolicy = Some(new ActorPerNeuronGroupPolicy(LocalActorDeploymentStrategy))
  this.defaultNeuronConstructor = neuronConstructor

  /**
    * Sets the stream to be used to feed the network with input.
    *
    * @param stream the stream to be set
    * @return the [[InputNeuronGroupRef]]
    */
  def setInput(stream: StreamSupport[_ <: InputPacket, N2S3InputPacket]) = {
    this.stream = Some(stream.asInstanceOf[StreamSupport[InputPacket, N2S3InputPacket]])
    this.shape = this.stream.get.shape
    if (this.isDeployed) {
    //  this.simulation.scheduleMessage(this.getActor.getContainer, SetInputStream(stream))
    }
    this
  }

  override def getIdentifierOf(ref: NeuronRef): Any = this.shape.toIndex(ref.getIndex:_*)

  override def newActorProps() = NetworkEntityActor.newPropsBuilder().setEntity(new InputLayer(this.stream.get))

  /**
    * Resets the current input stream and notifies the underlying actor about it.
    */
  def reset(): Unit = {
    //stream.get.clean()
    if (this.isDeployed) {
//      this.simulation.scheduleMessage(this.getActor.getContainer, SetInputStream(stream.get))
    }
  }


  /**
    * The columns of the current input stream
    *
    * @return an [[Int]] with the number of columns in the input stream
    */
  def columns: Int = this.stream.get.shape(1)
}
