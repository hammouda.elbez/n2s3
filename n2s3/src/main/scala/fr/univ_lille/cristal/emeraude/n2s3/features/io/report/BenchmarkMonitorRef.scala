/************************************************************************************************************
 * Contributors:
 * 		- created by guillermo.polito@univ-lille1.fr
 ***********************************************************************************************************/
package fr.univ_lille.cristal.emeraude.n2s3.features.io.report

import java.io._

import akka.actor.{ActorRef, Props}
import akka.pattern.ask
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.{Config, Done}
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.{N2S3, NeuronGroupObserverRef, NeuronGroupRef}
import fr.univ_lille.cristal.emeraude.n2s3.support.Time
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.LocalActorDeploymentStrategy

import scala.concurrent.Await

class BenchmarkMonitorRef(neuronGroup: NeuronGroupRef, timeOffset : Time = Time(0)) extends NeuronGroupObserverRef {

  implicit val timeout = Config.longTimeout
  var actor: Option[ActorRef] = None
  def getActors : Seq[ActorRef] = this.actor.toSeq

  override protected def deploy(n2s3: N2S3): Unit = {
    actor = Some(n2s3.system.actorOf(Props(new BenchmarkMonitor(n2s3, neuronGroup.neuronPaths, timeOffset)), LocalActorDeploymentStrategy))
    Await.result(actor.get ? Done, timeout.duration)
  }

  def getResult: BenchmarkResult = {
    Await.result(akka.pattern.ask(actor.get, GetResult()), timeout.duration).asInstanceOf[BenchmarkResult]
  }

  def exportToHtmlView(filename: String) = {
    new BenchmarkHTMLView(this.getResult).save(filename)
  }

  override def toString: String = {
    val result = this.getResult
    s"Score = ${result.evaluationByMaxSpiking.score} / ${result.inputNumber}"
  }

  def waitProcess() : Unit = {
    Await.result(actor.get ? Done, timeout.duration)
  }

  def saveCrops(filename : String,time : String,cropInfo : String) = {
    val file = new File(filename)
    val bw = new BufferedWriter(new FileWriter(file,true))

    bw.write("\nT: "+time)
    bw.write("\nCrop : "+cropInfo)

    bw.close()
  }
}
