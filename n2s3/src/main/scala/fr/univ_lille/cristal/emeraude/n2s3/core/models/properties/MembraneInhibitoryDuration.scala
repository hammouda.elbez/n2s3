package fr.univ_lille.cristal.emeraude.n2s3.core.models.properties

import fr.univ_lille.cristal.emeraude.n2s3.core.Property
import fr.univ_lille.cristal.emeraude.n2s3.support.Time

/**
  * Created by omariott on 13/04/17.
  */

/**
  * Property used to represent membrane inhibitory duration
  */
object MembraneInhibitoryDuration extends Property[Time]
