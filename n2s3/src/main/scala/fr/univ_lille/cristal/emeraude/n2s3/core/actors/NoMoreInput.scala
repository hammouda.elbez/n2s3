package fr.univ_lille.cristal.emeraude.n2s3.core.actors

/**
  * Response sent following a new input request, when current stream has no more input
  */
object NoMoreInput extends SyncMessage
