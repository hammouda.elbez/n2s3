package fr.univ_lille.cristal.emeraude.n2s3.features.logging

import akka.actor.{Actor, ActorRef, PoisonPill, Props}
import com.mongodb.BasicDBObject
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.Done
import fr.univ_lille.cristal.emeraude.n2s3.core.event.{SynapseWeightChange, _}
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.SynapticWeightFloat
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.{N2S3, NeuronGroupObserverRef, NeuronGroupRef}
import fr.univ_lille.cristal.emeraude.n2s3.features.io.report.BenchmarkMonitorRef
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.{LocalActorDeploymentStrategy, Message}
import org.mongodb.scala.bson._
import org.mongodb.scala.{Observer, Document, _}

import scala.collection.immutable
import scala.sys.process._

/**
  * Hammouda Elbez
  *
  * Fichier responsable sur la collection des informations de la simulation et le stockage dans la base de données Mongo
  *
  */


class SimMongoLog(n2s3 : N2S3,list : IndexedSeq[IndexedSeq[IndexedSeq[ConnectionPath]]],name : String, Spikes : Boolean,  Potential : Boolean, Label : Boolean,SynapticWeight : Boolean,Data:String,time: String,SynapseIndex : List[(Integer, Integer, Integer)], globalTimestamps: Long) {

  /** ******************************************************************************************************************
    * Setup DB connection & Collection
    * *****************************************************************************************************************/

  println("Connecting to database ")
  val USERNAME = ""
  val PASSWORD = ""
  val HOST = "127.0.0.1:27017"
  val mongoClient: MongoClient = MongoClient(if (USERNAME != "" && PASSWORD != "") "mongodb://"+USERNAME+":"+PASSWORD+"@"+HOST+"/?authSource=admin&readPreference=primaryPreferred" else "mongodb://"+HOST+"/?authSource=admin&readPreference=primaryPreferred")
  val database: MongoDatabase = mongoClient.getDatabase("n2s3-"+time.replaceAll("\\s", "-"))
  var singleLayersDocument = Document()
  var neuroneS,neuroneE = 0
  var id = 0
  var step = 1000
  var ArchiCollection : MongoCollection[Document] = _
  var InfoCollection : MongoCollection[Document] = _
  var SpikesCollection : MongoCollection[Document] = _
  var PotentialCollection : MongoCollection[Document] = _
  var LabelCollection : MongoCollection[Document] = _
  var SynapseWeightCollection : MongoCollection[Document] = _
  var layerS,layerE = ""
  var documentArchi : List[Document] = List.empty
  var Cnx: List[String] = List[String]()
  var Layers = Map[String,Int]()
  var Lid = 0
  var loggerSpike: ActorRef = _
  var loggerPotential : ActorRef = _
  var loggerLabel : ActorRef = _

  /**
    * Collections
    */

  ArchiCollection = database.getCollection("archi")
  ArchiCollection.deleteMany(new BasicDBObject())
  InfoCollection = database.getCollection("info")
  InfoCollection.deleteMany(new BasicDBObject())
  PotentialCollection = database.getCollection("potential")
  SpikesCollection = database.getCollection("spikes")
  LabelCollection = database.getCollection("labels")
  SynapseWeightCollection = database.getCollection("synapseWeight")
  
  var synapticWeightMonitoring : SynapticWeightMonitoring = _
  /**
    * Observer for the mongo operations
    */

  var observer: Observer[Completed] = new Observer[Completed] {
    override def onNext(result: Completed): Unit = None

    override def onError(e: Throwable): Unit = print(e.printStackTrace())

    override def onComplete(): Unit = None
  }

  def storeSimInfoAndDestroy(): Unit ={
    /**
      * stop the loggers, monitors and mongo client
      */
    if(loggerSpike != null){
      loggerSpike ! Done
      loggerSpike ! PoisonPill
    }  
    if(loggerPotential != null){
      loggerPotential ! Done
      loggerPotential ! PoisonPill
    } 
    if(loggerLabel != null){
      loggerLabel ! Done
      loggerLabel ! PoisonPill
    }
    if(synapticWeightMonitoring != null) synapticWeightMonitoring.destroy()

  }

  def storeBenchmarkTestInfo(benchmark : BenchmarkMonitorRef): Unit ={
    var NeuronLabel : List[Document]= List()
    benchmark.getResult.evaluationByMaxSpiking.labelAssoc.foreach(s => {
      NeuronLabel = NeuronLabel.::(Document("L" -> s._1.split(":")(0),"N" -> s._1.split(":")(1), "Label"-> s._2))
    })

    InfoCollection.insertOne(Document(
      "Acc" -> (benchmark.getResult.evaluationByMaxSpiking.score * 100.00D) / benchmark.getResult.inputNumber,
      "NLabel" -> NeuronLabel)).subscribe(observer)
      /**
      "FirstS" -> (benchmark.getResult.evaluationByFirstSpiking.score * 100.00D) / benchmark.getResult.inputNumber,
      "OneS" -> (benchmark.getResult.evaluationByOneSpiking.score * 100.00D) / benchmark.getResult.inputNumber,
      "QtInput" -> benchmark.getResult.quietInputCount,
      **/
    mongoClient.close()
  }

  if (n2s3.first){
  n2s3.first = false
  println("Starting Log")

  /** ******************************************************************************************************************
    * Architecture Save
    * *****************************************************************************************************************/

  var layerNeron : Map[String,Int] = Map()
  n2s3.layers.foreach(layer => {
    layerNeron += (layer.identifier -> layer.neurons.size)
  })
  InfoCollection.insertOne(Document("n" -> name,"L:N"->Document(layerNeron),"T"->time,"D"->Data)).subscribe(observer)

  n2s3.layers.foreach(elt => {
    Layers = Layers + (elt.getIdentifier -> Lid)
    Lid = Lid + 1})
  }else{
  println("Resume Log")
  }
  
  /** ******************************************************************************************************************
    * Starting Spiking & Potential monitoring
    * *****************************************************************************************************************/

  if(Spikes){
    println("Setting Spikes monitor")
    loggerSpike = n2s3.system.actorOf(Props(new NeuronsFireLog(name,SpikesCollection,observer,Layers,step, globalTimestamps)), LocalActorDeploymentStrategy)
    
    n2s3.layers.foreach(Layer =>
      if(Layer.identifier != "Input") {
        Layer.neuronPaths.foreach { n => {
          ExternalSender.askTo(n, Subscribe(NeuronFireEvent, ExternalSender.getReference(loggerSpike)))
        }}}
    )}

  if(Potential){
    println("Setting Potential monitor")
    loggerPotential = n2s3.system.actorOf(Props(new NeuronsPotentialLog(name,PotentialCollection,observer,Layers, globalTimestamps)), LocalActorDeploymentStrategy)
    n2s3.layers.foreach(Layer =>
      if(Layer.identifier != "Input"){
        Layer.neuronPaths.foreach{ n =>{
          ExternalSender.askTo(n, Subscribe(NeuronPotentialUpdateEvent, ExternalSender.getReference(loggerPotential)))}
        }})}

  if(Label) {
    println("Setting Label monitor")
    loggerLabel = n2s3.system.actorOf(Props(new LabelMonitoring(n2s3, n2s3.layers, LabelCollection, observer, globalTimestamps)), LocalActorDeploymentStrategy)
    ExternalSender.askTo(n2s3.inputLayerRef.get.getContainer, Subscribe(LabelChangeEvent, ExternalSender.getReference(loggerLabel)))
  }

  if(SynapticWeight){
    println("Setting SynapseWeight monitor")
    synapticWeightMonitoring = new SynapticWeightMonitoring(list,10,SynapseWeightCollection,observer,"",step,globalTimestamps,SynapseIndex)
    n2s3.addNetworkObserver(synapticWeightMonitoring)}

}

/** ******************************************************************************************************************
  * Spikes logger
  * *****************************************************************************************************************/

class NeuronsFireLog(name : String,mongoCollection: MongoCollection[Document], observer: Observer[Completed],Layers: Map[String,Int],step:Int,globalTimestamps:Timestamp) extends NetworkActor {
  var i = 0
  var id = 0
  var time: Long = 0
  var documents : List[Document] = List.empty

  override def initialize(): Unit = {}
  override def process(message: Message, sender : ActorRef): Unit = message match {

    case NeuronFireResponse(timestamp, path) =>
    //  if (((System.currentTimeMillis() - globalTimestamps)) - time >= 0.250){
        time = (System.currentTimeMillis() - globalTimestamps)
        documents = documents.::(Document("i" -> Document("L" -> path.actor.path.name.split(":")(0),
          "N" -> BsonInt32.apply(Integer.valueOf(path.actor.path.name.split(":")(1)))),"T" -> BsonDouble.apply(BigDecimal(time/1000.0).setScale(6, BigDecimal.RoundingMode.HALF_UP).toDouble), if(GlobalLogVars.ActualInput == "") "Input" -> "" else "Input" -> BsonInt32.apply(Integer.valueOf(GlobalLogVars.ActualInput))))
        id = id + 1
        i = i + 1

        if(i > 10000){
          mongoCollection.insertMany(documents).subscribe(observer)
          documents = List.empty
          i = 0
        }
     //   }

    case InputRestartResponse(source) =>
    case m => if(m.toString == "Done"){destroy()} else println("[NeuronsFireLogText] another msg : "+m)
  }

  override def destroy(): Unit = {
    if (documents != List.empty){
    mongoCollection.insertMany(documents).subscribe(observer)}
  }

}


/** ******************************************************************************************************************
  * Neuron potential Logger
  * *****************************************************************************************************************/

class NeuronsPotentialLog(name : String,mongoCollection: MongoCollection[Document], observer: Observer[Completed],Layers: Map[String,Int],globalTimestamps:Timestamp) extends NetworkActor {
  var i = 0
  var id = 0
  var time: Long = 0
  var documents : List[Document] = List.empty
  override def initialize(): Unit = {}

  override def process(message: Message, sender: ActorRef): Unit = message match {
    case NeuronPotentialResponse(timestamp, source, value) =>
      if (((System.currentTimeMillis() - globalTimestamps)) - time >= 0.250){
        time = (System.currentTimeMillis() - globalTimestamps)
        documents = documents.::(Document("T" -> BsonDouble.apply(BigDecimal(time/1000.0).setScale(6, BigDecimal.RoundingMode.HALF_UP).toDouble),"L" -> source.actor.path.name.split(":")(0), "N" -> BsonInt32(Integer.valueOf(source.actor.path.name.split(":")(1))), "V" ->
            BsonDouble.apply(BigDecimal(value.toDouble).setScale(6, BigDecimal.RoundingMode.HALF_UP).toDouble)))
        // "_id" -> id,
        id = id + 1
        i = i + 1

        if(i > 10000){
          mongoCollection.insertMany(documents).subscribe(observer)
          documents = List.empty
          i = 0
        }
        }

    case m => if(m.toString == "Done"){destroy()} else println("[NeuronsPotentialLogText] Unknown message " + m)
  }

  override def destroy(): Unit = {
    if (documents != List.empty){
    mongoCollection.insertMany(documents).subscribe(observer)}
  }

}

/** ******************************************************************************************************************
  * Label changement Logger for supervised learning
  * *****************************************************************************************************************/

class LabelMonitoring(n2s3 : N2S3, outputNeuron : Seq[NeuronGroupRef],mongoCollection: MongoCollection[Document],observer: Observer[Completed], globalTimestamps:Timestamp) extends Actor {
  var i = 0
  var id = 0
  var time: Long = 0
  var documents : List[Document] = List.empty
  override def postStop() = {

    outputNeuron.foreach{Layer =>

      Layer.neuronPaths.foreach { n => {
        ExternalSender.askTo(n, Unsubscribe(NeuronFireEvent, ExternalSender.getReference(self)))

      }}}

    ExternalSender.askTo(n2s3.inputLayerRef.get.getContainer, Unsubscribe(LabelChangeEvent, ExternalSender.getReference(self)))
  }

  def receive = {
    case LabelChangeResponse(start, end, label) =>
      time = (System.currentTimeMillis() - globalTimestamps)
      GlobalLogVars.ActualInput = label
      GlobalLogVars.globalInput = GlobalLogVars.globalInput + 1
      documents = documents.::(Document("L" -> BsonInt32(Integer.valueOf(label)),"T" ->BsonDouble.apply(BigDecimal(time/1000.0).setScale(6, BigDecimal.RoundingMode.HALF_UP).toDouble),"G" -> GlobalLogVars.globalInput))

      id = id + 1
      i = i + 1

      if(i > 10){
        mongoCollection.insertMany(documents).subscribe(observer)
        i = 0
        documents = List.empty
      }
    
    case m => if(m.toString == "Done"){destroy()}
  }

  def destroy(): Unit = {
    if (documents != List.empty){
    mongoCollection.insertMany(documents).subscribe(observer)}
  }

}

/** ******************************************************************************************************************
  * Synapse weight Logger
  * *****************************************************************************************************************/

class SynapticWeightMonitoring(list : IndexedSeq[IndexedSeq[IndexedSeq[ConnectionPath]]], refreshRate : Int = 1000/24,mongoCollection: MongoCollection[Document],observer: Observer[Completed], name : String = "",step:Int,globalTimestamps:Timestamp,SynapseIndex : List[(Integer, Integer, Integer)]) extends NeuronGroupObserverRef {
  var actor: Option[ActorRef] = None
  def getActors : Seq[ActorRef] = this.actor.toSeq


  override def deploy(n2s3: N2S3): Unit = {

    this.actor = Some(n2s3.system.actorOf(Props(new SynapticWeight(list, refreshRate, name,mongoCollection,observer,step,globalTimestamps,SynapseIndex)), LocalActorDeploymentStrategy))
  }
}

class SynapticWeight(list : Seq[Seq[Seq[ConnectionPath]]], refreshRate : Int = 1000/24, name : String,mongoCollection: MongoCollection[Document],observer: Observer[Completed],step:Int,globalTimestamps:Timestamp,SynapseIndex : List[(Integer, Integer, Integer)]) extends AutoRefreshNetworkActor(refreshRate, list.size) {

  var documents: List[Document] = List.empty

  class WeightPanel(list : Seq[Seq[ConnectionPath]]){
    var time: Long = 0
    val synapseList: Map[NetworkEntityPath, Seq[(Int, Int, ConnectionPath)]] = list.zipWithIndex.flatMap { case (l, x) => l.zipWithIndex.map { case (p, y) => (x, y, p) } }.groupBy(_._3.outputNeuron)
    var values: immutable.Iterable[(Int, String, Float)] = synapseList.flatMap(_._2.map(e => (e._1, e._2.toString(), 0f)))
    var valuesTmp: immutable.Iterable[(Int, String, Float)] = values

    var i = 0
    var x = 0
    var y = 0
    var tempdocs: List[Document] = List.empty
    var filteredconnectionList : Seq[(Int,Int,ConnectionPath)] = Seq.empty
    
    def update(): Unit = {
      values = synapseList.flatMap { case (neuron, connectionList) => {
        //print("\n",connectionList.head._1,"|",connectionList.head._2.toString())
        if (connectionList.size == 1) {
          Some((connectionList.head._1, connectionList.head._2.toString(),
            ExternalConnectionSender.askTo(connectionList.head._3, GetConnectionProperty(SynapticWeightFloat))
              .asInstanceOf[PropertyValue[Float]].value))
        }
        else {
          val values = ExternalSender.askTo(neuron, GetAllConnectionProperty(SynapticWeightFloat))
            .asInstanceOf[ConnectionPropertyValues[Float]].values.groupBy(_._1)
          
          filteredconnectionList = Seq.empty
          connectionList.foreach(
            cnx => {
              if(values.keys.toList.contains(cnx._3.connectionID)){
                filteredconnectionList = filteredconnectionList.+:(cnx._1,cnx._2,cnx._3)
              }
            }
          )
          filteredconnectionList.map{ case (x, y, p) =>
            (p.connectionID, p.outputNeuron.actor.path.name.toString(), values(p.connectionID).head._3)
          }
          
        }
      }
      }
      if (i < 10) {
        if(valuesTmp.isEmpty){
          valuesTmp = values
        }else{
          values.foreach(x =>{
            if(valuesTmp.exists(y => y == x)){
              values = values.dropWhile(y => y == x)}
          })}

        //print("\n",values.size,"\n\n")
        values.foreach(v => {
          SynapseIndex.foreach(cnx => {
            if (cnx._1 == v._1) {
              x = cnx._3
              y = cnx._2
            }
          })
          documents = documents.::(Document("T" -> BsonDouble.apply(BigDecimal((System.currentTimeMillis() - globalTimestamps)/1000.0).setScale(6, BigDecimal.RoundingMode.HALF_UP).toDouble), "C" -> BsonInt32.apply(Integer.valueOf(v._2.split(":")(0).takeRight(1))-1), "To" -> BsonInt32.apply(Integer.valueOf(v._2.split(":")(1))), "V" ->
            BsonDouble.apply(BigDecimal(v._3.toDouble).setScale(6, BigDecimal.RoundingMode.HALF_UP).toDouble),"index" -> Document("x" -> y, "y" -> x), "L" -> v._2.split(":")(0)))
        })
        valuesTmp = values
        values = List.empty
        i = i + 1
      } else {
        if(documents.nonEmpty){
          mongoCollection.insertMany(documents).subscribe(observer)
          i = 0
          documents = List.empty}
      }
    }
  }


  val panels: Seq[WeightPanel] = list.map{ l =>
    new WeightPanel(l)
  }

  override def initialize(): Unit = {
    println("Initializing ....")

  }

  override def destroy(): Unit = {
    if (documents != List.empty){
    mongoCollection.insertMany(documents).subscribe(observer)}
  }

  override def process(message: Message, sender : ActorRef): Unit = {
  }

  override def update(n: Int): Unit = {
    panels(n).update()
  }
}

class ActivityStoreActor(n2s3 : N2S3) extends NetworkActor {

  var startTimestamp : Option[Timestamp] = None

  var currentTimestamp : Timestamp = -1L
  var eventCount : Long = 0



  override def initialize(): Unit = {
    askTo(n2s3.inputLayerRef.get.getContainer, SubscribeSynchronized(SynapseWeightChange, ExternalSender.getReference(self), n2s3.buildProperties.getSynchronizerPolicy.getInputSynchronizer))

  }

  override def destroy(): Unit = {
    askTo(n2s3.inputLayerRef.get.getContainer, Unsubscribe(SynapseWeightChange, ExternalSender.getReference(self)))
  }

  override def process(message: Message, sender: ActorRef): Unit = message match {
    case SynchronizedEvent(s, _, m, _) =>
      m match {
        case SynapseWeightResponse(timestamp,source,value) =>
          println(timestamp, " " , source , " ", value)
      }
      ExternalSender.sendTo(s, Done)
  }

}

object GlobalLogVars {
  var ActualInput = ""
  var globalInput = 0
}
