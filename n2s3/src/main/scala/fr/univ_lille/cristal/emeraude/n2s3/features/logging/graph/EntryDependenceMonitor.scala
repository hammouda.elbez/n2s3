package fr.univ_lille.cristal.emeraude.n2s3.features.logging.graph

import akka.actor.{Actor, Props}
import akka.pattern.ask
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.{Config, Done}
import fr.univ_lille.cristal.emeraude.n2s3.core.event._
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.SynapseWeightAndDelay
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.N2S3
import fr.univ_lille.cristal.emeraude.n2s3.features.logging.graph.NeuronDependenceTikz.Data
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias._
import fr.univ_lille.cristal.emeraude.n2s3.support.Time
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.LocalActorDeploymentStrategy

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import scala.concurrent.Await

/**
  * Created by falezp on 16/11/16.
  */
class EntryDependenceMonitor(n2s3 : N2S3, maxDelay : Time, outputs : Seq[(String, NetworkEntityPath)], outputFilename : Option[String]) {

  case class ResultEntry(label : String, winner : NetworkEntityPath, involvedNeuron : Map[NetworkEntityPath, Seq[NetworkEntityPath]], involvedInput : Map[NetworkEntityPath, Seq[NetworkEntityPath]])

  class Result(outputs : Seq[(String, NetworkEntityPath)], data : Seq[ResultEntry]) {

    class SubResult1(data : Seq[Int]) {
      val avg : Double = data.sum.toDouble/data.size.toDouble
      val std : Double = data.map(i => (i-avg)*(i-avg)).sum/data.size.toDouble
    }

    val involvedNeuron = Map(outputs.map { case(label, path) =>
      (label, data.flatMap(_.involvedNeuron.getOrElse(path, Seq())).distinct)
    }:_*)

    val involvedNeuronCount : Map[String, SubResult1] = Map(outputs.map { case(label, path) =>
      (label, new SubResult1(data.filter(e => e.involvedNeuron.isDefinedAt(path)).map(e => e.involvedNeuron(path).size)))
    }:_*)
    val involvedInputCount : Map[String, SubResult1] = Map(outputs.map { case(label, path) =>
      (label, new SubResult1(data.filter(e => e.involvedInput.isDefinedAt(path)).map(e => e.involvedInput(path).size)))
    }:_*)

    val involvedNeuronWinnerCount : Map[String, SubResult1] = Map(outputs.map { case(label, path) =>
      (label, new SubResult1(data.filter(e => e.involvedNeuron.isDefinedAt(path) && e.label == label && e.winner == path).map(e => e.involvedNeuron(path).size)))
    }:_*)

    val involvedInputWinnerCount : Map[String, SubResult1] = Map(outputs.map { case(label, path) =>
      (label, new SubResult1(data.filter(e => e.involvedInput.isDefinedAt(path) && e.label == label && e.winner == path).map(e => e.involvedInput(path).size)))
    }:_*)
  }

  object Close
  object Print
  object Result

  class EntryDependenceMonitorActor(n2s3 : N2S3, maxDelay : Time, outputs : Seq[(String, NetworkEntityPath)], outputFilename : Option[String]) extends Actor {

    val resultEntries = mutable.ArrayBuffer[ResultEntry]()
    val fires = mutable.HashMap[NetworkEntityPath, ArrayBuffer[Timestamp]]()
    val data = mutable.ArrayBuffer[Data]()
    var currentLabel : String = ""
    var cpt = 0

    ExternalSender.askTo(n2s3.inputLayerRef.get.getContainer, SubscribeSynchronized(LabelChangeEvent, ExternalSender.getReference(self), n2s3.buildProperties.getSynchronizerPolicy.getInputSynchronizer))

    n2s3.layers.flatMap(_.neuronPaths).foreach { path =>
      ExternalSender.askTo(path, SubscribeSynchronized(NeuronFireEvent, ExternalSender.getReference(self), n2s3.buildProperties.getSynchronizerPolicy.getInputSynchronizer))
    }

    override def receive = {
      case SynchronizedEvent(s, _, m, _) =>
        m match {
          case LabelChangeResponse(start, end, label) =>
            println(start+" "+label)
            export()
            currentLabel = label
            fires.clear()
            data.clear()
          case NeuronFireResponse(timestamp, source) =>

            val inputs = ExternalSender.askTo(source, GetAllConnectionProperty(SynapseWeightAndDelay)) match {
              case PropertyValue(values: Seq[(NetworkEntityPath, (Float, Time))]@unchecked) => values
              case _ => Seq()
            }

            inputs.foreach { case(input_neuron, (weight, delay)) =>
              //println("in="+input_neuron)
              fires.get(input_neuron) match {
                case Some(listSpike) =>
                  listSpike.filter(t => timestamp >= t && timestamp-(t+delay.timestamp) < maxDelay.timestamp).foreach { t =>
                    data += Data((input_neuron, t), (source, t+delay.timestamp), weight)
                  }
                case None =>
              }
            }

            fires.getOrElseUpdate(source, ArrayBuffer()) += timestamp

          case _ => throw new RuntimeException("Unrecognized message : "+m)
        }
        ExternalSender.sendTo(s, Done)
      case Close =>
        ExternalSender.askTo(n2s3.inputLayerRef.get.getContainer, Unsubscribe(LabelChangeEvent, ExternalSender.getReference(self)))

        n2s3.layers.flatMap(_.neuronPaths).foreach { path =>
          ExternalSender.askTo(path, Unsubscribe(NeuronFireEvent, ExternalSender.getReference(self)))
        }
        sender ! Done
        context.stop(self)

      case Print =>
        export()
        sender ! Done
      case Result =>
        sender ! result()
      case m => throw new RuntimeException("Unrecognized message : "+m)
    }

    def export() = {
      if(currentLabel != "") {
/*
        println("outputs : "+outputs.mkString(", "))
        println("fires : "+fires.mkString(", "))
*/
        val outputDependence = Map(outputs.filter(e => fires.isDefinedAt(e._2)).map { case(outLabel, outPath) =>
          val currentData = mutable.ArrayBuffer[Data]()
          val processQueue = mutable.Queue[(NetworkEntityPath, Timestamp)]()
          processQueue.enqueue((outPath, fires(outPath).min))

          while (processQueue.nonEmpty) {
            val (n, t) = processQueue.dequeue()
            data.filter(d => d.to._2 <= t && d.to._2 >= t-maxDelay.timestamp && n == d.to._1).foreach {d =>
              processQueue.enqueue((d.from._1, d.from._2))
              currentData += d
            }

          }
          (outPath, currentData)
        }:_*)


        val allData = outputDependence.flatMap(_._2).toSeq.distinct
        val outputFire = outputs.filter(e => fires.isDefinedAt(e._2)).map{ case(outLabel, outPath) => (outPath, fires(outPath).min)}


        if(allData.isEmpty) {
          println("empty : "+data.mkString(", "))
        }

        if(allData.nonEmpty) {

          val neuronInvolved = outputDependence.map{case(out, list) =>
            (out, list.flatMap(data => Seq(data.to._1, data.from._1)).distinct)
          }


          val inputInvolved = outputDependence.map{case(out, list) =>
            (out, list.flatMap(data => Seq(data.to._1, data.from._1)).distinct.filter(path => n2s3.inputLayerRef.head.neuronPaths.contains(path)).distinct)
          }

          val winner = outputFire.minBy(_._2)._1

          resultEntries += ResultEntry(currentLabel, winner, neuronInvolved, inputInvolved)

          if(outputFilename.isDefined) {
            NeuronDependenceTikz.export(
              outputFilename.get + "_" + cpt + "_" + currentLabel,
              Some("label: " + currentLabel + ", Expected output: " + (outputs.filter(_._1 == currentLabel) match {
                case l if l.size == 1 => l.head._2
                case _ => "None"
              })),
              n2s3.layers.map(_.neuronPaths.filter(n1 => allData.exists(d1 => d1.from._1 == n1 || d1.to._1 == n1) || outputFire.exists(_._1 == n1))).filter(_.nonEmpty),
              allData,
              maxDelay,
              outputFire
            )
          }

          cpt += 1
        }

      }
    }

    def result() : Result = {
      new Result(outputs, resultEntries)
    }
  }



  val actor = n2s3.system.actorOf(Props(new EntryDependenceMonitorActor(n2s3, maxDelay, outputs, outputFilename)), LocalActorDeploymentStrategy)

  def print() : Unit = {
    implicit val timeout = Config.longTimeout
    Await.result(actor ? Print, timeout.duration)
  }

  def getResult : Result = {
    implicit val timeout = Config.longTimeout
    Await.result(actor ? Result, timeout.duration).asInstanceOf[Result]
  }

  def destroy() : Unit = {
    implicit val timeout = Config.longTimeout
    Await.result(actor ? Close, timeout.duration)
  }

}
