package fr.univ_lille.cristal.emeraude.n2s3.core.event

/**
 * Basic class for all events.
 *
 * @tparam Response is the type of the Response of the event
 */
abstract class Event[Response <: EventResponse] extends Serializable {

	/**
	 * When this parameter return true, all event observers will be removed after that event is trigger
	 */
	def isSingleUsage : Boolean = false

			/**
			 * Return a default response if none is given to the trigger method.
			 *
			 * @throws UnsupportedOperationException by default if subclasses don't override this method
			 */
			def defaultResponse : Response = throw new UnsupportedOperationException

}