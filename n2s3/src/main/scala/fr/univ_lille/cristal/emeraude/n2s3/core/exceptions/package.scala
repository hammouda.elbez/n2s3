package fr.univ_lille.cristal.emeraude.n2s3.core

/**
 * Contains the numerous exceptions thrown by this simulator (from the network to the
 * neurons/synapses)
 * @author wgouzer & qbailleul
 */
package object exceptions
