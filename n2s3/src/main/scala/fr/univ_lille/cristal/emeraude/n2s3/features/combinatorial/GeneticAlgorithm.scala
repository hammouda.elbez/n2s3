package fr.univ_lille.cristal.emeraude.n2s3.features.combinatorial

import scala.annotation.tailrec
import scala.util.Random

/**
  * Created by falezp on 12/04/16.
  */
trait Candidate {

  def evaluate() : Int

}

class GeneticAlgorithm[CandidateType <: Candidate](populationSize : Int, generationNumber : Int, crossOverNumber : Int, mutationNumber : Int,
                                                   crossOverFunction : (CandidateType, CandidateType) => CandidateType, mutationFunction : CandidateType => CandidateType, maximize : Boolean = true) {

  val unknownScore = -1

  def execute(initialPopulation : Seq[CandidateType]) : (CandidateType, Int) = {

    val rand = Random

    @tailrec
    def innerExecute(population : Map[CandidateType, Int], generation : Int) : (CandidateType, Int) = {
      if(generation < generationNumber) {
        println("[Genetic Algorithm] Generation " + (generation + 1))

        val populationCrossOver = scala.collection.mutable.ArrayBuffer[CandidateType]()
        for (crossOver <- 0 until crossOverNumber) {
          val parentIndex1 = rand.nextInt(population.size)

          val parentIndex2Raw = rand.nextInt(population.size - 1)
          val parentIndex2 = if (parentIndex2Raw < parentIndex1) parentIndex2Raw else parentIndex2Raw + 1

          populationCrossOver += crossOverFunction(population.toList(parentIndex1)._1, population.toList(parentIndex2)._1)
        }

        val populationMutation = scala.collection.mutable.ArrayBuffer[CandidateType]()
        for (mutation <- 0 until mutationNumber) {
          populationMutation += mutationFunction(population.toList(rand.nextInt(population.size))._1)
        }

        val populationFinal = population ++ populationCrossOver.map(candidate => (candidate, unknownScore)) ++ populationMutation.map(candidate => (candidate, unknownScore))

        val sortedPopulation = populationFinal.map { case (candidate, score) =>
          (candidate, if (score == unknownScore) candidate.evaluate() else score)
        }.toSeq.sortBy(_._2)

        println("### Score Generation "+generation+" ###")
        sortedPopulation.zipWithIndex.foreach{ case(entry, index) => println("#"+index+" : "+entry._2) }

        val best = if(maximize) sortedPopulation.maxBy(_._2) else sortedPopulation.minBy(_._2)
        println("Best Solution : "+best._2)
        println(best._1.toString)

        innerExecute(if(maximize) sortedPopulation.reverse.take(populationSize).toMap else sortedPopulation.take(populationSize).toMap, generation+1)
      }
      else {
        if(maximize) population.maxBy(_._2) else population.minBy(_._2)
      }
    }
    innerExecute(Map(initialPopulation.map(candidate => (candidate, unknownScore)):_*), 0)
  }

}
