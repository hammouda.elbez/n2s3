/************************************************************************************************************
 * Contributors:
 * 		- created by falezp on 07/06/16
 ***********************************************************************************************************/
package fr.univ_lille.cristal.emeraude.n2s3.models.neurons

import fr.univ_lille.cristal.emeraude.n2s3.core
import fr.univ_lille.cristal.emeraude.n2s3.core.Neuron.{ConnectionID, Inhibition, NeuronEnds}
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.{BackwardSpike, ElectricSpike}
import fr.univ_lille.cristal.emeraude.n2s3.core.event.{NeuronFireEvent, NeuronFireResponse, NeuronPotentialResponse, NeuronPotentialUpdateEvent}
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties._
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.Time
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message
import fr.univ_lille.cristal.emeraude.n2s3.support.UnitCast._
import squants.electro.CapacitanceConversions.CapacitanceConversions
import squants.electro.ElectricPotentialConversions.ElectricPotentialConversions
import squants.electro.{ElectricPotential, Millivolts}

 /**
  * Implementation of the Izhikevich neuron model 
  * 
  * More informations can be found at the following address:
  * 	- http://www.izhikevich.org/publications/spikes.htm
  *   - http://www.izhikevich.org/publications/spikes.pdf
  */

object Izhikevich extends NeuronModel {
  override def createNeuron(): Izhikevich = new Izhikevich()
}

class Izhikevich extends core.Neuron {

  /*
   * Parameters
   */
  val a = 0.02f
  val b = 0.2f
  val c = -65 millivolts
  val d = 8 millivolts

  var v = c
  var u = v*b

   var capacitance = 1 farads

   var th = 30 millivolts

  var lastTimestamps : Timestamp = 0

  val maxTimestep = 1 MilliSecond

  /********************************************************************************************************************
    * Properties and Events
    ******************************************************************************************************************/
  addProperty[ElectricPotential](MembranePotentialThreshold, () => th, th = _)
  addEvent(NeuronPotentialUpdateEvent)


  /**
    * Method used to describe the behavior of the neuron model
    *
    * @param timestamp is the current time
    * @param message   is the content
    */
  override def processSomaMessage(timestamp: Timestamp, message: Message, fromConnection : Option[(ConnectionID, NeuronConnection)], ends: NeuronEnds): Unit = message match {
    case ElectricSpike(charge) =>

      var currentTimestamp = lastTimestamps
      while(currentTimestamp != timestamp) {
        var deltaTimestamp = math.min(timestamp-currentTimestamp, maxTimestep.timestamp)
        var deltaStep = Time(deltaTimestamp).asMilliSecond
        currentTimestamp += deltaTimestamp
        val v_old = v.toMillivolts
        val u_old = u.toMillivolts
        val i = (charge/capacitance).toMillivolts
        v += Millivolts(deltaStep*(0.04*v_old*v_old+5.0*v_old+140.0-u_old)+i)
        u += Millivolts(deltaStep*a*(b*v_old-u_old))
        //println(getNetworkAddress+" : "+v)

        if(v >= th) {
          v = c
          u += d

          triggerEventWith(NeuronFireEvent, NeuronFireResponse(timestamp, getNetworkAddress))
          ends.sendToAllOutput(timestamp, ElectricSpike())
          ends.sendToAllInput(timestamp, BackwardSpike)
        }
      }

      triggerEventWith(NeuronPotentialUpdateEvent, NeuronPotentialResponse(timestamp, getNetworkAddress, v.toMillivolts.toFloat))

      lastTimestamps = timestamp
    case Inhibition(_) =>
      v = c
      u += d
      triggerEventWith(NeuronPotentialUpdateEvent, NeuronPotentialResponse(timestamp, getNetworkAddress, v.toMillivolts.toFloat))
  }
}
