package fr.univ_lille.cristal.emeraude.n2s3.features.logging.graph

import java.awt.{Color, Dimension, FlowLayout, Graphics}
import javax.swing.{JFrame, JPanel, WindowConstants}

import akka.actor.{ActorRef, Props}
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.{Config, Done}
import fr.univ_lille.cristal.emeraude.n2s3.core.event._
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.{N2S3, NeuronGroupObserverRef}
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.{LocalActorDeploymentStrategy, Message}

import scala.collection.mutable
import scala.concurrent.Await

/**
  * Created by pfalez on 12/04/17.
  */

class FireGraphRef(n2S3: N2S3, list : Seq[Seq[Seq[NetworkEntityPath]]], function : (Timestamp, Timestamp, Map[NetworkEntityPath, Seq[Timestamp]]) => Map[NetworkEntityPath, Float], neuronSize : Int = 4, name : String = "") extends NeuronGroupObserverRef {
  var actor: Option[ActorRef] = None
  def getActors : Seq[ActorRef] = this.actor.toSeq


  override def deploy(n2s3: N2S3): Unit = {
    this.actor = Some(n2s3.system.actorOf(Props(new FireGraph(n2s3, list, function, neuronSize, name)), LocalActorDeploymentStrategy))
  }
}

object FireGraph {

  def firstFunction(windowStart : Timestamp, windowEnd : Timestamp, fires : Map[NetworkEntityPath, Seq[Timestamp]]) : Map[NetworkEntityPath, Float] = {
    fires.map{ case (path, seq) =>
      path -> (1f-(seq.min-windowStart).toFloat/(windowEnd-windowStart).toFloat)
    }
  }

  def countFunction(windowStart : Timestamp, windowEnd : Timestamp, fires : Map[NetworkEntityPath, Seq[Timestamp]]) : Map[NetworkEntityPath, Float] = {
    val max = fires.map(_._2.size).max
    fires.map{ case (path, seq) =>
      path -> (if(max == 0) 0f else seq.size.toFloat/max.toFloat)
    }
  }
}

class FireGraph(n2s3: N2S3, list : Seq[Seq[Seq[NetworkEntityPath]]], function : (Timestamp, Timestamp, Map[NetworkEntityPath, Seq[Timestamp]]) => Map[NetworkEntityPath, Float], neuronSize : Int, name : String) extends NetworkActor {

  class SubPanel(width : Int, height : Int, pixelSize : Int) extends JPanel {

    setPreferredSize(new Dimension(width*pixelSize, height*pixelSize))
    setMinimumSize(new Dimension(width*pixelSize, width*pixelSize))

    var values : Seq[Seq[Float]] = Seq()

    def update(values : Seq[Seq[Float]]): Unit = {
      this.values = values
    }

    override def paintComponent(g: Graphics): Unit = {
      values.zipWithIndex.foreach { case (l, x) =>

        l.zipWithIndex.foreach { case (value, y) =>
          val v = math.min(1f, value)
          val rc = (math.max(0f, -2f * v * v + 4f * v - 1f) * 255f).toInt
          val bc = (math.max(0f, -2f * v * v + 1) * 255f).toInt
          val gc = (math.max(0f, 4f * v * (-v + 1f)) * 255f).toInt
          val color = (rc << 16) + (gc << 8) + bc

          g.setColor(new Color(color))
          g.fillRect(x * pixelSize, y * pixelSize, pixelSize, pixelSize)
        }
      }
    }

  }

  val frame = new JFrame()
  frame.setLayout(new FlowLayout())

  val panels : Seq[SubPanel] = list.map{ l =>
    val panel = new SubPanel(l.size, l.head.size, neuronSize)
    frame.add(panel)
    panel
  }

  frame.setTitle(name)

  frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE)
  frame.validate()
  frame.pack()
  frame.setVisible(true)

  val fires : mutable.HashMap[NetworkEntityPath, mutable.ArrayBuffer[Timestamp]] = mutable.HashMap()
  var window : Option[(Timestamp, Timestamp)] = None

  override def initialize(): Unit = {
    askTo(n2s3.inputLayerRef.get.getContainer, SubscribeSynchronized(LabelChangeEvent, ExternalSender.getReference(self), n2s3.buildProperties.getSynchronizerPolicy.getInputSynchronizer))

    list.flatMap(_.flatMap(_.map(p => askFuture(p, SubscribeSynchronized(NeuronFireEvent, ExternalSender.getReference(self), n2s3.buildProperties.getSynchronizerPolicy.getGlobalSynchronizer))))).foreach(f => Await.result(f, Config.longTimeout.duration))
  }

  override def destroy(): Unit = {
    askTo(n2s3.inputLayerRef.get.getContainer, Unsubscribe(LabelChangeEvent, ExternalSender.getReference(self)))

    list.flatMap(_.flatMap(_.map(p => askFuture(p, Unsubscribe(NeuronFireEvent, ExternalSender.getReference(self)))))).foreach(f => Await.result(f, Config.longTimeout.duration))

  }

  override def process(message: Message, sender : ActorRef): Unit = message match {
    case SynchronizedEvent(s, _, m, _) =>
      m match {
        case LabelChangeResponse(start, end, _) =>

          if(window.isDefined) {
            list.zip(panels).foreach { case (neurons, panel) =>
              val values = function(window.get._1, window.get._2, neurons.flatten.map { path =>
                path -> fires.getOrElse(path, Seq())
              }.toMap)

              panel.update(
                neurons.map {
                  _.map{ path =>
                    values.getOrElse(path, 0f)
                  }
                }
              )
              panel.repaint()
              panel.validate()
            }
          }

          window = Some(start, end)
          fires.clear()
        case NeuronFireResponse(t, source) =>
          fires.getOrElseUpdate(source, mutable.ArrayBuffer[Timestamp]()) += t
      }
      ExternalSender.sendTo(s, Done)
  }
}
