package fr.univ_lille.cristal.emeraude.n2s3.core.actors

import fr.univ_lille.cristal.emeraude.n2s3.core.NetworkContainer
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.{ActorCompanion, SinglePropsBuilder}

/**
  * Actor companion of [[NetworkContainerActor]].
  * It defines a way to create new [[NetworkContainerActor]] instances
  */
object NetworkContainerActor extends ActorCompanion{

  /**
    * Returns a props builder for a [[NetworkContainerActor]]
    */
  def newPropsBuilder() = new SinglePropsBuilder[NetworkContainerActor]()
}


/**
  * NetworkEntityActor that contains a NetworkContainer as NetworkEntity
  */
class NetworkContainerActor extends NetworkEntityActor(new NetworkContainer)