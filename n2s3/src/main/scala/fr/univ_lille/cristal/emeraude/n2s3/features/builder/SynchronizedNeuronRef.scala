package fr.univ_lille.cristal.emeraude.n2s3.features.builder

import akka.actor.ActorRef
import fr.univ_lille.cristal.emeraude.n2s3.core.Neuron.NeuronMessage
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message

/**
  * [[NetworkEntityRef]] that goes always through the synchronizer.
  * @param synchronizerActor the synchronizer actor ref to pass by
  * @param neuronRef the neuron ref that is being targetted
  */
class SynchronizedNeuronRef(synchronizerActor: ActorRef, neuronRef: NeuronRef) extends NetworkEntityRef {

  this.setActor(NetworkEntityPath(synchronizerActor))

  override def send(message: Message): Unit = super.send(new NeuronMessage(1,message, new ExternalNetworkEntityReference(actorPath.get), new ExternalNetworkEntityReference(neuronRef.actorPath.get), None))
}
