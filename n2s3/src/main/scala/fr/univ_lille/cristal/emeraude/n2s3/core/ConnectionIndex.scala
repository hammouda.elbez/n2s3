package fr.univ_lille.cristal.emeraude.n2s3.core

import fr.univ_lille.cristal.emeraude.n2s3.core.actors.Config
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.NeuronGroupRef

import scala.concurrent.{Await, Future}

/**
  * Created by pfalez on 13/06/17.
  */
class ConnectionIndex(inputLayer : NeuronGroupRef, outputLayer : NeuronGroupRef) {
  val index = outputLayer.neuronPaths.map{ output =>
    output -> ExternalSender.askFuture(output, GetAllConnectionProperty(ConnectionClassName))
      .asInstanceOf[Future[ConnectionPropertyValues[String]]]
  }.map{ case (output, future) =>
    output -> Await.result(future, Config.longTimeout.duration).values.groupBy(_._2).map { case(input, list) =>
      input -> list.map{case(id, _, _) => ConnectionPath(output, id)}
    }
  }.toMap


  def getConnectionsBetween(input : NetworkEntityPath, output : NetworkEntityPath) : Seq[ConnectionPath] = {
    index.getOrElse(output, Map()).getOrElse(input, Seq())
  }
}
