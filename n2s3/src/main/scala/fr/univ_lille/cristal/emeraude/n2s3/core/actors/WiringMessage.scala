package fr.univ_lille.cristal.emeraude.n2s3.core.actors

import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message

/*************************************************************************************************
 * Base class for all the messages that connect the neurones and synapses together.
 ************************************************************************************************/
abstract class WiringMessage extends Message
