package fr.univ_lille.cristal.emeraude.n2s3.features.logging.graph

import akka.actor.{Actor, Props}
import akka.pattern.ask
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.{Config, Done}
import fr.univ_lille.cristal.emeraude.n2s3.core.event._
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.SynapseWeightAndDelay
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.N2S3
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias._
import fr.univ_lille.cristal.emeraude.n2s3.support.Time
import fr.univ_lille.cristal.emeraude.n2s3.support.UnitCast._
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.LocalActorDeploymentStrategy

import scala.collection.mutable
import scala.concurrent.Await
/**
  * Created by falezp on 03/11/16.
  */
class TemporalInfluenceGraphic(n2s3 : N2S3, outputs : Map[String, NetworkEntityPath], outputFilename : String) {

  object Close
  object Print
  object Result

  class TemporalInfluenceGraphicActor(n2s3 : N2S3, outputs : Map[String, NetworkEntityPath], outputFilename : String) extends Actor {

    val inputQueue = mutable.Queue[(String, Timestamp)]()
    val outputFire = mutable.HashMap[NetworkEntityPath, Timestamp]()
    val hiddenFire = mutable.HashMap[NetworkEntityPath, mutable.ArrayBuffer[Timestamp]]()

    val result = mutable.HashMap[String, mutable.ArrayBuffer[(NetworkEntityPath, Double)]]()

    ExternalSender.askTo(n2s3.inputLayerRef.get.getContainer, SubscribeSynchronized(LabelChangeEvent, ExternalSender.getReference(self), n2s3.buildProperties.getSynchronizerPolicy.getInputSynchronizer))

    n2s3.layers.flatMap(_.neuronPaths).foreach { path =>
      ExternalSender.askTo(path, SubscribeSynchronized(NeuronFireEvent, ExternalSender.getReference(self), n2s3.buildProperties.getSynchronizerPolicy.getInputSynchronizer))
    }

    override def receive = {
      case SynchronizedEvent(s, _, m, _) =>
        m match {
          case LabelChangeResponse(start, end, label) =>
            inputQueue.enqueue(label -> start)
          case NeuronFireResponse(timestamp, source) =>
            if(inputQueue.nonEmpty && timestamp >= inputQueue.front._2) {
              if (outputs.exists(_._2 == source)) {
                if (!outputFire.isDefinedAt(source))
                  outputFire += source -> timestamp
              } else {
                if (!hiddenFire.isDefinedAt(source))
                  hiddenFire += source -> mutable.ArrayBuffer[Timestamp]()

                hiddenFire(source) += timestamp
              }
            }

            if(outputFire.size == outputs.size) {
              execute()
              outputFire.clear()
              hiddenFire.clear()
            }
          case _ => throw new RuntimeException("Unrecognized message : "+m)
        }
        ExternalSender.sendTo(s, Done)
      case Close =>
        ExternalSender.askTo(n2s3.inputLayerRef.get.getContainer, Unsubscribe(LabelChangeEvent, ExternalSender.getReference(self)))

        n2s3.layers.flatMap(_.neuronPaths).foreach { path =>
          ExternalSender.askTo(path, Unsubscribe(NeuronFireEvent, ExternalSender.getReference(self)))
        }
        sender ! Done
        context.stop(self)

      case Print =>
        result.foreach { case(label, list) =>
          println("# "+label)
          list.groupBy(_._1).foreach { case(neuron, l) =>
            val sum = l.foldLeft(0.0){ case(acc, (_, score)) => acc+score }
            val avg = sum/l.size.toDouble
            val std = math.sqrt(l.foldLeft(0.0){ case(acc, (_, score)) => acc+(score-avg)*(score-avg)}/l.size.toDouble)

            println(neuron +" : avg="+avg+", std="+std+", count="+l.size+"/"+list.size)
          }
        }
        sender ! Done
      case Result =>

      case m => throw new RuntimeException("Unrecognized message : "+m)
    }

    def execute() : Unit = {
      val tau_neuron = 20 MilliSecond
      val winner = outputFire.minBy(_._2)
      val label = inputQueue.dequeue()
      val patternStart = label._2

      val influences = mutable.HashMap[NetworkEntityPath, Double]()
      val trace = mutable.HashMap[NetworkEntityPath, mutable.ArrayBuffer[Timestamp]]()

      val queue = mutable.Queue[(NetworkEntityPath, Double, Timestamp)]()

      influences += winner._1 -> 1.0
      queue.enqueue((winner._1,  1.0, winner._2))

      def add_neuron(neuron : NetworkEntityPath, score : Double, timestamp: Timestamp) : Unit = {
        if(!influences.isDefinedAt(neuron)) {
          influences += neuron -> 0.0
          trace += neuron -> mutable.ArrayBuffer[Timestamp]()
        }

        influences(neuron) += score
        trace(neuron) += timestamp

        if(math.abs(score) > 0.01)
          queue.enqueue((neuron, score, timestamp))
      }

      while(queue.nonEmpty) {
        val current_neuron = queue.dequeue()

        val current_synapses = ExternalSender.askTo(current_neuron._1, GetAllConnectionProperty(SynapseWeightAndDelay)) match {
          case PropertyValue(values: Seq[(NetworkEntityPath, (Float, Time))]@unchecked) => values
          case _ => Seq()
        }

        current_synapses.foreach { case(input_neuron, (w, d)) =>
          val spikeList = hiddenFire.getOrElse(input_neuron, Seq()).map { timestamp =>
            timestamp + d.timestamp
          }.filter(_ <= current_neuron._3)

          if(spikeList.nonEmpty) {
            //val lastSpike = spikeList.max

            spikeList.foreach { t =>
              val score = current_neuron._2*w*math.exp(-(current_neuron._3 - t).toDouble / tau_neuron.timestamp.toDouble)
              add_neuron(input_neuron, score, t)
            }


          }
        }

      }
      influences.map{ case(path, score) =>  result.getOrElseUpdate(label._1, mutable.ArrayBuffer()) += ((path, score))}
    }
  }

  val actor = n2s3.system.actorOf(Props(new TemporalInfluenceGraphicActor(n2s3, outputs, outputFilename)), LocalActorDeploymentStrategy)

  def print() : Unit = {
    implicit val timeout = Config.longTimeout
    Await.result(actor ? Print, timeout.duration)
  }

  def result() : Unit = {
    implicit val timeout = Config.longTimeout
    Await.result(actor ? Result, timeout.duration)
  }

  def destroy() : Unit = {
    implicit val timeout = Config.longTimeout
    Await.result(actor ? Close, timeout.duration)
  }

}
