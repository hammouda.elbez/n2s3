package fr.univ_lille.cristal.emeraude.n2s3.features.io.input

import java.io.File
import javax.activation.MimetypesFileTypeMap
import javax.imageio.ImageIO

import scala.util.Random

/**
  * Created by falezp on 23/02/17.
  */
case class ImagePartInput(width : Int, height : Int) extends InputFormat[InputSample2D[Float]](Shape(width, height)) {
  def DataFrom(directory: String) = new ImagePartInputStream(directory, width, height)
}

class ImagePartInputStream(val directory: String, width : Int, height : Int) extends InputGenerator[InputSample2D[Float]] {

  private val folder = new File(directory)
  private val imageList = folder.listFiles().filter{ image =>
    new MimetypesFileTypeMap().getContentType(image).split("/").head == "image"
  }

  var imageCursor = 0

  private def toRGB(i : Int) : (Float, Float, Float) = (
    ((i >> 16) & 0xFF).toFloat/255f,
    ((i >> 8) & 0xFF).toFloat/255f,
    ((i >> 0) & 0xFF).toFloat/255f
  )
  private def toGrayScale(rbg : (Float, Float, Float)) : Float =  0.2126f*rbg._1+0.7152f*rbg._2+0.0722f*rbg._3

  def next(): InputSample2D[Float] = {

    val image = ImageIO.read(imageList(imageCursor))
    assert(image.getWidth>=width && image.getHeight>=height)

    val startX = Random.nextInt(image.getWidth-width)
    val startY = Random.nextInt(image.getHeight-height)

    val pixels = for(x <- 0 until width) yield {
      for(y <- 0 until height) yield toGrayScale(toRGB(image.getRGB(startX+x, startY+y)))
    }

    imageCursor += 1

    new InputSample2D[Float](pixels).setMetaData(InputLabel("new_image"))
  }

  def atEnd(): Boolean = imageCursor >= imageList.length

  def reset(): Unit = {
    imageCursor = 0
  }

  def shape: Shape = Shape(width, height)

  override def toString: String = "[ImagePart] "+directory+" : "+ imageCursor + " / " + imageList.length
}
