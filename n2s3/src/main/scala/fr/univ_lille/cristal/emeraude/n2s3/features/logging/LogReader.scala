package fr.univ_lille.cristal.emeraude.n2s3.features.logging

import scala.collection.mutable.{HashMap, ListBuffer, Map}

/**
 * Class allows methods to read log file
 */
object LogReader {

  /**
   * Object to save the description of Network
   */
  class NetworkDescr(val nbLayer: Int, val nbNeuronPerLayer: List[Int]) {
    override def toString(): String = nbLayer + ":" + nbNeuronPerLayer
  }

  /**
   * convert String to Float with replacing all , by . 
   */
  def toFloat(in: String): Float = {
    return in.replace(",", ".") toFloat
  }

  /**
   * Change an Array[String] to an List[Int]
   */
  def ArrayStrToListInt(array: Array[String], list: List[Int]): List[Int] = {
    if (array.size == 0) list
    else ArrayStrToListInt(array.tail, list :+ array.head.toInt)
  }

  /**
   * Build a NetworkDescr class from the String given
   */
  def buildNetworkDescr(pathname: String): NetworkDescr = {
    val source = scala.io.Source.fromFile(pathname)
    val line = ((source mkString) split "\n")(0) split "\t"
    new NetworkDescr(line(0).toInt, ArrayStrToListInt(line.tail, List[Int]()))
  }

  /**
   * Change a name of Neuron to an Index (from the NetworkDescr)
   */
  def namesToIndex(name: String, net: NetworkDescr): Int = {
    var offset = 0
    for (i <- 0 until name.substring(1, 2).toInt)
      offset += net.nbNeuronPerLayer(i)
    offset + name.substring(3).toInt
  }

  /**
   * @args : the path of the logfile
   * @return : Map of timestamp List Of active neuron at this time
   */
  def activitiesOfNeurons(path: String): Map[Int, List[Int]] = {
    val netDescr = buildNetworkDescr("output/log/network.log")
    val names: Map[String, Int] = HashMap[String, Int]()
    val activities: Map[Int, List[Int]] = HashMap[Int, List[Int]]()
    val source = scala.io.Source.fromFile(path)
    val lines = (source mkString) split "\n"
    lines.foreach { l =>
      val fields = l split "\t"
      names get fields(0) match {
        case Some(_) =>
        case _       => names += (fields(0) -> namesToIndex(fields(0), netDescr))
      }
      activities get fields(1).toInt match {
        case Some(_) =>
          if (!activities.get(fields(1).toInt).get.contains(names get fields(0) get))
            activities += (fields(1).toInt -> (activities.get(fields(1).toInt).get :+ (names get fields(0) get)).sorted)
        case _ => activities +=
          (fields(1).toInt -> (List(names get fields(0) get)))
      }
    }
    activities
  }
  
  
  /**
   * Build the list of Conductancies from the @arg lines.
   */
  def buildList(lines : Array[String], synapses : List[String], conductancies : List[(String, Float)], nbSynapses : Int) : List[(String, Float)] = {
    val fields = (lines.head) split "\t"
    println(synapses.length)
    if (synapses.length == nbSynapses) 
      return conductancies
    if (synapses.contains(fields(0)))
      buildList(lines.tail, synapses, conductancies, nbSynapses)
    else 
      buildList(lines.tail, fields(0) :: synapses, (fields(0), toFloat(fields(2))) :: conductancies, nbSynapses)
  }
  
  /**
   * @args nbSynapses : number of synapse 
   * @args path the path of the logfile
   * @return list of conductancies of the nbSynapses
   */
  def conductanciesOfSynaspses(path : String, nbSynapses : Int) : List[(String,Float)] = {
    val conductancies : ListBuffer[Float] = ListBuffer[Float]()
    val source = scala.io.Source.fromFile(path)
    val lines = ((source mkString) split "\n").reverse
    return buildList(lines, List[String]() , List[(String, Float)](), nbSynapses)
  }

  /**
   * this method can be used to read neuron.log or synapse.log
   * it will build a list with this form : (name , time, values)
   * as values we can get the potential of neurons or synaptics weight
   */
  def valuesOfTheNetwork(path: String): scala.collection.mutable.ListBuffer[(String,Int,Float)] = {
    val netDescr = buildNetworkDescr("output/log/network.log")
    val values : scala.collection.mutable.ListBuffer[(String,Int,Float)] = scala.collection.mutable.ListBuffer[(String,Int,Float)]()
    val source = scala.io.Source.fromFile(path)
    val lines = (source mkString) split "\n"
    var currentTimestamp = 0
    val timestamp : scala.collection.mutable.ListBuffer[String] = scala.collection.mutable.ListBuffer[String]()
    lines.foreach { l =>
      val fields = l split "\t"
      
//    check if we change timestamp
      if (currentTimestamp != fields(1).toInt) {
        currentTimestamp = fields(1).toInt
        timestamp.clear()
      }
      
      if ( ! timestamp.contains(fields(0)) ) {
          values += ((fields(0),fields(1).toInt,toFloat(fields(2))))
          timestamp += fields(0)
      }
    }
    values
  }
}