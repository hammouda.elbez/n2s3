/**************************************************************************************************
 * Contributors:
 * 		- created by bdanglot
 *    - (add your name here if you contribute to this code). 
 **************************************************************************************************/
package fr.univ_lille.cristal.emeraude.n2s3.features.logging.graph

import java.util.ArrayList
import java.util.concurrent.ConcurrentHashMap

/**************************************************************************************************
 * Class to display Weights of Synapses using BarChart
 *************************************************************************************************/
class SynapsesWeightsBarGraph extends Graph {

 // chart.getStyleManager().setChartType(ChartType.Bar)
  
  /**************************************************************************************************
   * Map which associate the name of synapses to their weights
   *************************************************************************************************/
  private val weights: java.util.Map[String, java.lang.Float] = new ConcurrentHashMap[String, java.lang.Float]()

  /**************************************************************************************************
   * Method to refresh the framework while simuling
   *
   * @param name
   *            of the synapses to refresh
   * @param value
   *            the new weight of this synapse
   *************************************************************************************************/
  def refreshSerie(name: String, value: Float) {
   /* pan.synchronized {
      addSerie(name, value)

      if (!this.chart.getSeriesMap().containsKey("Weight"))
        chart.addSeries("Weight", weights keySet, weights values)

      val tmp: java.util.List[java.lang.Float] = new ArrayList[java.lang.Float](this.weights.values())

      this.pan.updateSeries("Weight", weights keySet, tmp)
    }*/
  }

  /**************************************************************************************************
   * add content as couple (name,weight)
   *
   * @param name
   *            of the synapses to refresh
   * @param value
   *            the new weight of this synapse
   *************************************************************************************************/
  def addSerie(name: String, value: Float): Unit = {
    weights put (name, value)
  }

  /**************************************************************************************************
   * Method to launch in offLine (no simulation) It will add contents to the
   * framework (Beware to add some contents before using it) And then call the
   * super Launch()
   *************************************************************************************************/
  def launchOffline(): Unit = {
   // chart.addSeries("Weight", weights keySet, weights values)
    super.launch()
  }

}
