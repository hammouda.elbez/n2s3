package fr.univ_lille.cristal.emeraude.n2s3.core.models.properties

import fr.univ_lille.cristal.emeraude.n2s3.core.{ConnectionProperty, NetworkEntityPath}

/**
  * Created by falezp on 23/11/16.
  */
object ConnectionInputPath extends ConnectionProperty[NetworkEntityPath]
