package fr.univ_lille.cristal.emeraude.n2s3.features.io.input

import java.io.{DataInputStream, FileInputStream, IOException}


/**
 * Class Reader for mnist file
 */

object InputMnist extends InputFormat[InputSample2D[Float]](Shape(28, 28)) {
  def DataFrom(imageFile: String, labelFile: String, sizeOfChunk: Int  = 64) = new MnistFileInputStream(imageFile, labelFile, sizeOfChunk)

  def DataParts(imageFile: String, labelFile: String,from:Int,sizeOfChunk: Int  = 64) = new MnistFileInputStreamP(imageFile, labelFile, from,sizeOfChunk)

  def DataFromRepeated(imageFile: String, labelFile: String,iteration: Int = 1, sizeOfChunk: Int  = 64) = new MnistFileInputStreamI(imageFile, labelFile,iteration, sizeOfChunk)

}


object MnistFileInputStream{
  def apply(imageFile: String, labelFile: String, sizeOfChunk: Int  = 64) = new MnistFileInputStream(imageFile, labelFile, sizeOfChunk)
}

class MnistFileInputStream(val imageFile: String, val labelFile: String, val sizeOfChunk: Int  = 64) extends InputGenerator[InputSample2D[Float]] {


  //Magic number
  val MAGIC_IMAGE = 2051
  val MAGIC_LABEL = 2049

  //stream of file
  var labelsInputStream = new DataInputStream(new FileInputStream(labelFile))
  var imagesInputStream = new DataInputStream(new FileInputStream(imageFile))

  //Check the magic number
  if (labelsInputStream.readInt() != MAGIC_LABEL)
    throw new IOException("Error on magic number for label file: [" + labelFile + "]")

  if (imagesInputStream.readInt != MAGIC_IMAGE)
    throw new IOException("Error on magic number for image file: [" + imageFile + "]")

  //check the number of elements
  val numberImage = imagesInputStream.readInt()
  val numberLabel = labelsInputStream.readInt()
  private var numberOfReadImages: Int = 0

  var image : IndexedSeq[IndexedSeq[Float]] = _

  var cursor = 0

  if (numberImage != numberLabel)
    throw new IOException("The number of images contained in: [" + imageFile + "] does not match the number of label [" + labelFile + "]")

  val rows = imagesInputStream.readInt()
  val columns = imagesInputStream.readInt()

  val imagesBuffer = Array.ofDim[Double](sizeOfChunk, rows, columns)
  val label = Array.ofDim[Double](sizeOfChunk)

  def next(): InputSample2D[Float] = {

    if (this.numberOfReadImages >= this.numberLabel) {
      throw new IOException("Attempt to read after the end of the file")
    }

    var label = ""
      label = labelsInputStream.readUnsignedByte().toString


        var pixels = for(y <- 0 until columns) yield {
          for(x <- 0 until rows) yield imagesInputStream.readUnsignedByte().toFloat
        }

        image = for(x <- 0 until rows) yield {
          for(y <- 0 until columns) yield pixels(y)(x) / 255f
        }
        this.numberOfReadImages += 1

       /*   while (label != null && this.numberOfReadImages >= this.numberLabel && (Integer.valueOf(label) != 5)){

          pixels = for(y <- 0 until columns) yield {
            for(x <- 0 until rows) yield imagesInputStream.readUnsignedByte().toFloat
          }

          image = for(x <- 0 until rows) yield {
            for(y <- 0 until columns) yield pixels(y)(x) / 255f
          }
          this.numberOfReadImages += 1
          label = labelsInputStream.readUnsignedByte().toString

          }
*/

      new InputSample2D[Float](image).setMetaData(InputLabel(label))

  }

    def atEnd(): Boolean = this.numberOfReadImages >= this.numberImage

    def reset() = {
      labelsInputStream = new DataInputStream(new FileInputStream(labelFile))
      labelsInputStream.readInt()
      labelsInputStream.readInt()
      imagesInputStream = new DataInputStream(new FileInputStream(imageFile))
      imagesInputStream.readInt()
      imagesInputStream.readInt()
      imagesInputStream.readInt()
      imagesInputStream.readInt()
      cursor = 0
      numberOfReadImages = 0
    }

    def shape: Shape = Shape(columns, rows)

    override def toString = "[Mnist] "+imageFile+" : "+ numberOfReadImages + " / " + numberImage
}


class MnistFileInputStreamI(val imageFile: String, val labelFile: String, val iteration: Int = 1,val sizeOfChunk: Int  = 64) extends InputGenerator[InputSample2D[Float]] {

  //Iteration
  var i = 1

  //Magic number
  val MAGIC_IMAGE = 2051
  val MAGIC_LABEL = 2049

  //stream of file
  var labelsInputStream = new DataInputStream(new FileInputStream(labelFile))
  var imagesInputStream = new DataInputStream(new FileInputStream(imageFile))

  //Check the magic number
  if (labelsInputStream.readInt() != MAGIC_LABEL)
    throw new IOException("Error on magic number for label file: [" + labelFile + "]")

  if (imagesInputStream.readInt != MAGIC_IMAGE)
    throw new IOException("Error on magic number for image file: [" + imageFile + "]")

  //check the number of elements
  val numberImage = imagesInputStream.readInt()
  val numberLabel = labelsInputStream.readInt()
  private var numberOfReadImages: Int = 0

  var cursor = 0

  if (numberImage != numberLabel)
    throw new IOException("The number of images contained in: [" + imageFile + "] does not match the number of label [" + labelFile + "]")

  val rows = imagesInputStream.readInt()
  val columns = imagesInputStream.readInt()

  val imagesBuffer = Array.ofDim[Double](sizeOfChunk, rows, columns)
  val label = Array.ofDim[Double](sizeOfChunk)

  def next(): InputSample2D[Float] = {

    if (this.numberOfReadImages >= this.numberLabel) {
      throw new IOException("Attempt to read after the end of the file")
    }

    if(numberOfReadImages > numberImage && i < iteration){
      labelsInputStream = new DataInputStream(new FileInputStream(labelFile))
      imagesInputStream = new DataInputStream(new FileInputStream(imageFile))
      i = i+1
      println("Iteration n° "+i)
    }

    var label = labelsInputStream.readUnsignedByte().toString

    var pixels = for(y <- 0 until columns) yield {
      for(x <- 0 until rows) yield imagesInputStream.readUnsignedByte().toFloat
    }

    var image = for(x <- 0 until rows) yield {
      for(y <- 0 until columns) yield pixels(y)(x) / 255f
    }
    this.numberOfReadImages += 1


    while (label == null || Integer.valueOf(label) < 5){
      label = labelsInputStream.readUnsignedByte().toString

      pixels = for(y <- 0 until columns) yield {
        for(x <- 0 until rows) yield imagesInputStream.readUnsignedByte().toFloat
      }

      image = for(x <- 0 until rows) yield {
        for(y <- 0 until columns) yield pixels(y)(x) / 255f
      }
      this.numberOfReadImages += 1
    }

    new InputSample2D[Float](image).setMetaData(InputLabel(label))
  }

  def atEnd(): Boolean = this.numberOfReadImages >= this.numberImage

  def reset() = {
    labelsInputStream = new DataInputStream(new FileInputStream(labelFile))
    labelsInputStream.readInt()
    labelsInputStream.readInt()
    imagesInputStream = new DataInputStream(new FileInputStream(imageFile))
    imagesInputStream.readInt()
    imagesInputStream.readInt()
    imagesInputStream.readInt()
    imagesInputStream.readInt()
    cursor = 0
    numberOfReadImages = 0
  }

  def shape: Shape = Shape(columns, rows)

  override def toString = "[Mnist] "+imageFile+" : "+ numberOfReadImages + " / " + numberImage + " | Iteration n° "+i
}

class MnistFileInputStreamP(val imageFile: String, val labelFile: String,from:Int, val sizeOfChunk: Int  = 64) extends InputGenerator[InputSample2D[Float]] {


  //Magic number
  val MAGIC_IMAGE = 2051
  val MAGIC_LABEL = 2049

  //stream of file
  var labelsInputStream = new DataInputStream(new FileInputStream(labelFile))
  var imagesInputStream = new DataInputStream(new FileInputStream(imageFile))

  //Check the magic number
  if (labelsInputStream.readInt() != MAGIC_LABEL)
    throw new IOException("Error on magic number for label file: [" + labelFile + "]")

  if (imagesInputStream.readInt != MAGIC_IMAGE)
    throw new IOException("Error on magic number for image file: [" + imageFile + "]")

  //check the number of elements
  var numberImage = imagesInputStream.readInt()
  numberImage = numberImage / (numberImage/sizeOfChunk)
  var numberLabel = labelsInputStream.readInt()
  numberLabel = numberLabel / (numberLabel/sizeOfChunk)

  private var numberOfReadImages: Int = 0

  var image : IndexedSeq[IndexedSeq[Float]] = _

  var cursor = 0
  var i = 0
  if (numberImage != numberLabel)
    throw new IOException("The number of images contained in: [" + imageFile + "] does not match the number of label [" + labelFile + "]")

  val rows = imagesInputStream.readInt()
  val columns = imagesInputStream.readInt()

  val imagesBuffer = Array.ofDim[Double](sizeOfChunk, rows, columns)
  val label = Array.ofDim[Double](sizeOfChunk)

  def goto(): Unit = {
    while(from > i) {
      labelsInputStream.readUnsignedByte().toString
      for (y <- 0 until columns) yield {
        for (x <- 0 until rows) yield imagesInputStream.readUnsignedByte().toFloat
      }
      i+=1
    }
  }
  def next(): InputSample2D[Float] = {

    if (this.numberOfReadImages >= this.numberLabel) {
      throw new IOException("Attempt to read after the end of the file")
    }

      var label = ""
      label = labelsInputStream.readUnsignedByte().toString


      var pixels = for (y <- 0 until columns) yield {
        for (x <- 0 until rows) yield imagesInputStream.readUnsignedByte().toFloat
      }

      image = for (x <- 0 until rows) yield {
        for (y <- 0 until columns) yield pixels(y)(x) / 255f
      }
      this.numberOfReadImages += 1
    i+=1


    new InputSample2D[Float](image).setMetaData(InputLabel(label))

  }

  def atEnd(): Boolean = this.numberOfReadImages >= this.numberImage

  def reset() = {
    labelsInputStream = new DataInputStream(new FileInputStream(labelFile))
    labelsInputStream.readInt()
    labelsInputStream.readInt()
    imagesInputStream = new DataInputStream(new FileInputStream(imageFile))
    imagesInputStream.readInt()
    imagesInputStream.readInt()
    imagesInputStream.readInt()
    imagesInputStream.readInt()
    cursor = 0
    numberOfReadImages = 0
  }

  def shape: Shape = Shape(columns, rows)

  override def toString = "[Mnist] "+imageFile+" : "+ numberOfReadImages + " / " + numberImage
}

