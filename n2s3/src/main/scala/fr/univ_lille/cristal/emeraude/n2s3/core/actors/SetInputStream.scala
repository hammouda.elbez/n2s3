package fr.univ_lille.cristal.emeraude.n2s3.core.actors

import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.{InputPacket, InputTemporalPacket, StreamSupport}
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message

/** ******************************************************************************************************************
  * Actor messages
  * *****************************************************************************************************************/

case class SetInputStream(stream: StreamSupport[InputPacket, InputTemporalPacket]) extends Message
