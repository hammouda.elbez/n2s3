package fr.univ_lille.cristal.emeraude.n2s3.features.logging.graph;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * 
 * @author bdanglot Class to display potentials of neurons or weights of
 *         synapses. It can be used in both case. For clarity, neurons and
 *         synapses will be call "entities" and potential and weight "values"
 */
public class ValuesOfNetworkGraph extends AbstractGraph {

	/**
	 * Map which associate name of entity and a list of timestamp
	 */
	private final Map<String, List<Integer>> time = new ConcurrentHashMap<String, List<Integer>>();

	/**
	 * Map which associate name of entity and a list of values
	 */
	private final Map<String, List<Float>> values = new ConcurrentHashMap<String, List<Float>>();

	/**
	 * Constructor Set the chart type to line
	 */
	public ValuesOfNetworkGraph() {
		super();
		//this.chart.getStyleManager().setChartType(ChartType.Line);
	}

	/**
	 * Method to refresh the framework while simuling
	 * 
	 * @param name
	 *            of the entity to refresh
	 * @param time
	 *            the timestamp of the event
	 * @param value
	 *            the new value of this entity
	 */
	public synchronized void refreshSerie(String name, int time, float value) {

		this.addSerie(name, time, value);

	/*	if (!this.chart.getSeriesMap().containsKey(name))
			this.chart.addSeries(name, this.time.get(name),
					this.values.get(name));*/

		while (time - this.time.get(name).get(0) > 5000) {
			this.time.get(name).remove(0);
			this.values.get(name).remove(0);
		}
		
		/*this.chart.getSeriesMap().get(name).setMarker(SeriesMarker.NONE);
		this.pan.updateSeries(name, this.time.get(name), this.values.get(name));
*/
	}

	/**
	 * add a new content to the framework
	 * 
	 * @param name
	 *            of the entity to refresh
	 * @param time
	 *            the timestamp of the event
	 * @param value
	 *            the new value of this entity
	 */
	public synchronized void addSerie(String name, int time, float value) {

		if (values.containsKey(name)) {
			this.values.get(name).add(value);
			this.time.get(name).add(time);
		} else {
			// Create the new list
			List<Float> listPot = new CopyOnWriteArrayList<Float>();
			listPot.add(value);
			this.values.put(name, listPot);
			// Create the new list
			List<Integer> listTime = new CopyOnWriteArrayList<Integer>();
			listTime.add(time);
			this.time.put(name, listTime);
		}

	}

	/**
	 * Method to launch in offLine (no simulation) It will add contents to the
	 * framework (Beware to add some contents before using it) And then call the
	 * super Launch()
	 */
	public void launchOffline() {
		for (String key : this.values.keySet()) {
			//this.chart.addSeries(key, this.time.get(key), this.values.get(key));
		}
		super.launch();
	}

}
