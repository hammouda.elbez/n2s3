package fr.univ_lille.cristal.emeraude.n2s3.features.logging.graph;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 
 * @author bdanglot Class to display Weights of Synapses using BarChart
 */
public class SynapticsWeightsBarGraph extends AbstractGraph {

	/**
	 * Map which associate the name of synapses to their weights
	 */
	private final Map<String, Float> test = new ConcurrentHashMap<String, Float>();

	/**
	 * Constructor Set the chart type at Bar
	 */
	public SynapticsWeightsBarGraph() {
		super();
		//this.chart.getStyleManager().setChartType(ChartType.Bar);
	}

	/**
	 * Method to refresh the framework while simuling
	 * 
	 * @param name
	 *            of the synapses to refresh
	 * @param value
	 *            the new weight of this synapse
	 */
	public synchronized void refreshSerie(String name, float value) {
		this.addSerie(name, value);
/*
		if (!this.chart.getSeriesMap().containsKey("Weight"))
			this.chart.addSeries("Weight", this.test.keySet(),
					this.test.values());

		List<Float> tmp = new ArrayList<Float>(this.test.values());
		this.pan.updateSeries("Weight", this.test.keySet(), tmp);*/
	}

	/**
	 * add content as couple (name,weight)
	 * 
	 * @param name
	 *            of the synapses to refresh
	 * @param value
	 *            the new weight of this synapse
	 */
	public synchronized void addSerie(String name, float value) {
		this.test.put(name, value);
	}

	/**
	 * Method to launch in offLine (no simulation) It will add contents to the
	 * framework (Beware to add some contents before using it) And then call the
	 * super Launch()
	 */
	public void launchOffline() {
		//this.chart.addSeries("Weight", this.test.keySet(), this.test.values());
		super.launch();
	}

}
