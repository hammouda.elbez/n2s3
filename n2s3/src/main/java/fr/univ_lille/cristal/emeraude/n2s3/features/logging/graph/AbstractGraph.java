package fr.univ_lille.cristal.emeraude.n2s3.features.logging.graph;
import org.knowm.xchart.XChartPanel;

import javax.swing.*;

/**
 * 
 * @author bdanglot Abstract class which all Graph will extends
 *
 */
public abstract class AbstractGraph {

	/**
	 * The framework
	 */
	//protected Chart chart;
	/**
	 * The panel
	 */
	protected XChartPanel pan;

	private boolean run;

	/**
	 * Constuctor Init the framework 800x600, add it to the panel
	 */
	public AbstractGraph() {
		// Create Chart
		//this.chart = new Chart(800, 600);

		// Customize Chart
		//this.chart.getStyleManager().setChartTitleVisible(false);
		//this.chart.getStyleManager().setLegendPosition(LegendPosition.OutsideE);

		// Add Chart to Pan
		//this.pan = new XChartPanel(this.chart);

		// isn't running
		this.run = false;
	}

	public boolean isRunning() {
		return this.run;
	}

	/**
	 * Create a JFrame and add the panel to It
	 */
	public void launch() {
		this.run = true;
		// Create and set up the window.
		JFrame frame = new JFrame("XChart");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.add(pan);

		// Display the window.
		frame.pack();
		frame.setVisible(true);
	}
}
