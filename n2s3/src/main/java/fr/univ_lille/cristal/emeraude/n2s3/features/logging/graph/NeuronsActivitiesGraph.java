package fr.univ_lille.cristal.emeraude.n2s3.features.logging.graph;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * 
 * @author bdanglot
 * 
 *         Class to display a Scatter graph of Activities of Neurons (When they
 *         fire)
 *
 */
public class NeuronsActivitiesGraph extends AbstractGraph {

	/**
	 * Index of neurons : more it is deeper, more its index is high (N0,1 : 1,
	 * for example)
	 */
	private final List<Integer> indexOfActivNeurons = new CopyOnWriteArrayList<Integer>();

	private final List<Integer> time = new CopyOnWriteArrayList<Integer>();

	/**
	 * Constructor Call the super() constructor and change the ChartType to
	 * Scatter
	 */
	public NeuronsActivitiesGraph() {
		super();
		//this.chart.getStyleManager().setChartType(ChartType.Scatter);
	}

	/**
	 * Method to refresh the framework while simuling
	 * 
	 * @param t
	 *            new timestamps
	 * @param n
	 *            index of active neurons
	 */
	public synchronized void refreshSerie(int t, int n) {
		this.addSerie(t, n);

		/*if (!this.chart.getSeriesMap().containsKey("Activity of Neurons"))
			this.chart.addSeries("Activity of Neurons", time,
					indexOfActivNeurons);*/

		while (t - this.time.get(0) > 10000) {
			this.time.remove(0);
			this.indexOfActivNeurons.remove(0);
		}
		
		//this.pan.updateSeries("Activity of Neurons", time, indexOfActivNeurons);
	}

	/**
	 * add a new content to the framework
	 * 
	 * @param t
	 *            new timestamps
	 * @param n
	 *            index of active neurons
	 */
	public synchronized void addSerie(int t, int n) {
		this.time.add(t);
		this.indexOfActivNeurons.add(n);
	}

	/**
	 * Method to launch in offLine (no simulation) It will add contents to the
	 * framework (Beware to add some contents before using it) And then call the
	 * super Launch()
	 */
	public void launchOffline() {
		//this.chart.addSeries("Activity of Neurons", time, indexOfActivNeurons);
		this.launch();
	}

}
