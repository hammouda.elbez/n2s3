package fr.univ_lille.cristal.emeraude.n2s3.features.logging

import akka.actor.{ActorRef, Props}
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.{N2S3, NeuronGroupObserverRef, NeuronGroupRef}
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.LocalActorDeploymentStrategy
/**
  * Created by guille on 6/16/16.
  */
case class InputOutputGraphNetworkObserver(tuples: Seq[(NeuronGroupRef, Int, Int)]) extends NeuronGroupObserverRef {
  var actor: Option[ActorRef] = None
  var rateRefresh : Int = 1000/24
  def getActors : Seq[ActorRef] = this.actor.toSeq

  override protected def deploy(n2S3: N2S3): Unit = {
    val newActor = n2S3.system.actorOf(Props(new NeuronInputOutputGraph(tuples.map(each => (each._1.getIdentifier, each._2, each._1.neurons.map(_.actorPath.get), each._3)))), LocalActorDeploymentStrategy)
    this.actor = Some(newActor)
  }
}
