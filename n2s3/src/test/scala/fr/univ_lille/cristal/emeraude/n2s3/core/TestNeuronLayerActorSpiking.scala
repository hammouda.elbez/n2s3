package fr.univ_lille.cristal.emeraude.n2s3.core

import akka.actor.ActorSystem
import akka.testkit.TestActorRef
import fr.univ_lille.cristal.emeraude.n2s3.UnitSpec
import fr.univ_lille.cristal.emeraude.n2s3.core.Neuron.{NeuronMessage, SetSynchronizer}
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.NetworkContainerActor
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.NetworkEntityActor.AddChildEntity
//import org.scalatest.Matchers// mazdak
import org.scalatest._

import matchers._//mazdak
/**
  * Created by falezp on 07/04/16.
  */
//class TestNeuronLayerActorSpiking extends UnitSpec with Matchers {// mazdak
//abstract class UnitSpec extends AnyFlatSpec with should.Matchers

class TestNeuronLayerActorSpiking extends UnitSpec with should.Matchers {
 
  implicit val system = ActorSystem("TestActorSystem")

  "Spiking Neuron layer" should "spike neuron" in {
    val neuronLayer = TestActorRef[NetworkContainerActor]
    val synchronizer = TestActorRef[SynchronizerActor]

    val neuron = new MockNeuron
    neuronLayer ! AddChildEntity("neuron", neuron)
    val neuronPath = NetworkEntityPath(neuronLayer) / "neuron"
    ExternalSender.sendTo(neuronPath, SetSynchronizer(NetworkEntityPath(synchronizer)))

    ExternalSender.askTo(neuronPath, NeuronMessage(1, MockSpike, null, null, None))

    neuron.lastReceivedMessage shouldBe MockSpike
    neuron.lastReceivedTimestamp shouldBe 1
  }

  "Spiking Neuron layer" should "spike correct neuron" in {
    val neuronLayer = TestActorRef[NetworkContainerActor]
    val synchronizer = TestActorRef[SynchronizerActor]

    val neuron1 = new MockNeuron
    neuronLayer ! AddChildEntity("neuron1", neuron1)
    val neuronPath1 = NetworkEntityPath(neuronLayer)/"neuron1"
    ExternalSender.sendTo(neuronPath1, SetSynchronizer(NetworkEntityPath(synchronizer)))

    val neuron2 = new MockNeuron
    neuronLayer ! AddChildEntity("neuron2", neuron2)
    ExternalSender.sendTo(neuronPath1, SetSynchronizer(NetworkEntityPath(synchronizer)))

    ExternalSender.askTo(neuronPath1, NeuronMessage(1, MockSpike, null, null, None))

    neuron1.lastReceivedMessage shouldBe MockSpike
    neuron1.lastReceivedTimestamp shouldBe 1

    neuron2.lastReceivedMessage shouldBe null
  }
}
