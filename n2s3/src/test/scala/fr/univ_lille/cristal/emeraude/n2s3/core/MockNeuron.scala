package fr.univ_lille.cristal.emeraude.n2s3.core

import fr.univ_lille.cristal.emeraude.n2s3.core.Neuron.{ConnectionID, NeuronEnds}
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.NetworkEntityActor
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message

/**
  * Created by falezp on 07/04/16.
  */

class MockNeuronConnection extends NeuronConnection {

  var lastReceivedMessage : Message = _
  var lastReceivedTimestamp : Timestamp = _

  def processConnectionMessage(timestamp : Timestamp, message : Message) : Unit = {
    lastReceivedMessage = message
    lastReceivedTimestamp = timestamp

    getEnds.sendToOutput(timestamp+1, message)
  }

}

object MockSpike extends Message

object MockSpikeAsk extends Message
object MockSpikeAnswer extends Message

object MockNeuron extends NeuronModel{
  override def createNeuron(): Neuron = new MockNeuron
}

class MockNeuron extends Neuron {

  var lastReceivedMessage : Message = _
  var lastReceivedTimestamp : Timestamp = _

  def defaultConnection = new MockNeuronConnection

  def processSomaMessage(timestamp : Timestamp, message : Message, fromConnection : Option[(ConnectionID, NeuronConnection)], ends : NeuronEnds) : Unit = {
    lastReceivedMessage = message
    lastReceivedTimestamp = timestamp

    ends.sendToAllOutput(timestamp+1, message)
  }

}

class MockNeuronActor extends NetworkEntityActor(new MockNeuron)