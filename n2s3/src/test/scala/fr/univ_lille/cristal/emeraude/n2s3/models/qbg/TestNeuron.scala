package fr.univ_lille.cristal.emeraude.n2s3.models.qbg

import akka.actor.Props
import akka.pattern.ask
import akka.testkit.TestActorRef
import fr.univ_lille.cristal.emeraude.n2s3.UnitActorSpec
import fr.univ_lille.cristal.emeraude.n2s3.core.GetProperty
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.NetworkEntityActor
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties._
import fr.univ_lille.cristal.emeraude.n2s3.models.neurons.LIF

import scala.concurrent.Await
import scala.concurrent.duration._

/**************************************************************************************************
	* Test the public interface of a NeuronQBG.
	*/
class TestNeuron extends UnitActorSpec  {



	class LIFActor extends NetworkEntityActor(new LIF)

	"* Testing: creating a neuron" should "create a neuron & an actor without crashing" in{
		TestActorRef(Props(new LIFActor))
	}

	"* Testing: neurons " should " have [NeuronThreshold, NeuronPotential, NeuronRefractory, FixedParameter, SynapseLTP, SynapseLeak] properties" in {
		val anActor = TestActorRef(Props(new LIFActor))

		Await.result(ask(anActor, GetProperty(MembranePotentialThreshold))(100 seconds), 100 seconds)
		Await.result(ask(anActor, GetProperty(MembraneLeakTime))(100 seconds), 100 seconds)
		Await.result(ask(anActor, GetProperty(MembraneRefractoryDuration))(100 seconds), 100 seconds)
		Await.result(ask(anActor, GetProperty(FixedParameter))(100 seconds), 100 seconds)
	}

}