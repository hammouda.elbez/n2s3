package fr.univ_lille.cristal.emeraude.n2s3.core

import akka.pattern.ask
import akka.testkit.TestActorRef
import akka.util.Timeout
import fr.univ_lille.cristal.emeraude.n2s3.UnitActorSpec
import fr.univ_lille.cristal.emeraude.n2s3.core.Neuron.{ConnectionID, NeuronEnds, NeuronMessage}
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.{ElectricSpike, NetworkEntityActor}
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.MembranePotentialThreshold
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message
import squants.electro.ElectricPotential
import squants.electro.ElectricPotentialConversions.ElectricPotentialConversions

import scala.concurrent.Await
import scala.concurrent.duration._

/**
  * Created by guille on 8/5/16.
  */
class TestNeuronActor extends UnitActorSpec {

  "NeuronActor" should "Receive spikes in SOMA" in {
    var receivedMessage: Option[Message] = None
    val neuronActor = TestActorRef(new NetworkEntityActor(new Neuron {
      override def processSomaMessage(timestamp: Timestamp, message: Message, fromConnection : Option[(ConnectionID, NeuronConnection)], ends: NeuronEnds): Unit = {
        receivedMessage = Some(message)
      }
    }))

    neuronActor ! NeuronMessage(0, ElectricSpike(), null, null, None)
    receivedMessage.getOrElse(None) shouldBe ElectricSpike()
  }

  "NeuronActor" should "receive GetProperty message" in {
    var receivedMessage: Option[Message] = None
    val neuronActor = TestActorRef(new NetworkEntityActor(new Neuron {
        var threshold: ElectricPotential = 0 V

        addProperty[ElectricPotential](MembranePotentialThreshold, () => threshold, threshold = _)

        override def processSomaMessage(timestamp : Timestamp, message : Message, fromConnection : Option[(ConnectionID, NeuronConnection)], ends : NeuronEnds) : Unit = {
          receivedMessage = Some(message)
        }
      }))

    implicit val timeout = Timeout(5 seconds)
    val future = neuronActor ? GetProperty(MembranePotentialThreshold)
    val threshold = Await.result(future, timeout.duration)
    threshold shouldBe PropertyValue(0.V)
  }

  "NeuronActor" should "receive SetProperty message" in {
    var receivedMessage: Option[Message] = None
    val neuronActor = TestActorRef(new NetworkEntityActor(new Neuron {
        var threshold: ElectricPotential = 0 V

        addProperty[ElectricPotential](MembranePotentialThreshold, () => threshold, threshold = _)

        override def processSomaMessage(timestamp : Timestamp, message : Message, fromConnection : Option[(ConnectionID, NeuronConnection)], ends : NeuronEnds) : Unit = {
          receivedMessage = Some(message)
        }
      }))

    implicit val timeout = Timeout(5 seconds)
    neuronActor ! SetProperty(MembranePotentialThreshold, 2 V)

    val future = neuronActor ? GetProperty(MembranePotentialThreshold)
    val threshold = Await.result(future, timeout.duration)
    threshold shouldBe PropertyValue(2 V)
  }
}
