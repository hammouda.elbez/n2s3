/**
  * contributors:
  * damien.marchal@univ-lille1.fr, Copyright CNRS 2016
  */
package fr.univ_lille.cristal.emeraude.n2s3.features.io

import java.io.IOException

import fr.univ_lille.cristal.emeraude.n2s3.UnitSpec
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.MnistFileInputStream
import fr.univ_lille.cristal.emeraude.n2s3.support.N2S3ResourceManager

class TestMnistInputStream extends UnitSpec {
  val testImageFileName: String = N2S3ResourceManager.getByName("mnist-test-images").getAbsolutePath
  val testLabelFileName: String = N2S3ResourceManager.getByName("mnist-test-labels").getAbsolutePath


  "MnistInputStream" should "not be at the end of a not read file" in {
    val inputStream = new MnistFileInputStream(testImageFileName, testLabelFileName)
    inputStream shouldNot be ('atEnd)
  }

  "MnistInputStream" should "be at the end of a read file" in {
    val inputStream = new MnistFileInputStream(testImageFileName, testLabelFileName)
    inputStream.next(inputStream.numberImage)
    inputStream should be ('atEnd)
  }

  "MnistInputStream" should "know number of images in file" in {
    val inputStream = new MnistFileInputStream(testImageFileName, testLabelFileName)
    inputStream.numberImage shouldBe 10000
  }

  "MnistInputStream" should "know number of labels in file" in {
    val inputStream = new MnistFileInputStream(testImageFileName, testLabelFileName)
    inputStream.numberLabel shouldBe 10000
  }

  "MnistInputStream" should "know number of rows per image" in {
    val inputStream = new MnistFileInputStream(testImageFileName, testLabelFileName)
    inputStream.rows shouldBe 28
  }

  "MnistInputStream" should "know number of columns per image" in {
    val inputStream = new MnistFileInputStream(testImageFileName, testLabelFileName)
    inputStream.columns shouldBe 28
  }

  "MnistInputStream next" should "return an image" in {
    val inputStream = new MnistFileInputStream(testImageFileName, testLabelFileName)
    val data = inputStream.next()
    data.getShape(0) shouldBe 28
    data.getShape(1) shouldBe 28
  }

  "MnistInputStream next" should "be able to read all 60000 images without throwing an exception" in {
    val inputStream = new MnistFileInputStream(testImageFileName, testLabelFileName)
    for (i <- 0 until inputStream.numberImage) {
      inputStream.next()
    }
  }

  "MnistInputStream next" should "throw an exception after end of file" in {
    val inputStream = new MnistFileInputStream(testImageFileName, testLabelFileName)
    for (i <- 0 until inputStream.numberImage) {
      inputStream.next()
    }
    intercept[IOException](inputStream.next())
  }

  "MnistInputStream reset" should "take it back to the beginning" in {
    val inputStream = new MnistFileInputStream(testImageFileName, testLabelFileName)
    val first = inputStream.next()
    inputStream.reset()
    first should equal (inputStream.next())
  }

  "MnistInputStream reset" should "take it back to the beginning When everything was read before" in {
    val inputStream = new MnistFileInputStream(testImageFileName, testLabelFileName)
    inputStream.next(inputStream.numberImage)
    inputStream.reset()
    inputStream.next()
  }

  "MnistInputStream next with arguments" should "return a list of the desired size" in {
    val inputStream = new MnistFileInputStream(testImageFileName, testLabelFileName)
    inputStream.next(2000).length shouldBe 2000
  }

  "MinstReader " should " should throw an IOException if initialized with a non existing file" in {
    a[IOException] should be thrownBy {
      val reader = new MnistFileInputStream("src/test/resources/nonexistingfile", testLabelFileName)
    }
    a[IOException] should be thrownBy {
      val reader = new MnistFileInputStream(testImageFileName, "src/test/resources/nonexistingfile")
    }

  }

  "MinstReader " should "should throw an IOException if the files are not images/labels pairs" in {
    a[IOException] should be thrownBy {
      new MnistFileInputStream(testLabelFileName, testLabelFileName)
    }
    a[IOException] should be thrownBy {
      new MnistFileInputStream(testImageFileName, testImageFileName)
    }
  }

}