package fr.univ_lille.cristal.emeraude.n2s3.features.builder

import akka.actor.{ActorRef, ActorSystem, Props}
import akka.testkit.TestActorRef
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.{AbstractActorSystem, ActorDeploymentStrategy, PropsBuilder}

import scala.concurrent.ExecutionContextExecutor

/**
  * Created by falezp on 13/05/16.
  */
class TestActorSystem extends AbstractActorSystem[ActorRef] {
  implicit val system = ActorSystem("Test")

  override def actorOf(props: Props, selector: ActorDeploymentStrategy, name: String = ""): ActorRef = TestActorRef(props, name)
  override def actorOf(propsBuilder: PropsBuilder, selector: ActorDeploymentStrategy): TestActorRef[_] = TestActorRef(propsBuilder.build())
  override def actorOf(propsBuilder: PropsBuilder, selector: ActorDeploymentStrategy, name : String): TestActorRef[_] = TestActorRef(propsBuilder.build(), name)

  override def dispatcher: ExecutionContextExecutor = system.dispatcher

  override def shutdown() : Unit = system.terminate()//system.shutdown()//mazdak
}
