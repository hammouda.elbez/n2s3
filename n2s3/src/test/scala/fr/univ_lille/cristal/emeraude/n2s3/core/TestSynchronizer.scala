/**
 * Contributors:
 * 		created by Guillermo Polito
 *   
 */
  
package fr.univ_lille.cristal.emeraude.n2s3.core

import akka.actor.{Actor, ActorRef}
import akka.testkit.TestActorRef
import fr.univ_lille.cristal.emeraude.n2s3.UnitActorSpec
import fr.univ_lille.cristal.emeraude.n2s3.core.Neuron.NeuronMessage
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.{Done, Spike}
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message

/**
  * TestSuite for the Synchronizer.
  *
  *  First step towards real tests for N2S3.
  * Using Akka-TestKit, which provides
  * - synchronous messaging to actors and;
  * - access to the underlying actor object to send normal scala messages.
  *
  */
case class MockSpikeInline(timestamp : Timestamp, destination: ActorRef) extends Spike

class TestSynchronizer extends UnitActorSpec {

  "A new Synchronizer" should "have no messages in the queue" in {
    val aSynchronizerActor = TestActorRef[SynchronizerActor]

    aSynchronizerActor.underlyingActor.entity.asInstanceOf[Synchronizer].queue shouldBe empty
  }

  "A Synchronizer" should "keep spikes in timestamp order" in {
    val aSynchronizerActor = TestActorRef[SynchronizerActor]
    val synchronizer = aSynchronizerActor.underlyingActor.entity.asInstanceOf[Synchronizer]

    synchronizer.queue += NeuronMessage(1, MockSpike, null, null, None)
    synchronizer.queue += NeuronMessage(0, MockSpike, null, null, None)
    synchronizer.queue += NeuronMessage(3, MockSpike, null, null, None)
    synchronizer.queue += NeuronMessage(0, MockSpike, null, null, None)
    synchronizer.queue += NeuronMessage(2, MockSpike, null, null, None)
    synchronizer.queue += NeuronMessage(1, MockSpike, null, null, None)

    synchronizer.queue.toList.head.timestamp should be(0)
    synchronizer.queue.toList(1).timestamp should be(0)
    synchronizer.queue.toList(2).timestamp should be(1)
    synchronizer.queue.toList(3).timestamp should be(1)
    synchronizer.queue.toList(4).timestamp should be(2)
    synchronizer.queue.toList(5).timestamp should be(3)
  }

  "A Synchronizer" should "resend spikes when it receives enough Done" in {
    val aSynchronizerActor = TestActorRef[SynchronizerActor]
    val synchronizer = aSynchronizerActor.underlyingActor.entity.asInstanceOf[Synchronizer]
    val destination = TestActorRef(new Actor{
      var received : List[Message] = List()
      def receive = {
        case m : Message => received = m :: received
      }
    })
    val spike = NeuronMessage(1, MockSpike, null, synchronizer.getReferenceOf(new NetworkEntityPath(destination)), None)

    synchronizer.queue += spike
    synchronizer.ackRemain = 1
    aSynchronizerActor ! Done

    destination.underlyingActor.received.length shouldBe 1
  }

  "A Synchronizer" should "empty its queue when it receives enough Done" in {
    val aSynchronizerActor = TestActorRef[SynchronizerActor]
    val aDestinationActor = TestActorRef[MockNeuronActor]


    val synchronizer = aSynchronizerActor.underlyingActor.entity.asInstanceOf[Synchronizer]


    val spike = NeuronMessage(1, MockSpike, null, synchronizer.getReferenceOf(new NetworkEntityPath(aDestinationActor)), None)

    synchronizer.queue += spike
    synchronizer.ackRemain = 1
    aSynchronizerActor ! Done

    synchronizer.queue shouldBe empty
  }

  "A Synchronizer" should "not resend spikes when it does not receive enough Done" in {
    val aSynchronizerActor = TestActorRef[SynchronizerActor]
    val synchronizer = aSynchronizerActor.underlyingActor.entity.asInstanceOf[Synchronizer]
    synchronizer.queue += NeuronMessage(1, MockSpike, null, null, None)
    synchronizer.ackRemain = 2

    aSynchronizerActor ! Done
    synchronizer.queue.isEmpty should be(false)
    synchronizer.queue.length should be(1)
  }
}