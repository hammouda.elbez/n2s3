package fr.univ_lille.cristal.emeraude.n2s3.core
import akka.testkit.TestActorRef
import fr.univ_lille.cristal.emeraude.n2s3.UnitActorSpec
import fr.univ_lille.cristal.emeraude.n2s3.core.Neuron.{CreateNeuronConnectionWith, SetSynchronizer}
import fr.univ_lille.cristal.emeraude.n2s3.core.Synchronizer.{Start, WaitEndOfActivity}
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.NetworkEntityActor.AddChildEntity
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.{InputLayerActor, InputNeuron, NetworkContainerActor, SetInput}
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input._
import fr.univ_lille.cristal.emeraude.n2s3.models.synapses.StaticSynapse
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp

/**
  * Created by falezp on 03/05/16.
  */

class MockStream(numberOfInput : Int, timeLength : Int) extends InputGenerator[InputTemporalPacket] {

  var currentTimestamp : Timestamp = 0

  override def atEnd() = currentTimestamp >= timeLength

  def next() : InputTemporalPacket = {
    currentTimestamp += 1
    new InputTemporalPacket(shape, Map((Seq((currentTimestamp%numberOfInput).toInt), Seq(N2S3InputSpike(MockSpike, currentTimestamp)))))
  }

  def reset() : Unit = {
    currentTimestamp = 0
  }

  override def shape: Shape = Shape(numberOfInput)
}

class TestInput extends UnitActorSpec {

  "add input to network" should "create connection" in {

    val stream = new StreamEntry[InputTemporalPacket](Shape(4)) >> N2S3Entry
    GlobalStream.resetTimestamps()


    stream.append(new MockStream(4, 2))

    val input = TestActorRef(new InputLayerActor(stream))
    val synchronizer = TestActorRef[SynchronizerActor]
    val synchronizer_path = NetworkEntityPath(synchronizer)

    for(i <- 0 until 4) {
      input ! AddChildEntity(i, new InputNeuron)
      ExternalSender.sendTo(NetworkEntityPath(input)/i, SetSynchronizer(synchronizer_path))
    }

    synchronizer ! SetInput(new NetworkEntityPath(input))

    val outputNeuron = TestActorRef[NetworkContainerActor]
    outputNeuron ! AddChildEntity("neuron1", new MockNeuron)
    ExternalSender.sendTo(NetworkEntityPath(outputNeuron)/"neuron1", SetSynchronizer(synchronizer_path))

    for(i <- 0 until 4) {
      ExternalSender.askTo(NetworkEntityPath(input)/i, CreateNeuronConnectionWith(NetworkEntityPath(outputNeuron)/"neuron1", new MockNeuronConnection))
    }

    synchronizer ! Start
    ExternalSender.askTo(synchronizer_path, WaitEndOfActivity)

    val neuron = outputNeuron.underlyingActor.entity.asInstanceOf[NetworkContainer].childEntityAt("neuron1").asInstanceOf[MockNeuron]
    neuron.lastReceivedMessage shouldBe MockSpike
    neuron.lastReceivedTimestamp shouldBe 1+2

  }

}
