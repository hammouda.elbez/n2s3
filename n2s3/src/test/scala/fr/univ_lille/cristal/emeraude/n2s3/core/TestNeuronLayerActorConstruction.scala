package fr.univ_lille.cristal.emeraude.n2s3.core

import akka.testkit.TestActorRef
import fr.univ_lille.cristal.emeraude.n2s3.UnitActorSpec
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.NetworkContainerActor
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.NetworkEntityActor.AddChildEntity
import fr.univ_lille.cristal.emeraude.n2s3.core.exceptions.DuplicatedChildException

/**
  * Created by falezp on 07/04/16.
  */
class TestNeuronLayerActorConstruction  extends UnitActorSpec {

  "Adding a neuron into NetworkContainerActor" should "insert a neuron" in {
    val neuronLayer = TestActorRef[NetworkContainerActor]
    neuronLayer ! AddChildEntity(1, new MockNeuron)

    neuronLayer.underlyingActor.entity.asInstanceOf[NetworkContainer].numberOfChildEntity should be (1)
  }

  "Adding two neurons into with same index" should "fail" in {
    val neuronLayer = TestActorRef[NetworkContainerActor]
    val firstNeuron = new MockNeuron
    neuronLayer ! AddChildEntity(1, firstNeuron)

    intercept[DuplicatedChildException] {neuronLayer.receive(AddChildEntity(1, new MockNeuron))}
  }

  "Adding two neurons into with same index" should "keep first neuron" in {
    val neuronLayer = TestActorRef[NetworkContainerActor]
    val firstNeuron = new MockNeuron
    neuronLayer ! AddChildEntity(1, firstNeuron)
    intercept[DuplicatedChildException] {neuronLayer.receive(AddChildEntity(1, new MockNeuron))}

    neuronLayer.underlyingActor.entity.asInstanceOf[NetworkContainer].childEntityAt(1) should be(firstNeuron)
  }

  "Adding two neurons into with different index" should "create two neuron" in {
    val neuronLayer = TestActorRef[NetworkContainerActor]
    val neuron1 = new MockNeuron
    neuronLayer ! AddChildEntity(1, neuron1)
    val neuron2 = new MockNeuron
    neuronLayer ! AddChildEntity(2, neuron2)

    val container = neuronLayer.underlyingActor.entity.asInstanceOf[NetworkContainer]
    container.numberOfChildEntity shouldBe 2
    container.childEntityAt(1) should be(neuron1)
    container.childEntityAt(2) should be(neuron2)
  }
}
