/**
  * contributors:
  * damien.marchal@univ-lille1.fr, Copyright CNRS 2016
  */
package fr.univ_lille.cristal.emeraude.n2s3.features.io

import java.io.IOException

import fr.univ_lille.cristal.emeraude.n2s3.UnitSpec
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.{AERFileReader, AERInputStream}
import fr.univ_lille.cristal.emeraude.n2s3.support.N2S3ResourceManager


class TestAERInputStream extends UnitSpec {
  val filename = N2S3ResourceManager.getByName("freeway-test-dvs").getAbsolutePath

  //TODO: Using the following file makes the tests fail. Is the file right?
  //val filename = "src/test/resources/aerdata/freeway.mat.dat"

  "AERInputStream" should "not be at the end of a not read file" in {

    val a = new AERFileReader(filename)

    println(a.nextEvents(128).mkString(", "))

    val inputStream = new AERInputStream(filename)

    val r = inputStream.next()
    println(r.getAllData.map(e => (e._1, e._2)).mkString(", "))


    inputStream shouldNot be ('atEnd)
  }

  "AERInputStream" should "be at the end of a read file" in {
    val inputStream = new AERInputStream(filename, 1)

    for(i <- 0 until inputStream.size) {
      //println(i+"/"+inputStream.aeis.size().toInt)
      inputStream.next()
    }

    inputStream should be ('atEnd)
  }

  "AERInputStream" should "know number of images in file" in {
    val inputStream = new AERInputStream(filename)
    inputStream.size shouldBe 6505991
  }

  "AERInputStream" should "have integer range of possible address" in {
    val inputStream = new AERInputStream(filename)
    inputStream.shape.dimensionNumber shouldBe 1
    inputStream.shape(0) shouldBe Int.MaxValue
  }


  "AERInputStream next" should "be able to read all images without throwing an exception" in {
    val inputStream = new AERInputStream(filename, 1)
    for (i <- 0 until inputStream.size) {
        inputStream.next()
    }
  }

  "AERInputStream next" should "throw an exception after end of file" in {
    val inputStream = new AERInputStream(filename, 1)
    for (i <- 0 until inputStream.size) {
      inputStream.next()
    }
    inputStream should be ('atEnd)
    intercept[IOException](inputStream.next())
  }

  "AERInputStream reset" should "take it back to the beginning" in {
    val inputStream = new AERInputStream(filename, 1)
    val first = inputStream.next()
    inputStream.reset()
    val second = inputStream.next()

    first shouldBe second
  }

  "AERInputStream reset" should "take it back to the beginning When everything was read before" in {
    val inputStream = new AERInputStream(filename, 1)
    inputStream.next(inputStream.size)
    inputStream.reset()
    inputStream.next()
  }

  "AERInputStream next with arguments" should "return a list of the desired size" in {
    val inputStream = new AERInputStream(filename)
    inputStream.next(2000).length shouldBe 2000
  }

  "AERInputStream" should "throw an IOException if initialized with a non existing file" in {
    a[IOException] should be thrownBy {
      val reader = new AERInputStream("src/test/resources/nonexistingfile")
    }
  }
}