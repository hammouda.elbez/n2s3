package fr.univ_lille.cristal.emeraude.n2s3.core.synchronization

import akka.actor.{Actor, Props}
import akka.testkit.TestActorRef
import fr.univ_lille.cristal.emeraude.n2s3.UnitSpec
import fr.univ_lille.cristal.emeraude.n2s3.core.ExternalNetworkEntityReference
import fr.univ_lille.cristal.emeraude.n2s3.core.Neuron.NeuronMessage
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.NetworkEntityActor.ImplicitSenderRoutedMessage
import fr.univ_lille.cristal.emeraude.n2s3.features.builder._
import fr.univ_lille.cristal.emeraude.n2s3.models.neurons.LIF
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.{ActorCompanion, Message, PropsBuilder}

/**
  * Created by guille on 8/5/16.
  */
class TestSynchronizedNeuronRef extends UnitSpec {


  "Using a synchronized neuron ref" should "send message to neurons through synchronizer" in {
    val n2s3 = new N2S3(new TestActorSystem)
    val group = new NeuronGroupRef(n2s3)
    group.setNumberOfNeurons(10)
    group.setNeuronModel(LIF)

    group.ensureActorDeployed(n2s3)

    val synchronizerActor = n2s3.createSynchronizerActor(TestSynchronizer)
    val ref = new SynchronizedNeuronRef(synchronizerActor, group.neurons.head)

    ref.send(Ping)
    synchronizerActor.asInstanceOf[TestActorRef[TestSynchronizerActor]].underlyingActor.pinged shouldBe true
  }

  "Using a synchronized neuron ref" should "send wrapped message to synchronizer" in {
    val n2s3 = new N2S3(new TestActorSystem)
    val group = new NeuronGroupRef(n2s3)
    group.setNumberOfNeurons(10)
    group.setNeuronModel(LIF)

    group.ensureActorDeployed(n2s3)

    val synchronizerActor = n2s3.createSynchronizerActor(TestSynchronizer)
    val ref = new SynchronizedNeuronRef(synchronizerActor, group.neurons.head)

    ref.send(Ping)
    val message = synchronizerActor.asInstanceOf[TestActorRef[TestSynchronizerActor]].underlyingActor.message
    message.message shouldBe Ping
  }

  "Synchronized wrapped message" should "know original destination" in {
    val n2s3 = new N2S3(new TestActorSystem)
    val group = new NeuronGroupRef(n2s3)
    group.setNumberOfNeurons(10)
    group.setNeuronModel(LIF)

    group.ensureActorDeployed(n2s3)

    val synchronizerActor = n2s3.createSynchronizerActor(TestSynchronizer)
    val ref = new SynchronizedNeuronRef(synchronizerActor, group.neurons.head)

    ref.send(Ping)
    val message = synchronizerActor.asInstanceOf[TestActorRef[TestSynchronizerActor]].underlyingActor.message
    message.postSync.asInstanceOf[ExternalNetworkEntityReference].target shouldBe group.neurons.head.actorPath.get
  }
}

object TestSynchronizer extends ActorCompanion{
  override def newPropsBuilder(): PropsBuilder = new PropsBuilder {
    override def build(): Props = Props[TestSynchronizerActor]
  }
}

object Ping extends Message
class TestSynchronizerActor extends Actor{

  var message: NeuronMessage = _
  var pinged = false

  override def receive: Receive = {
    case m: ImplicitSenderRoutedMessage =>
      pinged = true
      message = m.message.asInstanceOf[NeuronMessage]
    case other =>
      println(other)
      pinged = true
  }
}
