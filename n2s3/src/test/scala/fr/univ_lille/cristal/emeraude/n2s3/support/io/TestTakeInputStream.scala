package fr.univ_lille.cristal.emeraude.n2s3.support.io

import java.io.IOException

import fr.univ_lille.cristal.emeraude.n2s3.UnitSpec
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.MnistFileInputStream
import fr.univ_lille.cristal.emeraude.n2s3.support.N2S3ResourceManager

/**
  * Created by guille on 6/2/16.
  */
class TestTakeInputStream extends UnitSpec {
  val testImageFileName: String = N2S3ResourceManager.getByName("mnist-test-images").getAbsolutePath
  val testLabelFileName: String = N2S3ResourceManager.getByName("mnist-test-labels").getAbsolutePath


  "Take input stream" should "read up to its limit if wrappee is bigger" in {
    val wrappee = new MnistFileInputStream(testImageFileName, testLabelFileName)
    val limit = 10.min(wrappee.numberImage - 1)
    val stream = new TakeInputStream(limit, wrappee)

    stream.next(limit).length shouldBe limit
  }

  "Take input stream with -1 limit" should "not be at end at the begininng" in {
    val wrappee = new MnistFileInputStream(testImageFileName, testLabelFileName)
    val stream = new TakeInputStream(-1, wrappee)

    stream shouldNot be('atEnd)
  }

  "Take input stream" should "have no limit if -1" in {
    val wrappee = new MnistFileInputStream(testImageFileName, testLabelFileName)
    val stream = new TakeInputStream(-1, wrappee)

    stream.next(wrappee.numberImage).length shouldBe wrappee.numberImage
  }

  "Take input stream" should "be at end if limit was read" in {
    val wrappee = new MnistFileInputStream(testImageFileName, testLabelFileName)
    val limit = 10.min(wrappee.numberImage - 1)
    val stream = new TakeInputStream(limit, wrappee)
    stream.next(limit)

    stream shouldBe 'atEnd
  }

  "Take input stream" should "be at end if end of wrappee file" in {
    val wrappee = new MnistFileInputStream(testImageFileName, testLabelFileName)
    val stream = new TakeInputStream(wrappee.numberImage+1, wrappee)
    stream.next(wrappee.numberImage)
    stream shouldBe 'atEnd
  }

  "Take input stream" should "read up to end of file if its limit if wrappee is smaller" in {
    val wrappee = new MnistFileInputStream(testImageFileName, testLabelFileName)
    val limit = wrappee.numberImage + 1
    val stream = new TakeInputStream(limit, wrappee)
    stream.next(wrappee.numberImage)
    a[IOException] shouldBe thrownBy(stream.next())
  }
}
