package fr.univ_lille.cristal.emeraude.n2s3.core.simulationRefs

import fr.univ_lille.cristal.emeraude.n2s3.UnitSpec
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.types.OneToOneConnection
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.{N2S3, NeuronGroupRef}

/**
  * Created by guille on 8/5/16.
  */
class TestNeuronGroupRefOneToOneConnection extends UnitSpec {

  "One-to-one connection between neuron groups A and B" should "connect all in A to all in B" in {
    val groupA = new NeuronGroupRef(new N2S3()).setNumberOfNeurons(2)
    val groupB = new NeuronGroupRef(new N2S3()).setNumberOfNeurons(2)

    groupA.connectTo(groupB, new OneToOneConnection)

    for (aNeuron <- groupA.neurons) {
      aNeuron.isConnectedTo(groupB.neurons(groupB.shape.toIndex(aNeuron.getIndex:_*))) shouldBe true
    }
  }

  "One-to-one connection between neuron groups A and B" should "not connect to neurons that do not share index" in {
    val groupA = new NeuronGroupRef(new N2S3()).setNumberOfNeurons(2)
    val groupB = new NeuronGroupRef(new N2S3()).setNumberOfNeurons(2)

    groupA.connectTo(groupB, new OneToOneConnection)

    for (
      aNeuron <- groupA.neurons;
      bNeuron <- groupB.neurons
    ) yield {
      aNeuron.isConnectedTo(bNeuron) shouldBe (aNeuron.getIndex == bNeuron.getIndex)
    }
  }

  "One-to-one connection between neuron groups A and B" should "not connect neurons in the same group" in {
    val groupA = new NeuronGroupRef(new N2S3()).setNumberOfNeurons(2)
    val groupB = new NeuronGroupRef(new N2S3()).setNumberOfNeurons(2)
    groupA.connectTo(groupB, new OneToOneConnection)

    for (
      aNeuron <- groupA.neurons;
      bNeuron <- groupA.neurons
    ) yield {
      aNeuron.isConnectedTo(bNeuron) shouldBe false
    }
  }
}