package fr.univ_lille.cristal.emeraude.n2s3

import org.scalatest._
import flatspec._//mazdak
import matchers._//mazdak
/*
 * Common style of test used in the N2S3 project.
 */
//abstract class UnitSpec extends FlatSpec with Matchers
abstract class UnitSpec extends AnyFlatSpec with should.Matchers
