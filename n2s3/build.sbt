/**
  * SBT project description
  *
  * Uses the sbt-assembly plugin to create a fat jar containing the library and all dependencies.
  * See: https://github.com/sbt/sbt-assembly
  */

name             := "N2S3"


test in assembly := {}

/*********************************************************************************************************************
  * Dependencies
  *******************************************************************************************************************/

libraryDependencies += "org.mongodb.scala" %% "mongo-scala-driver" % "2.6.0"

libraryDependencies ++= {
  val scalaXmlV = "1.0.6"//"1.0.2"// No scala-xml_2.12;1.0.2 is released!!! for 2.12, i found 1.0.6 (https://mvnrepository.com/artifact/org.scala-lang.modules/scala-xml)
  val akkaV = "2.7.0"//"2.5.32"//"2.4.16"OK with warning for shutdown//"2.5.32"//"2.4.16"//"2.5.0"//"2.4.17" OK//"2.3.7"// Scala 2.12.0 is not binary compatabl with akka 2.3.7
//https://doc.akka.io/docs/akka/2.5/project/migration-guide-2.4.x-2.5.x.html  
//You must first update all nodes to 2.4.16. It’s not supported to update directly from an older version than 2.4.16 to 2.5.x. For example, if you are running 2.4.11 you must first do a rolling update to 2.4.16, shut down all 2.4.11 nodes, and then do the rolling update to 2.5.x.

  val scalatestV = "3.2.15"//"2.2.1"
  Seq(
    "com.github.wookietreiber" %% "scala-chart" % "latest.integration",// added by mazdak from https://index.scala-lang.org/scala-chart/scala-chart
    "javax.activation" % "activation" % "1.1.1",
    "commons-io" % "commons-io" % "2.4",
    "org.scala-lang.modules" %% "scala-xml" % scalaXmlV,
    "com.typesafe.akka" %% "akka-actor"      % akkaV,
    "com.typesafe.akka" %% "akka-testkit"    % akkaV,
    "com.typesafe.akka" %% "akka-cluster"    % akkaV,
    "org.scalatest"     %% "scalatest"       % scalatestV % "test",
    "org.typelevel"  %% "squants"  % "1.6.0",//"com.squants"  %% "squants"  % "1.6.0",//"0.5.3",// https://github.com/typelevel/squants
    "com.storm-enroute" %% "scalameter" % "0.18",//"0.7",//[error] sbt.librarymanagement.ResolveException: unresolved dependency: com.storm-enroute#scalameter_2.12;0.7: not found
// http://scalameter.github.io/home/news/   //  
    "org.knowm.xchart" % "xchart" % "3.8.3" exclude("de.erichseifert.vectorgraphics2d", "VectorGraphics2D"),//3.3.1
    "net.sf" %% "jaer" % "1.0" from "https://sourcesup.renater.fr/frs/download.php/file/5047/jaer.jar"
  )
}
