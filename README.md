# N2S3 a Neural Network Simulator

Thank you for downloading N2S3. 

The aim of this simulator is to simulate different physical neural networks topology with different Synapses/Neurons models. 

# Installation and testing

To use N2S3 in your project just declare a dependency as follows in your sbt file:

```scala
libraryDependencies ++= Seq(
  "fr.univ-lille.cristal" %% "n2s3" % "1.0" exclude("net.sf", "jaer_2.11"),
  "net.sf" %% "jaer" % "1.0" from "https://sourcesup.renater.fr/frs/download.php/file/5047/jaer.jar"
)
```

If you want to compile the code in this repository you can execute:

```bash
$ sbt compile
```

Similarly, you can run the tests:

```bash
$ sbt test
```

Check the docs: https://sourcesup.renater.fr/wiki/n2s3/
  
# Changes Log

## v1.2

### Core
- Added MongoDB Log Class 
- Added possibility to use only parts of the MNIST Dataset 
- Added prune examples
- Added FMNIST dataset simulation example 
- Added Username and password variables to MongoLog class

## v1.1

N2S3 1.1 is a quality improvement release.
The highlights of this version are the introduction of a new Domain Specific Language (DSL), a reorganization of the existing neuron models and lots of cleanups.

### Infrastructure

- Update XChart version

### Core

- Introduction of a DSL to simplify the task of creating a simulation//TODO: introduce link to the documentation.
- The different models N2S3 supports have been reorganised:
  - Neuron Models are now located in the `models.neurons` package. The featured models are:
    - Izhikevich (http://www.izhikevich.org/publications/spikes.pdf)
    - Leaky-Integrate-and-Fire (http://www.cns.nyu.edu/~eorhan/notes/lif-neuron.pdf)
    - Spike-Response-Model (http://icwww.epfl.ch/~gerstner/SPNM/node27.html)
  - Synapse Models are now located in the `models.synapses` package. The featured synapses are:
    - Inhibitory Synapse
    - Simplified STDP (http://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=6033439)
    - Stantard STDP (https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3431193/)
    - Static Synapse
    - Ternary Synapse
  - Improve the usage of Squant units
  - There is no more `defaultSynapse` method in neuron model, therefore synapse model need to be given explicitly
  - Modification in the synapses identifications. All incoming synapses have an local unique identifier
    - Synapse weight graph require an explicit list of `NeuronConnection` in order to display then in the right order
    
### Support
 
 - Add a Resource manager that aims to automatically download and unzip used datasets. Removal of datasets in the code repertory
 
### Cleanups

- Removal of lots of compilation warnings
- Removal of unused classes from the support package, old inputs
- Removal of the deprecated connections creation in ConnectionPolicy and subclasses
- Removal and cleanup of commented code

### Documentation

- Updated tutorial for version 1.1 https://sourcesup.renater.fr/wiki/n2s3/tutorial
- Updated download page https://sourcesup.renater.fr/wiki/n2s3/installation
- //TODO make page on DSL
- //TODO doc about models

## v1.0

N2S3 is now feature complete, well tested and fully documented. External users are welcome!

N2S3 1.0 is mainly an infrastructure release oriented towards the ease of development and distribution. This version builds on version 0.2 with ~165 new commits! There are also some news in the core of the simulator and the documentation. Here you have the major changes:

### Infrastructure

- N2S3 is now an SBT multi-project [1]
- Better integration with IntelliJ IDEA and Eclipse [2]
- Supporting the system's distribution:
  - Clustering support
  - Refactoring to support the dynamic deployment of actors in different cluster nodes.
  - Two deployment strategies were implemented so far: Random and concrete node.
  - Fixes in the core to make it distributable. E.g, fixed serialization of messages, global state.
- The Continuous Integration service was extended and enhanced [4]
- A new input mechanism was implemented [5, 6]

### Core

- Preliminar version of supervised learning algorithm SpikeProp
- Preliminar version of Supervised Xor, an example of learning the Xor function with SpikeProp (Error-backpropagation in temporally encoded networks of spiking neurons)
- Added bio-inspired model i.e., a model with parameters similar to those in the nature.

### Examples

- Examples of Mnist/Freeway/sevenSegment with the bio-inspired model.
- Example of Masquelier's experiment: Spike Timing Dependent Plasticity Finds the Start of Repeating Patterns in Continuous Spike Trains
- Preliminar version of an example of direction detection with reservoir computing (using supervised STDP and only 2 directions)

### Documentation

- The core scaladocs were entirely revisited
- Technical documentation about the core implementation can be found in [7]

We are waiting for your contributions, And do not hesitate to share!

The N2S3 team

[1] https://sourcesup.renater.fr/wiki/n2s3/environmentsetup#sbt_integration

[2] https://sourcesup.renater.fr/wiki/n2s3/environmentsetup#ide_integration

[3] https://sourcesup.renater.fr/wiki/n2s3/simulation_cluster

[4] https://sourcesup.renater.fr/jenkins/job/N2S3/

[5] https://sourcesup.renater.fr/wiki/n2s3/simulation_stimuli

[6] https://sourcesup.renater.fr/wiki/n2s3/inputs

[7] https://sourcesup.renater.fr/wiki/n2s3/devdocs

## v0.2

### Infrastructural
 - all changes required to package and publish n2s3 as a library
 - all tests are green

### Documentation
 - on the documentation side, scaladocs were added on the main classes that are needed to setup experiments

### Core
 - Added ternary synapse model