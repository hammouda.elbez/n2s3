package fr.univ_lille.cristal.emeraude.n2s3.apps.tool

import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO

import fr.univ_lille.cristal.emeraude.n2s3.features.io.persistence.WeightLoad
import fr.univ_lille.cristal.emeraude.n2s3.features.logging.graph.SynapticWeightSelectGraph

import scala.collection.mutable

/**
  * Created by pfalez on 03/05/17.
  */
object GenerateWeightReconstructionOnOff extends App {

  val layers = Seq(
    "emotion/3/MNIST_CONV1_5_weights",
    "emotion/4/MNIST_CONV2_5_to_7_weights"
  )

  val output = "emotion/4/MNIST_CONV2_7_filter"
  val factor = 10

  val weights = layers.map{filename =>
    println("Loading "+filename+"...")
    WeightLoad.fromFile(filename)
  }

  assert(weights.nonEmpty)

  val inputWeights = weights.head
  val outputWeights = weights.last

  println((Seq(inputWeights.inShape)++(if(weights.size > 1) weights else Seq()).sliding(2).map{ l =>
    val input = l.head
    val output = l.last
    println(input.outShape+" "+output.inShape)
    assert(input.outShape == output.inShape)
    input.outShape
  }++Seq(outputWeights.outShape)).mkString(" -> "))

  assert(inputWeights.inShape.dimensionNumber == 3 && inputWeights.inShape.dimensions.last == 2)

  for(filter <- 0 until outputWeights.outShape.dimensions(0)) {

    val values = mutable.HashMap[(Int, Int), (Int, Float)]()

    def recurse(layer : Int, index : Int, value : Float) : Unit = {
      if(layer < 0) {
        val (oldCount, oldValue) = values.getOrElse((-1, index), (0, 0f))
        values.update((-1, index), (oldCount+1, oldValue+value))
      }
      else {
        val (oldCount, oldValue) = values.getOrElse((layer, index), (0, 0f))
        values.update((layer, index), (oldCount+1, oldValue+value))
        weights(layer).getInputsFromIndex(index).foreach { case (inputIndex, weight) =>
          recurse(layer - 1, inputIndex, value * weight)
        }
      }
    }

    recurse(weights.size-1, outputWeights.outShape.toIndex(filter, 0, 0), 1f)

    val coords = values.filter(_._1._1 == -1).map{ case((l, i), v) =>
      inputWeights.inShape.fromIndex(i)
    }

    val xMin = coords.map(_(0)).min
    val xMax = coords.map(_(0)).max
    val yMin = coords.map(_(1)).min
    val yMax = coords.map(_(1)).max

    val width = xMax-xMin+1
    val height = yMax-yMin+1

    val img = new BufferedImage(width*factor, height*factor, BufferedImage.TYPE_INT_ARGB)

    val valuesOnOff = for{
      x <- xMin to xMax
      y <- yMin to yMax

    } yield {
      val onIndex = -1 -> inputWeights.inShape.toIndex(x, y, 0)
      val offIndex = -1 -> inputWeights.inShape.toIndex(x, y, 1)

      val (onCount, onValue) = values.getOrElse(onIndex, (1, 0f))
      val (offCount, offValue) = values.getOrElse(offIndex, (1, 0f))

      (x, y, onValue/onCount.toFloat, offValue/offCount.toFloat)
    }

    val maxOnValues = valuesOnOff.map(_._3).max
    val maxOffValues = valuesOnOff.map(_._4).max

    SynapticWeightSelectGraph.paint(valuesOnOff.map{ case(x, y, on, off) =>
      (x, y, 0.5f+(on/maxOnValues)/2f-(off/maxOffValues)/2f)
    }, img.getGraphics, factor, SynapticWeightSelectGraph.grayScale)


    ImageIO.write(img, "png", new File(output+"_"+filter))
  }
}
