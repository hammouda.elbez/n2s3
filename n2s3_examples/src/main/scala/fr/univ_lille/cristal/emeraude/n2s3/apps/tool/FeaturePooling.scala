package fr.univ_lille.cristal.emeraude.n2s3.apps.tool

import java.io.{DataInputStream, FileInputStream, FileOutputStream, PrintWriter}

import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.{N2S3InputLabel, N2S3InputSpike, Shape}
import fr.univ_lille.cristal.emeraude.n2s3.features.io.persistence.ActivityPersistenceModel.EventFactory
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp

import scala.collection.mutable

/**
  * Created by pfalez on 06/06/17.
  */
object FeaturePooling extends App {

  val filename = "emotion/3/test/MNIST_CONV1_11_spikes"
  val output = "emotion/3/test/conv-11-pool-features"

  def labelFunction(l : String) : String = l
  def eventFunction(events : Seq[Timestamp]) : Float = {
    events.size.toFloat
  }

  def writeEntryFunction(writer : PrintWriter, label : String, shape : Shape, events : Map[Seq[Int], Float]) : Unit = {
    writer.print(label)
    for {
      i <- 0 until shape.dimensions(0)
    } {

      val value = (for {
        x <- 0 until shape.dimensions(1)
        y <- 0 until shape.dimensions(2)
      } yield {
        math.min(1f, events.getOrElse(Seq(i, x, y), 0f))
      }).sum

      writer.print(" "+(i+1)+":"+value)
    }
    writer.println()
  }

  var reader = new DataInputStream(new FileInputStream(filename))
  val writer = new PrintWriter(new FileOutputStream(output))
  val dimNumber : Int = reader.readInt()
  val s = Shape((0 until dimNumber).map(_ => reader.readInt()):_*)
  val totalEvent : Long = reader.readLong()

  var currentInput : Option[String] = None
  var currentEvents : mutable.HashMap[Long, mutable.ArrayBuffer[Timestamp]] = mutable.HashMap()

  println("header : "+s+" "+totalEvent)

  var i : Long = 0
  while(i < totalEvent) {
    val timestamp = reader.readLong()
    val metaDataSize = reader.readInt()
    val dataSize = reader.readInt()

    val labels = (0 until metaDataSize).map { _ =>
      EventFactory.createMetaData(reader, timestamp)
    }.filter(_.isInstanceOf[N2S3InputLabel]).map(_.asInstanceOf[N2S3InputLabel])

    assert(labels.size <= 1)

    if(labels.nonEmpty) {
      if (currentInput.isDefined) {
        writeEntryFunction(writer, labelFunction(currentInput.get), s, (for(i <- 0 until s.getNumberOfPoints) yield {
          (s.fromIndex(i), eventFunction(currentEvents.getOrElse(i.toLong, Seq())))
        }).toMap)
      }

      currentInput = Some(labels.head.getLabel)
      currentEvents.clear()
    }



    (0 until dataSize).foreach { _ =>
      EventFactory.createData(reader, timestamp) match {
        case (index, spike : N2S3InputSpike) =>
          currentEvents.getOrElseUpdate(index, mutable.ArrayBuffer[Timestamp]()) += spike.getStartTimestamp
      }
    }

    i += metaDataSize+dataSize
  }

  if (currentInput.isDefined) {
    writeEntryFunction(writer, labelFunction(currentInput.get), s, (for(i <- 0 until s.getNumberOfPoints) yield {
      (s.fromIndex(i), eventFunction(currentEvents.getOrElse(i.toLong, Seq())))
    }).toMap)
  }

  reader.close()
  writer.close()
}
