package fr.univ_lille.cristal.emeraude.n2s3.apps.tool

import javax.swing.JFrame

import fr.univ_lille.cristal.emeraude.n2s3.apps.tool.LifDiehlModel.{vrest, vth}
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.{PoissonDistribution, Time}
import fr.univ_lille.cristal.emeraude.n2s3.support.UnitCast._
import org.knowm.xchart.style.markers.SeriesMarkers
import org.knowm.xchart.{SwingWrapper, XYChart, XYChartBuilder, XYSeries}

import scala.collection.JavaConversions._
import scala.collection.mutable
/**
  * Created by pfalez on 08/06/17.
  */

abstract class NeuronDifferentialModel {

  def update(timeStep : Double, inputCurrent : Double) : Unit
  def values  : Map[String, Double]

  def process(input : Seq[(Timestamp, Double)]) : Map[String, Seq[Double]] = {

    val list : Map[String, mutable.ArrayBuffer[Double]] = values.map{ case(name, value) =>
      name -> mutable.ArrayBuffer[Double](value)
    }

    for(((t, v), i) <- input.zipWithIndex.tail) yield {
      val timeStep = Time(t-input(i-1)._1).asMilliSecond.toDouble
      update(timeStep, v)
      val result = values
      list.foreach{ case(name, list) =>
        list += result(name)
      }

    }

    list
  }
}

object LifModel extends NeuronDifferentialModel {
  val vth = -52.0
  val vrest = -65.0
  val r = 1.0
  val tau = 100 MilliSecond

  var v =vrest

  override def update(timeStep : Double, inputCurrent : Double) : Unit = {
    val v_old = v
    v += timeStep*(vrest-v_old)/tau.asMilliSecond+inputCurrent*r

    if(v >= vth) {
      v = vrest
    }
  }

  override def values: Map[String, Double] = {
    Map("V" -> v, "Vth" -> vth)
  }

}

object LifDiehlModel extends NeuronDifferentialModel {
  val vth = -52.0
  val vrest = -65.0
  val r = 1.0
  val tau = 100 MilliSecond

  val tau_ge = 1 MilliSecond
  val tau_gi = 2 MilliSecond

  val theta_plus = 0.05
  val theta_tau = 1e7 MilliSecond


  var v = vrest
  var theta = 0.0
  var ge = 0.0
  var gi = 0.0

  override def update(timeStep : Double, inputCurrent : Double) : Unit = {
    val v_old = v
    val ge_old = ge
    val gi_old = gi

    ge += -ge_old/tau_ge.asMilliSecond+inputCurrent
    gi += -gi_old/tau_gi.asMilliSecond

    val ee = 1.0
    val ei = 1.0
    v += timeStep*((vrest-v_old)+ge_old*(ee-v_old)+ge_old*(ei-v_old))/tau.asMilliSecond
    theta += -theta/theta_tau.asMilliSecond

    if(v >= vth+theta) {
      v = vrest
      theta += theta_plus
    }
  }

  override def values  : Map[String, Double] = {
    Map("V" -> v, "Vth" -> (vth+theta), "Ge" -> ge)
  }

}

object IzhikevichModel extends NeuronDifferentialModel {

  val a : Double = 0.02
  val b : Double = 0.5
  val c : Double = -65.0
  val d : Double = 8.0
  val vth : Double = 30.0

  var v = c
  var u = 0.0

  override def update(timeStep : Double, inputCurrent : Double) : Unit = {
    val v_old = v
    val u_old = u

    v += timeStep*(0.04*v_old*v_old+5.0*v_old+140.0-u_old+inputCurrent)
    u += timeStep*a*(b*v_old-u_old)

    if(v >= vth) {
      v = c
      u += d
    }
  }

  override def values  : Map[String, Double] = {
    Map("V" -> v, "U" -> u, "Vth" -> vth)
  }

}

class Plot(timestamp : Seq[Timestamp], input : Seq[Double], output : (String, Map[String, Seq[Double]])*) {
  val x : Array[Double] =  timestamp.map(t => Time(t).asSecond.toDouble).toArray

  val inputChart : XYChart = new XYChartBuilder().xAxisTitle("t").yAxisTitle("I").width(600).height(400).build
  val inputSeries: XYSeries = inputChart.addSeries("Input", x, input.toArray)
  inputSeries.setMarker(SeriesMarkers.NONE)

  val outputs = output.map { case(modelName, data) =>
    val outputChart: XYChart = new XYChartBuilder().title(modelName).xAxisTitle("t").yAxisTitle("V").width(600).height(400).build
    data.foreach { case(variableName, seq) =>
      val outputSeries: XYSeries = outputChart.addSeries(variableName, x, seq.toArray)
      outputSeries.setMarker(SeriesMarkers.NONE)
    }
    outputChart
  }


  new SwingWrapper[XYChart](Seq(inputChart)++outputs).displayChartMatrix
}

object NeuronModeling extends App {

  val start_t = 0 Second
  val end_t = 1 Second
  val step_t = 1 MilliSecond

  def spikeShape(deltaT : Timestamp): Double = {
    if(deltaT < step_t.timestamp) 1.0 else 0.0
  }

  val timestamps = start_t.timestamp until end_t.timestamp by step_t.timestamp
  val spikes = new PoissonDistribution(start_t.timestamp, end_t.timestamp, 0f, 22f).applyTo(1.0)
  val input = timestamps.map{t =>
    val prevSpikeList = spikes.filter(_ < t)
    val delta = if(prevSpikeList.nonEmpty) t-prevSpikeList.max else Long.MaxValue
    spikeShape(delta)
  }

  //val result = model.process(timestamps.zip(input))

  new Plot(timestamps, input,

    "LIF" -> LifModel.process(timestamps.zip(input)),
    "LIF_Dielh" -> LifDiehlModel.process(timestamps.zip(input)),
    "Izhikevich" -> IzhikevichModel.process(timestamps.zip(input))

  )
}
