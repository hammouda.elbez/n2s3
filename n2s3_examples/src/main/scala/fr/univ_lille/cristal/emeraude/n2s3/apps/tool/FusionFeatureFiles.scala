package fr.univ_lille.cristal.emeraude.n2s3.apps.tool

import java.io._

import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.{N2S3InputLabel, N2S3InputSpike, Shape}
import fr.univ_lille.cristal.emeraude.n2s3.features.io.persistence.ActivityPersistenceModel.EventFactory
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp

import scala.collection.mutable

/**
  * Created by pfalez on 22/05/17.
  */
object FusionFeatureFiles extends App {

  println("start...")

  val inputs = Seq(
    "emotion/3/test/MNIST_CONV1_5_spikes",
    "emotion/3/test/MNIST_CONV1_7_spikes",
    "emotion/3/test/MNIST_CONV1_9_spikes",
    "emotion/3/test/MNIST_CONV1_11_spikes"
  )

  val output = "emotion/3/test/all-conv-features"


  def labelFunction(l : String) : String = l
  def eventFunction(events : Seq[Timestamp]) : Float = {
    events.size
  }

  var readers = inputs.map(f => new DataInputStream(new FileInputStream(f)))
  val writer = new PrintWriter(new FileOutputStream(output))

  var nextInput : mutable.ArrayBuffer[Option[String]] = mutable.ArrayBuffer(readers.map(_ => None):_*)

  var currentEvents = readers.map{ _ =>
    mutable.HashMap[Long, mutable.ArrayBuffer[Timestamp]]()
  }
  var nextEvents = readers.map{ _ =>
    mutable.HashMap[Long, mutable.ArrayBuffer[Timestamp]]()
  }

  val readerShape = readers.map{ reader =>
    val dimNumber: Int = reader.readInt()
    Shape((0 until dimNumber).map(_ => reader.readInt()): _*)
  }


  val remainEvent = mutable.ArrayBuffer[Long](readers.map { reader =>
    val totalEvent: Long = reader.readLong()

    totalEvent
  }:_*)

  readers.indices.foreach{ i =>
    println("Input "+i+" : shape: "+readerShape(i)+", events: "+remainEvent(i))
  }

  var inputCount = 0

  while(remainEvent.forall(_ > 0)) {

    var startIndex = 0

    val input = nextInput.head
    assert(nextInput.tail.forall(_ == input))

    readers.zipWithIndex.foreach { case(reader, readerIndex) =>
      val s = readerShape(readerIndex)

      currentEvents(readerIndex) ++= nextEvents(readerIndex)
      nextEvents(readerIndex).clear()

      var isNewInput = false
      while(!isNewInput && remainEvent(readerIndex) > 0) {
        val timestamp = reader.readLong()
        val metaDataSize = reader.readInt()
        val dataSize = reader.readInt()

        val labels = (0 until metaDataSize).map { _ =>
          EventFactory.createMetaData(reader, timestamp)
        }.filter(_.isInstanceOf[N2S3InputLabel]).map(_.asInstanceOf[N2S3InputLabel])

        assert(labels.size <= 1)

        if (labels.nonEmpty) {
          nextInput(readerIndex) = Some(labels.head.getLabel)
          isNewInput = true
        }

        (0 until dataSize).foreach { _ =>
          EventFactory.createData(reader, timestamp) match {
            case (index, spike: N2S3InputSpike) =>
              val container = if (isNewInput) nextEvents(readerIndex) else currentEvents(readerIndex)
              container.getOrElseUpdate(index, mutable.ArrayBuffer[Timestamp]()) += spike.getStartTimestamp
          }
        }

        remainEvent(readerIndex) -= metaDataSize + dataSize
      }

      if(input.isDefined) {

        if (readerIndex == 0) {
          //println(inputCount)
          inputCount += 1
          writer.print(labelFunction(input.get))
        }

        writer.print(" " + (1 to s.getNumberOfPoints).map(i => (startIndex + i) + ":" + eventFunction(currentEvents(readerIndex).getOrElse(i.toLong, Seq()))).mkString(" "))
        currentEvents(readerIndex).clear()
        startIndex += s.getNumberOfPoints
      }

    }
    writer.println()
  }

  val input = nextInput.head
  if (input.isDefined) {
    var startIndex = 0
    readers.zipWithIndex.foreach { case (reader, readerIndex) =>
      if (readerIndex == 0)
        writer.print(labelFunction(input.get))

      val s = readerShape(readerIndex)
      writer.println(labelFunction(input.get) + " " + (1 to s.getNumberOfPoints).map(i => (startIndex+i)+ ":" + eventFunction(currentEvents(readerIndex).getOrElse(i.toLong, Seq()))).mkString(" "))
      startIndex += s.getNumberOfPoints
    }
  }


  readers.foreach(_.close())
  writer.close()

  /*
  val readers = inputs.map(f => new BufferedReader(new FileReader(f)))
  val writer = new PrintWriter(new FileWriter(output))

  var cpt = 0
  var remains = true
  while(remains) {
    println(cpt)
    cpt += 1
    val lines = readers.map(_.readLine())
    if(lines.contains(null))
      remains = false
    else {
      var label : Option[String] = None
      var featureIndex = 0
      lines.foreach{ l =>
        val fields = l.split("\\s+")
        val currLabel = fields(0)
        if(label.isDefined)
          assert(currLabel == label.get)
        else {
          label = Some(currLabel)
          writer.print(currLabel)
        }
        var i = 1
        while(i < fields.length) {
          writer.print(" "+featureIndex+":"+l.split(":").last)
          i += 1
          featureIndex += 1
        }
      }
      writer.println()
    }
  }

  writer.close()
  readers.foreach(_.close())*/
}
