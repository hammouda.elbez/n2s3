package fr.univ_lille.cristal.emeraude.n2s3.apps.tool

import java.io.{File, FileNotFoundException}

import com.typesafe.config.ConfigFactory
import fr.univ_lille.cristal.emeraude.n2s3.core.ConnectionIndex
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.Config
import fr.univ_lille.cristal.emeraude.n2s3.core.event.{NeuronPotentialResponse, NeuronPotentialUpdateEvent}
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.{MembranePotentialThreshold, MembraneThresholdType, MembraneThresholdTypeEnum}
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.N2S3
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.types.FullConnection
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.N2S3InputStreamCombinators._
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input._
import fr.univ_lille.cristal.emeraude.n2s3.features.io.persistence.WeightLoad
import fr.univ_lille.cristal.emeraude.n2s3.features.logging.PropertyLoggingRef
import fr.univ_lille.cristal.emeraude.n2s3.features.logging.graph.{Plot, SynapticWeightSelectAndMergeGraph, SynapticWeightSelectAndMergeGraphRef, SynapticWeightSelectGraph}
import fr.univ_lille.cristal.emeraude.n2s3.models.neurons.{Izhikevich, LIF}
import fr.univ_lille.cristal.emeraude.n2s3.models.synapses.{QBGParameters, StaticSynapse}
import fr.univ_lille.cristal.emeraude.n2s3.support.UnitCast._
import squants.electro.ElectricPotential
import squants.electro.ElectricPotentialConversions.ElectricPotentialConversions

/**
  * Created by pfalez on 23/05/17.
  */
object VisualizeNeuronCompetition extends App {


  implicit val timeout = Config.longTimeout

  QBGParameters.alf_m = 0.005f
  QBGParameters.alf_p = 0.01f
  QBGParameters.beta_m = 3f
  QBGParameters.beta_p = 3f

  val n2s3 = new N2S3("N2S3")

  val imageWidth = 28
  val imageHeight = 28
  val OnOffKernelSize = 9
  val conv1KernelSize = 5
  val filterNumber = 16

  val inputStream = InputMnist.Entry >>
    OnOffCell(OnOffKernelSize, OnOffKernelSize, OnOffFunctions.LaplacianOfGaussian(1.4f)) >>
    StreamReshapeSample[Float, InputSample3D[Float]](Seq(0, 0, 0), Seq(conv1KernelSize, conv1KernelSize, 2)) >>
    SampleToSpikeTrainConverter[Float, InputSample3D[Float]](0, 23, 150 MilliSecond, 350 MilliSecond) >>
    N2S3Entry

  val inputLayer = n2s3.createInput(inputStream)
  val dataFile = this.getClass.getResource("/train-images.idx3-ubyte").getFile
  val labelFile = this.getClass.getResource("/train-labels.idx1-ubyte").getFile
  inputStream.append(InputMnist.DataFrom(dataFile, labelFile).take(1))

  val layer = n2s3.createNeuronGroup()
    .setIdentifier("CONV1_"+conv1KernelSize)
    .setShape(filterNumber)
    .setNeuronModel(Izhikevich, Seq(
      (MembraneThresholdType, MembraneThresholdTypeEnum.Static),
      (MembranePotentialThreshold, 5 millivolts)
    ))

  inputLayer.connectTo(layer, new FullConnection(() => new StaticSynapse(1f)))

  n2s3.create()

  val inputToConv1Index = new ConnectionIndex(inputLayer, layer)

  WeightLoad.fromFile("emotion/3/MNIST_CONV1_"+conv1KernelSize+"_weights")
    .subshape(Shape(0, 0, 0), Shape(conv1KernelSize, conv1KernelSize, 2), Shape(0, 0, 0), Shape(filterNumber, 1, 1))
    .reshape(Shape(conv1KernelSize, conv1KernelSize, 2), Shape(filterNumber))
    .assignTo(inputLayer, layer)

  n2s3.addNetworkObserver(new SynapticWeightSelectAndMergeGraphRef(
    for (filter <- 0 until filterNumber) yield {
      for (x <- 0 until conv1KernelSize) yield {
        for (y <- 0 until conv1KernelSize) yield {
          for (cellType <- Seq(0, 1)) yield {
            val input = inputLayer.getNeuronPathAt(x, y, cellType)
            val output = layer.getNeuronPathAt(filter)
            inputToConv1Index.getConnectionsBetween(input, output).head
          }
        }
      }
    },
    SynapticWeightSelectAndMergeGraph.mergeOnOff,
    SynapticWeightSelectGraph.grayScale,
    normalize = true,
    8,
    10,
    "view_synapse_weights"
  ))

  val potentialLog = n2s3.addNetworkObserver(new PropertyLoggingRef[ElectricPotential](n2s3, NeuronPotentialUpdateEvent, layer.neuronPaths,  _ match {
    case NeuronPotentialResponse(t, s, v) => s -> (v millivolts)
  }))

  n2s3.runAndWait()

  layer.neuronPaths.foreach{ l =>
    Plot.propertyEvolution("Potential", potentialLog.get(l).map{case (t, v) => t -> v.toMillivolts})
  }


}
