package fr.univ_lille.cristal.emeraude.n2s3.apps

import fr.univ_lille.cristal.emeraude.n2s3.core.ConnectionIndex
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.MembranePotentialThreshold
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.types.FullConnection
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.{N2S3Simulation, NeuronGroupRef}
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.N2S3InputStreamCombinators._
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input._
import fr.univ_lille.cristal.emeraude.n2s3.features.io.report.BenchmarkMonitorRef
import fr.univ_lille.cristal.emeraude.n2s3.features.logging.graph.{SynapticWeightSelectGraph, SynapticWeightSelectGraphRef}
import fr.univ_lille.cristal.emeraude.n2s3.models.neurons.LIF
import fr.univ_lille.cristal.emeraude.n2s3.models.synapses.{InhibitorySynapse, QBGParameters, SimplifiedSTDP, SimplifiedSTDPWithNegative}
import fr.univ_lille.cristal.emeraude.n2s3.support.UnitCast._
import squants.electro.ElectricPotentialConversions._

/**
  * Recognition of Hexadecimal digit using synapses with positive and negative weight
  */
object ExampleDigitalHex extends N2S3Simulation {

  var inputStream: StreamSupport[InputSample2D[Float], N2S3InputPacket] = _
  var benchmarkMonitor: BenchmarkMonitorRef = _
  var unsupervisedLayer: NeuronGroupRef = _

  override def simulationPhases(): Unit = {
    this.train()
    this.test()
  }

  override def setUp(): Unit = {
    QBGParameters.alf_m = 0.005f
    QBGParameters.alf_p = 0.01f
    QBGParameters.beta_m = 3f
    QBGParameters.beta_p = 2f
    QBGParameters.stdpWindow = 50 MilliSecond

    val oversampling = 2
    inputStream =
      InputDigitalHex.Entry >>
        StreamOversampling(oversampling, oversampling) >>
        SampleToSpikeTrainConverter[Float, InputSample2D[Float]](0, 23, 350 MilliSecond, 150 MilliSecond) >> N2S3Entry

    val inputLayer = n2s3.createInput(inputStream)

    unsupervisedLayer = n2s3.createNeuronGroup("Layer1", 20)
      .setNeuronModel(LIF, Seq(
        (MembranePotentialThreshold, 2 * oversampling * oversampling millivolts)
      ))

    inputLayer.connectTo(unsupervisedLayer,
      (new FullConnection).setDefaultConnectionConstructor(() => new SimplifiedSTDPWithNegative))

    unsupervisedLayer.connectTo(unsupervisedLayer, new FullConnection(() => new InhibitorySynapse))

    n2s3.create()

    val inputToClassifierIndex = new ConnectionIndex(inputLayer, unsupervisedLayer)

    n2s3.addNetworkObserver(new SynapticWeightSelectGraphRef(
      for(outputIndex <- 0 until unsupervisedLayer.shape.getNumberOfPoints) yield {
        for(inputX <- 0 until inputLayer.shape.dimensions(0)) yield {
          for(inputY <- 0 until inputLayer.shape.dimensions(1)) yield {
            val input = inputLayer.getNeuronPathAt(inputX, inputY)
            val output = unsupervisedLayer.getNeuronPathAt(outputIndex)
            inputToClassifierIndex.getConnectionsBetween(input, output).head
          }
        }
      },
      SynapticWeightSelectGraph.heatMap,
      4,
      100)
    )
  }

  def prepareTraining(): Unit = {
    inputStream.append(InputDigitalHex.Data().repeat(1000).shuffle())
  }

  def prepareTesting(): Unit = {
    inputStream.append(InputDigitalHex.Data().repeat(10).shuffle())
    unsupervisedLayer.fixNeurons()
    inputStream.clean()
    benchmarkMonitor = n2s3.createBenchmarkMonitor(unsupervisedLayer)
  }

  override def tearDown(): Unit = {
    println(benchmarkMonitor)
    benchmarkMonitor.exportToHtmlView("test.html")
  }
}
