package fr.univ_lille.cristal.emeraude.n2s3.apps.tool

import java.io._

import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.{N2S3InputLabel, N2S3InputSpike, Shape}
import fr.univ_lille.cristal.emeraude.n2s3.features.io.persistence.ActivityPersistenceModel.EventFactory
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp

import scala.collection.mutable

/**
  * Created by pfalez on 17/05/17.
  */
object ActivityToFeature extends App {

  val filename = "emotion/5/test_MNIST_FULL_from_7_spikes_checked"
  val output = "emotion/5/test-features"

  def labelFunction(l : String) : String = l
  def eventFunction(events : Seq[Timestamp]) : Float = {
    events.size
  }

  var reader = new DataInputStream(new FileInputStream(filename))
  val writer = new PrintWriter(new FileOutputStream(output))
  val dimNumber : Int = reader.readInt()
  val s = Shape((0 until dimNumber).map(_ => reader.readInt()):_*)
  val totalEvent : Long = reader.readLong()

  var currentInput : Option[String] = None
  var currentEvents : mutable.HashMap[Long, mutable.ArrayBuffer[Timestamp]] = mutable.HashMap()

  println("header : "+s+" "+totalEvent)

  var i : Long = 0
  while(i < totalEvent) {
    val timestamp = reader.readLong()
    val metaDataSize = reader.readInt()
    val dataSize = reader.readInt()

    val labels = (0 until metaDataSize).map { _ =>
      EventFactory.createMetaData(reader, timestamp)
    }.filter(_.isInstanceOf[N2S3InputLabel]).map(_.asInstanceOf[N2S3InputLabel])

    assert(labels.size <= 1)

    if(labels.nonEmpty) {
      if (currentInput.isDefined) {
        writer.println(labelFunction(currentInput.get)+" "+(1 to s.getNumberOfPoints).map(i => i+":"+eventFunction(currentEvents.getOrElse(i.toLong, Seq()))).mkString(" "))
          // save
      }

      currentInput = Some(labels.head.getLabel)
      currentEvents.clear()
    }



    (0 until dataSize).foreach { _ =>
      EventFactory.createData(reader, timestamp) match {
        case (index, spike : N2S3InputSpike) =>
          currentEvents.getOrElseUpdate(index, mutable.ArrayBuffer[Timestamp]()) += spike.getStartTimestamp
      }
    }

    i += metaDataSize+dataSize
  }

  if (currentInput.isDefined) {
    writer.println(labelFunction(currentInput.get)+" "+(1 to s.getNumberOfPoints).map(i => i+":"+eventFunction(currentEvents.getOrElse(i.toLong, Seq()))).mkString(" "))
    // save
  }


  reader.close()
  writer.close()
}
