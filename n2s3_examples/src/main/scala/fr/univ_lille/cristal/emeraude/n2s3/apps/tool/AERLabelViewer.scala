package fr.univ_lille.cristal.emeraude.n2s3.apps.tool

import java.awt._
import java.awt.event.{ActionEvent, ActionListener, KeyEvent, KeyListener}
import java.io.{BufferedReader, FileReader, PrintWriter}
import javax.swing._

import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.AERInputStream
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp

import scala.collection.JavaConverters._
import scala.io.Source

/**
  * Created by falezp on 13/06/16.
  */

class AERLabelViewerView(aerFile : String, labelFile : String) extends JFrame {

  setLayout(new BorderLayout)

  val aerReader = new AERInputStream(aerFile)
  val labelReader = new BufferedReader(new FileReader(labelFile))
  val labelDisplayPanel = new LabelDisplayPanel(Source.fromFile(labelFile).getLines().filter(_ != "").map{line =>
    val fields = line.split("\t")
    (fields(1).toLong, fields(0))
  }.toSeq)

  val writer = new PrintWriter("label.out")

  val timeLabel = new JLabel("-- / --")
  val aerDisplayPanel = new AERDisplayPanel
  var currentTimestamp : Timestamp = 0


  add(timeLabel, BorderLayout.PAGE_START)
  add(aerDisplayPanel, BorderLayout.LINE_START)
  add(labelDisplayPanel, BorderLayout.LINE_END)

  setVisible(true)
  setSize(400, 400)
  setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE)

  val frame = this
  addKeyListener(new KeyListener {
    override def keyTyped(keyEvent: KeyEvent): Unit = {}

    override def keyPressed(keyEvent: KeyEvent): Unit = {
      if(keyEvent.getKeyCode ==	KeyEvent.VK_P) {
        println("play()")
        frame.play()
      }

      if(keyEvent.getKeyCode == KeyEvent.VK_A) {
        writer.println("0 "+currentTimestamp)
      }
      if(keyEvent.getKeyCode == KeyEvent.VK_Z) {
        writer.println("1 "+currentTimestamp)
      }
      if(keyEvent.getKeyCode == KeyEvent.VK_E) {
        writer.println("2 "+currentTimestamp)
      }
      if(keyEvent.getKeyCode == KeyEvent.VK_R) {
        writer.println("3 "+currentTimestamp)
      }
      if(keyEvent.getKeyCode == KeyEvent.VK_T) {
        writer.println("4 "+currentTimestamp)
      }
      if(keyEvent.getKeyCode == KeyEvent.VK_Y) {
        writer.println("5 "+currentTimestamp)
      }
      writer.flush()

    }

    override def keyReleased(keyEvent: KeyEvent): Unit = {}
  })

  val delay = 1000/24
  val taskPerformer = new ActionListener() {
    override def actionPerformed(actionEvent: ActionEvent): Unit = {
      SwingUtilities.invokeLater(new Runnable() {
        def run() {
          frame.repaint()
        }
      })
    }
  }
  new Timer(delay, taskPerformer).start()

  def addressToCoord(address : Int) : (Int, Int, Boolean) = {
    (127-((address >> 1) & 0x7F), 127-((address >> 8) & 0x7F), (address & 0x1) == 1)
  }

  def setTimer(timestamp: Timestamp) = {
    val us = timestamp%1000
    val ms = (timestamp/1000)%1000
    val s = timestamp/1000000
    timeLabel.setText(s+"s : "+ms+" ms : "+us+" us")
  }

  def play(): Unit = {
    new Thread() {

      override def run() {
        while (!aerReader.atEnd()) {

          val event = aerReader.next()
          /*setTimer(event.timestamp)
          val coord = addressToCoord(event.destinationIndex)
          currentTimestamp = event.timestamp
          aerDisplayPanel.next(coord._1, coord._2, coord._3, currentTimestamp)
          labelDisplayPanel.setTimestamp(currentTimestamp)*/
        }
        writer.close()
        println("end()")
      }
    }.start()
  }

}

class AERDisplayPanel extends JPanel {
  val aerWidth = 128
  val aerHeight = 128
  val caseSize = 4
  val integrationTime = 100000

  val dim = new Dimension(aerWidth*caseSize, aerHeight*caseSize)
  setPreferredSize(dim)
  setSize(dim)

  val values = Array.ofDim[Int](aerWidth, aerHeight)
  val timestamps = Array.ofDim[Timestamp](aerWidth, aerHeight)
  var currentTimestamp : Timestamp = 0

  override def paintComponent(g : Graphics) : Unit = {
    for(x <- 0 until aerWidth) {
      for(y <- 0 until aerHeight) {
        val v = values(x)(y)
        g.setColor(new Color(v + (v << 8) + (v << 16)))
        g.fillRect(x*caseSize, y*caseSize, caseSize, caseSize)
      }
    }
  }

  def next(x : Int, y : Int, sign : Boolean, t : Timestamp) : Unit = {
    timestamps(x)(y) = t
    values(x)(y) = if(sign) 255 else 0
    if(t != currentTimestamp) {
      for(i <- 0 until aerWidth) {
        for(j <- 0 until aerHeight) {
          values(i)(j) = 128+ math.max(0, 128-((t-timestamps(i)(j)).toFloat/integrationTime.toFloat*128f).toInt)*(if(values(i)(j) >= 128) 1 else -1)
        }
      }
      currentTimestamp = t
    }
  }

  def init(): Unit = {
    for(i <- 0 until aerWidth) {
      for(j <- 0 until aerHeight) {
        timestamps(i)(j) = Long.MinValue
        values(i)(j) = 128
      }
    }
  }
}

class LabelDisplayPanel(spikes : Seq[(Timestamp, String)]) extends JPanel {

  val maxTime : Timestamp = 10000000
/*
  val chart : Chart = new Chart(600, 400)
  chart.getStyleManager.setChartType(ChartType.Scatter)
  chart.getStyleManager.setXAxisMin(0f)
  chart.getStyleManager.setXAxisMax(toSecond(maxTime))

  val innerPanel = new XChartPanel(chart)
  add(innerPanel)

  spikes.groupBy(_._2).keys.zipWithIndex.foreach{ case(n, i) => chart.addSeries(n, Array(toSecond(maxTime)), Array(i.toDouble))}
*/
  def setTimestamp(timestamp: Timestamp) : Unit = {
    spikes.groupBy(_._2).zipWithIndex.foreach { case ((n, list), i) =>
      val times = list.filter(e => e._1 >=  timestamp && e._1 <=  timestamp+maxTime).map(e => toSecond(e._1-timestamp)).toList
      //innerPanel.updateSeries(n, times.asJava, times.map(_ => new java.lang.Double(i)).asJava)
    }
  }

  def toSecond(timestamp: Timestamp) = timestamp.toDouble/1e6
}

object AERLabelViewer extends App {
  new AERLabelViewerView("data/aerdata/freeway.mat.dat", "freeway_result")
}
