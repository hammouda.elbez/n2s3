package fr.univ_lille.cristal.emeraude.n2s3.apps

import java.io.{File, PrintWriter}

import akka.actor.Props
import akka.pattern.ask
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.{Config, Done, ElectricSpike}
import fr.univ_lille.cristal.emeraude.n2s3.core.event.{NeuronFireEvent, NeuronPotentialUpdateEvent, Subscribe}
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.{MembranePotentialThreshold, MembraneThresholdFloat, MembraneThresholdType, MembraneThresholdTypeEnum}
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.N2S3
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.types.FullConnection
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input._
import fr.univ_lille.cristal.emeraude.n2s3.features.logging.{NeuronsFireLogText, NeuronsPotentialLogText}
import fr.univ_lille.cristal.emeraude.n2s3.models.neurons.SRM
import fr.univ_lille.cristal.emeraude.n2s3.models.synapses.{InhibitorySynapse, StandardSTDP}
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.UnitCast._
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.LocalActorDeploymentStrategy
import fr.univ_lille.cristal.emeraude.n2s3.support.{InputDistribution, Time}
import squants.electro.ElectricPotentialConversions.ElectricPotentialConversions

import scala.concurrent.Await
import scala.util.Random

class SpikePatternGenerator(entryNumber : Int, patternDuration : Time, spikeFrequency : Float, patternFrequency : Float) extends InputGenerator[InputTemporalPacket] {

  private val pattern = (0 until entryNumber/2).map { _ =>
    InputDistribution.poisson(Random, 0, patternDuration.timestamp, patternDuration.asSecond*spikeFrequency)
  }

  var spikes = Seq[(Timestamp, Int, Boolean)]()
  var patternStarts = Seq[Timestamp]()
  var duration: Timestamp = 0


  var cursor : Int = 0

  var currentSecond : Int = 0

  def generateSequence(duration : Time): Unit = {
    reset()

    this.duration = duration.timestamp
    patternStarts = InputDistribution.poisson(Random, 0, duration.timestamp-patternDuration.timestamp, patternFrequency)

    patternStarts = for(i <- patternStarts.indices.drop(1) if patternStarts(i) > patternStarts(i-1)+patternDuration.timestamp) yield {
      patternStarts(i)
    }


    spikes = (0 until entryNumber).flatMap { i =>
      InputDistribution.poisson(Random, 0, duration.timestamp, duration.asSecond * spikeFrequency).filter { t =>
        !patternStarts.exists(s => t >= s && t < s+patternDuration.timestamp && i < entryNumber/2)
      }.map(t => (t, i, false))++(if(i < entryNumber/2) patternStarts.flatMap { start =>
        pattern(i).map(t => (t +start, i, true))
      } else Nil)
    }.sortBy(_._1)

    println("avg : "+(spikes.size.toFloat/entryNumber.toFloat)/duration.asSecond)
  }

  def save(filename : String) = {
    /*
    val writer = new PrintWriter(new File(filename))
    spikes.foreach { case(t, i, b) => writer.println(i+" "+t+" "+(if(b) 1 else 0))}
    writer.close()
*/
    val writer = new AERFileWriter(filename)
    spikes.foreach { case(t, i, b) =>
      writer.appendEvent(i.toShort, t.toInt)
    }
    println(spikes.size+" events saved")
    writer.close()
  }

  def saveLabel(filename : String) = {
    val writer = new PrintWriter(new File(filename))
    patternStarts.foreach { t =>
      writer.println("set obj rect from "+t+", graph 0 to "+(t+patternDuration.timestamp)+", graph 1")
    }
    writer.close()
  }

  override def shape: Shape = Shape(entryNumber)

  override def next(): InputTemporalPacket = {
    val chunkSize = 64

    if((spikes(cursor)._1/1000000L).toInt != currentSecond) {
      currentSecond = (spikes(cursor)._1/1000000L).toInt
      println("[Spike Train] "+currentSecond+"s")
    }

    val list = Map(
      (for(i <- 0 until chunkSize if cursor+i < spikes.size) yield {
          val s = spikes(cursor+i)
        (Seq(s._2), Seq(N2S3InputSpike(ElectricSpike(), s._1)))
      }):_*
    )
    cursor += chunkSize

    new InputTemporalPacket(shape, list)
  }

  override def atEnd(): Boolean = cursor >= spikes.size

  override def reset(): Unit = {
    cursor = 0
  }
}

/**
  * Experience reproduced from:
  * Spike timing dependent plasticity finds the start of repeating patterns in continuous spike trainsty
  * T Masquelier, R Guyonneau, SJ Thorpe
  */

object ExamplePatternDetector extends App {

  val nbInput = 2000

  implicit val timeout = Config.defaultTimeout

  val data = new SpikePatternGenerator(nbInput, 50 MilliSecond, 64f, 10f)

  data.generateSequence(10 Second)
  data.save("test_aer")

  inputStream.append(InputAER.DataFromFile("test_aer"))

  val n2s3 = new N2S3()

  val inputStream = InputAER.Entry >> InputAER.Cochlea(nbInput) >> N2S3Entry
  val inputLayer = n2s3.createInput(inputStream)
/*
  data.generateSequence(100 Second)
  data.save("spike_train")
  inputStream.append(data)
*/
  val unsupervisedLayer = n2s3.createNeuronGroup()
    .setIdentifier("Output_Layer")
    .setNumberOfNeurons(1)
    .setNeuronModel(SRM, Seq(
      (MembraneThresholdType, MembraneThresholdTypeEnum.Static),
      (MembranePotentialThreshold, 300 millivolts)
    ))

  inputLayer.connectTo(unsupervisedLayer, new FullConnection(() => new StandardSTDP(0.475f)))
  unsupervisedLayer.connectTo(unsupervisedLayer, new FullConnection(() => new InhibitorySynapse))
  n2s3.createSynapseWeightGraphOn(inputLayer, unsupervisedLayer).setWidth(100).setCaseDimension(16, 16)
/*
  println("Start Training ...")
  n2s3.runAndWait()

  println("Start Testing ...")
  unsupervisedLayer.neurons.foreach(neuron => ExternalSender.sendTo(neuron, SetProperty(bio.FixedParameter, true)))
*/

  n2s3.runAndWait()

  //  TODO use new loggers
  /*
  val fireLogger = n2s3.system.actorOf(Props(new NeuronsPotentialLogText("out", GlobalStream.prefix)), LocalActorDeploymentStrategy)
  val logger = n2s3.system.actorOf(Props(new NeuronsFireLogText("fire.out"/*, GlobalStream.prefix*/)), LocalActorDeploymentStrategy)

  unsupervisedLayer.neuronPaths.foreach{ n =>
    ExternalSender.askTo(n, Subscribe(NeuronPotentialUpdateEvent, ExternalSender.getReference(fireLogger)))
    ExternalSender.askTo(n, Subscribe(NeuronFireEvent, ExternalSender.getReference(logger)))
  }*/

  inputStream.append(InputAER.DataFromFile("test_aer"))

  n2s3.runAndWait()

  //Await.result(fireLogger ? Done, timeout.duration)
  //Await.result(logger ? Done, timeout.duration)

  println("end")
  System.exit(0)
/*
  val benchmarkMonitor = n2s3.createBenchmarkMonitor(unsupervisedLayer)

  n2s3.runAndWait()
  println(benchmarkMonitor.getResult)
  benchmarkMonitor.exportToHtmlView("test.html")*/
}
