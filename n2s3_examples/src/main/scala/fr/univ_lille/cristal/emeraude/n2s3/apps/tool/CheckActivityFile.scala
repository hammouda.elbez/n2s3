package fr.univ_lille.cristal.emeraude.n2s3.apps.tool

import java.io._

import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.{N2S3InputLabel, Shape}
import fr.univ_lille.cristal.emeraude.n2s3.features.io.persistence.ActivityPersistenceModel.{EventFactory, LabelObj, SpikeObj}

/**
  * Created by pfalez on 17/05/17.
  */
object CheckActivityFile extends App {

  val filename = "emotion/5/test_MNIST_FULL_from_7_spikes"

  var reader = new DataInputStream(new FileInputStream(filename))
  val dimNumber : Int = reader.readInt()
  val s = Shape((0 until dimNumber).map(_ => reader.readInt()):_*)
  val totalEvent : Long = reader.readLong()

  println("header : "+s+" "+totalEvent)

  var countTimestamp : Long = 0
  var countEvent : Long = 0
  var countLabel : Long = 0
  var hasNext : Boolean = true
  while(hasNext) {
    try {

      val timestamp = reader.readLong()
      val metaDataSize = reader.readInt()
      val dataSize = reader.readInt()

      countLabel += (0 until metaDataSize).map { _ =>
        EventFactory.createMetaData(reader, timestamp)
      }.count(_.isInstanceOf[N2S3InputLabel])

      (0 until dataSize).map { _ =>
        EventFactory.createData(reader, timestamp)
      }

      countTimestamp += 1
      countEvent += metaDataSize+dataSize
    }
    catch {
      case _ : IOException =>
        hasNext = false
    }
  }

  reader.close()
  reader = new DataInputStream(new FileInputStream(filename))

  println("timestamps : "+countTimestamp+", events : "+countEvent+", labels : "+countLabel)

  val outFile = new FileOutputStream(filename+"_checked")
  val writer = new DataOutputStream(outFile)

  writer.writeInt(reader.readInt())
  (0 until dimNumber).foreach(_ => writer.writeInt(reader.readInt()))
  writer.writeLong(countEvent)
  reader.readLong()

  for(i <- 0L until countTimestamp) {

    //println(i+"/"+countTimestamp)

    val timestamp = reader.readLong()
    writer.writeLong(timestamp)

    val metaDataSize = reader.readInt()
    writer.writeInt(metaDataSize)

    val dataSize = reader.readInt()
    writer.writeInt(dataSize)

    (0 until metaDataSize).foreach { _ =>
      EventFactory.createMetaData(reader, timestamp)  match {
        case label : N2S3InputLabel =>
          LabelObj(label.getEndTimestamp, label.getLabel).writeTo(writer)
      }
    }
    (0 until dataSize).foreach { _ =>
      EventFactory.createData(reader, timestamp) match {
        case (index, _) =>
          SpikeObj(index).writeTo(writer)
      }
    }
  }

  reader.close()
  writer.close()

}
