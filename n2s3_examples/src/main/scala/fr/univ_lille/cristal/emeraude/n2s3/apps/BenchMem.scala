package fr.univ_lille.cristal.emeraude.n2s3.apps

import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties._
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.N2S3
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.types.FullConnection
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input._
import fr.univ_lille.cristal.emeraude.n2s3.models.neurons.LIF
import fr.univ_lille.cristal.emeraude.n2s3.models.synapses.InhibitorySynapse
import fr.univ_lille.cristal.emeraude.n2s3.support.UnitCast._
import squants.electro.ElectricPotentialConversions.ElectricPotentialConversions

/**
  * Created by falezp on 02/03/17.
  */
object BenchMem extends App {

  val nNeurons = args(0).toInt
  val nConnection = args(1).toInt

  val n2s3 = new N2S3()
  val inputLayer = n2s3.createInput(new StreamEntry[N2S3InputPacket](Shape(1)))


  val layer1 = n2s3.createNeuronGroup()
    .setIdentifier("Layer1")
    .setNumberOfNeurons(nNeurons)
    .setNeuronModel(LIF,  Seq(
      (MembraneThresholdType, MembraneThresholdTypeEnum.Static),
      (MembranePotentialThreshold, 1060 volts),
      (MembraneRefractoryDuration, 517 MilliSecond),
      (MembraneInhibitionDuration, 10.2 MilliSecond),
      (MembraneLeakTime, 187 MilliSecond)))

  val layer2 = n2s3.createNeuronGroup()
    .setIdentifier("Layer2")
    .setNumberOfNeurons(nNeurons)
    .setNeuronModel(LIF,  Seq(
      (MembraneThresholdType, MembraneThresholdTypeEnum.Static),
      (MembranePotentialThreshold, 2.24 volts),
      (MembraneRefractoryDuration, 470 MilliSecond),
      (MembraneInhibitionDuration, 182 MilliSecond),
      (MembraneLeakTime, 477 MilliSecond)))

  for (i <- 1.to(nConnection)){
    layer1.connectTo(layer2, new FullConnection(() => new InhibitorySynapse))
  }

  n2s3.create()
  n2s3.destroy()
}