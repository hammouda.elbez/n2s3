package fr.univ_lille.cristal.emeraude.n2s3.apps

import java.awt.image.BufferedImage
import java.io.File
import java.text.SimpleDateFormat
import java.util.Date

import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties._
import fr.univ_lille.cristal.emeraude.n2s3.core.{ConnectionIndex, ConnectionPropertyValues, ExternalSender, GetAllConnectionProperty}
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.types.FullConnection
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.{N2S3, NeuronGroupRef,InputNeuronGroupRef}
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input._
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.N2S3InputStreamCombinators._
import fr.univ_lille.cristal.emeraude.n2s3.features.io.report.BenchmarkMonitorRef
import fr.univ_lille.cristal.emeraude.n2s3.features.logging.SimMongoLog
import fr.univ_lille.cristal.emeraude.n2s3.models.neurons.LIF
import fr.univ_lille.cristal.emeraude.n2s3.models.synapses.{InhibitorySynapse, QBGParameters, SimplifiedSTDP}
import fr.univ_lille.cristal.emeraude.n2s3.support.N2S3ResourceManager
import fr.univ_lille.cristal.emeraude.n2s3.support.UnitCast._
import javax.imageio.ImageIO
import squants.electro.ElectricPotentialConversions.ElectricPotentialConversions
import fr.univ_lille.cristal.emeraude.n2s3.core.ConnectionPath

/**
  * Experience reproduced from:
  * Extraction of temporally correlated features from dynamic vision sensors with spike-timing-dependent plasticity
  * O Bichler, D Querlioz, SJ Thorpe, JP Bourgoin, C Gamrat, Neural Networks 32
  */

object ExampleFreeWayLogged extends App {

  val n2s3 = new N2S3()
  var SynapseIndex : List[(Integer, Integer, Integer)] = List.empty
  var lastTimestamp: Long = 0

  QBGParameters.alf_m = 0.05f
  QBGParameters.alf_p = 0.1f
  QBGParameters.beta_m = 0f
  QBGParameters.beta_p = 0f

  QBGParameters.init_mean = 0.8f
  QBGParameters.init_std = 0.16f

  val dataFile =  N2S3ResourceManager.getByName("freeway-dvs").getAbsolutePath
  val retinaStream =  InputAER.Entry >> InputAER.Retina(128, 128, separateSign = false) >> N2S3Entry
  val inputLayer = n2s3.createInput(retinaStream)

  var logger: SimMongoLog = null
  var SimName = "Freeway"
  var globalTime: Long = 0L
  var folderName = ""
  val formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss")
  var SimTime = formatter.format(new Date())
  var benchmarkMonitor: BenchmarkMonitorRef = null
  SynapseIndex = List.empty

  val layer1 = n2s3.createNeuronGroup()
    .setIdentifier("Layer1")
    .setNumberOfNeurons(60)
    .setNeuronModel(LIF,  Seq(
      (MembraneThresholdType, MembraneThresholdTypeEnum.Static),
      (MembranePotentialThreshold, 1060 millivolts),
      (MembraneRefractoryDuration, 517 MilliSecond),
      (MembraneInhibitionDuration, 10.2 MilliSecond),
      (MembraneLeakTime, 187 MilliSecond)))

  val layer2 = n2s3.createNeuronGroup()
    .setIdentifier("Layer2")
    .setNumberOfNeurons(10)
    .setNeuronModel(LIF,  Seq(
      (MembraneThresholdType, MembraneThresholdTypeEnum.Static),
      (MembranePotentialThreshold, 2.24 millivolts),
      (MembraneRefractoryDuration, 470 MilliSecond),
      (MembraneInhibitionDuration, 182 MilliSecond),
      (MembraneLeakTime, 477 MilliSecond)))

  inputLayer.connectTo(layer1, new FullConnection(() => new SimplifiedSTDP(14.7 MilliSecond)))
  val Layer1WTAconnection = layer1.connectTo(layer1, new FullConnection(() => new InhibitorySynapse))
  layer1.connectTo(layer2, new FullConnection(() => new SimplifiedSTDP(46.5 MilliSecond)))
  val Layer2WTAconnection = layer2.connectTo(layer2, new FullConnection(() => new InhibitorySynapse))

  n2s3.create()

  val inputToLayer1Index = new ConnectionIndex(inputLayer, layer1)
    folderName = SimName.split("-")(0) + SimTime


  val listOfConnexionsinputToLayer1 = for(outputIndex <- 0 until layer1.shape.getNumberOfPoints) yield {
    for(inputX <- 0 until 128) yield {
      for(inputY <- 0 until 128) yield {
        var input = inputLayer.getNeuronPathAt(inputX, inputY)
        var output = layer1.getNeuronPathAt(outputIndex)
        inputToLayer1Index.getConnectionsBetween(input, output).head
      }
    }
  }

  SynapsesIndex(listOfConnexionsinputToLayer1)

  // Start logger
  globalTime = System.currentTimeMillis()
  logger = new SimMongoLog(n2s3, listOfConnexionsinputToLayer1, SimName.split("-")(0), true, true, false, true, "FreeWay", SimTime,SynapseIndex, globalTime)

  println("### Training First Layer ###")
  retinaStream.append(InputAER.DataFromFile(dataFile).repeat(1))//.repeat(8)
  n2s3.runAndWait()

  logger.storeSimInfoAndDestroy()

  println("### Training Second Layer ###")

  layer1.fixNeurons()
  Layer1WTAconnection.disconnect()
  
  /*
    n2s3.createSynapseWeightGraphOn(layer1, layer2)
      .setCaseDimension(1, 1)
  */

  val layer1ToLayer2Index = new ConnectionIndex(inputLayer, layer2)

  val listOfConnexionsinputToLayer2 = for(outputIndex <- 0 until layer2.shape.getNumberOfPoints) yield {
    for(layer1Index <- 0 until layer1.shape.getNumberOfPoints) yield {
      for(i <- 0 to 0) yield {
        var input = layer1.getNeuronPathAt(layer1Index)
        var output = layer2.getNeuronPathAt(outputIndex)

        layer1ToLayer2Index.getConnectionsBetween(input, output).head
      }
    }
  }
  SynapsesIndex(listOfConnexionsinputToLayer2)

  logger = new SimMongoLog(n2s3, listOfConnexionsinputToLayer2, SimName.split("-")(0), true, true, false, true, "FreeWay", SimTime,SynapseIndex, globalTime)

  retinaStream.append(InputAER.DataFromFile(dataFile).repeat(1)) //.repeat(8)
  n2s3.runAndWait()

  logger.storeSimInfoAndDestroy()
  //println("### Testing ###")

  //Layer2WTAconnection.disconnect()
  //layer2.fixNeurons()

  //inputLayer.setInput(FreeWay.input("data/aerdata/freeway.mat.dat"))
  //retinaStream.append(InputAER.DataFromFile(dataFile))
  //benchmarkMonitor = n2s3.createBenchmarkMonitor(layer2)

  //logger.storeBenchmarkTestInfo(benchmarkMonitor)
  /*
  val logs = n2s3.system.actorOf(Props(new NeuronsFireLogText("results/freeway_result")), LocalActorDeploymentStrategy)

  for(neuron <- layer2.neurons){
    neuron.send(Subscribe(NeuronFireEvent, ExternalSender.getReference(logs)))
  }
  */
  //n2s3.runAndWait()
  n2s3.destroy()

  def SynapsesIndex(listOfConnexions : IndexedSeq[IndexedSeq[IndexedSeq[ConnectionPath]]]): Unit = {
    var x = -1
    var y = 0
    SynapseIndex = List[(Integer,Integer,Integer)]()

    listOfConnexions.head.foreach(
      cnx => {
        cnx.toList.foreach(c => {
          x+=1
        SynapseIndex = SynapseIndex.::(c.connectionID,x,y)
        })
        y+=1
        x = -1
        }
    )

    SynapseIndex = SynapseIndex.reverse
  }
  }