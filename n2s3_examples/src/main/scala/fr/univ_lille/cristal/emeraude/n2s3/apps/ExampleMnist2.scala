package fr.univ_lille.cristal.emeraude.n2s3.apps

import java.awt.image.BufferedImage
import java.io.File
import java.text.SimpleDateFormat
import java.util.Date

import akka.actor.{ActorRef, Props}
import fr.univ_lille.cristal.emeraude.n2s3.core
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.Config
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.{AdaptiveThresholdAdd, MembranePotentialThreshold, SynapticWeightFloat}
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.types.FullConnection
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.{InputNeuronGroupRef, N2S3, NeuronGroupObserverRef, NeuronGroupRef}
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input._
import fr.univ_lille.cristal.emeraude.n2s3.features.io.report.BenchmarkMonitorRef
import fr.univ_lille.cristal.emeraude.n2s3.features.logging.graph.{SynapticWeightSelectGraph, SynapticWeightSelectGraphRef}
import fr.univ_lille.cristal.emeraude.n2s3.features.logging.{NetworkLoading, NetworkSaving, SimMongoLog, SynapticWeight}
import fr.univ_lille.cristal.emeraude.n2s3.models.neurons.LIF
import fr.univ_lille.cristal.emeraude.n2s3.models.synapses.{InhibitorySynapse, QBGParameters, SimplifiedSTDP}
import fr.univ_lille.cristal.emeraude.n2s3.support.N2S3ResourceManager
import fr.univ_lille.cristal.emeraude.n2s3.support.UnitCast._
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.{LocalActorDeploymentStrategy, Message}
import javax.imageio.ImageIO
import javax.swing.JPanel
import org.mongodb.scala.{Completed, Document, MongoCollection, Observer}
import squants.electro.ElectricPotential
import squants.electro.ElectricPotentialConversions.ElectricPotentialConversions

import scala.collection.immutable

/**
  * Experience reproduced from:
  * Simulation of a memristor-based spiking neural network immune to device variations
  * D Querlioz, O Bichler, C Gamrat, Neural Networks (IJCNN), The 2011 International Joint Conference on
  *
  * Run test on big networks and test only at the end of training (No prune, No trace log)
  * */



object  ExampleMnist2 extends App {

  var n2s3 : N2S3 = _
  var SynapseIndex : List[(Integer, Integer, Integer)] = List.empty
  var folderName = ""
  var croppedCnx = List[ConnectionPath]()
  var alpha = 0.1F
  var ArrAlpha = Array(0.01F,0.05F,0.1F,0.15F,0.2F)
  var ArrBeta = Array(0.01F,0.1F,0.15F,0.2F,0.5F)
  var beta = 0.2F
  var CTotal = 0F
  var CRmaining = 0F
  var Ratio = 0F
  var inputStream = InputMnist.Entry >> SampleToSpikeTrainConverter[Float, InputSample2D[Float]](0, 22, 150 MilliSecond, 350 MilliSecond) >> N2S3Entry
  var SimName = ""
  var SimTime = ""
  var NeuroneLabels = List[(Int, String)]()
  var logger: SimMongoLog = null
  var log = true
  var prune = false
  var globalIteration = 1
  var sizeChunk = 60000

  implicit val timeout = Config.longTimeout
  alpha = 0.05F
  beta = 0.1F
  SimName = "20N" 
  println("a:" + alpha + " b:" + beta)
  QBGParameters.alf_m = 0.005f
  QBGParameters.alf_p = 0.01f
  QBGParameters.beta_m = 2f
  QBGParameters.beta_p = 2f
  val formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss")
  var iteration = 0
  var CropInfo = ""
  var globalTime: Long = 0L
  var simTime = ""
  var benchmarkMonitor: BenchmarkMonitorRef = null
  n2s3 = new N2S3("N2S3")

  /** Vars of Threshold  **/

  CTotal = 0F
  CRmaining = 0F
  Ratio = 0F

  /** **/

  folderName = ""
  SynapseIndex = List.empty
  croppedCnx = List.empty
  NeuroneLabels = List.empty

  //val inputLayer = n2s3.createInput(Mnist.distributedInput("data/train2k-images.idx3-ubyte", "data/train2k-labels.idx1-ubyte"))
  val inputLayer = n2s3.createInput(inputStream)
  val dataFile = N2S3ResourceManager.getByName("mnist-train-images").getAbsolutePath
  val labelFile = N2S3ResourceManager.getByName("mnist-train-labels").getAbsolutePath
  var stream: MnistFileInputStreamP = null
  //inputStream.append(stream)

  val unsupervisedLayer1 = n2s3.createNeuronGroup()
    .setIdentifier("Layer1")
    .setNumberOfNeurons(50)
    .setNeuronModel(LIF, Seq(
      (MembranePotentialThreshold, 15 millivolts)))
  
  val unsupervisedLayer2 = n2s3.createNeuronGroup()
    .setIdentifier("Layer2")
    .setNumberOfNeurons(20)
    .setNeuronModel(LIF, Seq(
      (MembranePotentialThreshold, 5 millivolts)))

  inputLayer.connectTo(unsupervisedLayer1, new FullConnection(() => new SimplifiedSTDP()))
  var Layer1WTAconnection = unsupervisedLayer1.connectTo(unsupervisedLayer1, new FullConnection(() => new InhibitorySynapse()))
  unsupervisedLayer1.connectTo(unsupervisedLayer2, new FullConnection(() => new SimplifiedSTDP()))
  var Layer2WTAconnection = unsupervisedLayer2.connectTo(unsupervisedLayer2, new FullConnection(() => new InhibitorySynapse()))

  n2s3.create()

  iteration = 1
  inputLayer.neurons.toList.foreach(n => {
    CTotal += n.group.connections.length
  })
  CTotal = CTotal * unsupervisedLayer1.neurons.size
  CRmaining = CTotal

  //showHeatMap(inputLayer,unsupervisedLayer1)
  SynapsesIndexInput(inputLayer, unsupervisedLayer1)
  SimTime = formatter.format(new Date())
  folderName = SimName.split("-")(0) + SimTime
  new File(folderName).mkdir()


  var inputToClassifierIndex = new ConnectionIndex(inputLayer, unsupervisedLayer1)
  var listOfConnexions = for (outputIndex <- 0 until unsupervisedLayer1.shape.getNumberOfPoints) yield {
    for (inputX <- 0 until InputMnist.shape.dimensions(0)) yield {
      for {
        inputY <- 0 until InputMnist.shape.dimensions(1)
        input = inputLayer.getNeuronPathAt(inputX, inputY)
        output = unsupervisedLayer1.getNeuronPathAt(outputIndex)
        if inputToClassifierIndex.getConnectionsBetween(input, output).nonEmpty
      } yield inputToClassifierIndex.getConnectionsBetween(input, output).head
    }
  }

  val dataTestFile = N2S3ResourceManager.getByName("mnist-test-images").getAbsolutePath
  val labelTestFile = N2S3ResourceManager.getByName("mnist-test-labels").getAbsolutePath


    simTime = ""
    // L1
    unsupervisedLayer1.unfixNeurons()
    stream = InputMnist.DataParts(dataFile, labelFile, sizeChunk, sizeChunk)
    inputStream.append(stream)

    println("Start Training L1")

    globalTime = System.currentTimeMillis()
    if (log) {
      logger = new SimMongoLog(n2s3, listOfConnexions, SimName.split("-")(0), true, true, true, true, "MNIST", SimTime, SynapseIndex,globalTime)
    }
    simTime = "Train L1"
    n2s3.runAndWait()

    simTime = simTime + " | " + (System.currentTimeMillis() - globalTime) + "\n"

    if (log) {
      logger.storeSimInfoAndDestroy()
    }

    // L2
    unsupervisedLayer1.fixNeurons()
    Layer1WTAconnection.disconnect()

    n2s3.first = false
    stream = InputMnist.DataParts(dataFile, labelFile, sizeChunk, sizeChunk)
    inputStream.append(stream)

    println("Start Training L2")
    
    var layerTolayerIndex = new ConnectionIndex(unsupervisedLayer1, unsupervisedLayer2)

    var listOfConnexions2 = for (outputIndex <- 0 until unsupervisedLayer2.shape.getNumberOfPoints) yield {
      for (_ <- 0 until 1) yield {
      for {
          inputIndex <- 0 until unsupervisedLayer1.shape.getNumberOfPoints
          input = unsupervisedLayer1.getNeuronPathAt(inputIndex)
          output = unsupervisedLayer2.getNeuronPathAt(outputIndex)
          if layerTolayerIndex.getConnectionsBetween(input, output).nonEmpty
        } yield layerTolayerIndex.getConnectionsBetween(input, output).head
    }}
    
    SynapsesIndex(unsupervisedLayer1, unsupervisedLayer2)
    
    if (log) {
      logger = new SimMongoLog(n2s3, listOfConnexions2, SimName.split("-")(0), true, true, true, true, "MNIST", SimTime, SynapseIndex, globalTime)
    }
    simTime = "Train L2"
    n2s3.runAndWait()

    if (log) {
      logger.storeSimInfoAndDestroy()
    }
    println(System.currentTimeMillis() - globalTime)
    simTime = simTime + " | " + (System.currentTimeMillis() - globalTime) + "\n"

    if (prune) {
      Ratio = Ratio + (alpha * (CRmaining / CTotal))
      println("Ratio " + Ratio)
      println("cropping : " + System.currentTimeMillis())
      CropInfo = cropNetwork(inputLayer, Ratio)
      println("end : " + System.currentTimeMillis())
      //     cropInput(n2s3.inputLayerRef.get, Ratio)
    }

    // Testing
    inputStream.append(InputMnist.DataFrom(dataTestFile, labelTestFile))

    println("Start Testing cropped " + "[" + iteration + "]" + " with " + Ratio)

    benchmarkMonitor = n2s3.createBenchmarkMonitor(unsupervisedLayer2)

    simTime = simTime + "Test cropped " + iteration

    n2s3.runAndWait()

    println(System.currentTimeMillis() - globalTime)

    simTime = simTime + " | " + (System.currentTimeMillis() - globalTime) + "\n"

    benchmarkMonitor.exportToHtmlView(folderName + "/" + "test cropped " + SimName + " [" + iteration + "]" + ".html")

    if (log) {
      logger.storeBenchmarkTestInfo(benchmarkMonitor)
    }

  n2s3.destroy()
  sys.exit(0)

  def showHeatMap(inputL: InputNeuronGroupRef, layer : NeuronGroupRef): Unit ={
    val inputToClassifierIndex = new ConnectionIndex(inputL, layer)

    var listOfConnexions =  for(outputIndex <- 0 until layer.shape.getNumberOfPoints) yield {
      for(inputX <- 0 until InputMnist.shape.dimensions(0)) yield {
        for(inputY <- 0 until InputMnist.shape.dimensions(1)) yield {
          val input = inputL.getNeuronPathAt(inputX, inputY)
          val output = layer.getNeuronPathAt(outputIndex)
          inputToClassifierIndex.getConnectionsBetween(input, output).head
        }}}

    n2s3.addNetworkObserver(new SynapticWeightSelectGraphRef(
      listOfConnexions,
      SynapticWeightSelectGraph.heatMap,
      4,
      100)
    )
  }

  def showNeurones(n2s3 : N2S3): Unit = {
    var x = 0
  n2s3.layers.foreach(l =>{
    l.neurons.foreach(n =>{
        x = 0
      n.group.connections.foreach(cnx =>{
        cnx.connections.list.foreach(c =>{
          if(c.connectionID >= 784){
              x+=1
          }else{
            if(c.connectionID == n.getIdentifier){
              x+=1
            }
          }
        })
      })
    })
  })
  }

  def SynapsesIndexInput(inputL: InputNeuronGroupRef,layer : NeuronGroupRef): Unit = {
    val inputToClassifierIndex = new ConnectionIndex(inputL, layer)

    var listOfConnexions =  for(outputIndex <- 0 until layer.shape.getNumberOfPoints) yield {
      for(inputX <- 0 until InputMnist.shape.dimensions(0)) yield {
        for(inputY <- 0 until InputMnist.shape.dimensions(1)) yield {
          val input = inputL.getNeuronPathAt(inputX, inputY)
          val output = layer.getNeuronPathAt(outputIndex)
          inputToClassifierIndex.getConnectionsBetween(input, output).head
        }}}

    var x = -1
    var y = 0
    SynapseIndex = List[(Integer,Integer,Integer)]()

    listOfConnexions.head.foreach(
      cnx => {
        cnx.toList.foreach(c => {
          x+=1
        SynapseIndex = SynapseIndex.::(c.connectionID,x,y)
        })
        y+=1
        x = -1
        }
    )

    SynapseIndex = SynapseIndex.reverse
  }

  def SynapsesIndex(layer1: NeuronGroupRef,layer2 : NeuronGroupRef): Unit = {
    val layerTolayerIndex = new ConnectionIndex(layer1, layer2)

    var listOfConnexions = for (outputIndex <- 0 until layer2.shape.getNumberOfPoints) yield {
        for (_ <- 0 until 1) yield {
        for {
            inputIndex <- 0 until layer1.shape.getNumberOfPoints
            input = layer1.getNeuronPathAt(inputIndex)
            output = layer2.getNeuronPathAt(outputIndex)
            if layerTolayerIndex.getConnectionsBetween(input, output).nonEmpty
          } yield layerTolayerIndex.getConnectionsBetween(input, output).head
      }}


    var x = -1
    var y = 0
    SynapseIndex = List[(Integer,Integer,Integer)]()

    listOfConnexions.head.foreach(
      cnx => {
        cnx.toList.foreach(c => {
          x+=1
        SynapseIndex = SynapseIndex.::(c.connectionID,x,y)
        })
        y+=1
        x = -1
        }
    )

    SynapseIndex = SynapseIndex.reverse
  }


  def saveCroppedSynapses(layer : NeuronGroupRef,name : String): Unit ={

    layer.neurons.foreach(n =>{
      var values : ConnectionPropertyValues[Float] = ExternalSender.askTo(n.getNetworkAddress, GetAllConnectionProperty(SynapticWeightFloat)) match {
        case x:ConnectionPropertyValues[Float] => x
      }

      val width = 28
      val height = 28
      val image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB)
      var x = 0
      var y = 0

      for(x <- 0 until 28){
        for(x <- 0 until 28){
          image.setRGB(x,y,0 )
        }
      }

      values.values.foreach(F => {

       SynapseIndex.foreach(nn => {
      if(F._1 == nn._1){
          y = nn._2
          x = nn._3
        }
        })

      //    println(index+" : X = "+x+" Y = "+y)

          var v = F._3

          val rc = math.max(0f, -2f*v*v+4f*v-1f)
          val bc = math.max(0f, -2f*v*v+1)
          val gc = math.max(0f, 4f*v*(-v+1f))

          image.setRGB(x, y, math.round(bc*255) + (math.round(gc*255) << 8) + (math.round(rc*255) << 16))

      })

      import java.awt.image.BufferedImage
      val newImage = new BufferedImage(300, 300, BufferedImage.TYPE_INT_RGB)

      val g = newImage.createGraphics
      g.drawImage(image, 0, 0, 300, 300, null)
      g.dispose()
      var file = new File(folderName+"/"+name)
      if (!file.exists()) {
        file.mkdir()
      }
      val outputfile = new File(folderName+"/"+file.getName +"/"+n.getIdentifier)
      ImageIO.write(newImage, "png", outputfile)
    })

  }

  def cropNetwork(inputLayer: NeuronGroupRef,Ratio : Double): String = {
    var d = 0
    var total = 0
    var SynUpdate = ""
    try {


      inputLayer.connections.foreach(
        np => {
          np.connections.list.foreach(cnx =>{
            if(!croppedCnx.contains(cnx)){

              if(!cnx.toString.contains(inputLayer.identifier)){
                var Value = 0F
                try {
                  Value = ExternalConnectionSender.askTo(cnx, GetConnectionProperty(SynapticWeightFloat))
                    .asInstanceOf[PropertyValue[Float]].value

                  if (Value < Ratio) {
                 //  if( Random.nextInt(3) > 1) {
                    np.disconnectByPath(cnx)
                    croppedCnx = croppedCnx ::: List(cnx)
                    d += 1
                  }else{
                    ExternalConnectionSender.askTo(cnx,SetConnectionProperty(SynapticWeightFloat.asInstanceOf[ConnectionProperty[Any]] ,(Value + beta*Ratio).toFloat))
                    SynUpdate += cnx+":"+(Value + beta*Ratio)+"\n"
                  }

                }catch {
                  case e @ (_ : Exception ) => println("Cnx doesn't exist "+e)
                }
              }
              total += 1
            }
          })
        })
    } catch {
      case e @ (_ : Exception ) => println("Exc :"+e)
    }
    CTotal = total
    CRmaining = CTotal - d
    println("Deleted "+d+" / "+total)
    d+" / "+total

  }

  def cropInput(inputLayer: InputNeuronGroupRef,Ratio : Double): Unit = {
    var d = 0
    var total = 0
    try {

     inputLayer.connections.head.connections.list.foreach(cnx =>{
                try {
                  if(croppedCnx.contains(cnx)){
                    inputLayer.connections.head.disconnectByPath(cnx)
                    d += 1
                  }
                }catch {
                  case e @ (_ : Exception ) => println("Cnx doesn't exist "+e)
                }
              total += 1
             })

    } catch {
      case e @ (_ : Exception ) => println("Exc :"+e)
    }
    CTotal = total
    CRmaining = CTotal - d
    println("Deleted "+d+" / "+total)

  }

  def NeuronThresholdVariation(layer : NeuronGroupRef): String ={

    var res = ""
    layer.neurons.foreach(n =>{
      var values : ConnectionPropertyValues[Float] = ExternalSender.askTo(n.getNetworkAddress, GetAllConnectionProperty(SynapticWeightFloat)) match {
        case x: ConnectionPropertyValues[Float] => x
      }
      var threshold : Double = ExternalSender.askTo(n.getNetworkAddress,core.GetProperty(MembranePotentialThreshold)).asInstanceOf[PropertyValue[ElectricPotential]].value.value * (values.values.length.toDouble/inputStream.shape.getNumberOfPoints)

      ExternalSender.askTo(n.getNetworkAddress,core.SetProperty(MembranePotentialThreshold, threshold millivolts))

      res = res +"\nSetting threshold of "+n.getIdentifier+" to "+n.getNetworkAddress+" "+ExternalSender.askTo(n.getNetworkAddress,core.GetProperty(MembranePotentialThreshold))

      })
      res

      }

  def cropLateral(Layer: NeuronGroupRef,benchmarkMonitor: BenchmarkMonitorRef): Unit = {

    var lateral = 0
    benchmarkMonitor.getResult.neurons.zipWithIndex.foreach {
      case (actor, neuronIndex) =>
        benchmarkMonitor.getResult.labels.zipWithIndex.foreach { case (label, labelIndex) =>

          if ( benchmarkMonitor.getResult.evaluationByMaxSpiking.labelAssoc(actor) == label) {
            NeuroneLabels = NeuroneLabels.::(neuronIndex,label)
          }
          //else {
          //   benchmarkMonitor.getResult.evaluationByMaxSpiking.crossScore(labelIndex)(neuronIndex)
          // }
        }
    }
    println("####")
    NeuroneLabels.foreach( (i) =>{
      println(i._1+" , "+i._2)
    })
    println("####")

    Layer.connections.foreach( cnx => {
      cnx.connections.list.foreach( cnxP => {
        val t: Option[(Int, String)] = NeuroneLabels.find((x) => x._1 == (cnxP.connectionID - 784))
        val t2 : Option[(Int,String)] =  NeuroneLabels.find((x) => x._1 == cnxP.outputNeuron.actor.path.name.split(":")(1).toInt)

        if(t.get._2 != t2.get._2){
          cnx.disconnectByPath(cnxP)
          println("removed "+t.get._1+ " with "+t2.get._1)
          lateral+=1
        }

      })
    })

    println("lateral cropped : "+lateral)
  }

def checkTocrop(from:Integer,to:Integer,dimention:Integer,items: Array[Array[Integer]]): Boolean ={
  var b = 0
  var i = 1

  if (Math.abs(to - from) < dimention && to != from){
    while(i < items.length){
     b = dimention * i
     if(Math.min(from,to) < b && Math.max(from,to) >= b ){
      return false
     }
      i+=1
    }
    true
   }else{
    false
  }
}

}
