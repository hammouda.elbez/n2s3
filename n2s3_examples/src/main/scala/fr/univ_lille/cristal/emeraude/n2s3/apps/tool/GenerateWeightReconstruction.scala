package fr.univ_lille.cristal.emeraude.n2s3.apps.tool

import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO

import fr.univ_lille.cristal.emeraude.n2s3.features.io.persistence.WeightLoad
import fr.univ_lille.cristal.emeraude.n2s3.features.logging.graph.SynapticWeightSelectGraph
/**
  * Created by pfalez on 03/05/17.
  */
object GenerateWeightReconstruction extends App {

  val input = "emotion/5/MNIST_FULL_from_7_weights"
  val output = "emotion/5/filter"
  val factor = 10

  val weights = WeightLoad.fromFile(input)

  println(input+" : "+weights.inShape+" -> "+weights.outShape)

  for(filter <- 0 until weights.outShape.dimensions(0)) {

    val l = weights.getInputs(Seq(filter))
    val xMin = l.map(_._1(1)).min
    val xMax = l.map(_._1(1)).max
    val yMin = l.map(_._1(2)).min
    val yMax = l.map(_._1(2)).max
    val zMin = l.map(_._1(0)).min
    val zMax = l.map(_._1(0)).max

    val width = xMax-xMin+1
    val height = yMax-yMin+1


    val img = new BufferedImage(width*factor, height*factor, BufferedImage.TYPE_INT_ARGB)


    for(z <- zMin to zMax) {
      SynapticWeightSelectGraph.paint((for {
        x <- xMin to xMax
        y <- yMin to yMax
      //z <- zMin to zMax
      } yield {
        weights.get(Seq(z, x, y), Seq(filter)).map(v => (x - xMin, y - yMin, v))
      }).flatten, img.getGraphics, factor)

      ImageIO.write(img, "png", new File(output + "_" + filter+"_"+z))
    }

  }
}
