package fr.univ_lille.cristal.emeraude.n2s3.apps.tool

import java.awt.event.{ActionEvent, ActionListener}
import java.awt.{Color, Graphics, GridBagConstraints, GridBagLayout}
import java.io.{DataInputStream, FileInputStream}
import javax.swing._

import fr.univ_lille.cristal.emeraude.n2s3.features.io.input._
import fr.univ_lille.cristal.emeraude.n2s3.features.io.persistence.ActivityPersistenceModel.EventFactory
import fr.univ_lille.cristal.emeraude.n2s3.features.logging.graph.SynapticWeightSelectGraph
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp

import scala.collection.mutable

/**
  * Created by pfalez on 18/05/17.
  */

class ActivityViewer(filename : String, xIndex : Int, yIndex : Int, valueFunction : Seq[Timestamp] => Float) extends JFrame {
  // Display

  val thisFrame : ActivityViewer = this

  val mainPane = new JPanel(new GridBagLayout)
  setContentPane(mainPane)

  val c = new GridBagConstraints
  c.fill = GridBagConstraints.HORIZONTAL
  c.gridx = 0
  c.gridy = 0
  c.gridwidth = 6
  c.gridheight = 1
  c.weightx = 0.0
  c.weighty = 0.0


  val labelPanel = new JLabel()
  mainPane.add(labelPanel, c)

  c.gridy = 1
  c.fill = GridBagConstraints.BOTH
  c.weightx = 1.0
  c.weighty = 1.0


  val displayPanel = new DisplayPanel()
  mainPane.add(displayPanel, c)

  c.gridy = 2
  c.gridx = 0
  c.gridwidth = 2
  c.fill = GridBagConstraints.HORIZONTAL
  c.weightx = 1.0
  c.weighty = 0.0


  val nextInputButton = new JButton("Next Input")
  mainPane.add(nextInputButton, c)
  nextInputButton.addActionListener(new ActionListener {
    override def actionPerformed(actionEvent: ActionEvent): Unit = {
      thisFrame.readNext()
    }
  })

  c.gridx = 2
  c.gridwidth = 1

  val allFilterButton = new JButton("All")
  mainPane.add(allFilterButton, c)
  allFilterButton.addActionListener(new ActionListener {
    override def actionPerformed(actionEvent: ActionEvent): Unit = {
      thisFrame.allFilter()
    }
  })
  c.gridx = 3
  val previousImageButton = new JButton("<<")
  mainPane.add(previousImageButton, c)
  previousImageButton.addActionListener(new ActionListener {
    override def actionPerformed(actionEvent: ActionEvent): Unit = {
      thisFrame.previousImage()
    }
  })
  c.gridx = 4

  val imageCursorPanel = new JLabel()
  mainPane.add(imageCursorPanel, c)

  c.gridx = 5
  val nextImageButton = new JButton(">>")
  mainPane.add(nextImageButton, c)
  nextImageButton.addActionListener(new ActionListener {
    override def actionPerformed(actionEvent: ActionEvent): Unit = {
      thisFrame.nextImage()
    }
  })

  setSize(300, 400)
  setVisible(true)
  setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE)


  // Data
  var reader = new DataInputStream(new FileInputStream(filename))
  val dimNumber : Int = reader.readInt()
  val shape = Shape((0 until dimNumber).map(_ => reader.readInt()):_*)
  val totalEvent : Long = reader.readLong()

  assert(xIndex != yIndex)
  assert(xIndex >= 0 && xIndex < shape.dimensionNumber)
  assert(yIndex >= 0 && yIndex < shape.dimensionNumber)

  println("shape: "+shape)

  var readEvent : Long = 0
  var nextData : mutable.HashMap[Long, mutable.ArrayBuffer[Timestamp]] = mutable.HashMap()
  var nextInput : Option[N2S3InputLabel] = None

  // start
  if(hasNext())
    readNext()

  refreshImageCursor()

  def hasNext() : Boolean = {
    readEvent < totalEvent
  }

  def readNext(): Unit = {
    assert(hasNext())

    val spikes =  mutable.HashMap[Long, mutable.ArrayBuffer[Timestamp]]()
    spikes ++= nextData
    nextData.clear()

    var inputComplete = false

    while(hasNext() && !inputComplete) {

      val timestamp = reader.readLong()
      val metaDataSize = reader.readInt()
      val dataSize = reader.readInt()

      val labels = (0 until metaDataSize).map { _ =>
        EventFactory.createMetaData(reader, timestamp)
      }.filter(_.isInstanceOf[N2S3InputLabel]).map(_.asInstanceOf[N2S3InputLabel])

      assert(labels.size <= 1)

      if(labels.nonEmpty) {

        if(nextInput.isDefined) {
          val shapeWithoutIndex = Shape(shape.dimensions.zipWithIndex.filter { case (v, i) => i != xIndex && i != yIndex }.map(_._1): _*)

          val data = for (n <- 0 until shapeWithoutIndex.getNumberOfPoints) yield {
            for (x <- 0 until shape.dimensions(xIndex)) yield {
              for (y <- 0 until shape.dimensions(yIndex)) yield {
                val index = Array.ofDim[Int](shape.dimensionNumber)
                index(xIndex) = x
                index(yIndex) = y
                val remain = shapeWithoutIndex.fromIndex(n)
                var i = 0
                var j = 0
                while (i < shape.dimensionNumber) {
                  if (i != xIndex && i != yIndex) {
                    index(i) = remain(j)
                    j += 1
                  }
                  else {
                    // nothing
                  }
                  i += 1
                }
                //println(spikes.getOrElse(shape.toIndex(index:_*), Seq())+" -> "+valueFunction(spikes.getOrElse(shape.toIndex(index:_*), Seq())))
                valueFunction(spikes.getOrElse(shape.toIndex(index: _*), Seq()))
              }
            }
          }

          displayPanel.update(nextInput.get.getLabel, data)
          SwingUtilities.invokeLater(new Runnable {
            override def run(): Unit = repaint()
          })
          labelPanel.setText("Label: \"" + nextInput.get.getLabel + "\"")

          inputComplete = true
        }
        nextInput = Some(labels.head)
      }



      (0 until dataSize).map { _ =>
        EventFactory.createData(reader, timestamp) match {
          case (index, spike : N2S3InputSpike) =>
            val container = if(labels.isEmpty) spikes else nextData
            container.getOrElseUpdate(index, mutable.ArrayBuffer[Timestamp]()) += spike.getStartTimestamp
        }
      }

      readEvent += metaDataSize + dataSize
    }

    if(!hasNext())
      nextInputButton.setEnabled(false)

    refreshImageCursor()
  }

  def refreshImageCursor() : Unit = {
    imageCursorPanel.setText((displayPanel.getCurrentImageNumber()+1)+"/"+displayPanel.getImageNumber())
    previousImageButton.setEnabled(!displayPanel.isAllFilter && displayPanel.getCurrentImageNumber() >= 0)
    nextImageButton.setEnabled(!displayPanel.isAllFilter && displayPanel.getCurrentImageNumber() < displayPanel.getImageNumber())
  }

  def previousImage() : Unit = {
    displayPanel.previousImage()
    refreshImageCursor()
  }

  def nextImage() : Unit = {
    displayPanel.nextImage()
    refreshImageCursor()
  }

  def allFilter() : Unit = {
    displayPanel.setAllFilter()
    refreshImageCursor()
  }



}

class DisplayPanel extends JPanel {


  var currentImage = 0
  var allFilter = true
  var data : Seq[Seq[Seq[Float]]] = Seq()
  var label : String = ""

  def update(label : String, data : Seq[Seq[Seq[Float]]]): Unit = {
    currentImage = 0
    this.data = data.map{ l1 =>
      val max = l1.flatten.max
      l1.map { l2 =>
        l2.map { v =>
          v/(if(max == 0f) 1f else max)
        }
      }
    }
    this.label = label
  }

  override def paintComponent(g: Graphics): Unit = {
    if(data.nonEmpty && data.head.nonEmpty && data.head.head.nonEmpty) {

      if(allFilter) {
        paintAllFilter(g)
      }
      else {
        paintAllFilter(g)
      }

    }

  }

  private def paintAllFilter(g : Graphics) : Unit = {


    val pixelWidth = math.max(1, math.floor(getWidth / data.head.size).toInt)
    val pixelHeight = math.max(1, math.floor(getHeight / data.head.head.size).toInt)

    for {
      x <- data.head.indices
      y <- data.head.head.indices
    } {
      g.setColor(Color.BLACK)
      g.drawRect(x * pixelWidth, y * pixelHeight, pixelWidth, pixelHeight)
      val filters = data.map(filter => filter(x)(y) != 0.0).zipWithIndex.filter(_._1)
      if(filters.nonEmpty) {
        val w = pixelWidth / filters.size
        for (((_, i1), i2) <- filters.zipWithIndex) {
          g.setColor(SynapticWeightSelectGraph.heatMap(i1.toFloat / data.size.toFloat))
          g.fillRect(x * pixelWidth + i2 * w, y * pixelHeight, w, pixelHeight)
        }
      }
    }
  }

  private def paintOneFilter(g : Graphics) : Unit = {
    val pixelWidth = math.max(1, math.floor(getWidth / data.head.size).toInt)
    val pixelHeight = math.max(1, math.floor(getHeight / data.head.head.size).toInt)

    for {
      x <- data.head.indices
      y <- data.head.head.indices
    } {
      val v = data(currentImage)(x)(y)
      g.setColor(new Color(v, v, v))
      g.fillRect(x * pixelWidth, y * pixelHeight, pixelWidth, pixelHeight)
    }
  }

  def nextImage(): Unit = {
    currentImage = math.min(data.size-1, currentImage+1)
    SwingUtilities.invokeLater(new Runnable {
      override def run(): Unit = repaint()
    })
  }

  def previousImage(): Unit = {
    currentImage = math.max(0, currentImage-1)
    SwingUtilities.invokeLater(new Runnable {
      override def run(): Unit = repaint()
    })
  }

  def getCurrentImageNumber() : Int = {
    currentImage
  }

  def getImageNumber() : Int = {
    data.size
  }

  def setAllFilter() : Unit = {
    allFilter = !allFilter
  }

  def isAllFilter = allFilter

}

object ActivityViewver extends App {

  val filename = "emotion/3/test/MNIST_CONV1_5_spikes"

  new ActivityViewer(filename, 1, 2, seq => seq.size)

}
