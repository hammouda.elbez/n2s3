import com.typesafe.sbt.SbtMultiJvm
import com.typesafe.sbt.SbtMultiJvm.MultiJvmKeys.{MultiJvm, scalatestOptions}

lazy val commonSettings = Seq(
  version          := "1.1",
  organization     := "fr.univ-lille.cristal",
  scalaVersion     := "2.12.17",//"2.12.6",//
  scalacOptions    := Seq("-unchecked", "-deprecation", "-encoding", "utf8", "-language:postfixOps", "-Xmax-classfile-name","78"),

 /********************************************************************************************************************
  * Dependecy information
  ******************************************************************************************************************/
  resolvers += "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/releases",

  /********************************************************************************************************************
    * Publishing options
    ******************************************************************************************************************/
  publishTo := {
    val nexus = "https://oss.sonatype.org/"
    if (isSnapshot.value)
      Some("snapshots" at nexus + "content/repositories/snapshots")
    else
      Some("releases"  at nexus + "service/local/staging/deploy/maven2")
  },
  credentials += Credentials(Path.userHome / ".ivy2" / ".credentials"),

  publishMavenStyle := true,
  publishArtifact in Test := false,
  pomIncludeRepository := { x => false },
  pomExtra := <url>https://sourcesup.renater.fr/projects/n2s3/</url>
    <licenses>
      <license>
        <name>CeCILL-B Version 1</name>
        <url>http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html</url>
        <distribution>repo</distribution>
      </license>
    </licenses>
    <scm>
      <connection>scm:git://git.renater.fr/n2s3.git</connection>
      <developerConnection>scm:git:ssh://git@git.renater.fr:2222/n2s3.git</developerConnection>
      <url>https://sourcesup.renater.fr/scm/?group_id=2343</url>
    </scm>
    <developers>
      <developer>
        <id>emeraude-team</id>
        <name>Emeraude Team</name>
        <email>n2s3-dev@univ-lille1.fr</email>
      </developer>
    </developers>
)

lazy val root = (project in file(".")).
  aggregate(n2s3, n2s3_cluster, examples, daemon)

lazy val n2s3 = (project in file("n2s3")).
  settings(commonSettings: _*)

lazy val n2s3_cluster = (project in file("n2s3_cluster")).
  settings(commonSettings: _*).
  dependsOn(n2s3)

lazy val examples = (project in file("n2s3_examples")).
  settings(commonSettings: _*).
  dependsOn(n2s3, n2s3_cluster)

lazy val daemon = (project in file("n2s3_daemon")).
  settings(commonSettings: _*).
  dependsOn(n2s3)

lazy val integration_tests = (project in file("n2s3_integration_tests")).
  settings(SbtMultiJvm.multiJvmSettings: _*).
  settings(commonSettings: _*).
  configs(MultiJvm).
  dependsOn(n2s3, n2s3_cluster)
